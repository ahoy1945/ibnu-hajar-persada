var gulp = require('gulp');
var include = require('gulp-include');
var elixir = require('laravel-elixir');

var Task = elixir.Task;

elixir.extend('include', function(src, output){

	new Task('include', function() {
		return gulp.src(src)
				   .pipe(include())
				   .pipe(gulp.dest(output));
	})
	.watch("./resources/assets/js/**/**/*.js");
		
});

elixir(function(mix) {
    mix.sass('admin/app.scss')
       .include('resources/assets/js/admin/app.js', 'public/js');
});

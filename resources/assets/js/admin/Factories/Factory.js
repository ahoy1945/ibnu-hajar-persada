(function() {
	'use strict';

	angular.module('AppFactory', [
			'LokasiFactory',
			'InventoryFactory',
			'PesertaFactory',
			'MasterDataFactory',
			'TabunganFactory'
		]);
})();
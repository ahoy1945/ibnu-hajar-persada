(function() {
	'use strict';

	angular.module('LokasiFactory', [])
		   .factory('LokasiFactory', LokasiFactory);

	LokasiFactory.$inject = ['$http'];	   
	function LokasiFactory($http) {
		return {
			getDesa: function(id_kec) {
				return $http.get('api/desa/'+id_kec);
			},
			getKecamatan: function(id_kab) {
				return $http.get('api/kecamatan/'+id_kab);	
			},
			getKabupaten: function(id_prov) {
				return $http.get('api/kabupaten/'+id_prov);	
			},
			getProvinsi: function() {
				return $http.get('api/provinsi');
			}
		}
	}	
 
})();
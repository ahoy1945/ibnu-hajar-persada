(function() {
	'use strict';

	angular.module('InventoryFactory', [])
	       .factory('InventoryFactory', InventoryFactory);

	InventoryFactory.$inject = ['$http'];       
	function InventoryFactory($http) {
		return {
			update: function(data) {
				data.input = data.input || 1;
				
				return $http({
					method: 'PATCH',
					url: '/inventory/'+data.id,
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
			},
			updatePerlengkapanJamaah: function(data) {
				return $http({
					method: "PATCH",
					url: '/inventory/perlengkapan-jamaah/'+data.id,
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
			}
		}
	}	       
})();
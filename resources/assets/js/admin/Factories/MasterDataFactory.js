(function() {
	'use strict';

	angular.module('MasterDataFactory', [])
		   .factory('MasterDataFactory', MasterDataFactory);


	MasterDataFactory.$inject = ['$http']	   
	function MasterDataFactory($http) {
		return {
			getPaket: getPaket
		};

		function getPaket(type) {
			return $http({
				method: "GET",
				url: '/api/paket?type='+type
			});
		}
	}		   
})();
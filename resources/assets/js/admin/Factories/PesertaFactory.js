(function() {
	'use strict';

	angular.module('PesertaFactory', [])
		   .factory('PesertaFactory', PesertaFactory);

	PesertaFactory.$inject = ['$http'];	   
	function PesertaFactory($http) {
		return {
			searchMahram: searchMahram,
			show: show
		}

		function searchMahram(keyword) {
			return $http({
				method: 'GET',
				url: '/peserta/searchmahram?keyword='+keyword,
			});
		}

		function show(id) {
			return $http({
				method: 'GET',
				url: '/peserta/'+id
			});
		}
	}	   	
})();
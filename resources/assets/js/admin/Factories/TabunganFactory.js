(function() {
	'use strict';

	angular.module('TabunganFactory', [])
		   .factory('TabunganFactory', TabunganFactory);

	TabunganFactory.$inject = ['$http'];
	function TabunganFactory($http)
	{
		return {
			search: search
		}

		function search(keyword) {
			return $http({
				method: 'GET',
				url: '/tabungan/search?keyword='+keyword,
			});
		}
	}
})();
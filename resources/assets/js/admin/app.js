//= include '../../../../bower_components/jquery/dist/jquery.min.js'
//= include '../../../../bower_components/admin-lte/dist/js/app.js' 
//= include '../../../../bower_components/moment/moment.js'
//= include '../../../../bower_components/moment/src/id.js'
//= include '../../../../bower_components/lodash/lodash.min.js'
//= include '../../../../bower_components/angular/angular.js'
//= include '../../../../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js'
//= include '../../../../bower_components/angular-loading-bar/src/loading-bar.js'
//= include '../../../../bower_components/bootstrap/dist/js/bootstrap.min.js' 
//= include 'libs/bootstrap-datepicker.js'
//= include 'libs/accounting.js'
//= include 'misc.js'  
 
(function() {
	'use strict';

	angular.module('App', [
			'ui.bootstrap',
			'angular-loading-bar',
			'AppConfig',
			'AppDirective',
			'AppFactory',
			'Controller',
			'Filter',
		]); 

})();

//= include 'config.js'

//= include '/Controllers/Controller.js'
//= include '/Controllers/AgenController.js'
//= include '/Controllers/InventoryController.js'
//= include '/Controllers/PendaftaranController.js'
//= include '/Controllers/PembayaranController.js'
//= include '/Controllers/DokumenController.js'
//= include '/Controllers/PemberangkatanController.js'
//= include '/Controllers/PembayaranMultiController.js'
//= include '/Controllers/KeuanganController.js'
//= include '/Controllers/IdCardController.js'
//= include '/Controllers/PendaftaranUpdateController.js'
//= include '/Controllers/LaporanUmmuAhmadController.js'
//= include '/Controllers/TabunganController.js'
 

//= include '/Factories/Factory.js'
//= include '/Factories/LokasiFactory.js'
//= include '/Factories/InventoryFactory.js'
//= include '/Factories/PesertaFactory.js'
//= include '/Factories/MasterDataFactory.js
//= include '/Factories/TabunganFactory.js'

//= include '/Filter/Filter.js'
//= include '/Filter/CommonFilter.js'

//= include '/Directives/Directive.js'
//= include '/Directives/highcharts.js'


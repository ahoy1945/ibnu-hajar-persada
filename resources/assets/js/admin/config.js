(function() {
	'use strict';

	angular.module('AppConfig', [])
	       .constant('urls', {
	       		TEMPLATE: '../templates'
	       })
	       .config(['$httpProvider', function($httpProvider) {
	       		$httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
	       }]);
})();
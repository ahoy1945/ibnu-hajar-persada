(function() {
	'use strict';

	angular.module('CommonFilter', [])
		   .filter('monthNumbToString', function(){
		   		return function(filter) {
		   			if(filter){
		   				 
		   				var data = filter.substring(5,7);
		   				 
		   				if(data == '01') {
		   					return 'Januari'
		   				 
		   				}else if(data == '02') {
		   					return 'Februari';
		   				}else if(data == '03') {
		   					return 'Maret'
		   				}else if(data == '04') {
		   					return 'April'
		   				}else if(data == '05') {
		   					return 'Mei'
		   				}else if(data == '06') {
		   					return 'Juni'
		   				}else if(data == '07') {
		   					return 'Juli'
		   				}else if(data == '08') {
		   					return 'Agustus'
		   				}else if(data == '09') {
		   					return 'September';
		   				}else if(data == '10') {
		   					return 'Oktober'
		   				}else if(data == '11') {
		   					return 'November'
		   				}else if(data == '12') {
		   					return 'Desember'
		   				}
		   			}
		   		}
		   })
})();
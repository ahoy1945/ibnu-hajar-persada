(function() {
	'use strict';

	angular.module('IdCardController', [])
	       .controller('IdCardController', IdCardController);

	IdCardController.$inject = ['$scope', '$http'];
	function IdCardController($scope, $http) {
		var vm = this;

		vm.input = {
			tipe_paket: (window.parameters.tipe_paket ? window.parameters.tipe_paket : 'umroh' )
		};

		vm.years = function() {
			var years = [];
			var currentYear = new Date().getFullYear()
			
			for(var i =currentYear - 3;i <= currentYear + 3;i++){
			   years.push(i);
			}
			return years;
		};
	}

})();
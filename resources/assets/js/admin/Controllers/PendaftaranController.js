(function() {
	'use strict';

	angular.module('PendaftaranController', [])
		   .controller('PendaftaranController', PesertaController);

	PesertaController.$inject = ['$scope', 'LokasiFactory', 'PesertaFactory', 'MasterDataFactory', '$modal']	   
	function PesertaController($scope, LokasiFactory, PesertaFactory, MasterDataFactory, $modal ) {
		var vm = this;
		vm.selected;
		vm.dataMahram;
		vm.berangkat = window.tgl_berangkat;
		vm.pesawat = window.pesawat;
		vm.bus = window.bus;
		vm.paketHaji = window.paketHaji;
		vm.date = [];
		vm.data = {
			tgl_lahir: '',
			tgl_pembuatan_passpor: '',
			tgl_expired_passpor: '',
			programIsSelected: '',
			mahram: '',
			provinsi: '',
			kabupaten: '',
			kecamatan: '',
			desa: '',
			alamat: '',
			nama_mahram: '',
			tgl_berangkat: ''


		};

		vm.datepicker = {
			tgl_lahir: false,
			tgl_pembuatan_passpor: false,
			tgl_expired_passpor: false
		}
		vm.currentYear = (function() {
			var date = new Date();
			return date.getFullYear()+"-01-01";
		})();
		vm.openDatepicker = openDatepicker;
		vm.searchMahram = searchMahram;
		vm.selectMahram = selectMahram;
		vm.showDetail = showDetail;
		vm.addTglKeberangkatanValue = addTglKeberangkatanValue;

		function addTglKeberangkatanValue()
		{
			
		}

		function selectMahram(item)
		{	
			angular.element("#mahram_id").val(item.id);	
		}

		var date = new Date();

		for(var i = date.getFullYear(); i <= date.getFullYear() + 5;i++) {
			vm.date.push(i);
		}

		function searchMahram(keyword) {
			return PesertaFactory.searchMahram(keyword)
						  .then(function(response) {
						  	vm.dataMahram = response.data; 
						  	return response.data.map(function(item) {
						  		 item.namaandprov = item.nama_peserta+" ("+item.alamat.provinsi+")";
						  		 return item;
						  	});
						  }, function(error) {
						  	console.log(error);
						  })
						  .catch(function(error) {
						  	console.log('error was occured '+ error);
						  });
		}

		function openDatepicker(name) {
			vm.datepicker[name] = !vm.datepicker[name]; 
		}

		LokasiFactory.getProvinsi()
					  .then(function(response) {
					  	vm.provinsi = response.data;
					 
					  }, function(error) {
					  	console.log(error);
					  })
					  .catch(function(error) {
					  	console.log(error);
					  });
		function showDetail(id) {
			vm.detailPeserta;

			

			$modal.open({
				animation: true,
				size: 'lg',
				templateUrl: 'myModalContent.html',
				controller: function($scope, $modalInstance, PesertaFactory, data) {
					 
					PesertaFactory.show(id)
						  .then(function(response) {
						  	$scope.data = response.data
							console.log($scope.data)
						  }, function(response) {
						  	console.log(response);
						  })
						  .catch(function(error) {
						  	console.log('error was ocuured');
						  });

					$scope.close = function() {
						$modalInstance.close();
					}	  	
				},
				resolve: {
					data: function() {
						return id;
					}
				}
			});

			

		}			   


		$scope.$watch('vm.data.programIsSelected', function(newValue, oldValue) {
			vm.paket = window.tgl_berangkat.filter(function(tgl_berangkat) {	
				return tgl_berangkat.tipe === newValue;
			});	
			vm.dokumen = window.dokumen.filter(function(dokumen) {
				return dokumen.tipe === newValue;
			});
			 
			/*if(vm.data.programIsSelected !== '') {
				MasterDataFactory.getPaket(newValue)
								 .then(function(response) {
								 	vm.paket = response.data;
								 }, function(error) {
								 	console.log('error was occured');
								 })
								 .catch(function(error) {
								 	console.log('error was occured '+ error);
								 });
			}*/
		});
 
	}	

})();
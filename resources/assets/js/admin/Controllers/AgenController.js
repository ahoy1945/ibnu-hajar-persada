(function() {
	'use strict';

	angular.module('AgenController', [])
		   .controller('AgenController', AgenController);

	AgenController.$inject = ['LokasiFactory', '$scope'];	   
	function AgenController(LokasiFactory, $scope) {
		var vm = this;

		vm.lokasi = {
			provinsi: false,
			kabupaten: false,
			kecamatan: false,
			desa: false,
			alamat: false
		}

		vm.input = {
			provinsi: '',
			kabupaten: '',
			kecamatan: '',
			desa: ''
		};

		LokasiFactory.getProvinsi()
					  .then(function(response) {
					  	vm.provinsi = response.data;
					 
					  }, function(error) {
					  	console.log(error);
					  })
					  .catch(function(error) {
					  	console.log(error);
					  });

		$scope.$watch('vm.input.provinsi', function(newValue, oldValue) {
			if(newValue !== '') {
				 	LokasiFactory.getKabupaten(vm.input.provinsi)
		 				 .then(function(response) {
		 				 	vm.kabupaten = response.data;
		 				 })

			} 
		});	

		$scope.$watch('vm.input.kabupaten', function(newValue, oldValue) {
			if(newValue !=='') {
				LokasiFactory.getKecamatan(vm.input.kabupaten)
							 .then(function(response) {
							 	vm.kecamatan = response.data;
							 });
			}
		});	 

		$scope.$watch('vm.input.kecamatan', function(newValue, oldValue) {
			if(newValue !== '') {
				LokasiFactory.getDesa(vm.input.kecamatan)
							 .then(function(response) {
							 	vm.desa = response.data;
							 });

						 
			}
		});

		$scope.$watch('vm.input.desa', function(newValue, oldValue) {
			if(vm.input.provinsi !== '' && vm.input.kecamatan !== '' && vm.input.desa !== '' && vm.input.kabupaten !== '') {
					vm.lokasi.alamat = true;
				}	
		});
	}		   	
})();
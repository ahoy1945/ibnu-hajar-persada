(function() {
	'use strict';

	angular.module('PembayaranMultiController', [])
		   .controller('PembayaranMultiController', PembayaranMultiController);

	PembayaranMultiController.$inject = ['$http'];	   
	function PembayaranMultiController($http) {
		var vm = this;
		vm.input = {};		
		vm.searchPeserta = searchPeserta;
		vm.selectPeserta = selectPeserta;
		vm.rekening = window.rekening;
		vm.tipeKeuangan = window.tipeKeuangan.filter(function(item) {
			return item.tipe_keuangan === "Pemasukan";
		});
		

		function searchPeserta(keyword) {
			return $http({
				method: 'GET',
				url: '/api/search/peserta/?keyword='+keyword
			})
			.then(function(response){
				return response.data;
			}, function(error) {
				console.log('error was ocuured');
			})
			.catch(function(error) {
				console.log('error was ocured');
			});
		}

		function selectPeserta(that, item) {
		  console.log(item);
			var dom = angular.element("#cariJamaah").parent().parent();
			     
			var html = ["<div class='form-group'>",
            "<label class='col-md-2 control-label'></label>",
              "<input type='hidden' name='peserta_id[]' value='"+item.id+"' class='form-control' placeholder='Rincian'>",
              "<div class='col-md-3'>",
                "<input type='text' readonly  value='"+item.nama_peserta+"' class='form-control' placeholder='Rincian'>",
              "</div>",
              "<div class='col-md-2'>",
              	 "<select class='form-control' name='tipe_keuangan_id[]' required>",
                    "<option value=''>Pilih</option>"]
           
              for(var i = 0; i < vm.tipeKeuangan.length; i++) {
              	 html.push("<option value='"+vm.tipeKeuangan[i].id+"'>"+vm.tipeKeuangan[i].nama_tipe+"</option>");
              }
    
              

              var html2 = ["</select>",
              "</div>",
              "<div class='col-md-2'>",
              	"<input type='text' onkeyup='formatNumber(this)' name='jumlah[]' placeholder='Jumlah' required class='form-control'>",
              "</div>",
              "<div class='col-md-2'>",
                "<input type='text' name='diskon'  placeholder='diskon($)' class='form-control'>",,
              "</div>",
              "<div class='col-md-1'>",
              	"<button class='btn btn-danger' onclick='deleteDom(this, event)'>",
                     "<i class='fa fa-trash'></i>",
                "</button>",
              "</div>",
              "<div class='col-md-2'>",
              "</div>"];

              var html3 = [];
              if(! item.diskon && item.user.is_agen === 0) {
                
              }

              var html4 = ["<div class='col-md-9 col-md-offset-2' style='margin-top:15px;'>",
              	"<textarea name='keterangan[]' class='form-control' placeholder='Keterangan(optional)'></textarea>",
              "</div>"];

              html4.forEach(function(item) {
                html3.push(item);
              });

              html3.forEach(function(item){
                html2.push(item);
              });
             
              html2.forEach(function(item) {
              	html.push(item);
              });

              html = html.join('');
           

 
	    dom.after(html);
	     
		}

	}
})();
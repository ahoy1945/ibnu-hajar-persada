(function() {
	'use strict';

	angular.module('InventoryController', [])
		   .controller('InventoryController', InventoryController);

	InventoryController.$inject  =  ['$modal', 'InventoryFactory'];	   
	function InventoryController($modal, InventoryFactory) {
		var vm = this;
		
		vm.checked = {};
		
		vm.updateStatusStok = updateStatusStok;

		vm.openModal = openModal;
		
		function updateStatusStok(id, id_barang) {

			var data = {
				id: id,
				id_barang: id_barang
			}

			InventoryFactory.updatePerlengkapanJamaah(data)
							.then(function(response) {
								console.log(response);
							}, function(response) {
								console.log(response);
							})
							.catch(function(error) {
								console.error("error was occured "+error);
							});			
		}	

		function openModal(event, id, type) {
			event.preventDefault();

			$modal.open({
				animation: true,
				size: 'sm',
				templateUrl: 'myModalContent.html',
				controller: function($scope, $modalInstance, data) {
				 	$scope.data = {
				 		id: data.id,
				 		type: data.type,
				 		input: '',
				 		tanggal: '', 
				 		keterangan: ''
				 	};

				 	$scope.status = {
				 		opened: false
				 	}

				 	$scope.heading = (data.type == 'increment'? 'Tambahkan' : 'Kurangi');

					$scope.closeModal = closeModal;

					$scope.update = update;

					$scope.openDatepicker = openDatepicker;

					function update() {	
					    $scope.data.tanggal = $('#tanggal').val();

						InventoryFactory.update($scope.data)
									    .then(function(response) {
									    	alert('Stok berhasil di update');
									        location.reload();
									    }, function(error) {

									    })
									    .catch(function(error) {

									    });
					}

					function closeModal() {
						$modalInstance.close();
					}

					function openDatepicker() {
						$scope.status.opened = true;
					}
				},
				resolve:  {
					data: function() {
						return { id: id, type: type}
					}
				}
			});
		}		
	}	   
})();
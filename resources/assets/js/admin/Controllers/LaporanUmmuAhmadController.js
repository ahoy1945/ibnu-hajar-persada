(function() {
	'use strict';

	angular.module('LaporanUmmuAhmadController', [])
	       .controller('LaporanUmmuAhmadController', LaporanUmmuAhmadController);

    LaporanUmmuAhmadController.$inject = [];
	function LaporanUmmuAhmadController() {
		var vm = this;

		vm.input = {
			 tipe_paket: (typeof window.parameters.tipe_paket != 'undefined' ? window.parameters.tipe_paket : 'umroh')
		}
	}       
})();
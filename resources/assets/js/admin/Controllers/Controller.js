(function() {
	'use strict';

	angular.module('Controller', [
			'AgenController',
			'InventoryController',
			'PendaftaranController',
			'PembayaranController',
			'DokumenController',
			'PemberangkatanController',
			'PembayaranMultiController',
			'KeuanganController',
			'IdCardController',
			'PendaftaranUpdateController',
			'LaporanUmmuAhmadController',
			'TabunganController'
		]);
})();
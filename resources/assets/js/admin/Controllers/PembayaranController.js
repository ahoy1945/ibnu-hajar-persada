(function() {
	'use strict';

	angular.module('PembayaranController', [])
	       .controller('PembayaranController', PembayaranController);

    PembayaranController.$inject = ['$http', '$scope', '$modal', 'PesertaFactory'];
    function PembayaranController($http, $scope, $modal, PesertaFactory) {
    	var vm = this;
    	var tipeKeuangan = window.tipe_keuangan;
    	vm.input = {
            tipe: (window.parameters.tipe ? window.parameters.tipe : '' ),
       
        };
    	vm.submit = submit;
        vm.input.perlengkapan = 0
    	vm.showDetail = showDetail;
      
        

        function showDetail(id)
        {
            $modal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'myModalContent.html',
                controller: function($scope, $modalInstance, PesertaFactory, data) {
                     
                    PesertaFactory.show(id)
                          .then(function(response) {
                            $scope.data = response.data
                            console.log($scope.data)
                          }, function(response) {
                            console.log(response);
                          })
                          .catch(function(error) {
                            console.log('error was ocuured');
                          });

                    $scope.close = function() {
                        $modalInstance.close();
                    }       
                },
                resolve: {
                    data: function() {
                        return id;
                    }
                }
             });   
        }

    	function submit(data, tipe) {
            console.log(data);
    		return $http({
    			method: 'POST',
    			url: '/pembayaran',
    			data: $.param(data),
    			headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
    		})
    		.then(function(response) {
    			console.log(response);
    		})

    	}	 

    }

})();
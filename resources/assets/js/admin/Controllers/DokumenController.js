(function() {
	'use strict';

	angular.module('DokumenController',[])
		   .controller('DokumenController', DokumenController);

	DokumenController.$inject = ['$http']; 	   
	function DokumenController($http) {
		var vm = this;

		vm.updateDokumenCheck = updateDokumenCheck;

		function updateDokumenCheck(id, dokumen_id) {
			var data = {
				id: id,
				dokumen_id: dokumen_id
			}

			return $http({
				method: 'POST',
				url: '/dokumen/check',
				data: $.param(data),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			})
			.then(function(response) {
				console.log(response);
			})
			.catch(function(error) {
				console.log(error);
			})
		}
	}		   	
})();
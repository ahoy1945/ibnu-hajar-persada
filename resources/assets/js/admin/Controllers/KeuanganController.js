(function() {
	'use strict';

	angular.module('KeuanganController', [])
		   .controller('KeuanganController', KeuanganController);


	KeuanganController.$inject = ['$scope', '$http'];	   
	function KeuanganController($scope, $http) {
		var vm = this;
		
		vm.add = {};  
		vm.add.mata_uang = angular.element("#mata_uang").val();
		vm.add.id_rekening = angular.element("#id_rekening").val();
		vm.data = {};
		vm.tipe_keuangan = "";
		vm.ref_tipe_keuangan_id = {};
		vm.addInput = addInput;
		vm.input = [];

		vm.deleteInput = deleteInput;
		vm.save = save;

		function addInput(e) {
			e.preventDefault();
			vm.add.tanggal = angular.element("#tanggal").val();
			 
			vm.input.push(vm.add);
			vm.add = {};
		
		}

		function deleteInput(e,index) {
			e.preventDefault();

			vm.input.splice(index, 1);
		}

		function save(e) {
			e.preventDefault();
			 
			return $http({
				method: 'POST',
				url: '/keuangan/store',
				data: JSON.stringify(vm.input),
				headers: {'Content-Type': 'application/json'}
			}).then(function(response) {
				// console.log(response.data.url);

				alert('Tersimpan');
				window.location = response.data.url;
			}, function(error) {
				console.log(error);
			})
			.catch(function(error) {
				console.log('error was occured');
			});
		}

		$scope.$watch('vm.add.tipe', function(newValue, oldValue) {
			//console.log(newValue);
			if(newValue != typeof 'undefined') {
				vm.tipe_keuangan =  window.tipe_keuangan.filter(function(item) {
					return item.tipe_keuangan == vm.add.tipe;
				});
				console.log(vm.tipe_keuangan);
			}
		});

		$scope.$watch('vm.add.ref_tipe_keuangan_id', function(newValue, oldValue) {
			if(newValue != typeof 'undefined') {
				vm.ref_tipe_keuangan_id = window.tipe_keuangan.filter(function(item) {
					return item.id == vm.add.ref_tipe_keuangan_id;
				}).map(function(item) {
					return item.nama_tipe;
				});
			}
		});

	
	}	

	function gakDipaket()
	{
			var tipe = function() {
				switch(vm.add.tipe){
					case 'Pemasukan':
						return 'debit';
						break;
					case 'Pengeluaran':
						return 'kredit';
						break;
				}
			}();

			var subtipe = JSON.parse(vm.add.subtipe);
			var lengthDom = $('.generateDomKeuangan').length + 1;

			if(vm.add.tipe != '' && vm.add.subtipe != '') {
				var html = ["<span class='generateDomKeuangan'><div class='form-group'><br/></br>",
								"<label class='col-md-2 control-label'>Tanggal</label>",
								"<div class='col-md-7'>",
		 							"<input required  name='tanggal_"+lengthDom+"' onClick=$(this).datepicker().datepicker('show') data-date-format='yyyy-mm-dd' type='text' class='form-control'>",
								"</div>", 

							"</div>",
							"<div class='form-group'>",
								"<label class='col-md-2 control-label'>Type</label>",
								"<div class='col-md-7'>",
		 							"<input   name='type_"+lengthDom+"' readonly type='text' class='form-control' value="+tipe+">",
								"</div>", 
							"</div>",
							"<div class='form-group'>",
								"<label class='col-md-2 control-label'></label>",
								"<div class='col-md-7'>",
		 							"<input   readonly type='text' class='form-control' value="+subtipe.nama_tipe+">",
		 							"<input name='ref_tipe_keuangan_id_"+lengthDom+"' readonly type='hidden' class='form-control' value="+subtipe.id+">",
								"</div>", 
							"</div>"];

				var html2 = ["<div class='form-group'>",
								"<label class='col-md-2 control-label'>Amount</label>",
								"<div class='col-md-7'>",
		 							"<input required  name='amount_"+lengthDom+"' onkeyup='formatNumber(this)' placeholder='10.000.000'  type='text' class='form-control'>",
								"</div>", 
							"</div>",
  							"<div class='form-group'>",
								"<label class='col-sm-2 col-md-2 control-label'>Keterangan</label>",
								"<div class='col-sm-7 col-md-7'>",
									"<textarea required name='keterangan_"+lengthDom+"' class='form-control' placeholder='Keterangan'></textarea>",
								"</div>",
								"<div class='col-md-1'>",
									"<button class='btn btn-danger' onClick='deleteDomKeuangan(event, this)'><i class='fa fa-trash' ></i></button>",	
								"</div>",
 							"</div></span>"];

 				var barangForm = [ "<div class='form-group'>",
 								   		"<label class='col-md-2 control-label'>Nama Barang</label>",
								    	"<div class='col-md-7'>",
		 									"<input required  type='text' name='nama_barang_"+lengthDom+"' class='form-control' placeholder='Sepatu'>",
										"</div>", 
 								    "</div>",
 								    "<div class='form-group'>",
									"<label class='col-md-2 control-label'>Jumlah</label>",
								    "<div class='col-md-7'>",
		 								"<input  required type='text' name='jumlah_barang_"+lengthDom+"' class='form-control' placeholder='10'>",
									"</div>", 
									"</div>"];

				if(subtipe.nama_tipe == "Barang") {
					barangForm.forEach(function(item) {
						html.push(item);
					});
				}				

				html2.forEach(function(item) {
					html.push(item);
				});

				var html = html.join('');
 							
				angular.element("#body-form").append(html);	 


			}  
	}
})();
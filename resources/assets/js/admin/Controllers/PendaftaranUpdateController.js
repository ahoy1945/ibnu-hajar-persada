(function() {
	'use strict';

	angular.module('PendaftaranUpdateController', [])
		   .controller('PendaftaranUpdateController', PendaftaranUpdateController);

	PendaftaranUpdateController.$inject = ['$scope', 'LokasiFactory', 'PesertaFactory', 'MasterDataFactory', '$modal']	   
	function PendaftaranUpdateController($scope, LokasiFactory, PesertaFactory, MasterDataFactory, $modal ) {
		var paketPeserta;
		if (! window.jamaah.paket) {
			paketPeserta = window.jamaah.harga_paket_id+','+jamaah.tgl_berangkat;
		} else {
			paketPeserta = window.jamaah.paket.harga_paket_id+','+jamaah.paket.tgl_berangkat;
		}
		var vm = this;

		vm.selected;
		vm.dataMahram;
		vm.berangkat = window.tgl_berangkat;
		vm.pesawat = window.pesawat;
		vm.bus = window.bus;
		vm.date = [];
		vm.data = {
			tgl_lahir: '',
			paket: paketPeserta,
			tgl_pembuatan_passpor: '',
			tgl_expired_passpor: '',
			programIsSelected: window.jamaah.tipe_paket,
			mahram: (window.jamaah.peserta_id == null ? "1": "0"),
			provinsi: (window.provinsi ? window.provinsi.id : '' ) ,
			kabupaten: (window.kabupaten ? window.kabupaten.id : ''),
			kecamatan: (window.kecamatan ? window.kecamatan.id : ''),
			desa: (window.desa ? window.desa.id : ''),
			alamat: '',
			nama_mahram: '',
			tgl_berangkat: '',
			hubungan_mahram_kepala_keluarga: window.jamaah.hubungan_mahram 
		};

		vm.datepicker = {
			tgl_lahir: false,
			tgl_pembuatan_passpor: false,
			tgl_expired_passpor: false
		}

		vm.openDatepicker = openDatepicker;
		vm.searchMahram = searchMahram;
		vm.selectMahram = selectMahram;
		vm.showDetail = showDetail;
		 
	 
	 	
		function selectMahram(item)
		{	
			angular.element("#mahram_id").val(item.id);	
		}

		var date = new Date();

		for(var i = date.getFullYear(); i <= date.getFullYear() + 5;i++) {
			vm.date.push(i);
		}

		function searchMahram(keyword) {
			return PesertaFactory.searchMahram(keyword)
						  .then(function(response) {
						  	vm.dataMahram = response.data; 
						  	return response.data.map(function(item) {
						  		 return item;
						  	});
						  }, function(error) {
						  	console.log(error);
						  })
						  .catch(function(error) {
						  	console.log('error was occured '+ error);
						  });
		}

		function openDatepicker(name) {
			vm.datepicker[name] = !vm.datepicker[name]; 
		}

		LokasiFactory.getProvinsi()
					  .then(function(response) {
					  	vm.provinsi = response.data;
					 
					  }, function(error) {
					  	console.log(error);
					  })
					  .catch(function(error) {
					  	console.log(error);
					  });
		function showDetail(id) {
			vm.detailPeserta;

			

			$modal.open({
				animation: true,
				size: 'lg',
				templateUrl: 'myModalContent.html',
				controller: function($scope, $modalInstance, PesertaFactory, data) {
					 
					PesertaFactory.show(id)
						  .then(function(response) {
						  	$scope.data = response.data
							console.log($scope.data)
						  }, function(response) {
						  	console.log(response);
						  })
						  .catch(function(error) {
						  	console.log('error was ocuured');
						  });

					$scope.close = function() {
						$modalInstance.close();
					}	  	
				},
				resolve: {
					data: function() {
						return id;
					}
				}
			});

			

		}			   

		 
		


		$scope.$watch('vm.data.provinsi', function(newValue, oldValue) {
			console.log(newValue, oldValue);
			if(newValue !== '') {
				 	LokasiFactory.getKabupaten(vm.data.provinsi)
		 				 .then(function(response) {
		 				 	vm.kabupaten = response.data;
		 				 })

			} 
		});	

		$scope.$watch('vm.data.kabupaten', function(newValue, oldValue) {
			if(newValue !=='') {
				LokasiFactory.getKecamatan(vm.data.kabupaten)
							 .then(function(response) {
							 	vm.kecamatan = response.data;
							 });
			}
		});	 

		$scope.$watch('vm.data.kecamatan', function(newValue, oldValue) {
			if(newValue !== '') {
				LokasiFactory.getDesa(vm.data.kecamatan)
							 .then(function(response) {
							 	vm.desa = response.data;
							 });

						 
			}
		});

		$scope.$watch('vm.data.desa', function(newValue, oldValue) {
			if(vm.data.provinsi !== '' && vm.data.kecamatan !== '' && vm.data.desa !== '' && vm.data.kabupaten !== '') {
					vm.data.alamat = true;
				}	
		});

		$scope.$watch('vm.data.programIsSelected', function(newValue, oldValue) {
			vm.paket = window.tgl_berangkat.filter(function(tgl_berangkat) {	
				return tgl_berangkat.tipe === newValue;
			});
			
			var newDokumen = [];	
			vm.dokumen = window.dokumen.filter(function(dokumen) {
				return dokumen.tipe === newValue;
			});  	

			for (var i = 0; i < vm.dokumen.length; i++) {
				for(var j = 0; j < window.jamaah.documents.length; j++) {
				 
					if(vm.dokumen[i].id == window.jamaah.documents[j].pivot.dokumen_id) {
						vm.dokumen[i].checked = true;
						break;
						 
					}  
				}
				 
			};

			console.log(vm.dokumen);
		});
 
	}	

})();
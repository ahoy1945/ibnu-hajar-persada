(function() {
	'use strict';

	angular.module('PemberangkatanController', [])
		   .controller('PemberangkatanController', PemberangkatanController);


	PemberangkatanController.$inject = ['$scope'];	   
	function PemberangkatanController($scope) {
		var vm = this;
		vm.hargapaket;
		vm.input  = {};
		vm.date = [];
		vm.openDatepicker = openDatepicker;

		function openDatepicker() {
			$('#tanggal_pemberangkatan').datepicker();
		}

		var date = new Date();

		for(var i = date.getFullYear(); i <= date.getFullYear() + 5;i++) {
			vm.date.push(i);
		}

		$scope.$watch('vm.input.tipe', function(newValue, oldValue) {
			 console.log(newValue);
			 vm.hargapaket = window.hargapaket.filter(function(hargapaket) {
			 	return hargapaket.tipe_paket === newValue;
			 });

			 console.log(vm.hargapaket);
		});
	}	
})();
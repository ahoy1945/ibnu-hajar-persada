(function() {
	'use strict';

	angular.module('TabunganController', [])
		   .controller('TabunganController', TabunganController);

	TabunganController.$inject = ['$scope', '$http', 'TabunganFactory'];
	function TabunganController($scope, $http, TabunganFactory)
	{
		var vm = this;
		vm.data;
		vm.nama;
		vm.search = search;
		vm.select = select;

		function search(keyword) {
			return TabunganFactory.search(keyword)
						  .then(function(response) {
						  	vm.data = response.data; 
						  	console.log(response);
						  	return response.data.map(function(item) {
						  		 return item;
						  	});
						  }, function(error) {
						  	console.log(error);
						  })
						  .catch(function(error) {
						  	console.log('error was occured '+ error);
						  });
		}

		function select(item)
		{	

			angular.element("#tabungan_id").val(item.id);	
		}
	}
})();
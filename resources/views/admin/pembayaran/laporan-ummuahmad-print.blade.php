<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style type="text/css">
  * {
    box-sizing: border-box;
  }
   body {
    width: 100%;
    font-family: arial;
   }
   .container {
    width: 100%;
    margin: 0 auto;
    
   }

   .container .header {
    text-align: center;
   }

   .container .header img {
    width: 300px;
   }

   .data-list td, .data-list th {
    border: 1px solid black;
   }
   th {
    border-right: 1px solid black;
   }

   tbody tr:last-child td {
     
   }
   th:last-child, td:last-child {
    
   }

   

   table.data-list  {
     
    
    text-align: left;
    border-spacing: 0px;
    border: 1px solid black;
   }

   table.tanda-tangan {
    text-align: center;
   }
  </style>
</head>
<body>  
 <div class="container">
  <div class="header">
   <img src="{{ public_path('dist/img/logo.png') }}">
   <P>{{ $settings[1]->meta_value }} TLP : {{ $settings[2]->meta_value }}</P>   
  <h2>Laporan Ummu Ahmad</h2>
  </div>

  <div class="body-table">

    <table border="0"  class="data-list" align="center" width="550">
      <thead>
        <tr height="50">
          <?php $no = 1; ?>
          <th height="30" width="25">No</th>
          <th width="90">Nama</th>
          <th width="80">Tgl Berangkat</th>
          <th width="80">Harga Paket($)</th>
          <th>Diskon($)</th>
          <th>Total Bayar($)</th>
          <th>Sisa($)</th>
          <th>Tota Bayar(Rp)</th>
          <th>Status</th>
                    
        </tr>
      </thead>
      <tbody>
         <?php 
                          $i = 1; 
                          $totalBayarDollar = 0;
                          $totalBayarRupiah = 0;
                          $totalHargaPaket = 0;
                          $totalBelumLunas  = 0;
                          $totalSudahLunas = 0;
                          $sisa  = 0;
                          $totalDiskon = 0;
                          $totalJamaah = 0;
                        ?> 

          @foreach($jamaah  as $value)

                        <tr>
                          <td>{{$i++}}</td>
                           
                          <td>
                               {{$value->nama_peserta}}
                          </td> 


                          <td>
                              {{$value->paket->tgl_berangkat}}
                          </td>
                           
                          <td>
                              ${{$value->paket->harga->harga_paket}}
                              <?php $totalHargaPaket += $value->paket->harga->harga_paket ?>
                          </td>
                          <td>
                              ${{$value->diskon}}
                              <?php $totalDiskon  += $value->diskon ?>
                          </td>
                          <td>
                              ${{$value->jumlah_bayar_dollar}}
                              <?php $totalBayarDollar += $value->jumlah_bayar_dollar ?>
                              @if($value->belum_dikonversi)
                                <p style="color:red">
                                  <small><strong>{{$value->belum_dikonversi}}</strong> pembayaran belum dikonversi</small>
                                </p>
                              @endif
                          </td>
                          <td>
                          @if(($value->paket->harga->harga_paket-$value->diskon)-$value->jumlah_bayar_dollar < 0)

                            0
                          
                          @else
                            
                          ${{($value->paket->harga->harga_paket-$value->diskon)-$value->jumlah_bayar_dollar}}</td>
                          <?php $sisa += ($value->paket->harga->harga_paket-$value->diskon)-$value->jumlah_bayar_dollar   ?>
                          @endif
                          <td>
                              <?php $totalBayarRupiah += $value->jumlah_bayar_rupiah ?>
                              Rp. {{ number_format($value->jumlah_bayar_rupiah,0,",",".") }}
                              @if($value->belum_dikonversi)
                                <p style="color:red">
                                    <small><strong>{{$value->belum_dikonversi}}</strong> pembayaran belum dikonversi</small>
                                </p>
                              @endif
                          </td>
                          <td>
                            @if($value->jumlah_bayar_dollar >= ($value->paket->harga->harga_paket - $value->diskon))
                             Lunas
                              <?php $totalSudahLunas +=1 ?>
                            @else
                              Belum
                              <?php $totalBelumLunas +=1 ?>
                            @endif
                          </td> 
                         
                        </tr>
                        <?php $totalJamaah += 1 ?>
                      @endforeach                
          
      </tbody>
    </table>
     <br>
                  <table align="center" width="550" style="font-weight: bold;" >
                      <tr>
                        <td width="120">
                          Total Harga Paket
                        </td>
                        <td width="20">:</td>
                        <td width="200">${{$totalHargaPaket}}</td>
                        <td width="130" >Total Jamaah</td>
                        <td width="20">:</td>
                        <td>{{$totalJamaah}}</td>
                      </tr> 
                      <tr>
                        <td>
                        Total Bayar Dollar
                        </td>
                        <td>:</td>
                        <td>${{$totalBayarDollar}}</td>
                        <td>Belum Lunas</td>
                        <td>:</td>
                        <td>{{$totalBelumLunas}}</td>
                      </tr>
                      <tr>
                        <td>Total Diskon</td>
                        <td>:</td>
                        <td>${{$totalDiskon}}</td>
                         <td>Sudah Lunas 
                        </td>
                        <td>:</td>
                        <td>{{$totalSudahLunas}}</td>
                      </tr>
                      <tr>
                         <td>Total Sisa</td>
                         <td>:</td>
                         <td>${{$sisa}}</td>
                         <td>
                            Total Bayar Rupiah 
                         </td>
                         <td>:</td>
                         <td>Rp.{{number_format($totalBayarRupiah,0,",",".")}}</td>
                      </tr>
                         
                  </table>

  </div>
 </div>   
</body>
</html>
@extends('admin.layouts.default')

@section('title', 'Edit Pembayaran ')

@section('content')
	
	<form action="{{route('admin.pembayaran.update', $pembayaran->id)}}" n  method="POST"  class="col-md-12 form-horizontal" >
		 <div class="box box-info">

                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Edit Pembayaran {{$pembayaran->peserta->nama_peserta}}</h3>
                  </div>
                  @include('admin.pembayaran.partials.form-edit')		
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

@section('script')
  <script type="text/javascript">
      $(document).ready(function() {
        var tgl_bayar = $('#tgl_bayar');
      
        tgl_bayar.datepicker();
      });
  </script>
    

@stop
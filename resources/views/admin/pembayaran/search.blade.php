@extends('admin.layouts.default')

@section('title', 'Perlengkapan Jamaah')
 
@section('content')
  <div class="col-md-12" ng-controller="PembayaranController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <a href="{{route('admin.pembayaran.create-multi-store')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                      
                      &nbsp;
                      
                    </div>
                  
                  
                       <div class="col-md-6 col-md-pull-1">
                      <form action="{{ route('admin.pembayaran.index') }}" method="GET">
                       <div class="input-group input-group-sm">
                           
                            <select class="form-control" name="tipe" ng-model="vm.input.tipe">
                              <option value="umroh">Umroh</option>
                              <option value="haji">Haji</option>
                            </select>   

                          <span class="input-group-btn">
                         
                          </span> 
                             <select name="tgl_berangkat" class="form-control" ng-if="vm.input.tipe == 'umroh'">
                             <option value="">-</option>
                             @foreach($tglPemberangkatan as $key => $value)
                                <option value="{{$value}}" @if(isset($parameters['tgl_berangkat']) && $parameters['tgl_berangkat'] == $value) selected @endif>{{formatDate($value)}}</option>
                             @endforeach
                           </select>
                       
                           <select name="tgl_berangkat" class="form-control" ng-if="vm.input.tipe == 'haji'">
                             <option value="-">-</option>
                             @foreach($rangeYears as $range)
                                <option value="{{$range}}" @if(isset($parameters['tgl_berangkat']) && $parameters['tgl_berangkat'] == $range) selected @endif>{{$range}}</option>
                             @endforeach
                           </select>  
                          

                           
                          <span class="input-group-btn">
                             
                          </span>
                           <input type="text" @if(isset($parameters['keyword'])) value="{{$parameters['keyword']}}" @endif  name="keyword" class="form-control" placeholder="Nama Jamaah">
                           
                          <span class="input-group-btn">
                             
                          </span>

                          <span class="input-group-btn">
                             
                          </span>

                           
                          <span class="input-group-btn">
                            <button type="submit"   class="btn btn-info btn-flat" type="button">Search</button>
                          </span>

                         
                      </form>
                      
                      </div>

                      
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>No. Kwitansi</th>
                        <th>Tgl Bayar</th>
                       
                        <th width="80">Harga Paket($)</th>
                        <th width="100">Total Bayar(Rp)</th>
                        <th width="180">Total Bayar($)</th>
                        <th width="100">Agen</th>
                        <th>Action</th>  
                      </tr>
                     
                    </thead>
                    <tbody>
                         <?php $i = 1; ?>
                      @foreach($peserta  as $value)
                        
                        <tr>
                          <td>
                              @if($page != "1")
                              {{ ($i++*$page)+1 }}
                              @else
                                  {{$i++*$page}}
                              @endif
                          </td>
                          <td>{{ $value->
                            nama_peserta }}  </td>
                          <td>{{ $value->no_kwitansi }}</td>
                          <td>
                          {{ $value->tgl_bayar }}
                              <?php $rek = \App\Rekening::find($value->rekening_id) ?>
                          {{$rek->bank}}<i>({{$rek->mata_uang}})</i>
                          </td>
                           
                          <td>${{ \App\HargaPaket::find($value->harga_paket_id)->harga_paket }}</td>  
                          <td>
                          @if($value->mata_uang == "idr")
                            Rp. {{ number_format($value->jumlah,0,",",".") }}
                          @endif
                           </td>
                           <td style="text-align:center;">
                               <?php $konversi = \App\PesertaPembayaran::find($value->pembayaran_id)  ?>
                               @if($value->mata_uang == "usd")
                                    {{$value->jumlah}}
                              @elseif(! $konversi->konversi->count())
                                 <form method="POST" action="{{route('admin.pembayaran.konversi', $value->id)}}">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-warning">Konversi</button>
                              
                                 </form>
                            
                              @else
                                 <?php $jumlah = $konversi->konversi->toArray(); ?>
                                   $1 = Rp. {{ number_format(end($jumlah)['kurs'],0,",",".") }} 
                                  ${{$value->jumlah/end($jumlah)['kurs']}}      
                                   
                              @endif
                           </td>
                          <td style="text-align:center;">@if($value->user->is_agen == "0")
                                <strong>-</strong>
                              @else
                                {{$value->name}}
                              @endif
                          </td>
                          <td style="text-align:center;">
                          <a ng-click="vm.showDetail({{$value->peserta_id}})" class="btn btn-warning"><i class="fa fa-eye"></i></a>
                         
                          <form style="float:left;" method="POST" action="{{route('admin.pembayaran.print', $value->no_kwitansi)}}">
                               {{ csrf_field() }} 
                                 <input type="hidden" name="tipe" value="multi">
                                   <input type="hidden" name="no_kwitansi" value="{{$value->no_kwitansi}}">
                               <input type="hidden" name="peserta_id" value="{{$value->peserta_id}}"> 
                              <button type="submit" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i></button>
                          </form>

                           <a href="{{ route('admin.pembayaran.edit', $value->id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                          <form method="POST" style="float:right;" action="{{route('admin.pembayaran.destroy', $value->pembayaran_id)}}">
                              {{csrf_field()}}
                              
                              
                              {{method_field('DELETE')}}
                              <button class="btn btn-danger" onclick="return confirm('Yakin anda akan menghapus pembayaran {{$value->nama_peserta}} ?')"><i class="fa fa-trash"></i></button>
                          </form>
                          </td>
                          
                          
                       
                        </tr>
                      @endforeach  
                         
                       
                    </tbody>
                  </table>
                   {!! $peserta->appends($parameters)->render() !!} 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
       @include("admin.pembayaran.partials.show")           
   </div>
    
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $("#tanggal").datepicker();
    });
  </script> 
@stop

@section('style')
  <style>
    th {
      text-align: center;
      vertical-align: middle !important;
      white-space: nowrap !important;
    }
    .table-bordered>tbody>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #95a5a6 !important;
    }
  </style>
@stop
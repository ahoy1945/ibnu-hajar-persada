@extends('admin.layouts.default')

@section('title', 'Perlengkapan Jamaah')
 
@section('content')
  <div class="col-md-12" ng-controller="PembayaranController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <a href="{{route('admin.pembayaran.create-multi-store')}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                      
                      &nbsp;
                      
                    </div>
                  
                  
                       <div class="col-md-6 col-md-pull-1">
                      <form action="{{ route('admin.pembayaran.index') }}" method="GET">
                       <div class="input-group input-group-sm">
                             <select class="form-control" name="tipe" ng-model="vm.input.tipe">
                              <option value="umroh">Umroh</option>
                              <option value="haji">Haji</option>
                            </select>   

                          <span class="input-group-btn">
                         
                          </span>

                          <select name="tgl_berangkat" class="form-control" ng-if="vm.input.tipe == 'umroh'" ng-model='vm.input.tgl_berangkat'>
                             <option value="">-</option>
                             @foreach($tglPemberangkatan as $key => $value)
                                <option value="{{$value}}">{{formatDate($value)}}</option>
                             @endforeach
                           </select>
                       
                           <select name="tgl_berangkat" class="form-control" ng-if="vm.input.tipe == 'haji'" ng-model='vm.input.tgl_berangkat'>
                             <option value="-">-</option>
                             @foreach($rangeYears as $range)
                                <option value="{{$range}}">{{$range}}</option>
                             @endforeach
                           </select> 

                           
                          <span class="input-group-btn">
                             
                          </span>
                           <input type="text"  name="keyword" class="form-control" placeholder="Nama Jamaah">
                           
                          <span class="input-group-btn">
                             
                          </span>

                          <span class="input-group-btn">
                             
                          </span>

                           
                          <span class="input-group-btn">
                            <button type="submit"   class="btn btn-info btn-flat" type="button">Search</button>
                          </span>

                         
                      </form>
                      
                      </div>

                      
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>No. Kwitansi</th>
                        <th width="100">Tgl Bayar</th>
                        <th width="10">Mata Uang</th>
                        <th width="40">Harga Paket($)</th>
                        <th width="50">Diskon($)</th>
                        <th width="100">Total Bayar(Rp)</th>
                        <th width="130">Total Bayar($)</th>
                        <th width="100">Agen</th>
                        <th width="220">Action</th>  
                      </tr>
                     
                    </thead>
                    <tbody>
                         <?php $i = 1; ?>
                      @foreach($peserta  as $value)
                        
                        <tr>
                          <td>
                              @if($page != "1")
                              {{ ($i++*$page)+1 }}
                              @else
                                  {{$i++*$page}}
                              @endif
                          </td>
                          <td>{{ $value->peserta->nama_peserta }}  </td>
                          <td>{{ $value->no_kwitansi }}</td>
                          <td>
                          {{ $value->tgl_bayar }}
                          <?php $rek = \App\Rekening::find($value->rekening_id) ?>
                          {{$rek->bank}}<i>({{$rek->mata_uang}})</i>
                          </td>
                          <td>{{strtoupper($value->mata_uang)}}</td>
                          <td>${{ $value->peserta->paket->harga->harga_paket + \App\Pakethotel::where('paket_id', $value->peserta->paket->harga->hotel[0]['paket_id'])->where('kamar', $value->peserta->paket->tipe_kamar )->first()->harga  }}
                         
                          </td> 
                          <td style="text-align:center;">
                              {{$value->diskon}}
                          </td> 
                          <td>
                          @if($value->mata_uang == "idr")
                            Rp. {{ number_format($value->jumlah,0,",",".") }}
                          @endif
                           </td>
                           <td style="text-align:center;">
                              @if($value->mata_uang == "usd")
                                    {{$value->jumlah}}
                              @elseif(! $value->konversi->count())
                                 <form method="POST" action="{{route('admin.pembayaran.konversi', $value->id)}}">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-warning">Konversi</button>
                              
                                 </form>
                            
                              @else
                                 <?php $jumlah = $value->konversi->toArray(); ?>
                                   $1 = Rp. {{ number_format(end($jumlah)['kurs'],0,",",".") }} 
                                  ${{$value->jumlah/end($jumlah)['kurs']}}      
                                   
                              @endif
                           </td>
                          <td style="text-align:center;">@if($value->peserta->user->is_agen == "0")
                                <strong>-</strong>
                              @else
                                {{$value->peserta->user->name}}
                              @endif
                          </td>
                          <td style="text-align:center;">
                          <a ng-click="vm.showDetail({{$value->peserta->id}})" class="btn btn-warning"><i class="fa fa-eye"></i></a>
                         
                          <form style="float:left;" method="POST" action="{{route('admin.pembayaran.print')}}">
                               {{ csrf_field() }} 
                               <input type="hidden" name="tipe" value="multi">
                               <input type="hidden" name="no_kwitansi" value="{{$value->no_kwitansi}}">
                               <input type="hidden" name="peserta_id" value="{{$value->peserta_id}}"> 
                              <button type="submit" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i></button>
                          </form>

                           <a href="{{ route('admin.pembayaran.edit', $value->id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                          <form method="POST" style="float:right;" action="{{route('admin.pembayaran.destroy', $value->id)}}">
                              {{csrf_field()}}
                              {{method_field('DELETE')}}
                              <button class="btn btn-danger" onclick="return confirm('Yakin anda akan menghapus pembayaran {{$value->peserta->nama_peserta}} ?')"><i class="fa fa-trash"></i></button>
                          </form>
                          </td>
                          
                          
                       
                        </tr>
                      @endforeach  
                         
                       
                    </tbody>
                  </table>
                   {!! $peserta->render() !!} 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
       @include("admin.pembayaran.partials.show")           
   </div>
    
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $("#tanggal").datepicker();
    });
  </script> 
@stop

@section('style')
  <style>
    th {
      text-align: center;
      vertical-align: middle !important;
      white-space: nowrap !important;
    }
    .table-bordered>tbody>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #95a5a6 !important;
    }
  </style>
@stop
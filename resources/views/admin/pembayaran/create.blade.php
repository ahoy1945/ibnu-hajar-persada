@extends('admin.layouts.default')

@section('title', 'Buat Pembayaran')

@section('content')
	
	<form action="{{route('admin.pembayaran.store')}}" ng-controller="PembayaranController as vm" method="POST"  class="col-md-12 form-horizontal" name="userForm" novalidate novalidate>
		 <div class="box box-info">
             
                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Buat Pembayaran</h3>
                  </div>
                  
                  @include('admin.alert.form_errors')
                  @include('admin.pembayaran.partials.form-create')		
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

@section('script')
    
    

@stop
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">
 	* {
		box-sizing: border-box;
	}
	 body {
	 	width: 100%;
	 	font-family: arial;
	 }
	 .container {
	 	width: 100%;
	 	margin: 0 auto;
	 	
	 }

	 .container .header {
	 	text-align: center;
	 }

	 .container .header img {
	 	width: 300px;
	 }

	 .data-list td, .data-list th {
	 	border: 1px solid black;
	 }
	 th {
	 	border-right: 1px solid black;
	 }

	 tbody tr:last-child td {
	 	 
	 }
	 th:last-child, td:last-child {
	  
	 }

	 

	 table.data-list  {
	 	 
	 	
	 	text-align: left;
	 	border-spacing: 0px;
	 	border: 1px solid black;
	 }

	 table.tanda-tangan {
	 	text-align: center;
	 }
	</style>
</head>
<body>	
 <div class="container">
 	<div class="header">
	 <img src="{{ public_path('dist/img/logo.png') }}">
	 <P>{{ $settings[1]->meta_value }} TLP : {{ $settings[2]->meta_value }}</P>	 	
	<h2>KWITANSI</h2>
 	</div>

 	<div class="body-table">
 		<table border="0">
 			<tr>
 				<td>No. Kwitansi</td>
 				<td>:</td>
 				<td>1111</td>
 				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
 				<td>Paket</td>
 				<td>:</td>
 				<td>{{ \App\HargaPaket::find($jamaah->paket->harga_paket_id)->nama_paket}}</td>
 			</tr>
 			<tr>
 				<td>Tanggal Kwitansi</td>
 				<td>:</td>
 				<td>{{formatDate(date('Y-m-d'))}}</td>
 				<td></td>
 				<td>Total Harga</td>
 				<td>:</td>
 				<td>${{ \App\HargaPaket::find($jamaah->paket->harga_paket_id)->harga_paket}}</td> 
 			</tr>
 			<tr>
 				<td>Nama</td>
 				<td>:</td>
 				<td>{{$jamaah->nama_peserta}}</td>
 				<td></td>
 				
 			</tr>
 		</table>
 		<br>
 		<table border="0"  class="data-list" align="center" width="550">
 			<thead>
 				<tr height="50">
 					<?php $no = 1; ?>
 					<th height="30" width="25">No</th>
 					<th width="90">Tanggal</th>
 					<th width="80">Pembayaran</th>
 					<th width="80">Mata Uang</th>
 					<th>Nominal</th>
 					<th>Nominal($)</th>
                    
 				</tr>
 			</thead>
 			<tbody>
 				 @foreach($jamaah->pembayaran as $pembayaran)
 				 	<tr>
 				 		<td style="text-align:center;" height="20" >{{ $no++ }}</td>
 				 		<td>{{ formatDate($pembayaran->tgl_bayar) }}</td>
 				 		<td>{{ ucwords($pembayaran->pembayaran) }}</td>
 				 		<td>{{ strtoupper($pembayaran->mata_uang) }}</td>
 				 		<td>@if($pembayaran->mata_uang == "idr") 
 				 				Rp.
 				 			@else
 				 				$
 				 			@endif	 
 				 			{{ number_format($pembayaran->jumlah,0,",",".") }}
 				 		</td>
 				 		<td>
 				 			@if(!empty($pembayaran->konversi->toArray()))
 				 				<?php $kwitansi = $pembayaran->konversi->toArray() ?>
 				 				{{$pembayaran->jumlah/last($kwitansi)['kurs']}}
 				 
 				 			@endif
 				 		</td>

 				 	</tr>
 				 @endforeach
 				 
 				 
 			</tbody>
 		</table>
 		Terbilang : {{ ucwords($terbilang) }} Rupiah
 		<br>
 		<br>
 		<br>
 		<br>
 		<table class="tanda-tangan" width="100%" style="margin:0 auto;">
 			<tr>
 				<td>Jamaah</td>
 				<td >PT. Ibnu Hajar Persada</td>
 			</tr>
 			<tr>
 				<td height="40" ></td>
 				<td ></td>
 			</tr>

 			<tr>
 				<td>{{$jamaah->nama_peserta}}</td>
 				<td>Staff</td>
 			</tr>

 		</table>

 	</div>
 </div>		
</body>
</html>
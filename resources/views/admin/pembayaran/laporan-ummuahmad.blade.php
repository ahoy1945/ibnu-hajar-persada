@extends('admin.layouts.default')

@section('title', 'Perlengkapan Jamaah')
 
@section('content')
  <div class="col-md-12" ng-controller="LaporanUmmuAhmadController as vm">
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      
                      
                      &nbsp;
                      
                    </div>
                         
                      <div class="col-md-6 col-md-pull-1">
                      <form action="{{ route('admin.pembayaran.laporan') }}" method="GET">
                       <div class="input-group input-group-sm">
                          <input type="hidden" name="tipe" value="{{$parameters['tipe']}}">
                          <select name="tipe_paket" class="form-control" ng-model="vm.input.tipe_paket">
                              <option value="umroh">Umroh</option>
                              <option value="haji">Haji</option>
                          </select>
                          
                           <span class="input-group-btn">
                             
                          </span>

                             <select name="tgl_berangkat" ng-if="vm.input.tipe_paket == 'umroh'" class="form-control">
                                @foreach($tglPemberangkatan as $key => $value)
                                    <option @if(isset($parameters['tgl_berangkat']) && $parameters['tgl_berangkat'] == $value) selected=""  @endif value="{{$value}}">{{formatDate($value)}}</option>
                                @endforeach
                            </select> 

                             <select name="tgl_berangkat" ng-if="vm.input.tipe_paket == 'haji'" class="form-control">
                                
                                @foreach($rangeYears as $year)
                                  <option value="{{$year}}">{{$year}}</option>
                                @endforeach 
                            </select>   

                          <span class="input-group-btn">
                             
                          </span>

                          <select name="status" class="form-control">
                               <option value="">Status</option>
                               <option @if(isset($parameters['status']) && $parameters['status'] == "belum") selected @endif value="belum">Belum Bayar</option>
                               <option @if(isset($parameters['status']) && $parameters['status'] == "lunas") selected @endif value="lunas">Lunas</option>
                               <option @if(isset($parameters['status']) && $parameters['status'] == "sisa") selected @endif value="sisa">Belum Lunas</option>
                            </select> 

                           <span class="input-group-btn"></span>
                             
                           <input type="text" @if(isset($parameters['keyword'])) value="{{$parameters['keyword']}}" @endif  name="keyword" class="form-control" placeholder="Nama Jamaah">
                           
                          <span class="input-group-btn">
                             
                          </span>

                          <span class="input-group-btn">
                             
                          </span>

                           
                          <span class="input-group-btn">
                            <button type="submit"   class="btn btn-info btn-flat" type="button">Search</button>
                          </span>

                         
                      </form>

                         <a style="margin-left:10px;margin-top:-20px;" href="{{route('admin.pembayaran.laporan-ummu-ahmad-print', $parameters)}}" class="btn btn-warning">
                           <i class="fa fa-print"></i>
                         </a>

                      </div>

                      
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>
                          @if(isset($parameters['tipe_paket']) && $parameters['tipe_paket'] == 'haji')
                            Tahun Daftar
                          @else
                            Tgl Berangkat
                          @endif
                        </th>
                      
                        <th width="80">Harga Paket($)</th>
                        <th>Diskon($)</th>
                        <th width="150">Total Bayar($)</th>
                        <th>Sisa($)</th>
                        <th width="150">Total Bayar(Rp)</th>
                        <th>Status</th>
                        <th width="110">Action</th>
                        
                      </tr>
                     
                    </thead>
                   
                    <tbody>
                        <?php 
                          $i = 1; 
                          $totalBayarDollar = 0;
                          $totalBayarRupiah = 0;
                          $totalHargaPaket = 0;
                          $totalBelumLunas  = 0;
                          $totalSudahLunas = 0;
                          $sisa  = 0;
                          $totalDiskon = 0;
                          $totalJamaah = 0;
                        ?> 
                      @foreach($jamaah  as $value)
                        <?php $hargaPaketHotel = \App\Pakethotel::where('paket_id', $value->paket->harga->id)->where('kamar', $value->paket->tipe_kamar)->first()->harga ?>
                        <tr>
                           <td>{{$i++}}</td>
                          <td>
                               {{$value->nama_peserta}}
                          </td> 


                          <td>
                              {{substr($value->paket->tgl_berangkat, 0,4)}}
                          </td>
                           
                          <td>
                              ${{$value->paket->harga->harga_paket + $hargaPaketHotel}}
                              <?php $totalHargaPaket += $value->paket->harga->harga_paket + $hargaPaketHotel ?>
                          </td>
                          <td>
                              ${{$value->diskon}}
                              <?php $totalDiskon  += $value->diskon ?>
                          </td>
                          <td>
                              ${{$value->jumlah_bayar_dollar}}

                              <?php $totalBayarDollar += $value->jumlah_bayar_dollar ?>
                              @if($value->belum_dikonversi)
                                <p style="color:red">
                                  <small><strong>{{$value->belum_dikonversi}}</strong> pembayaran belum dikonversi</small>
                                </p>
                              @endif
                          </td>
                          <td>
                          @if(($value->paket->harga->harga_paket-$value->diskon)-$value->jumlah_bayar_dollar < 0)

                            $0
                          
                          @else
                            
                          ${{(($value->paket->harga->harga_paket + $hargaPaketHotel)-$value->diskon)-$value->jumlah_bayar_dollar}}</td>
                          <?php $sisa += ($value->paket->harga->harga_paket-$value->diskon)-$value->jumlah_bayar_dollar   ?>
                          @endif
                          <td>
                              <?php $totalBayarRupiah += $value->jumlah_bayar_rupiah ?>
                              Rp. {{ number_format($value->jumlah_bayar_rupiah,0,",",".") }}
                              @if($value->belum_dikonversi)
                                <p style="color:red">
                                    <small><strong>{{$value->belum_dikonversi}}</strong> pembayaran belum dikonversi</small>
                                </p>
                              @endif
                          </td>
                          <td>
                            @if($value->jumlah_bayar_dollar >= (($value->paket->harga->harga_paket - $hargaPaketHotel) - $value->diskon))
                             <button class="btn btn-success">Lunas</button>
                              <?php $totalSudahLunas +=1 ?>
                            @else
                              <button class="btn btn-danger">Belum</button>
                              <?php $totalBelumLunas +=1 ?>
                            @endif
                          </td> 
                          <td>
                            <a ng-click="vm.showDetail({{$value->id}})" style="margin-left:8px;"  class="btn btn-warning"><i class="fa fa-eye"></i></a>
                            
                            <form method="POST" style="float:right;" action="{{route('admin.pembayaran.print', $value->no_kwitansi)}}">
                              {{csrf_field()}}
                              <input type="hidden" name="peserta_id" value="{{$value->id}}">
                              <input type="hidden" name="tipe" value="single">
                              
                            </form>
                          </td>
                        </tr>
                        <?php $totalJamaah += 1 ?>
                      @endforeach 
                    </tbody>
                  </table> 
                  <br>
                  <table style="font-weight: bold;" >
                      <tr>
                        <td width="120">
                          Total Harga Paket
                        </td>
                        <td width="20">:</td>
                        <td width="200">${{$totalHargaPaket}}</td>
                        <td width="130" >Total Jamaah</td>
                        <td width="20">:</td>
                        <td>{{$totalJamaah}}</td>
                      </tr> 
                      <tr>
                        <td>
                        Total Bayar Dollar
                        </td>
                        <td>:</td>
                        <td>${{$totalBayarDollar}}</td>
                        <td>Belum Lunas</td>
                        <td>:</td>
                        <td>{{$totalBelumLunas}}</td>
                      </tr>
                      <tr>
                        <td>Total Diskon</td>
                        <td>:</td>
                        <td>${{$totalDiskon}}</td>
                         <td>Sudah Lunas 
                        </td>
                        <td>:</td>
                        <td>{{$totalSudahLunas}}</td>
                      </tr>
                      <tr>
                         <td>Total Sisa</td>
                         <td>:</td>
                         <td>${{$sisa}}</td>
                         <td>
                            Total Bayar Rupiah 
                         </td>
                         <td>:</td>
                         <td>Rp.{{number_format($totalBayarRupiah,0,",",".")}}</td>
                      </tr>
                         
                  </table>
                   
                </div><!-- /.box-body -->
              </div><!-- /.box -->
       @include("admin.pembayaran.partials.show")           
   </div>
    
@stop

 @section('style')
  <style>
    th {
      text-align: center;
      vertical-align: middle !important;
      white-space: nowrap !important;
    }
    .table-bordered>tbody>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #95a5a6 !important;
    }
  </style>
@stop
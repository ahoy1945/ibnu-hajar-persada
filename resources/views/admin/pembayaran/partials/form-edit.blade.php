
 {!! csrf_field() !!}
 {!! method_field('PATCH') !!}

 <div class="form-group">
	<label class="col-md-2 control-label">Nama</label>
	<div class="col-md-7">
		<input type="text" name="nama" readonly   value="{{ $pembayaran->peserta->nama_peserta }}"  class="form-control">
	</div>

	 
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Tanggal</label>
	<div class="col-md-7">
		<input type="text" data-date-format="yyyy-mm-dd" name="tgl_bayar"  value="{{$pembayaran->tgl_bayar}}" id="tgl_bayar"  class="form-control">
	</div>
	@if($errors->has('tgl_bayar'))
		<div class="col-md-offset-2 col-md-7">
			<small style="color:red">{{ $errors->first('tgl_bayar') }}</small>
		</div>
	@endif
 </div>



  <div class="form-group">
	<label class="col-md-2 control-label">Tipe Pembayaran</label>
	<div class="col-md-7">
		<select name="tipe_keuangan_id" class="form-control">
			@foreach($tipeKeuangan as $keuangan)
				<option @if($pembayaran->tipe_keuangan_id === $keuangan->id) selected @endif value="{{$keuangan->id}}">{{$keuangan->nama_tipe}}</option>
			@endforeach	
		</select>
	</div>
	@if($errors->has('tipe_keuangan_id'))
		<div class="col-md-offset-2 col-md-7">
			<small style="color:red">{{ $errors->first('tipe_keuangan_id') }}</small>
		</div>
	@endif
 </div>


   <div class="form-group">
	<label class="col-md-2 control-label">Cara Pembayaran</label>
	<div class="col-md-7">
		 
		 <select name="rekening_id" class="form-control"  >
		 	 @foreach($rekening as $key => $value)
		 	 	<option value="{{$value->id}}" @if($value->id ==$pembayaran->rekening_id) selected @endif>

						@if($value->bank == 'CASH')
								{{$value->bank}} ({{strtoupper($value->mata_uang)}})
						@else
								{{$value->bank}} {{$value->no_rek}} {{$value->bank}} A/n {{$value->atas_nama}} ({{strtoupper($value->mata_uang)}})
						@endif
					
		 	 	</option>
		 	 @endforeach
		 </select>
	</div>
	@if($errors->has('pembayaran'))
		<div class="col-md-offset-2 col-md-7">
			<small style="color:red">{{ $errors->first('pembayaran') }}</small>
		</div>
	@endif
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Mata uang</label>
	<div class="col-md-7">
		<select name="mata_uang" class="form-control">
			<option @if($pembayaran->mata_uang === "idr") selected @endif  value="idr">Rupiah</option>
			<option @if($pembayaran->mata_uang === "usd") selected @endif value="usd">Dollar</option>
		</select>
	</div>
	@if($errors->has('mata_uang'))
		<div class="col-md-offset-2 col-md-7">
			<small style="color:red">{{ $errors->first('mata_uang') }}</small>
		</div>
	@endif
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Jumlah</label>
	<div class="col-md-7">
		<input type="text" value="{{$pembayaran->jumlah}}" name="jumlah" class="form-control">
	</div>
	@if($errors->has('jumlah'))
		<div class="col-md-offset-2 col-md-7">
			<small style="color:red">{{ $errors->first('jumlah') }}</small>
		</div>
  	@endif
 </div>


 <div class="form-group">
	<label class="col-md-2 control-label" >Keterangan</label>
	<div class="col-md-7">
		 <textarea name="keterangan" class="form-control">{{$pembayaran->keterangan}}</textarea>
	</div>
	@if($errors->has('keterangan'))
		<div class="col-md-offset-2 col-md-7">
			<small style="color:red">{{ $errors->first('Keterangan') }}</small>
		</div>
  	@endif
 </div>


 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-4">
 		<div class="col-md-6">
 			<button type="submit" class="btn btn-primary">Simpan</button>
 		</div>
 	</div>
 </div>
                  
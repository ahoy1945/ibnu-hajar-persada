 <tr>
                           
                          <td>
                          	 	 {{$value->nama_peserta}}
                          </td>	
                          <td>
                          		{{$value->paket->tgl_berangkat}}
                          </td>
                           
                          <td>
                          		${{$value->paket->harga->harga_paket}}
                              <?php $totalHargaPaket += $value->paket->harga->harga_paket ?>
                          </td>
                          <td>
                          		${{$value->diskon}}
                          </td>
                          <td>
                          		${{$value->jumlah_bayar_dollar}}
                          		@if($value->belum_dikonversi)
                          			<p style="color:red">
                          				<small><strong>{{$value->belum_dikonversi}}</strong> pembayaran belum dikonversi</small>
                          			</p>
                          		@endif
                          </td>
                          <td>
                          @if(($value->paket->harga->harga_paket-$value->diskon)-$value->jumlah_bayar_dollar < 0)

                            0
                          
                          @else
                            
                          ${{($value->paket->harga->harga_paket-$value->diskon)-$value->jumlah_bayar_dollar}}</td>

                          @endif
                          <td>
                          		Rp. {{ number_format($value->jumlah_bayar_rupiah,0,",",".") }}
                          		@if($value->belum_dikonversi)
                          			<p style="color:red">
                          					<small><strong>{{$value->belum_dikonversi}}</strong> pembayaran belum dikonversi</small>
                          			</p>
                          		@endif
                          </td>
                          <td>
                            @if($value->jumlah_bayar_dollar >= ($value->paket->harga->harga_paket - $value->diskon))
                             <button class="btn btn-success">Lunas</button>
                            @else
                              <button class="btn btn-danger">Belum</button>
                            @endif
                          </td> 
                          <td>
                            <a ng-click="vm.showDetail({{$value->id}})" style="margin-left:8px;"  class="btn btn-warning"><i class="fa fa-eye"></i></a>
                            
                            <form method="POST" style="float:right;" action="{{route('admin.pembayaran.print', $value->no_kwitansi)}}">
                              {{csrf_field()}}
                              <input type="hidden" name="peserta_id" value="{{$value->id}}">
                              <input type="hidden" name="tipe" value="single">
                              <button class="btn btn-primary">
                                <i class="fa fa-file-pdf-o"></i>
                              </button>
                            </form>
                          </td>
                        </tr>
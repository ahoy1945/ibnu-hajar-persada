 <script type="text/ng-template" id="myModalContent.html">

                <div class="modal-header">
                  <h3 class="modal-title">@{{data.nama_peserta}}</h3>
              </div>
              <div class="modal-body">
                      <h3>History Pembayaran</h3> 
                      <table class="table table-bordered">
                         <thead>
                            <th>No. Kwitansi</th>
                            <th>Tgl Bayar</th>
                            <th>Cara Bayar</th>
                            <th>Mata uang</th>
                            <th>Jumlah</th>
                         </thead> 
                         <tbody>
                          <tr ng-repeat="pembayaran in data.pembayaran">
                            <td>@{{pembayaran.no_kwitansi}}</td> 
                            <td>@{{pembayaran.tgl_bayar}}</td> 
                            <td>
                              <span ng-if="pembayaran.rekening.bank != 'CASH'">
                                  Transfer @{{pembayaran.rekening.bank}}<i>(@{{pembayaran.rekening.mata_uang}})</i>
                              </span>
                               <span ng-if="pembayaran.rekening.bank == 'CASH'">
                                  @{{pembayaran.rekening.bank}}<i>(@{{pembayaran.rekening.mata_uang}})</i>
                              </span>
                            </td> 
                            <td>@{{pembayaran.mata_uang}}</td> 
                            <td>@{{pembayaran.jumlah}}</td> 
                          </tr>
                         </tbody>
                      </table>

                     <h3>Biodata</h3> 
                     <div class="form-group">
                      <label class="col-md-4 control-label">Nama</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{ data.nama_peserta }}</label>
                         <label ng-if="data.nama_peserta == ''">-</label>
                      </div>
                      

                      <div class="form-group">
                      <label class="col-md-4 control-label">TTL</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{ data.tempat_lahir }}, @{{ data.tgl_lahir }}</label>
                         <label ng-if="data.tempat_lahir == '' || data.tgl_lahir == ''">-</label> 
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Jenis Kelamin</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{ data.jenis_kelamin }}</label>
                         <label ng-if="data.jenis_kelamin == ''">-</label>
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Pendidikan Terakhir</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{ data.pendidikan }}</label>
                         <label ng-if="data.pendidikan == ''">-</label>

                      </div>

                       <div class="form-group">
                      <label class="col-md-4 control-label">Pekerjaan</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{ data.pekerjaan }}</label>
                         <label ng-if="data.pekerjaan == ''">-</label>

                      </div>

                       <div class="form-group">
                      <label class="col-md-4 control-label">No Telp</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{ data.no_telp }}</label>
                         <label ng-if="data.no_telp == ''">-</label>

                      </div>

                       <div class="form-group">
                      <label class="col-md-4 control-label">No Hp</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{ data.no_hp }}</label>
                         <label ng-if="data.no_hp == ''">-</label>

                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Ukuran Baju/Kerudung</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{ data.ukuran_baju_kerudung }}</label>
                         <label ng-if="data.ukuran_baju_kerudung == ''">-</label>

                      </div>
                      
                      </div> 

                      <div class="form-group">
                      <label class="col-md-4 control-label">Alamat</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{ data.alamat.alamat }} @{{ data.alamat.desa }}  @{{ data.alamat.kecamatan }}  @{{ data.alamat.kabupaten }} @{{ data.alamat.provinsi }}</label>
                         <label ng-if="data.provinsi == ''">-</label>

                      </div>
                      
                      </div>

                      <h3>No. Identitas</h3> 
                      <div class="form-group">
                      <label class="col-md-4 control-label">KTP</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.identitas.no_ktp}}</label>
                         <label ng-if="data.identitas.no_ktp == ''">-</label>

                      </div>
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Passpor</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.identitas.no_passpor}}</label>
                         <label ng-if="data.identitas.no_passpor == ''">-</label>

                      </div>
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Tempat Pembuatan</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.identitas.tempat_pembuatan}}</label>
                         <label ng-if="data.identitas.tempat_pembuatan == ''">-</label>

                      </div>
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Tanggal Pembuatan</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.identitas.tgl_pembuatan}}</label>
                         <label ng-if="data.identitas.tgl_pembuatan == ''">-</label>

                      </div>
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Tanggal Expired</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.identitas.tgl_expired}}</label>
                         <label ng-if="data.identitas.tgl_expired == ''">-</label>

                      </div>
                      </div>
                      
                      <h3>Paket yang dipilih</h3> 

                      <div class="form-group">
                      <label class="col-md-4 control-label">Nama Paket</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.paket.harga.nama_paket}}(@{{ data.tipe_paket }})</label>
                      </div>
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Paket Hotel</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.paket.paket_hotel}}</label>
                      </div>
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Tipe Kamar</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.paket.tipe_kamar}}</label>
                      </div>
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Tanggal Berangkat</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.paket.tgl_berangkat}}</label>
                      </div>
                      </div>

                      <div class="form-group">
                      <label class="col-md-4 control-label">Pesawat</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.paket.pesawat.nama_pesawat}}</label>
                      </div>
                      </div>

                      <div class="form-group" ng-show="data.tipe_paket == 'haji'">
                      <label class="col-md-4 control-label">Kode Porsi</label>
                      <label class="col-md-1" >:</label>
                      <div class="col-md-7">
                          
                         <label>@{{data.kode_porsi}}</label>
                      </div>
                      </div>

                       
                       
                      <div class="col-md-12" ng-show="data.attach_file">
                            
                            <img width="600" class="img-responsive center-block" src="@{{data.attach_file}}"/>
                      </div>
                        



                
              </div>
              <br>
              <div class="modal-footer">
                 {/* <button class="btn btn-primary" type="button" ng-click="close()">OK</button>*/}
              </div> 
            
</script>    

 {!! csrf_field() !!}

 <input type="hidden" ng-model="vm.input.peserta_id" ng-init="vm.input.peserta_id={{ $peserta->id }}" name="peserta_id" value="{{ $peserta->id }}">
 


 <div class="form-group">
	<label class="col-md-2 control-label">Nama</label>
	<div class="col-md-7">
		<input type="text" name="nama" readonly   value="{{ $peserta->nama_peserta }}"  class="form-control">
	</div>

	 
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Tanggal</label>
	<div class="col-md-7">
		<?php $date = date('Y-m-d'); ?>
		<input type="text" ng-model="vm.input.tgl_bayar" name="tgl_bayar" ng-init="vm.input.tgl_bayar='{{ $date }}'" readonly      value="{{ date('Y-m-d') }}"   class="form-control" required  >
	</div>
 </div>



  <div class="form-group">
	<label class="col-md-2 control-label">Tipe Pembayaran</label>
	<div class="col-md-7">
		<input type="text" ng-model="vm.input.tipe_pembayaran" ng-init="vm.input.tipe_pembayaran ='{{ucwords($peserta->tipe_paket)}}'" value="{{ ucwords($peserta->tipe_paket) }}" readonly name="tipe_pembayaran" class="form-control">
		 
	</div>
 </div>


   <div class="form-group">
	<label class="col-md-2 control-label">Cara Pembayaran</label>
	<div class="col-md-7">
		 <select name="pembayaran" class="form-control" ng-model="vm.input.pembayaran" required>
		 	<option value="">Pilih</option>
		 	<option value="cash">Cash</option> 
		 	@foreach($rekening as $rek)
		 		<option value="{{ $rek->bank }} {{ $rek->no_rek }} A\n {{ $rek->atas_nama }}">Transfer {{ $rek->bank }} {{ $rek->no_rek }} A\n {{ $rek->atas_nama }}</option>
		 	@endforeach
		 </select>
	</div>
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Mata uang</label>
	<div class="col-md-7">
		<select name="mata_uang" class="form-control" ng-model="vm.input.mata_uang" required>
			<option value="">Pilih</option>
			<option value="rupiah">Rupiah</option>
			<option value="rupiah">Dollar</option>
		</select>
	</div>
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Jumlah</label>
	<div class="col-md-7">
		<input type="text" name="jumlah" class="form-control" ng-model="vm.input.jumlah" required placeholder="1000000">
	</div>
 </div>
 
  <div class="form-group">
  	<label class="col-md-2 control-label">Dengan Perlengkapan?</label>
  	<div class="col-md-7">
		<label style="width: 140px;"><input name="perlengkapan" type="radio" ng-model="vm.input.perlengkapan" value="1"> Ya</label>
		<label><input type="radio" name="perlengkapan"  ng-model="vm.input.perlengkapan" value="0"> Tidak</label>
	</div>
  </div>

  <div class="form-group" ng-if="vm.input.perlengkapan == 1">
  	<label class="col-md-2 control-label">Jumlah</label>
  	<div class="col-md-7">
		<input type="text" ng-model="vm.input.harga_perlengkapan" name="harga_perlengkapan"  class="form-control" value="1500000">	
	</div>
  </div>
 


 <div class="form-group">
	<label class="col-md-2 control-label" >Keterangan</label>
	<div class="col-md-7">
		 <textarea ng-model="vm.input.keterangan" required name="keterangan" class="form-control"></textarea>
	</div>
 </div>


 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-4">
 		<div class="col-md-6">
 			<button type="button" ng-disabled="userForm.$invalid"  ng-click="vm.submit(vm.input, false)" class="btn btn-primary">Simpan</button>
 		</div>
 		<div class="col-md-6">
 				<button type="submit" ng-disabled="userForm.$invalid"   class="btn btn-warning">Simpan & Print</button>
 		</div>
 	
 	</div>
 </div>
                  
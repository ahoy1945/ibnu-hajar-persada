
 {!! csrf_field() !!}

 <div class="form-group">
	<label class="col-md-2 control-label">Tanggal Bayar</label>
	<div class="col-md-9">
		<input type="text" name="tgl_bayar"    value="{{ date('Y-m-d') }}"  class="form-control">
	</div>
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Mata Uang</label>
	<div class="col-md-9">
		<select class="form-control" name="mata_uang">
			<option value="idr">IDR</option>
			<option value="usd">USD</option>
		</select>
	</div>
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Metoda Pembayaran</label>
	<div class="col-md-9">
		<select class="form-control" name="rekening_id">
			@if($rekening->count())
				@foreach($rekening as $key => $value)
					<option value="{{$value->id}}">
						@if($value->bank == 'CASH')
								{{$value->bank}} ({{strtoupper($value->mata_uang)}})
						@else
								{{$value->bank}} {{$value->no_rek}} {{$value->bank}} A/n {{$value->atas_nama}} ({{strtoupper($value->mata_uang)}})
						@endif
					</option>
				@endforeach
			@endif
		</select>
	</div>
 </div>

  
   <div class="form-group">
	<label class="col-md-2 control-label">Cari</label>
	<div class="col-md-9">
		<input type="text"  ng-model="vm.input.peserta" typeahead='mahram.nama_peserta for mahram in vm.searchPeserta($viewValue)' id="cariJamaah" typeahead-on-select="vm.selectPeserta(this, $item);"   placeholder="Cari Pendaftar" class="form-control">
	</div>

 </div>

  <div class="form-group">
	<label class="col-md-2 control-label"></label>
	<div class="col-md-9">
			<button type="submit" class="btn btn-primary">Simpan</button>
	</div>
 </div>



          
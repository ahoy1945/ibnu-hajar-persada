@extends('admin.layouts.default')

@section('title', 'Edit User')

@section('content')
	
	<form action="{{ route('admin.profile.update', $user->id) }}" method="POST" enctype="multipart/form-data" class="col-md-12 form-horizontal"  >
		 <div class="box box-info">
    
                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Edit Profile</h3>
                  </div>
                  
                  @include('admin.alert.form_errors')
                  @include('admin.alert.alert')
                  {!! csrf_field() !!}
                 {!! method_field('PATCH') !!}
                 <div class="form-group">
                  <label class="col-md-2 control-label">Nama</label>
                  <div class="col-md-7">
                    <input type="text" name="name" value="{{ $user->name }}" class="form-control" id="inputEmail3">
                  </div>
                 </div>

                  <div class="form-group">
                  <label class="col-sm-2 col-md-2 control-label">Email</label>
                  <div class="col-sm-7 col-md-7">
                    <input type="email" name="email" value="{{ $user->email }}" class="form-control" id="inputEmail3">
                  </div>
                 </div>

                  <div class="form-group">
                  <label class="col-sm-2 col-md-2 control-label" autocomplete="off">Password (Kosongkan, jika tidak ingin diganti)</label>
                  <div class="col-sm-7 col-md-7">
                    <input type="password" name="password" class="form-control" id="inputEmail3">
                  </div>
                 </div>

                 

                 <div class="form-group">
                  <div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                 </div>	
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

@section('script')
    
    

@stop
@extends('admin.layouts.default')

@section('title', 'Dashboard')

@section('content')

	<div class="col-md-12">
    	<div class="row">
        
		   <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
            <!-- export excel -->
            {{-- <a class="text-center pull-right" href="{{ route('admin.dashboard.pemasukan_perbulan') }}">
                <button class="btn btn-info btn-flat">
                  <i class="fa fa-file-excel-o"></i>
                </button>
            </a> --}}
            <div class="info-box-content">
              <span class="info-box-text">Total Pemasukan Perbulan</span>
              <span class="info-box-number">  </span>
               <table class="table bordered">
              <thead>
                <tr style="height:78px;">
                   <th rowspan="2">Bulan</th>
                    <th rowspan="2">Nilai(Rp)</th>
                </tr>
                <tr>
                    
                </tr>
              </thead>
              <tbody>
                @foreach($totalPemasukan as $key => $value)
                    <tr>
                      <td>{{monthToString(substr($key,5, 6))}}&nbsp;{{substr($key,0, 4)}}</td>
                      <td>{{ number_format($value,0,",",".") }}</td>
                    </tr>

                @endforeach
                    <tr>
                      <td>Total</td>
                      <td>{{ number_format(array_sum($totalPemasukan),0,",",".") }}</td>
                    </tr>
              </tbody>
            </table>
            </div>
            <!-- /.info-box-content -->
          </div>
           
           </div>
        <!-- /.col -->


          <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
            <!-- export excel -->
           {{--  <a class="text-center pull-right" href="{{ route('admin.dashboard.pengeluaran_perbulan') }}">
                <button class="btn btn-info btn-flat">
                  <i class="fa fa-file-excel-o"></i>
                </button>
            </a> --}}
            <div class="info-box-content">
              <span class="info-box-text">Total Pengeluaran Perbulan</span>
              <span class="info-box-number">  </span>
               <table class="table bordered">
              <thead>
                <tr style="height:78px;">
                   <th rowspan="2">Bulan</th>
                    <th rowspan="2">Nilai(Rp)</th>
                </tr>
                <tr>
                    
                </tr>
              </thead>
              <tbody>
                @foreach($totalPengeluaranPerbulan as $key => $value)
                    <tr>
                      <td>{{monthToString(substr($key,5, 6))}}&nbsp;{{substr($key,0, 4)}}</td>
                      <td>{{ number_format($value,0,",",".") }}</td>
                    </tr>
                @endforeach
                  <tr>
                      <td>Total</td>
                      <td>{{ number_format(array_sum($totalPengeluaranPerbulan),0,",",".") }}</td>
                    </tr>
              </tbody>
            </table>
            </div>
            <!-- /.info-box-content -->
          </div>
           
           </div>
        
         </div>	

        <div class="row">

         <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
            <!-- export excel -->
            {{-- <a class="text-center pull-right" href="{{ route('admin.dashboard.pemasukan_pertahun') }}">
                <button class="btn btn-info btn-flat">
                  <i class="fa fa-file-excel-o"></i>
                </button>
            </a> --}}
            <div class="info-box-content">
              <span class="info-box-text">Total Pemasukan Pertahun</span>
              <span class="info-box-number">  </span>
               <table class="table bordered">
              <thead>
                <tr style="height:78px;">
                   <th rowspan="2">Tahun</th>
                    <th rowspan="2">Nilai(Rp)</th>
                </tr>
                <tr>
                    
                </tr>
              </thead>
              <tbody>
                @foreach($totalPemasukanPerTahun as $key => $value)
                    <tr>
                      <td>{{$key}}</td>
                      <td>{{ number_format($value,0,",",".") }}</td>
                    </tr>
                @endforeach
              </tbody>
            </table>
            </div>
            <!-- /.info-box-content -->
          </div>
           
           </div>

         

         <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
            <!-- export excel -->
            {{-- <a class="text-center pull-right" href="{{ route('admin.dashboard.pengeluaran_pertahun') }}">
                <button class="btn btn-info btn-flat">
                  <i class="fa fa-file-excel-o"></i>
                </button>
            </a> --}}
            <div class="info-box-content">
              <span class="info-box-text">Total Pengeluaran Pertahun</span>
              <span class="info-box-number">  </span>
               <table class="table bordered">
              <thead>
                <tr style="height:78px;">
                   <th rowspan="2">Tahun</th>
                    <th rowspan="2">Nilai(Rp)</th>
                </tr>
                <tr>
                    
                </tr>
              </thead>
              <tbody>
                @foreach($totalPengeluaranPerTahun as $key => $value)
                    <tr>
                      <td>{{$key}}</td>
                      <td>{{ number_format($value,0,",",".") }}</td>
                    </tr>
                @endforeach
              </tbody>
            </table>
            </div>
            <!-- /.info-box-content -->
          </div>
           
           </div>
        </div> 

         <div class="row">

          <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>
            <!-- export excel -->
           {{--  <a class="text-center pull-right" href="{{ route('admin.dashboard.jamaah_perbulan') }}">
                <button class="btn btn-info btn-flat">
                  <i class="fa fa-file-excel-o"></i>
                </button>
            </a> --}}
            <div class="info-box-content">
              <span class="info-box-text">Jamaah Umroh Perbulan</span>
              <span  >
                <table class="table table-bordered">
                  <thead>
                    <tr >
                      <th style="vertical-align:middle">Bulan</th>
                      <th style="text-align:center;">Langsung</th>
                      <th style="text-align:center;">Agen</th>
                    </tr>
                  </thead>
                  <tbody>

                   @foreach($jamaahPerbulan   as $keyJamaah => $valueJamaah )
                        <?php //echo "<pre>".print_r($valueJamaah ,1)."</pre>"; die();?>
                        <tr>
                            <td>{{ formatDate($keyJamaah) }}</td>
                            <td>{{ $valueJamaah['langsung'] }}</td>
                            <td>{{ $valueJamaah['agen'] }}</td>
                        </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

          <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>
            <!-- export excel -->
            {{-- <a class="text-center pull-right" href="{{ route('admin.dashboard.jamaah_pertahun') }}">
                <button class="btn btn-info btn-flat">
                  <i class="fa fa-file-excel-o"></i>
                </button>
            </a> --}}
            <div class="info-box-content">
              <span class="info-box-text">Jamaah Umroh Pertahun</span>
              <span  >
                <table class="table table-bordered">
                  <thead>
                    <tr >
                      <th  style="vertical-align:middle">Tahun</th>
                      <th  style="text-align:center;">Langsung</th>
                      <th  style="text-align:center;">Agen</th>
                    </tr>

                  </thead>
                  <tbody>
                    @foreach($jamaahPertahun   as $keyJamaah => $valueJamaah )
                        <tr>
                            <td>{{ $keyJamaah }}</td>
                            <td>{{ $valueJamaah['langsung'] }}</td>
                            <td>{{ $valueJamaah['agen'] }}</td>
                        </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

                  <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>
            <!-- export excel -->
            {{-- <a class="text-center pull-right" href="{{ route('admin.dashboard.jamaah_pertahun') }}">
                <button class="btn btn-info btn-flat">
                  <i class="fa fa-file-excel-o"></i>
                </button>
            </a> --}}
            <div class="info-box-content">
              <span class="info-box-text">Jamaah Haji Pertahun</span>
              <span  >
                <table class="table table-bordered">
                  <thead>
                    <tr >
                      <th  style="vertical-align:middle">Tahun</th>
                      <th  style="text-align:center;">Langsung</th>
                      <th  style="text-align:center;">Agen</th>
                    </tr>

                  </thead>
                  <tbody>
                    @foreach($jamaahHajiPertahun   as $keyJamaah => $valueJamaah )
                        <tr>
                            <td>{{ $keyJamaah }}</td>
                            <td>{{ $valueJamaah['langsung'] }}</td>
                            <td>{{ $valueJamaah['agen'] }}</td>
                        </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
</div> 

  	</div><!-- /.col -->
@endsection
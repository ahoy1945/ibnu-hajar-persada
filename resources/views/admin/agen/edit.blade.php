@extends('admin.layouts.default')

@section('title', 'Edit Agen')

@section('content')
	
	<form action="{{route('admin.agen.update', $agen->id)}}" method="POST" ng-controller="AgenController as vm"  class="col-md-12 form-horizontal">
		 <div class="box box-info">
    
                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Edit Agen</h3>
                  </div>
                  
                  @include('admin.alert.form_errors')
                  @include('admin.agen.partials.form-edit')		
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

@section('script')
    
    

@stop
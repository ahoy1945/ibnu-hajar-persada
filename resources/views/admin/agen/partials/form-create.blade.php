
 {!! csrf_field() !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Nama</label>
	<div class="col-md-7">
		<input type="text" name="name"  class="form-control" id="inputEmail3"  >
	</div>
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">No. Hp</label>
	<div class="col-md-7">
		<input type="text" name="hp"  class="form-control" id="inputEmail3"  >
	</div>
 </div>

 <div class="box-header with-border">
        <h3 class="box-title">Rekening</h3>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">No Rek</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" name="no_rek"  class="form-control" id="inputEmail3"  >
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Atas Nama</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" name="nama_rek"  class="form-control" id="inputEmail3">
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Bank</label>
	<div class="col-sm-7 col-md-7">
		<select name="bank" class="form-control">
			<option value="">Pilih</option>
			<option value="BCA">BCA</option>
			<option value="BRI">BRI</option>
			<option value="Mandiri">Mandiri</option>
			<option value="CIMB">CIMB</option>
			<option value="MEGA">MEGA</option>
			<option value="Permata">Permata</option>
			<option value="BSM">BSM</option>
		</select>
	</div>
 </div>

 <div class="box-header with-border">
        <h3 class="box-title">Alamat Lengkap</h3>
 </div>

 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Provinsi</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" class="form-control" name="provinsi">
		 
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Kota/Kabupaten</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" class="form-control" name="kabupaten">
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Kecamatan</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" class="form-control" name="kecamatan">
	</div>
 </div>

   <div class="form-group" ng-if="vm.input.kecamatan">
	<label class="col-sm-2 col-md-2 control-label">Desa</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" class="form-control" name="desa">
	</div>
 </div>

  <div class="form-group" ng-if="vm.lokasi.alamat">
	<label class="col-sm-2 col-md-2 control-label">Alamat</label>
	<div class="col-sm-7 col-md-7">
		 <textarea class="form-control" name="alamat"></textarea>
	</div>
 </div>

 <div class="box-header with-border">
        <h3 class="box-title">Akun</h3>
 </div>



 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Email</label>
	<div class="col-sm-7 col-md-7">
		<input type="email" name="email"  class="form-control" id="inputEmail3">
	</div>
 </div>
 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Password</label>
	<div class="col-sm-7 col-md-7">
		<input type="password" name="password"  class="form-control" id="inputEmail3">
	</div>
 </div>

 


 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
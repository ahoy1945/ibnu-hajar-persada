@extends('admin.layouts.default')

@section('title', 'Agen')

@section('content')
	<div class="col-md-12">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Data Agen</h3>
                      &nbsp;
                      <a class="text-center" href="{{ route('admin.agen.create') }}">
                          <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                        </a>
                    </div>
                  
                  
                     <div class="col-md-4 col-md-push-4">
                      <form action="#" method="GET">
                       <div class="input-group input-group-sm">

                      </form>

                      </div>
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                      
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>hp</th>
                        <th>Asal</th>
                        <th>No Rek</th>
                         
                       
                       
						            <th></th>
                      </tr>
                    </thead>
                            <?php $i = 1; ?>
                    <tbody>
                      @foreach($agens as $agen)
                      <tr>
                        <td>
                           @if($page != "1")
                              {{ ($page*10+$i)-10 }}
                              <?php $i++; ?>
                          @else
                              {{$i++*$page}}
                          @endif
                        </td>
                        <td>{{ $agen->name }}</td>
                        <td>{{ $agen->email }}</td>
                        <td>{{ $agen->agen->no_hp }}</td>
                        <td>{{ $agen->agen->kabupaten }}</td>
                        <td>
                         <?php $rek =  json_decode($agen->agen->no_rek, 1)  ?>
                         {{ $rek['bank'] }} {{ $rek['no_rek'] }} a/n  {{ $rek['nama_rek'] }} 
                        </td>
                         <td>
                              <form method="POST" action="{{ route('admin.agen.destroy', $agen->id) }}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                              <a href="{{ route('admin.agen.show', $agen->id) }}" class="btn btn-primary"> <i class="fa fa-eye"></i> </a>
                              <a href="{{ route('admin.agen.edit', $agen->id) }}" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>
                              <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus staff {{ $agen->name }} ?');"><i class="fa fa-trash"></i></button>
                              </form>
                           </td>
                          </tr> 
                      @endforeach
					    
                    </tbody>
                  </table>
                  {!! $agens->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
@stop
@extends('admin.layouts.default')

@section('title', 'Detail Agen')

@section('content')
 <div class="col-md-12 form-horizontal">
	 <div class="box box-info">	    
	    <div class="box-body">
	      <div class="box-header with-border">
	        <h3 class="box-title">{{ $agen->name }}</h3>
			   
	      </div>

<div class="row">
	<div class="col-md-3 col-md-push-9">	
		 
	</div>
	<div class="col-md-9 col-md-pull-3">

		     <div class="box-header with-border">
		        <h3 class="box-title">Biodata</h3>
		     </div>
			 <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">Nama</label>
				<div class="col-sm-9 col-md-9">
					<input type="text" class="form-control" value="{{ $agen->name }}" readonly="">
				</div>
			 </div>

			 
			 	 
			 <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">Email</label>
				<div class="col-sm-9 col-md-9">
					 <input type="text" name="email" class="form-control" value="{{ $agen->email }}" readonly="">
				</div>
			 </div>
			 <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">No HP</label>
				<div class="col-sm-9 col-md-9">
					 <input type="text" name="hp" value="{{ $agen->agen->no_hp }}" class="form-control" readonly="">
				</div>
			 </div>

			  <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">No Rekening</label>
				<div class="col-sm-9 col-md-9">
					<?php $rek = json_decode($agen->agen->no_rek, 1) ?>
					 <input type="text" name="hp" value="{{ $rek['bank']  }} {{ $rek['no_rek'] }} a/n {{ $rek['nama_rek'] }}" class="form-control" readonly="">
				</div>
			 </div>
			  
			 <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">Alamat</label>
				<div class="col-sm-9 col-md-9">
					<textarea name="alamat" class="form-control" readonly="">{{ $agen->agen->alamat }} {{ $agen->agen->desa }} {{ $agen->agen->kecamatan }} {{ $agen->agen->kabupaten }} {{ $agen->agen->provinsi }}</textarea>
				</div>
			 </div>

 
			 
 
	</div>
</div><!--  div row -->

	 	</div><!-- /.box-body -->
	 </div><!-- /.box --> 	
 </div>
@stop
 <script type="text/ng-template" id="myModalContent.html">

                <div class="modal-header">
                  <h3 class="modal-title">@{{ heading }}</h3>
              </div>
              <div class="modal-body">
                   <input type="text" placeholder="Jumlah Barang" ng-model="data.input" class="form-control"> <br>
                     <input type="text" id="tanggal" class="form-control"   placeholder="Tanggal"  
                  datepicker-popup="yyyy-MM-dd"  
                  ng-model="data.tanggal" is-open="status.opened" ng-click="openDatepicker()" close-on-date-selection="true"
                  show-button-bar="false">
                  <br>

                    <textarea class="form-control" ng-model="data.keterangan" placeholder="Keterangan"> </textarea> 
              </div>
              <br>
              <div class="modal-footer">
                  <button class="btn btn-primary" type="button" ng-click="update()">OK</button>
                  <button class="btn btn-warning" type="button" ng-click="closeModal()">Cancel</button>
              </div> 
            
</script>    

 {!! csrf_field() !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Nama Barang</label>
	<div class="col-md-7">
		<input type="text" name="nama"  class="form-control">
	</div>
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Jumlah</label>
	<div class="col-md-7">
		<input type="text" value="{{ old('jumlah') }}" name="jumlah"  class="form-control"  >
	</div>
 </div>


 <div class="form-group">
	<label class="col-md-2 control-label">Tipe</label>
	<div class="col-md-7">
		 <select class="form-control" name="tipe">
		 	<option value="">Pilih</option>
		 	<option value="umroh">Umroh</option>
		 	<option value="haji">Haji</option>
		 	<option value="umrah dan haji">Umrah/Haji</option>
		 </select>
	</div>
 </div>


 <div class="form-group">
	<label class="col-md-2 control-label">Keterangan</label>
	<div class="col-md-7">
		 <textarea name="ket" class="form-control"></textarea>
	</div>
 </div>


 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
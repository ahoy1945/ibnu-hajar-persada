
 {!! csrf_field() !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Nama Jamaah</label>
	<div class="col-md-7">
		<input type="text" name="nama"  class="form-control">
	</div>
 </div>

  <div class="form-group">
	<label class="col-md-2 control-label">No Kwitansi</label>
	<div class="col-md-7">
		<input type="text" name="no_kwitansi"  class="form-control">
	</div>
 </div>
 

 <br></br>

  @if(count($inventories))
  	@foreach($inventories as $inventory)
  	<div class="form-group">
		<label class="col-md-2 control-label">{{$inventory->nama}}</label>
		<div class="col-md-7">
			<input type="text" name="jumlah_barang[]" placeholder="Jumlah" value="0"  class="form-control">
			<input type="hidden" name="nama_barang[]" value="{{$inventory->nama}}">
		</div>
	</div>

  	@endforeach
  @endif

 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
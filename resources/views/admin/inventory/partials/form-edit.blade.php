
 {!! csrf_field() !!}
 {!! method_field('PATCH') !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Nama Barang</label>
	<div class="col-md-7">
		<input type="text" name="nama" value="{{$barang->nama}}"  class="form-control">
	</div>
 </div>

 <input type="hidden" name="tipe_input" value="">

 <div class="form-group">
	<label class="col-md-2 control-label">Jumlah</label>
	<div class="col-md-7">
		<input type="text" readonly   value="{{$barang->jumlah}}" name="jumlah"  class="form-control"  >
	</div>
 </div>


 <div class="form-group">
	<label class="col-md-2 control-label">Tipe</label>
	<div class="col-md-7">
		<input type="text" name="tipe" readonly class="form-control" value="{{$barang->tipe}}">	
	</div>
 </div>


 <div class="form-group">
	<label class="col-md-2 control-label">Keterangan</label>
	<div class="col-md-7">
		 <textarea name="ket" class="form-control">{{$barang->ket}}</textarea>
	</div>
 </div>


 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
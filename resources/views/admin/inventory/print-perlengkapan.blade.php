<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">
 	* {
		box-sizing: border-box;
	}
	 body {
	 	width: 100%;
	 	font-family: arial;
	 }
	 .container {
	 	width: 100%;
	 	margin: 0 auto;
	 	
	 }

	 .container .header {
	 	text-align: center;
	 }

	 .container .header img {
	 	width: 300px;
	 }

	 tr td, tr th {
	 	border: 1px solid black;
	 }
	 th {
	 	border-right: 1px solid black;
	 }

	 tbody tr:last-child td {
	 	 
	 }
	 th:last-child, td:last-child {
	  
	 }

	 

	 table  {
	 	 
	 	
	 	text-align: left;
	 	border-spacing: 0px;
	 	border: 1px solid black;
	 }

	 table.tanda-tangan {
	 	text-align: center;
	 }
	</style>
</head>
<body>	
 <div class="container">
 	<div class="header">
	 <img src="{{ public_path('dist/img/logo.png') }}">
	 <P>{{ $settings[1]->meta_value }} TLP : {{ $settings[2]->meta_value }}</P>	 	
	<h2>TANDA TERIMA PERLENGKAPAN</h2>
 	</div>
 	<div class="body-table">
 		<table border="0"  class="data-list" align="center" width="550">
 			<thead>
 				<tr height="50">
 					<th>Nama Jamaah</th>
 				 
 					<th>No. Passpor</th>
 					@foreach($inventories as $inventory)
 						<th>{{ $inventory->nama }}</th>
 					@endforeach  
                    
 				</tr>
 			</thead>
 			<tbody>
 				<tr height="40">
 					<td>{{ $jamaah->nama_peserta }}</td>
 					<td>{{ $jamaah->identitas->no_passpor}}</td>
				  @foreach($inventories as $inventory)
				  	<td style="text-align:center;">
				  		@if(in_array($inventory->id, $jamaah->inventories_id)) 
				 			<img width="20" src="{{ public_path('dist/img/check_mark.png') }}">
				  		@else
				  			-
				  		@endif		 
                  	   
				  	</td>
                          
                   @endforeach 
 				</tr>
 				 
 			</tbody>
 		</table>

 		<br/>
 		<br/>
 		<br/>
 		<table class="tanda-tangan" border="0"  width="300" style="margin:0 auto;border:0px #FFFFFF solid;">
 			<tr>
 				<td width="50%">Yang Menerima</td>
 				<td width="50%">PT. Ibnu Hajar Persada</td>
 			</tr>
 			<tr>
 				<td height="50" ></td>
 				<td ></td>
 			</tr>

 			<tr>
 				<td height="20" valign="bottom">--------------------------------</td>
 				<td>Staff</td>
 			</tr>
 		 
 	</div>
 </div>		
</body>
</html>
@extends('admin.layouts.default')

@section('title', 'Barang')

@section('content')
	<div class="col-md-12" ng-controller="InventoryController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Data Barang</h3>
                      &nbsp;
                      <a class="text-center" href="{{ route('admin.inventory.create') }}">
                          <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                        </a>
                    </div>
                  
                  
                     <div class="col-md-4 col-md-push-4">
                      <form action="#" method="GET">
                       <div class="input-group input-group-sm">

                      </form>

                      </div>
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Tipe</th>
                        <th>Keterangan</th>
                                
						            <th></th>
                      </tr>
                    </thead>
                    <tbody>
                       
                          @foreach($items as $item)
                            <tr>
                              <td>{{ $item->nama }}</td>
                              <td>{{ $item->jumlah }}</td>
                              <td>{{ ucwords($item->tipe) }}</td>
                              <td>{{ $item->ket }}</td>
                             
                              <td>
                              
                              <form method="POST" action="{{ route('admin.inventory.destroy', $item->id) }}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <a href="#" ng-click="vm.openModal($event, {{ $item->id }}, 'increment')" class="btn btn-default"> <i class="fa fa-plus"></i> </a>
                               <a href="#" ng-click="vm.openModal($event, {{ $item->id }}, 'decrement')" class="btn btn-default "> <i class="fa fa-minus"></i> </a>
                               <a href="{{ route('admin.inventory.edit', $item->id) }}" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>

                                  <a href="{{ route('admin.inventory.show', $item->id) }}" class="btn btn-primary "> <i class="fa fa-eye"></i> </a>


                              
                              <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus staff {{ $item->nama }} ?');"><i class="fa fa-trash"></i></button>
                              </form>
                           </td>
                            </tr>
                          @endforeach  
                       
                    </tbody>
                  </table>
                  {!! $items->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
       @include("admin.inventory.partials.modal")           
   </div>
    
@stop
 <!DOCTYPE html>
<html>
<head>
  <title>Print Perlengkapan</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="/css/AdminLTE.min.css">
  <style type="text/css">
  * {
    box-sizing: border-box;
  }
   body {
    width: 100%;
    font-family: arial;
   }
   .container {
    width: 100%;
    margin: 0 auto;
    
   }

   .container .header {
    text-align: left;
   }

   .container .header img {
    width: 300px;
   }

   .data-list td, .data-list th {
    border: 1px solid black;
   }
   th {
    border-right: 1px solid black;
   }

   tbody tr:last-child td {
     
   }
   th:last-child, td:last-child {
    
   }

   

   table.data-list  {
     
    
    text-align: left;
    border-spacing: 0px;
    border: 1px solid black;
   }

   table.tanda-tangan {
    text-align: center;
   }
  </style>
<body>
<div class="container">
    <div class="header">
    <center>
       
    </center>
    </div>
    <div class="body-table">
    	<table width="540" style="border-bottom: 2px solid #111111;">
    		<tr >
    			<td width="60%"><img src="dist/img/logo.png" width="300px;"></td>
    			<td width="40%">
    				<div  style="">No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{$no_kwitansi}}<div>
    				<div style="">Tanggal&nbsp;&nbsp;: {{$tanggal}}</div>
    				<div style="font-size:32px;"><b>TANDA TERIMA</b></div>
    			</td>
    		</tr>

    	</table>
    	<table width="540">
    		<tr>
    			<td width="60%;">
    				<p>Telah terima dari: <b>PT. IBNU HAJAR PERSADA</b></p>
        			<p>Berupa:</p>
    			</td>
    			<td width="40%;">
    			 <p>Untuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <img width="17" src="dist/img/unchecked_checkbox.png">&nbsp;Umroh &nbsp;&nbsp; <img width="17" src="dist/img/unchecked_checkbox.png">&nbsp;Haji</p>
           <p>Tgl/Bulan : ......................................</p>
          </td>
    		</tr>
    	</table>
        
        <table border="0"  class="data-list" align="center" width="700px">
          <thead>
 			<tr>
 				<th>No</th>
 				<th>Perlengkapan</th>
 				<th>Jml</th>
 		 
 			</tr>          
          </thead>
          <tbody>
            <?php $i = 0; ?>
           	@foreach($barang as $barang)
           		<tr>
           			<td width="20" style="text-align:center;">{{++$i}}</td>
           			<td>{{$barang['nama_barang']}}</td>
           			<td>{{$barang['jumlah_barang']}}</td>
           		 
           		</tr>
           	@endforeach
             	
 
          </tbody>
        </table>

        <br>
 		<table class="tanda-tangan"  width="500" style="margin:0 auto;">
 			<tr>
 				<td width="50%">Yang Menerima</td>
 				<td width="50%">PT. Ibnu Hajar Persada</td>
 			</tr>
 			<tr>
 				<td height="50" ></td>
 				<td ></td>
 			</tr>

 			<tr>
 				<td>{{$jamaah}}</td>
 				<td>......................................</td>
 			</tr>

 		</table>
    </div>
</div>
</body>
</html>

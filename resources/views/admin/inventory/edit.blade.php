@extends('admin.layouts.default')

@section('title', 'Tambah Barang')

@section('content')
	
	<form action="{{route('admin.inventory.update', $barang->id)}}" method="POST"  class="col-md-12 form-horizontal">
		 <div class="box box-info">
    
                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Tambah Barang Baru</h3>
                  </div>
                  
                  @include('admin.alert.form_errors')
                  @include('admin.inventory.partials.form-edit')		
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

@section('script')
    
    

@stop
@extends('admin.layouts.default')

@section('title', 'Perlengkapan Jamaah')

@section('content')
	<div class="col-md-12" ng-controller="InventoryController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Perlengkapan Jamaah {{ ucwords($tipe_paket) }} </h3>
                      &nbsp;
                      
                    </div>
 
                      <div class="col-md-6 col-md-pull-1">
                        <form class="form-inline" action="{{ route('admin.inventory.perlengkapan-jamaah') }}" method="GET">
                           <input type="hidden" name="tipe_paket" value="@if($tipe_paket){{$tipe_paket}}@endif">
                            <div class="form-group">

                              <select class="form-control" name="tanggal">
                                <option value="">Pilih</option>
                                @if($parameters['tipe_paket'] == 'umroh')
                                    @foreach($tglPemberangkatan as $value)
                                      <option value="{{ $value }}"
                                      @if(!empty($tanggal == $value) || old('tanggal') == $value) selected @endif>{{ formatDate($value) }}</option>
                                    @endforeach
                                @else

                                    @foreach($rangeYears as $year)
                                      <option value="{{ $year }}"
                                      @if(!empty($tanggal == $year) || old('tanggal') == $year) selected @endif>{{ $year }}</option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Search</button>
                        </form>

                        
                      </div>
                      <div class="col-md-2 pull-right">
                         <a style="float:right;" href="{{route('admin.inventory.kwitansi.create', ['tipe_paket' => $tipe_paket])}}" class="btn btn-primary">
                                <i class="fa fa-print"></i>
                          </a>
                         <form method="POST" action="{{ route('admin.inventory.perlengkapan.pdf') }}">
                          {{ csrf_field() }}
                          <input type="hidden" name="tipe_paket" value="{{ $tipe_paket }}" >
                          <input type="hidden" name="tanggal"  value="{{ $tanggal }}">
                          <span class="input-group-btn pull-left">
                                <button type="submit" class="btn btn-warning">
                                  <i class="fa fa-file-pdf-o"></i>
                                </button>
                          </form>
                         
                      </div>

                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th rowspan="2"  width="250">Nama</th>
                        <th rowspan="2"  width="10">U</th>
                        <th rowspan="2"  width="20">JK</th>
                         <th rowspan="2"  width="20">Hub</th>
                        <th colspan="{{ $inventories->count() }}">Perlengkapan yang sudah diterima</th>
                        <th rowspan="2"  width="20"></th>
                      </tr>
                      <tr>
                         @foreach($inventories as $inventory)
                            <th width="10">{{ $inventory->nama }}</th>
                         @endforeach
                      </tr>
                    </thead>
                    <tbody>
                     
                       @if(count($jamaah) > 0)  
                          @foreach($jamaah as $key => $value)
                            <tr>
                               <td>{{ $value['nama_peserta'] }}</td>
                               <td>{{ age($value['tgl_lahir']) }}</td>
                               <td>{{ $value['jenis_kelamin'] }}</td>
                               @if($value['td_rowspan'] > 1)
                                <td style="vertical-align:middle;text-align:center;content:nowrap;" rowspan="{{ $value['td_rowspan'] }}">Kel</td>
                               @elseif($value['hubungan_mahram'] == null || $value['peserta_id'] == $value['id'] || $value['peserta_id'] == null  )
                                 <td></td>
                               @endif
                               
                               @foreach($inventories as $inventory)
                                    @if(in_array($inventory->id, $value['inventories']))
                                       <td style="text-align:center;"><input  ng-click="vm.updateStatusStok('{{$value['id']}}', '{{$inventory['id']}}')" type="checkbox" checked name="barang"></td>
                                    @else
                                       <td style="text-align:center;"><input  ng-click="vm.updateStatusStok('{{$value['id']}}', '{{$inventory['id']}}')" type="checkbox"   name="barang"></td>
                                    @endif
                                   
                               @endforeach 
                                <td>
                                <form method="POST" action="{{ route('admin.inventory.print' , $value['id']) }}">
                                  {{ csrf_field() }}
                                  <button class="btn btn-warning" type="submit"><i class="fa fa-print"></i></button>
                                  

                                </form>
                               </tr>
                          @endforeach
                       @else
                          <tr>
                            <td colspan="{!! $inventories->count() + 5 !!}" align="center">Tidak ada data</td>
                          </tr>
                       @endif 
                         
                       
                    </tbody>
                  </table>
                  <i style="color:red;">Yang ditampilkan hanya yang sudah bayar</i>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
       @include("admin.inventory.partials.modal")           
   </div>
    
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $("#tanggal").datepicker();
    });
  </script> 
@stop

@section('style')
  <style>
    th {
      text-align: center;
      vertical-align: middle !important;
      white-space: nowrap !important;
    }
    .table-bordered>tbody>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #95a5a6 !important;
    }
  </style>
@stop
@extends('admin.layouts.default')

@section('title', 'Detail')

@section('content')
	 <div class="col-md-12" ng-controller="InventoryController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Data History</h3>
                      &nbsp;
                       
                    </div>
                  
                  
                     <div class="col-md-3">
                        <form action="{{ route('admin.inventory.show', $id) }}" method="GET">
                          <div class="input-group input-group-sm  ">
                             <input type="text" value="{{ $tanggal }}" id="tgl_inventory" name="tanggal" class="form-control">
                               
                              <span class="input-group-btn">
                              <button type="submit"  class="btn btn-info btn-flat" type="button">Search</button>
                             </span>
                          </div>
                        </form>
                    </div>

                    <div class="col-md-2 ">
                       <form method="GET" action="{{ route('admin.inventory.show', $id) }}">
                        <input type="hidden" name="excel" value="1">
                        <input type="hidden" name="tanggal"  value="{{ $tanggal }}">
                        <span class="input-group-btn pull-right">
                              <button type="submit" class="btn btn-warning">

                                <i class="fa fa-file-excel-o"></i>

                              </button>

                         </form>
                     </div>


                  </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>Masuk</th>
                        <th>Keluar</th>
                        <th>Sisa</th>
                        <th>Keterangan</th>
                              
                      </tr>
                    </thead>
                    <tbody>
                       @foreach($inventories as $inventory)
                            <tr>
                                <td>{{ formatDate($inventory->tanggal) }}</td>
                                <td>{{ $inventory->masuk }}</td>
                                <td>{{ $inventory->keluar }}</td>
                                <td>{{ $inventory->sisa }}</td>
                                <td>{{ $inventory->keterangan }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                  @if(!empty($inventories))
                      {!! $inventories->render() !!}
                  @endif
                </div><!-- /.box-body -->
              </div><!-- /.box -->
               
   </div>
	 
@stop

@section('script')
    
    

@stop
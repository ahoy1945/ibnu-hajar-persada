 <!DOCTYPE html>
<html>
<head>
  <title>Print Perlengkapan</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="/css/AdminLTE.min.css">
  <style type="text/css">
  * {
    box-sizing: border-box;
  }
   body {
    width: 100%;
    font-family: arial;
   }
   .container {
    width: 100%;
    margin: 0 auto;
    
   }

   .container .header {
    text-align: left;
   }

   .container .header img {
    width: 300px;
   }

   .data-list td, .data-list th {
    border: 1px solid black;
   }
   th {
    border-right: 1px solid black;
   }

   tbody tr:last-child td {
     
   }
   th:last-child, td:last-child {
    
   } 

   

   table.data-list  {
     
    
    text-align: left;
    border-spacing: 0px;
    border: 1px solid black;
   }

   table.tanda-tangan {
    text-align: center;
   }
  </style>
<body>
<div class="container">
    <div class="header">
    <center>
      <h3>DAFTAR JAMAAH {{ strtoupper($tipe_paket) }} {{  formatDate($tanggal) }}</h3> 
      <h3>PT.IBNU HAJAR PERSADA</h3>  
    </center>
    </div>
    <div class="body-table">
        <table border="0"  class="data-list" align="center" width="700px">
          <thead>
            <tr>
              <th rowspan="2" width="20">No</th>
              <th rowspan="2"  width="150">Nama</th>
              <th rowspan="2"  width="20">Age</th>
              <th rowspan="2"  width="20">JK</th>
              <th rowspan="2"  width="50">No. telp</th>
              <th rowspan="2"  width="80">Alamat</th>
              <th colspan="{{ $inventories->count() }}">Perlengkapan yang sudah diterima</th>
            </tr>
            <tr>
               @foreach($inventories as $inventory)
                  <th width="10">{{ $inventory->nama }}</th>
               @endforeach
            </tr>
          </thead>
          <tbody>
           
             @if(count($jamaah) > 0)  
                <?php $i=1; ?>
                @foreach($jamaah as $key => $value)
                  <tr>
                    <td>{{ $i }}</td>
                     <td>{{ $value['nama_peserta'] }}</td>
                     <td>{{ age($value['tgl_lahir']) }}</td>
                     <td>{{ $value['jenis_kelamin'] }}</td>
                     <td>{{ $value['no_telp'] or null }}</td>
                     <td>{{ $value['provinsi'] or null }}</td>
                     @foreach($inventories as $inventory)
                          @if(in_array($inventory->id, $value['inventories']))
                             <td style="text-align:center;">V</td>
                          @else
                             <td style="text-align:center;"></td>
                          @endif
                         
                     @endforeach 
                     </tr>
                     <?php $i+=1; ?>
                @endforeach
             @else
                <tr>
                  <td colspan="{!! $inventories->count() + 5 !!}" align="center">Tidak ada data</td>
                </tr>
             @endif 
               
             
          </tbody>
        </table>
    </div>
</div>
</body>
</html>

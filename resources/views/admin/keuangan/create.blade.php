@extends('admin.layouts.default')

@section('title', 'Buat Keuangan')
 
@section('content')
	
	<div  class="col-md-12 form-horizontal" ng-controller="KeuanganController as vm" >
		 <div class="box box-info" >
                
                <div class="box-body">
                  <div class="box-header with-border">
                    <div class="col-md-2">
                      <h3 class="box-title">Data Keuangan</h3>
                    
                    
                    </div>
                    
                  </div>

                  @include('admin.alert.alert')
                  @include('admin.alert.form_errors')
                  <div id="body-form">
                    @include('admin.keuangan.partials.form')	
                  </div>
 
                  
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
      </div><!-- /.box -->
	</div>
@stop

@section('style')
   <style>
    th {
      text-align: center;
      vertical-align: middle !important;
      white-space: nowrap !important;
    }
    .table-bordered>tbody>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #95a5a6 !important;
    }
    </style>
@stop

@section('script')
  <script type="text/javascript">

    $(document).ready(function() {
           $('#tanggal').datepicker();
       });

    function kurang(){
      var saldo = $('#_saldo').val();
      var debit = $('#debit').val();

      kurangi = saldo-debit;

      $('#saldo').val(kurangi);
    }

    function tambah(){
      var saldo = $('#_saldo').val();
      var kredit = $('#kredit').val();
      var debit = $('#debit').val();

      if (debit.length > 0)
      {
        alert('kosongkan debit terlebih dahulu \n jika ingin mengisi kredit');
        $('#kredit').val('');
        $('#saldo').val('');
        return true;
      } 

      tambahkan = (parseFloat(saldo)+parseFloat(kredit));

      $('#saldo').val(tambahkan);
    }


    function cek()
    {
      var saldo = $('#saldo').val();
      var kredit = $('#kredit').val();
      var debit = $('#debit').val();

      if (kredit.length > 0 && debit.length > 0)
      {
        alert('kosongkan debit terlebih dahulu \n jika ingin mengisi kredit')
      }

      if (saldo == null || saldo == '') {
        alert('saldo tidak boleh kosong');
        return false;
      } else {
        return true;
      }

    }
  </script>
@stop
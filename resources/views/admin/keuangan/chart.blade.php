@extends('admin.layouts.default')

@section('title', 'Keuangan')

@section('content')

  <div class="col-md-12">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Statistik</h3>
                      &nbsp;
                    
                    </div>
                     
                      
                  
                     <div class="col-md-6 col-md-pull-1">
                      <form action="{{ route('admin.keuangan.chart') }}" method="GET">
                       <div class="input-group input-group-sm">
                           
                          <input type="text"  data-date-format="yyyy-mm-dd" id="from" name="from" class="form-control" placeholder="From">
                           
                          <span class="input-group-btn">
                             
                          </span>
                           <input type="text" data-date-format="yyyy-mm-dd" id="to" name="to" class="form-control" placeholder="To">
      
                         
                          <span class="input-group-btn">
                            <button type="submit" style="margin-top:3px;" class="btn btn-info btn-flat" type="button">Search</button>
                          </span>

                         
                      </form>
                 
                      </div>
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div id="data-chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
@stop

@section('script')
  
    <script type="text/javascript">
         $(document).ready(function() {
           $('#from').datepicker({
              format: "yyyy-mm",
              viewMode: "months", 
              minViewMode: "months"
           });
           $('#to').datepicker({
             format: "yyyy-mm",
             viewMode: "months", 
             minViewMode: "months"
           });
           // $('#keluar').datepicker();
           
           var data = {!! json_encode($data) !!};
           console.log(data.data.kredit);
           console.log(data.data.debit);
      
       $('#data-chart').highcharts({
        chart: {
          type: 'area'
        }, 
        title: {
            text: 'Statistik Keuangan',
            x: -20 //center
        },
        xAxis: {
            categories: data.categories
        },
        yAxis: {
            title: {
                text: 'Jumlah (Rp)'
            },
           "plotOptions": {
              "area": {
                "marker": {
                  "enabled": false,
                  "symbol": "circle",
                  "radius": 2,
                  "states": {
                    "hover": {
                      "enabled": true
                    }
                  }
                }
            }
          }
        },
        tooltip: {
            valuePrefix: 'Rp'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Debit',
            data: data.data.debit
        }, {
            name: 'Kredit',
            data: data.data.kredit
        }]
    });

         /*  var dataChart = {
              labels: [],
              kredit: [],
              debit: []
           }
           
           for(dataLabel in data) {
              dataChart.labels.push(dataLabel);
              dataChart.kredit.push(data[dataLabel].kredit);
              dataChart.debit.push(data[dataLabel].debit);

           }

           console.log(dataChart);


           var ctx = document.getElementById("myChart").getContext("2d");
           var data = {
              labels: dataChart.labels,
              datasets: [
                  {
                      label: "Debit",
                      fillColor: "#00C0EF",
                      strokeColor: "#3C8DBC",
                      pointColor : "#bb574e",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(220,220,220,1)",
                      data: dataChart.debit
                  },
                  {
                      label: "Kredit",
                      fillColor : "#f8b1aa",
                      strokeColor : "#bb574e",
                      pointColor: "rgba(151,187,205,1)",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(151,187,205,1)",
                      data: dataChart.kredit
                  }
              ]
          };
           var options = {
              scaleLabel : "<%=addCommas(value)%>",
              multiTooltipTemplate: "<%= datasetLabel %> - <%= addCommas(value) %>"

           }
           var chart = new Chart(ctx).Bar(data, options);*/
           

          
       });

     function addCommas(nStr)
           {
                nStr += '';
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + '.' + '$2');
                }
                return x1 + x2;
          }
    </script>
@stop
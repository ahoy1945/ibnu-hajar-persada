@extends('admin.layouts.default')

@section('title', 'Detail Keuangan')

@section('content')
 <div class="col-md-12 form-horizontal">
	 <div class="box box-info">	    
	    <div class="box-body">
	      <div class="box-header with-border">
	        <h3 class="box-title">Detail Keuangan</h3>
			  <div class="pull-right">
			    <?php $url = "?mata_uang=".$mata_uang."" ?>
			    <form method="POST" action="{{ route('admin.keuangan.destroy', $keuangan->id.$url) }}">
			      {!! csrf_field() !!}
			      {!! method_field('DELETE') !!}
			  	<a href="{{ route('admin.keuangan.index', ['mata_uang' => $mata_uang]) }}" class="btn btn-primary btn-flat" title="back to home"><i class="fa fa-backward"></i></a>
				<a href="{{ route('admin.keuangan.edit', $keuangan->id.$url) }}" class="btn btn-warning btn-flat" title="edit keuangan"><i class="fa fa-pencil"></i></a>
			    <button class="btn btn-danger btn-flat" title="delete keuangan" onclick="return confirm('yakin ingin menghapus data {{ $keuangan->keterangan }} ?');"><i class="fa fa-trash"></i></button>
			    </form>
			  </div>
	      </div>

<div class="row">

	<div class="col-md-12">

		     <div class="box-header with-border">
		        <h3 class="box-title">Keuangan</h3>
		     </div>
			 <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">Tanggal</label>
				<div class="col-sm-9 col-md-9">
					<input type="text" class="form-control" value="{{ $keuangan->tanggal->format('d-M-Y') }}" readonly="">
				</div>
			 </div>
			 <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">Keterangan</label>
				<div class="col-sm-9 col-md-9">
					<textarea class="form-control" readonly="">{{ $keuangan->keterangan }}</textarea>
				</div>
			 </div>
			  <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">Debit</label>
				<div class="col-sm-9 col-md-9">
					<input type="text" class="form-control" value="Rp. {{ number_format($keuangan->debit,0,",",".") }}" readonly="">
				</div>
			 </div>			 
			 <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">Kredit</label>
				<div class="col-sm-9 col-md-9">
					 <input type="text" class="form-control" value="Rp. {{ number_format($keuangan->kredit,0,",",".") }}" readonly="">
				</div>
			 </div>
			 <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label">Saldo</label>
				<div class="col-sm-9 col-md-9">
					 <input type="text" value="Rp. {{ number_format($keuangan->saldo,0,",",".") }}" class="form-control" readonly="">
				</div>
			 </div>
			 <div class="form-group">
				<label class="col-sm-2 col-md-2 control-label"><i>Data created at</i></label>
				<div class="col-sm-9 col-md-9">
					<i><input type="text" class="form-control" value="{{ $keuangan->created_at->format('d-M-Y H:i:s') }}" readonly=""></i>
				</div>
			 </div>

	</div>
</div><!--  div row -->

	 	</div><!-- /.box-body -->
	 </div><!-- /.box --> 	
 </div>
@stop
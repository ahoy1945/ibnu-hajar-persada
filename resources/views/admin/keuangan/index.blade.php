@extends('admin.layouts.default')

@section('title', 'Keuangan')

@section('content')

  <div class="col-md-12">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Data Keuangan</h3>
                      &nbsp;
                      <a class="text-center" href="{{ route('admin.keuangan.create', ['mata_uang' => $query['mata_uang'] ]) }}">
                          <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                        </a>
                    </div>
                   
                      
                  
                     <div class="col-md-6 col-md-pull-1">
                      <form action="{{ route('admin.keuangan.index') }}" method="GET">
                       <div class="input-group input-group-sm">
                          <input type="hidden" name="mata_uang" value="{{ $query['mata_uang'] }}">
                           
                          <input type="text"  data-date-format="yyyy-mm-dd" id="from" name="from" class="form-control" placeholder="From">
                           
                          <span class="input-group-btn">
                             
                          </span>
                           <input type="text" data-date-format="yyyy-mm-dd" id="to" name="to" class="form-control" placeholder="To">
                           
                          <span class="input-group-btn">
                             
                          </span>


                          <select name="type" class="form-control" id="">
                            <option value="all">All</option>
                            <option value="debit">Debit</option>
                            <option value="kredit">Credit</option>
                          </select>
                           
                          <span class="input-group-btn">
                            <button type="submit" style="margin-top:3px;" class="btn btn-info btn-flat" type="button">Search</button>
                          </span>

                         
                      </form>
                      <form method="POST" action="{{ route('admin.keuangan.export.excel') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="mata_uang" value="{{ $query['mata_uang'] }}">
                        <input type="hidden" name="from" value="{{ $query['from'] }}" >
                        <input type="hidden" name="to"  value="{{ $query['to'] }}">
                        <input type="hidden" name="type" value="{{ $query['type'] }}" >
                       <span class="input-group-btn pull-right">
                             <button type="submit" class="btn btn-warning">
                               
                               <i class="fa fa-file-excel-o"></i>
                          
                             </button>

                        </form>  
                      </div>
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                        <th>Rekening</th>
                        <th>Tipe</th>
                        <th>Debit</th>
                        <th>Kredit</th>
                        <th>Saldo</th>
                        <th>Who Insert</th>
                        <th style="width:140px;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $last = (count($keuangan)-1); 
                        $i = 1;
                        $satuan = ($query['mata_uang'] == 'rp') ? 'Rp' : 'USD';
                      ?>
                     @foreach($keuangan as $key => $data)
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>{{ $data->tanggal->format('d-M-Y') }}</td>
                      <td>{{ $data->keterangan }}</td>
                      <td>{{ $data->no_rek }} an. {{ $data->atas_nama }}</td>
                      <td>{{$data->tipe->nama_tipe}}</td>
                      <td> @if($data->debit) {{ $satuan }} {{ number_format($data->debit,0,",",".") }} @else - @endif</td>
                      <td> @if($data->kredit) {{ $satuan }} {{ number_format($data->kredit,0,",",".") }} @else - @endif</td>
                      <td> 
                          <?php $sum = 0 ?>
                          <?php for ($j=$last; $j >= $key ; $j--) { 

                                if($j == $last) {
                                  $sum += ($keuangan->toArray()['data'][$j]['debit'] - $keuangan->toArray()['data'][$j]['kredit']) + $jumlahBefore;
                                  
                                } else {
                                    $sum += $keuangan->toArray()['data'][$j]['debit'] - $keuangan->toArray()['data'][$j]['kredit'];
                                }
                              
                          } 
                          ?>
                           {{ $satuan }} {{ number_format($sum,0,",",".") }}
                          
                      </td>
                      <td>{{ $data->user->name }}</td>
                      <td>
                        <?php $url = "?mata_uang=".$query['mata_uang']."" ?>
                        <form method="POST" action="{{ route('admin.keuangan.destroy', $data->id.$url) }}">
                          {!! csrf_field() !!}
                          {!! method_field('DELETE') !!}
                        <a href="{{ route('admin.keuangan.show', $data->id.$url ) }}" class="btn btn-primary"> <i class="fa fa-eye"></i> </a>
                        <a href="{{ route('admin.keuangan.edit', $data->id.$url) }}" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>
                        <button style="float:right;" class="btn btn-danger" onclick="return confirm('yakin ingin menghapus data {{ $data->keterangan }} ?');"><i class="fa fa-trash"></i></button>
                        </form>
                      </td>
                  </tr>
                   
                     @endforeach
                    </tbody>
                  </table>

                 
                 {!! $keuangan->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
@stop

@section('script')
    <script type="text/javascript">
         $(document).ready(function() {
           $('#from').datepicker();
           $('#to').datepicker();
           // $('#keluar').datepicker();
       });
    </script>
@stop
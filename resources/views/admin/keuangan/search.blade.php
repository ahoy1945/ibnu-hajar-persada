@extends('admin.layouts.default')

@section('title', 'Pemasukan')

@section('content')

  <div class="col-md-12">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Data Pemasukan</h3>
                      &nbsp;
                      <a class="text-center" href="{{ route('admin.pemasukan.create') }}">
                          <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                        </a>
                    </div>
                  
                  
                     <div class="col-md-4 col-md-push-4">
                      <form action="{{ route('admin.pemasukan.search') }}" method="GET">
                       <div class="input-group input-group-sm">
                          <input type="text" required name="keyword" class="form-control">
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-info btn-flat" type="button">Go!</button>
                          </span>
                      </form>
                      </div>
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th></th>
                        <th>Nama Barang</th>
                        <th>Jumlah</th>
                        <th>Harga Satuan</th>
                        <th>Total Harga</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                     @foreach($pemasukan as $data)
            <tr>
              <td>
              </td>
              <td>{{ $data->nama_barang }}</td>
              <td> @if($data->jumlah) {{ $data->jumlah }} @else - @endif</td>
              <td> @if($data->harga_satuan) Rp. {{ number_format($data->harga_satuan,0,",",".") }} @else - @endif</td>
              <td> @if($data->total_harga) Rp. {{ number_format($data->total_harga,0,",",".") }} @else - @endif</td>
              <td>
                <form method="POST" action="{{ route('admin.pemasukan.destroy', $data->id) }}">
                  {!! csrf_field() !!}
                  {!! method_field('DELETE') !!}
                <a href="{{ route('admin.pemasukan.show', $data->id) }}" class="btn btn-primary"> <i class="fa fa-eye"></i> </a>
                <a href="{{ route('admin.pemasukan.edit', $data->id) }}" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>
                <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus data {{ $data->nama_barang }} ?');"><i class="fa fa-trash"></i></button>
                </form>
              </td>
            </tr>

                     @endforeach
                    </tbody>
                  </table>
                  {!! $pemasukan->appends($keyword)->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
@stop
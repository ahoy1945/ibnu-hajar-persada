{{ csrf_field() }}
{{ method_field('PATCH') }}
<input type="hidden" name="mata_uang" value="{{ $mata_uang }}">
<div class="form-group">
	<label class="col-md-2 control-label">Tanggal</label>
	<div class="col-md-7">
		 <input  name="tanggal"  type="text" value="{{ $data->tanggal->format('Y-m-d') }}" class="form-control" >
	</div>
</div>

	 
  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Type</label>
	<div class="col-sm-7 col-md-7">
		<select name="type" required  ng-model="vm.add.tipe" class="form-control">
				<option value="">Pilih</option>
				<option value="Pemasukan">Pemasukan</option>
				<option value="Pengeluaran">Pengeluaran</option>
		</select>
	</div>
 </div> 

 <div class="form-group" ng-if="vm.add.tipe">
	<label class="col-sm-2 col-md-2 control-label"></label>
	<div class="col-sm-7 col-md-7">
		<select name="ref_tipe_keuangan_id" required ng-model="vm.add.ref_tipe_keuangan_id" class="form-control">
				<option value="">Pilih</option>
				<option ng-repeat="tipe in vm.tipe_keuangan" value="@{{tipe.id}}">@{{tipe.nama_tipe}}</option> 
		</select>
	</div>
 </div>

 <div class="form-group" ng-if="vm.ref_tipe_keuangan_id[0] == 'xxx' || vm.ref_tipe_keuangan_id[0] == 'ccc'">
	<label class="col-sm-2 col-md-2 control-label">Nama Barang</label>
	<div class="col-sm-7 col-md-7" >
		  <input type="text" name="nama_barang"  required class="form-control" ng-model="vm.add.nama_barang">
	</div>
  </div> 

  <div class="form-group" ng-if="vm.ref_tipe_keuangan_id[0] == 'barang'">
	<label class="col-sm-2 col-md-2 control-label">Jumlah Barang</label>
	<div class="col-sm-7 col-md-7">
		  <input type="text" required name="jumlah_barang"   class="form-control" ng-model="vm.add.jumlah_barang">
	</div>
 </div> 

 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Amount</label>
	<div class="col-sm-7 col-md-7">
		 <input type="text" name="amount" 
			@if($data->type == 'debit')
				value="{{ $data->debit }}"
			@else
				value="{{ $data->kredit }}"
			@endif
		  class="form-control">
	</div>
 </div>

 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">No Rekening</label>
	<div class="col-sm-7 col-md-7">
		<select name="id_rekening" required  class="form-control">
				<option value="">Pilih</option>
				@foreach($rekening as $rek)
				<option value="{{ $rek['id'] }}" 
					@if($data->id_rekening == $rek['id']) selected @endif>
					{{ strtoupper($rek['mata_uang']) }} {{ $rek['bank'] }} {{ $rek['no_rek'] }} an. {{ $rek['atas_nama'] }}</option>
				@endforeach
		</select>
	</div>
 </div> 
 
  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Keterangan</label>
	<div class="col-sm-7 col-md-7">
		<textarea name="keterangan" class="form-control" placeholder="Keterangan">{{ $data->keterangan }}</textarea>
	</div>
 </div> 


<br>
<br>
 <div class="form-group">
 	<label class="col-sm-2 col-md-2 control-label"></label>
	<div class="col-sm-7 col-md-7">
		<button type="submit" class="btn btn-primary btn-plat">Submit</button>
	</div>
 </div>
                  
{{ csrf_field() }}
<form role='form' name='blgoPost' novalidate="">
	<input type="hidden" id="mata_uang" name="mata_uang" value="{{ $mata_uang }}">
    <div class="form-group">
	<label class="col-md-2 control-label">Tanggal</label>
	<div class="col-md-7">
		 <input  name="tanggal"   ng-model="vm.add.tanggal"   data-date-format="yyyy-mm-dd" type="text" class="form-control" id="tanggal">
	</div>
 </div>


  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Type</label>
	<div class="col-sm-7 col-md-7">
		<select name="type" required  ng-model="vm.add.tipe" class="form-control">
				<option value="">Pilih</option>
				<option value="Pemasukan">Pemasukan</option>
				<option value="Pengeluaran">Pengeluaran</option>
		</select>
	</div>
 </div> 

 <div class="form-group" ng-if="vm.add.tipe">
	<label class="col-sm-2 col-md-2 control-label"></label>
	<div class="col-sm-7 col-md-7">
		<select name="ref_tipe_keuangan_id" required ng-model="vm.add.ref_tipe_keuangan_id" class="form-control">
				<option value="">Pilih</option>
				<option ng-repeat="tipe in vm.tipe_keuangan" value="@{{tipe.id}}">@{{tipe.nama_tipe}}</option> 
		</select>
	</div>
 </div>

  <div class="form-group" ng-if="vm.ref_tipe_keuangan_id[0] == 'Barang'">
	<label class="col-sm-2 col-md-2 control-label">Nama Barang</label>
	<div class="col-sm-7 col-md-7" >
		  <input type="text"  required class="form-control" ng-model="vm.add.nama_barang">
	</div>
  </div> 

  <div class="form-group" ng-if="vm.ref_tipe_keuangan_id[0] == 'barang' || vm.ref_tipe_keuangan_id[0] == 'Barang'">
	<label class="col-sm-2 col-md-2 control-label">Jumlah Barang</label>
	<div class="col-sm-7 col-md-7">
		  <input type="text" required   class="form-control" ng-model="vm.add.jumlah_barang">
	</div>
 </div> 
 
 


 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Amount</label>
	<div class="col-sm-7 col-md-7">
		 <input required type="text" name="amount" ng-model="vm.add.amount" onkeyup="formatNumber(this)" class="form-control">
	</div>
 </div>

<div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">No Rekening</label>
	<div class="col-sm-7 col-md-7">
		<select name="id_rekening" ng-model="vm.add.id_rekening" required  class="form-control">
				<option value="">Pilih</option>
				@foreach($rekening as $rek)
					<option value="{{ $rek['id'] }}">{{ strtoupper($rek['mata_uang']) }} {{ $rek['bank'] }} {{ $rek['no_rek'] }} an. {{ $rek['atas_nama'] }}</option>
				@endforeach
		</select>
	</div>
 </div> 
 
  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Keterangan</label>
	<div class="col-sm-7 col-md-7">
		<textarea name="keterangan" required ng-model="vm.add.keterangan" class="form-control" placeholder="Keterangan"></textarea>
	</div>
 </div> 

 <div class="form-group">
 	<label class="col-sm-2 col-md-2 control-label"></label>
	<div class="col-sm-7 col-md-7">
		<button type="submit" ng-disabled="blgoPost.$invalid" ng-click="vm.addInput($event)" class="btn btn-primary btn-plat">Submit</button>
	</div>
 </div>       
    
</form>  
 
 <table class="table table-bordered" ng-if="vm.input.length">
 	<thead>
 		 
 		<th>Keterangan</th>
 		<th>Tipe</th>
 		<th>Amount</th>
 		<th></th>
 	</thead>
 	<tbody id="queue">
 		<tr ng-repeat="data in vm.input">
 		 
 			<td>@{{data.keterangan}}</td>
 			<td>@{{data.tipe}}</td>
 			<td>@{{data.amount}}</td>
 			<td><button class="btn btn-danger" ng-click='vm.deleteInput($event, $index)'><i class="fa fa-trash"></i></button></td>
 		</tr>
 		<tr>
 			<td colspan="5" style="text-align:center;"><button ng-click="vm.save($event)" class="btn btn-primary">Simpan</button></td> 			
 		</tr>
 	</tbody>
 </table>
 
                  
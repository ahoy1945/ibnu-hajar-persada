@extends('admin.layouts.default')

@section('title', 'Edit Keuangan')
 
@section('content')	
     <div class="box box-info">
                
                <div class="box-body">
                  <div class="box-header with-border">
                    <h3 class="box-title">Data Keuangan</h3>
                  </div>

	<form action="{{route('admin.keuangan.update', $data->id)}}" ng-controller="KeuanganController as vm" method="POST" class="col-md-12 form-horizontal">
                  @include('admin.alert.alert')
                  @include('admin.alert.form_errors')
                  @include('admin.keuangan.partials.form-edit')
                  <input type="hidden" value="{{ $data->saldo }}" id="_saldo">		
	</form>
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
      </div><!-- /.box -->
@stop

@section('script')
  <script type="text/javascript">

    $(document).ready(function() {
           $('#tanggal').datepicker();
           // $('#keluar').datepicker();
       });

    function kurang(){
      var saldo = $('#_saldo').val();
      var debit = $('#debit').val();

      kurangi = saldo-debit;

      $('#saldo').val(kurangi);
    }

    function tambah(){
      var saldo = $('#_saldo').val();
      var kredit = $('#kredit').val();
      var debit = $('#debit').val();

      if (debit.length > 0)
      {
        alert('kosongkan debit terlebih dahulu \n jika ingin mengisi kredit');
        $('#kredit').val('');
        $('#saldo').val('');
        return true;
      } 

      tambahkan = (parseFloat(saldo)+parseFloat(kredit));

      $('#saldo').val(tambahkan);
    }


    function cek()
    {
      var saldo = $('#saldo').val();
      var kredit = $('#kredit').val();
      var debit = $('#debit').val();

      if (kredit.length > 0 && debit.length > 0)
      {
        alert('kosongkan debit terlebih dahulu \n jika ingin mengisi kredit')
      }

      if (saldo == null || saldo == '') {
        alert('saldo tidak boleh kosong');
        return false;
      } else {
        return true;
      }

    }
  </script>
@stop
  @include('admin.layouts.partials._header')

        <!-- Main content -->
        <section class="content">
          
          <div class="row">
              @yield('content')
          </div>
         
        </section><!-- /.content -->
  @include('admin.layouts.partials._footer')
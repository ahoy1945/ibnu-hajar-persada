<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
        
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="treeview">
              <a href=" ">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Pendaftaran</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('admin.daftar.create') }}"><i class="fa fa-plus"></i> Daftar </a></li>
                    <li><a href="{{ route('admin.daftar.online.index') }}"><i class="fa fa-users"></i> Data Pendaftar Online</a></li>
                <li><a href="{{ route('admin.daftar.index') }}"><i class="fa fa-users"></i> Data Pendaftar </a></li>
              </ul>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Data Jamaah</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              

              <ul class="treeview-menu">
                <li class="treeview">
                  <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Data Jamaah</span>
                    <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                       <li><a href="{{ route('admin.data-jamaah.index', ['tipe' => 'umroh']) }}"><i class="fa fa-file"></i> Data Jamaah Umroh </a></li>
                   <li><a href="{{ route('admin.data-jamaah.index', ['tipe' => 'haji']) }}"><i class="fa fa-file"></i> Data Jamaah Haji </a></li>
                  </ul>
                  <a href="{{route('admin.data_jamaah.manifest.index')}}">
                    <i class="fa fa-list"></i>
                    <span>Manifest</span>
                  </a>
                  <a href="{{route('admin.data_jamaah.room_list.index')}}">
                    <i class="fa fa-archive"></i>
                    <span>Room List & Bus</span>
                  </a>
                  <a href="{{route('admin.laporan_perkuota.index')}}">
                    <i class="fa fa-calculator"></i>
                    <span>Laporan Kuota</span>
                  </a>
                   <a href="{{route('admin.print-idcard.create')}}">
                    <i class="fa fa-print"></i>
                    <span>Cetak IDCard</span>
                  </a>
                </li>


              </ul>
               <ul class="treeview-menu">
                <li class="treeview">
                  <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Dokumen</span>
                    <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                       <li><a href="{{ route('admin.dokumen.data-jamaah', ['tipe' => 'umroh']) }}"><i class="fa fa-file"></i> Dokumen Umroh </a></li>
                   <li><a href="{{ route('admin.dokumen.data-jamaah', ['tipe' => 'haji']) }}"><i class="fa fa-file"></i> Dokumen Haji </a></li>
                  </ul>
                </li>
              </ul>
               
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>Master Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/harga_paket"><i class="fa fa-suitcase"></i>Harga Paket</a></li>
                 <li><a href="{{route('admin.dokumen.index')}}"><i class="fa fa-navicon"></i>Dokumen</a></li>
                 <li><a href="{{ route('admin.rekening.index') }}"><i class="fa fa-money"></i>Rekening</a></li>
                 <li><a href="/tipe_keuangan"><i class="fa fa-money"></i>Tipe Keuangan</a></li>
                 <li><a href="/tanggal_pemberangkatan"><i class="fa fa-calendar"></i>Tanggal Keberangkatan</a></li>
                 <li><a href="{{ route('admin.master_data.kuota.index') }}"><i class="fa fa-male"></i>Maks Kuota</a></li>
                 <li><a href="{{ route('admin.master_data.pesawat.index') }}"><i class="fa fa-plane"></i>List Pesawat</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-archive"></i>
                <span>Inventory</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('admin.inventory.index') }}"><i class="fa fa-circle-o"></i>Barang</a></li>
                 <li><a href="{{ route('admin.inventory.perlengkapan-jamaah', ['tipe_paket' => 'umroh']) }}"><i class="fa fa-circle-o"></i>Perlengkapan Jamaah Umroh</a></li>
                  <li><a href="{{ route('admin.inventory.perlengkapan-jamaah', ['tipe_paket' => 'haji']) }}"><i class="fa fa-circle-o"></i>Perlengkapan Jamaah Haji</a></li>
              </ul>
            </li>

            <li>
              <a href="{{ route('admin.agen.index') }}">
                <i class="fa fa-users "></i> <span>Agen</span>
              </a>
            </li>
               <li>
                <a href="{{ route('admin.users.index') }}">
                  <i class="fa fa-key "></i> <span>Hak Akses</span>
                </a>
              </li>

               <li>
                <a href="{{ route('admin.settings.index') }}">
                  <i class="fa fa-gear "></i> <span>Pengaturan</span>
                </a>
              </li>
          <li>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

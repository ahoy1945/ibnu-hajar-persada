<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
        
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
           
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Pendaftaran</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('admin.daftar.create') }}"><i class="fa fa-plus"></i> Daftar </a></li>
                <li><a href="{{ route('admin.daftar.index') }}"><i class="fa fa-users"></i> Data Pendaftar </a></li>
              </ul>
        
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

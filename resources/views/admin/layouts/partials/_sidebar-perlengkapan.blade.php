<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
        
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            
            
         
            <li class="treeview">
              <a href="#">
                <i class="fa fa-archive"></i>
                <span>Inventory</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('admin.inventory.index') }}"><i class="fa fa-circle-o"></i>Barang</a></li>
                 <li><a href="{{ route('admin.inventory.perlengkapan-jamaah', ['tipe_paket' => 'umroh']) }}"><i class="fa fa-circle-o"></i>Perlengkapan Jamaah Umroh</a></li>
                  <li><a href="{{ route('admin.inventory.perlengkapan-jamaah', ['tipe_paket' => 'haji']) }}"><i class="fa fa-circle-o"></i>Perlengkapan Jamaah Haji</a></li>
              </ul>
            </li>

        </li>
          <li>
              <a href="{{ route('admin.agen.index') }}">
                <i class="fa fa-users "></i> <span>Agen</span>
              </a>
            </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('head')
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/AdminLTE.min.css">
    @yield('style')
    <base href="/">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body ng-app="App" class="hold-transition skin-blue sidebar-mini">
     @include('admin.alert.loading')  
   
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href=" " class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Ibnu Hajar Persada</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        @include('admin.layouts.partials._navbar') 
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      @if(Auth::user()->roles->nama == "perlengkapan")
        @include('admin.layouts.partials._sidebar-perlengkapan')
      @elseif(Auth::user()->roles->nama == "keuangan")
        @include('admin.layouts.partials._sidebar-keuangan')
      @elseif(Auth::user()->roles->nama == "manajer")
         @include('admin.layouts.partials._sidebar-manajer')
      @elseif(Auth::user()->roles->nama == "admin")
         @include('admin.layouts.partials._sidebar-admin')
      @elseif(Auth::user()->roles->nama == "owner")
         @include('admin.layouts.partials._sidebar')
      @elseif(Auth::user()->roles->nama == "agen")
         @include('admin.layouts.partials._sidebar-agen')

      @endif

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
         
        </section>
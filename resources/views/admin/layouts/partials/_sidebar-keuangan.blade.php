<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
        
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Pendaftaran</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('admin.daftar.create') }}"><i class="fa fa-plus"></i> Daftar </a></li>
                    <li><a href="{{ route('admin.daftar.online.index') }}"><i class="fa fa-users"></i> Data Pendaftar Online</a></li>
                <li><a href="{{ route('admin.daftar.index') }}"><i class="fa fa-users"></i> Data Pendaftar </a></li>
              </ul>
          
              <li class="treeview">
                  <a href="#">
                    <i class="fa fa-money"></i>
                    <span>Keuangan</span>
                    <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"></a></li>
                    <li><a href="{{ route('admin.pembayaran.index') }}"><i class="fa fa-money"></i>Pembayaran</a></li>
                    <li><a href="{{ route('admin.keuangan.index', ['mata_uang' => 'rp']) }}"><i class="fa fa-money"></i>Keuangan (Rp)</a></li>
                    <li><a href="{{ route('admin.keuangan.index', ['mata_uang' => 'dollar']) }}"><i class="fa fa-money"></i>Keuangan (Dollar)</a></li>
                     <li><a href="{{ route('admin.rekening.laporan') }}"><i class="fa fa-money"></i>Laporan Bank</a></li>
                    <li><a href="{{route('admin.pembayaran.laporan', ['tipe' => 'ummuahmad'])}}"><i class="fa fa-list-alt"></i>Laporan Ummu Ahmad</a></li>
                  </ul>
                </li>
            
        </li>
          <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>Master Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/harga_paket"><i class="fa fa-suitcase"></i>Harga Paket</a></li>
                 <li><a href="{{route('admin.dokumen.index')}}"><i class="fa fa-navicon"></i>Dokumen</a></li>
                 <li><a href="{{ route('admin.rekening.index') }}"><i class="fa fa-money"></i>Rekening</a></li>
                 <li><a href="/tipe_keuangan"><i class="fa fa-money"></i>Tipe Keuangan</a></li>
                 <li><a href="/tanggal_pemberangkatan"><i class="fa fa-calendar"></i>Tanggal Keberangkatan</a></li>
                 <li><a href="{{ route('admin.master_data.kuota.index') }}"><i class="fa fa-male"></i>Maks Kuota</a></li>
                 <li><a href="{{ route('admin.master_data.pesawat.index') }}"><i class="fa fa-plane"></i>List Pesawat</a></li>
              </ul>
            </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

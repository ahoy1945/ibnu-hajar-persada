 </div><!-- /.content-wrapper -->

       <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
        </footer>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    
     <script src="/js/app.js"></script>
     <script type="text/javascript">

        function deleteDom(that, event)
        {
          event.preventDefault();

          var dom = $(that);

          dom.parent().parent().html("");
        }

        function deleteDomKeuangan(e, that) {
          e.preventDefault();
 
          $(that).parent().parent().parent().remove();
        }

        function formatNumber(that)
        {
             $(that).val(accounting.formatNumber(that.value));
        }

     </script>
     <script type="text/javascript" src="/js/libs/jquery.table2excel.js"></script>
      <script type="text/javascript">
      $(document).ready(function() {
          $('.excel').click(function (){
              $(".table-bordered").table2excel({
                exclude: ".noExl",
                name: "Excel Document Name",
                filename: "Room List & Bus",
                exclude_img: true,
                exclude_links: true,
                exclude_inputs: true
              });
          });
        });

        $("#tgl_pembuatan").datepicker();
        $("#tgl_expired").datepicker();

        $("#tgl_inventory").datepicker( {
          format: "yyyy-mm",
          viewMode: "months", 
          minViewMode: "months"
        });
      
      </script>
 
     @yield('script')
  </body>
</html>
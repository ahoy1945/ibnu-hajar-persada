@extends('admin.layouts.default')

@section('title', 'Laporan Perkuota')

@section('content')
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid rgba(66, 56, 56, 0.78) !important;
  }
</style>
  <div class="col-md-12" ng-controller="PendaftaranController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-2">
                      <h3 class="box-title">Laporan Perkuota</h3>
                      &nbsp;
                    </div>
                </div><!-- /.box-header -->

                <div class="box-header">
                  <div class="col-md-6">
                      <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-th-list"></i></span>

                        <div class="info-box-content">
                        @foreach($kuota as $value)
                          <span class="info-box-text">{{ $value->tipe_paket }} {{ formatDate($value->tgl_berangkat) }}</span>
                          <span class="info-box-number">{{ $value->max_kuota }}</span>
                        @endforeach
                        </div>
                       </div>
                   </div>

                  <div class="col-md-6">
                    <div class="info-box">
                      <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>

                      <div class="info-box-content">
                        <span class="info-box-text">Total Terdaftar</span>
                        <span class="info-box-number">{{ $total }} Jamaah</span>
                      </div>
                     </div>
                  </div>
                </div>

                <div class="box-header">
                  <div class="col-md-8 ">
                      <form class="form-inline" form action="{{ route('admin.laporan_perkuota.index') }}" method="GET">
                        <div class="form-group">
                          <select class="form-control" name="tipe_paket">
                            <option value="">Pilih Paket</option>
                            <option value="Umroh" @if($tipe == 'Umroh') selected @endif>Umroh</option>
                            <option value="Haji" @if($tipe == 'Haji') selected @endif>Haji</option>
                          </select>
                        </div> 
                       
                        <div class="form-group">
                          <select class="form-control" name="tgl_berangkat">
                            <option value="">Pilih tgl brgkt</option>
                            @foreach($date as $value)
                              <option value="{{ $value->tanggal_pemberangkatan }}" @if($tgl_berangkat == $value->tanggal_pemberangkatan) selected @endif>
                                {{ formatDate($value->tanggal_pemberangkatan) }}
                              </option>
                            @endforeach
                          </select>
                        </div>

                         <div class="form-group">
                          <select class="form-control" name="tipe_kamar">
                            <option value="">Pilih room</option>
                            <option value="double" @if($tipe_kamar == 'double') selected @endif>Double</option>
                            <option value="triple" @if($tipe_kamar == 'triple') selected @endif>Triple</option>
                            <option value="quad" @if($tipe_kamar == 'quad') selected @endif>Quad</option>
                            <option value="quint" @if($tipe_kamar == 'quint') selected @endif>Quint</option>
                          </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Search</button>
                      </form>
                     </div>
                </div> 

                <div class="box-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>

                        {{-- <th width="20">No</th> --}}
                        <th >Nama</th>
                        <th >Tgl Lahir</th>
                        <th >Tgl Berangkat</th>
                        <th >Tipe Kamar</th>
                        <th >Tipe Paket</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $kuota)
                        <tr>
                          <td>{{ $kuota['nama_peserta']}}</td>
                          <td>{{ formatDate($kuota['tgl_lahir']) }}</td>
                          <td>{{ formatDate($kuota['tgl_berangkat']) }}</td>
                          <td>{{ $kuota['tipe_kamar'] }}</td>
                          <td>
                            {{ $kuota['tipe_paket'] }}<br>
                            <b>{{ $kuota['nama_paket'] }}</b>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                 {!! $data->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
@stop

@extends('admin.layouts.default')

@section('title', 'Pendaftaran')

@section('content')
@if(isset($pendaftaranOnline))
  <form ng-controller="PendaftaranUpdateController as vm" enctype="multipart/form-data" action="{{route('admin.daftar.online.move_store', $value->id)}}" method="POST"  class="col-md-12 form-horizontal">
@else
	<form ng-controller="PendaftaranUpdateController as vm" enctype="multipart/form-data" action="{{route('admin.daftar.update', $value->id)}}" method="POST"  class="col-md-12 form-horizontal">
@endif

     <div class="box box-info">
               
                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                  </div>
                  @if(isset($pendaftaranOnline))
                    @include('admin.peserta.partials._form-move-online')   
                  @else
                    @include('admin.peserta.partials._form-update')		
                  @endif
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      var tgl_pembuatan_passpor = $("#tgl_pembuatan_passpor");
      var tgl_expired_passpor   = $("#tgl_expired_passpor");
      var tgl_lahir             = $("#tgl_lahir");
      
      tgl_pembuatan_passpor.datepicker().on("changeDate", function() {
        tgl_pembuatan_passpor.datepicker("hide");
      });

      tgl_expired_passpor.datepicker().on("changeDate", function(){
        tgl_expired_passpor.datepicker("hide");
      });

      tgl_lahir.datepicker().on("changeDate", function() {
        tgl_lahir.datepicker("hide");
      });
     
    });
  </script>
@stop
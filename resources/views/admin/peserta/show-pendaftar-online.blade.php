@extends('admin.layouts.default')

@section('title', 'Data Pendaftar Online')

@section('content')
	
<div class="col-md-12">
    <div class="box box-info">

        <div class="box-body">
          <div class="box-header with-border">
            <h3 class="box-title">Detail Pendaftar Online</h3>

            <a class="text-center" href="{{ route('admin.daftar.online.index') }}">
              <button class="btn btn-warning btn-flat pull-right"><i class="fa fa-arrow-left"></i></button>
            </a>
          </div>

          <div class="form-group">
             <label  class="col-sm-2 control-label">NAME</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ ucwords($peserta['nama_peserta']) }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">PLACE OF BIRTH</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ ucwords($peserta['tempat_lahir']) }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">AGE</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ date_diff(date_create($peserta['tgl_lahir']), date_create('today'))->y }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">SEX</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ $peserta['jenis_kelamin'] }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">PASSPORT</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ $peserta['no_passpor'] }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">DATE OF ISSUE</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ formatDate($peserta['tgl_pembuatan']) }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">DATE OF EXP</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ formatDate($peserta['tgl_expired']) }}</label>
           </div>
        <div class="form-group">
             <label  class="col-sm-2 control-label">ISS. OFFICE</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ $peserta['tempat_pembuatan'] }}</label>
           </div>
        <div class="form-group">
             <label  class="col-sm-2 control-label">STATUS</label>
             <label  class="col-sm-1 control-label">:</label>
             <label></label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">KABUPATEN</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ $peserta['kabupaten'] }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">PROVINSI</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ $peserta['provinsi'] }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">KODE POS</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ $peserta['kodepos'] }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">PESAWAT</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>{{ $peserta['tipe_pesawat'] }}</label>
           </div>
          <div class="form-group">
             <label  class="col-sm-2 control-label">HOTEL</label>
             <label  class="col-sm-1 control-label">:</label>
             <label>-</label>
           </div>
        </div>
    </div>
  </div>

@stop

 {!! csrf_field() !!}
 {!!method_field('PATCH')!!}
  <div class="box-header with-border">
        <h3 class="box-title">No. Identitas</h3>
 </div>

 <div class="form-group">
	<label class="col-md-3 control-label">No. KTP</label>
	<div class="col-md-7">
		<input type="text" value="{{$value->identitas->no_ktp}}" name="ktp" value="{{ old('ktp') }}"  class="form-control">
	</div>
	@if($errors->has('ktp'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('ktp') }}</small>
		</div>
	@endif
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">No. Passpor</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" value="{{$value->identitas->no_passpor}}" name="no_passpor" value="{{old('no_passpor')}}"  class="form-control" >
	</div>
	@if($errors->has('no_passpor'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('no_passpor') }}</small>
		</div>
	@endif
 </div>

<div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Tempat Pembuatan</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" value="{{$value->identitas->tempat_pembuatan}}" name="tempat_pembuatan_passpor" value="{{old('tempat_pembuatan_passpor')}}" class="form-control" >
	</div>
	@if($errors->has('tempat_pembuatan_passpor'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tempat_pembuatan_passpor') }}</small>
		</div>
	@endif
 </div>

 <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Tanggal Pembuatan</label>
	<div class="col-sm-7 col-md-7">
		    <input type="text" value="{{ editDate($value->identitas->tgl_pembuatan) }}" id="tgl_pembuatan_passpor"  data-date-format="yyyy-mm-dd" name="tgl_pembuatan_passpor" value="{{old('tgl_pembuatan_passpor')}}" class="form-control">
	</div>
	@if($errors->has('tgl_pembuatan_passpor'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tgl_pembuatan_passpor') }}</small>
		</div>
	@endif
  </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Tanggal Expired</label>
	<div class="col-sm-7 col-md-7">

	    <input type="text" value="{{ editDate($value->identitas->tgl_expired) }}" id="tgl_expired_passpor" data-date-format="yyyy-mm-dd" name="tgl_expired_passpor" value="{{old('tgl_expired_passpor')}}" class="form-control">
	</div>
	@if($errors->has('tgl_expired_passpor'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tgl_expired_passpor') }}</small>
		</div>
	@endif
  </div>

  <div class="box-header with-border">
        <h3 class="box-title">Biodata</h3>
 </div>


 <div class="form-group">
	<label class="col-md-3 control-label">Nama (Sesuai di passpor)</label>
	<div class="col-md-7">
		<input type="text" value="{{$value->nama_peserta}}" name="nama_peserta" value="{{old('nama_peserta')}}"    class="form-control">
	</div>
	@if($errors->has('nama_peserta'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('nama_peserta') }}</small>
		</div>
	@endif
 </div>

 <div class="form-group">
	<label class="col-md-3 control-label">Tempat Lahir</label>
	<div class="col-md-7">
		<input type="text"  value="{{$value->tempat_lahir}}" name="tempat_lahir" value="{{old('tempat_lahir')}}"  class="form-control">
	</div>
	@if($errors->has('tempat_lahir'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tempat_lahir') }}</small>
		</div>
	@endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Tanggal Lahir</label>
	<div class="col-md-7">
        <input type="text" value="{{ editDate($value->tgl_lahir) }}" name="tgl_lahir" value="{{old('tgl_lahir')}}" id="tgl_lahir" class="form-control" data-date-format="yyyy-mm-dd">
	</div>
	@if($errors->has('tgl_lahir'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tgl_lahir') }}</small>
		</div>
	@endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Nama Ayah Kandung</label>
	<div class="col-md-7">
		<input type="text" value="{{$value->nama_ayah}}" name="nama_ayah" value="{{old('nama_ayah')}}"  class="form-control">
	</div>
	@if($errors->has('nama_ayah'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('nama_ayah') }}</small>
		</div>
	@endif
 </div>

   <div class="form-group">
	<label class="col-md-3 control-label">No. Telepon</label>
	<div class="col-md-7">
		<input type="text" value="{{$value->no_telp}}" name="no_telp" value="{{old('no_telp')}}"  class="form-control">
	</div>
	@if($errors->has('no_telp'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('no_telp') }}</small>
		</div>
	@endif
 </div>

    <div class="form-group">
	<label class="col-md-3 control-label">No. Hp</label>
	<div class="col-md-7">
		<input type="text" value="{{$value->no_hp}}" name="no_hp" value="{{old('no_hp')}}"  class="form-control">
	</div>
	@if($errors->has('no_hp'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('no_hp') }}</small>
		</div>
	@endif
 </div>


  <div class="form-group">
	<label class="col-md-3 control-label">Pekerjaan</label>
	<div class="col-md-7">
		 <select name="pekerjaan" class="form-control">
		 	<option value="">Pilih</option>
			<option value="PNS" @if(old('pekerjaan') == "PNS" || $value->pekerjaan == "PNS") selected  @endif>PNS</option>
			<option value="TNI/POLRI" @if(old('pekerjaan') == "TNI/POLRI" || $value->pekerjaan == "TNI/POLRI") selected  @endif>TNI/POLRI</option>
			<option value="Dagang" @if(old('pekerjaan') == "Dagang" || $value->pekerjaan == "pekerjaan") selected  @endif>Dagang</option>
			<option value="Tani/Nelayan" @if(old('pekerjaan') == "Tani/Nelayan" || $value->pekerjaan == "Tani/Nelayan") selected  @endif>Tani/Nelayan</option>
			<option value="Swasta" @if(old('pekerjaan') == "Swasta" || $value->pekerjaan == "Swasta") selected  @endif>Swasta</option>
			<option value="Ibu Rumah Tangga" @if(old('pekerjaan') == "Ibu Rumah Tangga" || $value->pekerjaan == "Ibu Rumah Tangga") selected  @endif>Ibu Rumah Tangga</option>
			<option value="Pelajar/Mahasiswa"  @if(old('pekerjaan') == "Pelajar/Mahasiswa") || $value->pekerjaan == "Pelajar/Mahasiswa" selected  @endif>Pelajar/Mahasiswa</option>
			<option value="BUMN/BUMD" @if(old('pekerjaan') == "BUMN/BUMD" || $value->pekerjaan == "BUMN/BUMD") selected  @endif>BUMN/BUMD</option>
			<option value="Lain-lain" @if(old('pekerjaan') == "Lain-lain" || $value->pekerjaan == "Lain-lain") selected  @endif>Lain-lain</option>
		 </select>
	</div>
	@if($errors->has('pekerjaan'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('pekerjaan') }}</small>
		</div>
	@endif
 </div>

    <div class="form-group">
	<label class="col-md-3 control-label">Pendidikan</label>
	<div class="col-md-7">
		<select class="form-control" name="pendidikan">
			<option value="">Pilih</option>
			<option value="SD"  @if(old('pendidikan') == "SD" || $value->pendidikan == "SD") selected  @endif>SD</option>
			<option value="SLTP" @if(old('pendidikan') == "SLTP" || $value->pendidikan == "SLTP") selected  @endif>SLTP</option>
			<option value="SLTA" @if(old('pendidikan') == "SLTA" || $value->pendidikan == "SLTA") selected  @endif>SLTA</option>
			<option value="SM/D1/D2/D3" @if(old('pendidikan') == "SM/D1/D2/D3" || $value->pendidikan == "SM/D1/D2/D3") selected  @endif>SM/D1/D2/D3</option>
			<option value="S1" @if(old('pendidikan') == "S1" || $value->pendidikan == "S1") selected  @endif>S1</option>
			<option value="S2" @if(old('pendidikan') == "S2" || $value->pendidikan == "S2") selected  @endif>S2</option>
			<option value="S3" @if(old('pendidikan') == "S3" || $value->pendidikan == "S3") selected  @endif>S3</option>
		</select>
	</div>
	@if($errors->has('pendidikan'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('pendidikan') }}</small>
		</div>
	@endif
 </div>


   <div class="form-group">
	<label class="col-md-3 control-label">Golongan Darah</label>
	<div class="col-md-7">
		<select class="form-control" name="golongan_darah">
			<option value="">Pilih</option>
			<option value="A" @if(old('golongan_darah') == "A" || $value->golongan_darah == "A") selected  @endif>A</option>
			<option value="B" @if(old('golongan_darah') == "B" || $value->golongan_darah == "B") selected  @endif>B</option>
			<option value="AB" @if(old('golongan_darah') == "AB" ||  $value->golongan_darah == "AB") selected  @endif>AB</option>
			<option value="O" @if(old('golongan_darah') == "O" ) selected  @endif>O</option>
		</select>
	</div>
	@if($errors->has('golongan_darah'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('golongan_darah') }}</small>
		</div>
	@endif
 </div>


	  <div class="form-group">
		<label class="col-md-3 control-label">Ukuran Baju/Kerudung</label>
		<div class="col-md-7">
			<select class="form-control" name="ukuran_baju_kerudung">
				<option value="">Pilih</option>
				<option value="M" @if(old('ukuran_baju_kerudung') == "M" || $value->ukuran_baju_kerudung == "M") selected  @endif>M</option>
				<option value="L" @if(old('ukuran_baju_kerudung') == "L" || $value->ukuran_baju_kerudung == "L") selected  @endif>L</option>
				<option value="XL" @if(old('ukuran_baju_kerudung') == "XL" || $value->ukuran_baju_kerudung == "XL") selected  @endif>XL</option>
				<option value="XXL" @if(old('ukuran_baju_kerudung') == "XXL" || $value->ukuran_baju_kerudung == "XXL") selected  @endif>XXL</option>
				<option value="XXXL" @if(old('ukuran_baju_kerudung') == "XXXL" || $value->ukuran_baju_kerudung == "XXL") selected  @endif>XXXL</option>
			</select>
		</div>
		@if($errors->has('ukuran_baju_kerudung'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('ukuran_baju_kerudung') }}</small>
			</div>
    	@endif
	 </div>

	 <div class="form-group">
	<label class="col-md-3 control-label">Photo<i>(Kosongkan jika tidak ingin diganti)</i></label>
	<div class="col-md-7">
		 <input type="file" name="photo" class="form-control">
	</div>
		@if($errors->has('photo'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('photo') }}</small>
			</div>
    	@endif
    </div>


  <div class="form-group">
	<label class="col-md-3 control-label">Jenis Kelamin</label>
	<div class="col-md-7">
		<label style="width: 140px;"><input type="radio" class="radio-inline" name="jk" @if(old('jk') == "L" || $value->jenis_kelamin == "L") checked="true"  @endif value="L"> Laki - Laki</label>
		<label ><input type="radio" class="radio-inline" name="jk" @if(old('jk') == "P" || $value->jenis_kelamin == "P") checked="true"  @endif value="P"> Perempuan</label>
	</div>
		@if($errors->has('jk'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('jk') }}</small>
			</div>
    	@endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Kewarganegaraan</label>
	<div class="col-md-7">
		<label style="width: 140px;"><input type="radio" name="kewarganegaraan" value="WNI" @if(old('kewarganegaraan') == "WNI" || $value->kewarganegaraan == "WNI") checked="true"  @endif> WNI</label>
		<label><input type="radio"  name="kewarganegaraan" value="Asing" @if(old('kewarganegaraan') == "Asing" || $value->kewarganegaraan == "Asing" ) checked="true"  @endif> Asing</label>
	</div>
	@if($errors->has('kewarganegaraan'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kewarganegaraan') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Pergi Haji/Umroh</label>
	<div class="col-md-7">
		<label style="width: 140px;"><input @if(old('pernah_haji_umroh') == "1" || $value->pernah_haji_umroh == "1") checked="true"  @endif type="radio" name="pernah_haji_umroh" value="1"> Pernah</label>
		<label><input @if(old('pernah_haji_umroh') == "0" || $value->pernah_haji_umroh == "0") checked="true"  @endif type="radio" name="pernah_haji_umroh" value="0"> Belum Pernah</label>
	</div>
	@if($errors->has('pernah_haji_umroh'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('pernah_haji_umroh') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Kepala Keluarga(Mahram)</label>
	<div class="col-md-7">
		<label style="width: 140px;"><input type="radio"  ng-model="vm.data.mahram" name="kepala_keluarga" value="1"> Ya</label>
		<label><input type="radio" ng-model="vm.data.mahram" name="kepala_keluarga" value="0"> Bukan</label>
	</div>
	@if($errors->has('kepala_keluarga'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kepala_keluarga') }}</small>
			</div>
    @endif
 </div>


 <span ng-if="vm.data.mahram == '0'">
 	 <div class="form-group">
	<label class="col-md-3 control-label">Nama Mahram</label>
	<div class="col-md-7">

		   <input type="text"  ng-model="vm.selected" typeahead='mahram.nama_peserta for mahram in vm.searchMahram($viewValue)' typeahead-on-select="vm.selectMahram($item);"   placeholder="Cari Pendaftar" class="form-control pull-right"  >
		   <input type="hidden" value="{{$value->peserta_id}}" name="mahram_id" id="mahram_id">
	</div>
	@if($errors->has('mahram_id'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('mahram_id') }}</small>
			</div>
    @endif
 </div>

 <div class="form-group">
	<label class="col-md-3 control-label">Hubungan Mahram</label>
	<div class="col-md-7">
		<select name="hubungan_mahram" class="form-control">
			<option value="">Pilih</option>
			<option value="anak" @if(strtolower($value->hubungan_mahram) == "anak") selected @endif>Anak</option>
			<option value="ibu" @if(strtolower($value->hubungan_mahram) == "ibu") selected @endif>Ibu</option>
			<option value="woman group" @if(strtolower($value->hubungan_mahram) == "woman group") selected @endif>Woman Group</option>
			<option value="istri" @if(strtolower($value->hubungan_mahram) == "istri") selected @endif>Istri</option>
			<option value="ayah mertua" @if(strtolower($value->hubungan_mahram) == "ayah mertua") selected @endif>Ayah Mertua</option>
			<option value="saudara kandung" @if(strtolower($value->hubungan_mahram) == "saudara kandung") selected @endif>Saudara Kandung</option>
			<option value="saudara seayah" @if(strtolower($value->hubungan_mahram) == "saudara seayah") selected @endif>Saudara Seayah</option>
			<option value="saudara seibu" @if(strtolower($value->hubungan_mahram) == "saudara seibu") selected @endif>Saudara Seibu</option>
			<option value="paman" @if(strtolower($value->hubungan_mahram) == "paman") selected @endif>Paman</option>
			<option value="kakek saudara" @if(strtolower($value->hubungan_mahram) == "kakek saudara") selected @endif>Kakek Saudara</option>
			<option value="anak saudari" @if(strtolower($value->hubungan_mahram) == "anak saudari") selected @endif>Anak Saudari</option>
			<option value="saudara laki-laki" @if(strtolower($value->hubungan_mahram) == "saudara laki-laki") selected @endif>Saudara laki-laki</option>
			<option value="kakak laki-laki"  @if(strtolower($value->hubungan_mahram) == "kakak laki-laki") selected @endif>Kakak laki-laki</option>
				\<option value="kakak perempuan" @if(strtolower($value->hubungan_mahram) == "kakak perempuan") selected @endif>Kakak Perempuan</option>
			<option value="adik laki-laki" @if(strtolower($value->hubungan_mahram) == "adik laki-laki") selected @endif>Adik laki-laki</option>
			<option value="adik perempuan"  @if(strtolower($value->hubungan_mahram) == "adik perempuan") selected @endif>Adik Perempuan</option>

		</select>
	</div>
	@if($errors->has('hubungan_mahram'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('hubungan_mahram') }}</small>
			</div>
    @endif
 </div>
 </span>

 <span ng-if="vm.data.mahram == '1'">
 	<div class="form-group">
	<label class="col-md-3 control-label">Sebagai</label>
	<div class="col-md-7">
		<select name="hubungan_mahram"   class="form-control">
			<option value="">Pilih</option>
			<option value="single" @if(strtolower($value->hubungan_mahram) == "single") selected @endif>Single</option>
			<option value="ayah" @if(strtolower($value->hubungan_mahram) == "ayah") selected @endif>Ayah</option>
			<option value="suami" @if(strtolower($value->hubungan_mahram) == "suami") selected @endif >Suami</option>
			<option value="paman" @if(strtolower($value->hubungan_mahram) == "paman") selected @endif>Paman</option>
			<option value="kakek" @if(strtolower($value->hubungan_mahram) == "kakek") selected @endif>Kakek</option>
			<option value="kakak laki-laki" @if(strtolower($value->hubungan_mahram) == "kakak laki-laki") selected @endif>Kakak laki-laki</option>
			<option value="adik laki-laki" @if(strtolower($value->hubungan_mahram) == "adik laki-laki") selected @endif>Adik laki-laki</option>
			<option value="saudara laki-laki" @if(strtolower($value->hubungan_mahram) == "saudata laki-laki") selected @endif>Saudara laki-laki</option>

		</select>
	</div>
	@if($errors->has('hubungan_mahram'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('hubungan_mahram') }}</small>
			</div>
    @endif
 </div>
 </span>


 <div class="box-header with-border">
        <h3 class="box-title">Alamat Lengkap</h3>
 </div>

 <div class="form-group">
	<label class="col-md-3 control-label">Provinsi</label>
	<div class="col-sm-7 col-md-7">
		  <input type="text" name="provinsi" value="{{$value->alamat->provinsi}}" class="form-control">
	</div>
	@if($errors->has('provinsi'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('provinsi') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group" >
	<label class="col-md-3 control-label">Kota/Kabupaten</label>
	<div class="col-sm-7 col-md-7">
		  <input type="text" name="kabupaten" value="{{$value->alamat->kabupaten}}" class="form-control">
	</div>
	@if($errors->has('kabupaten'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kabupaten') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Kecamatan</label>
	<div class="col-sm-7 col-md-7">
		 <input type="text" name="kecamatan" value="{{$value->alamat->kecamatan}}" class="form-control">
	</div>
	@if($errors->has('kecamatan'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kecamatan') }}</small>
			</div>
    @endif
 </div>

   <div class="form-group">
	<label class="col-md-3 control-label">Desa</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" name="desa" value="{{$value->alamat->desa}}" class="form-control">
		 
	</div>
	@if($errors->has('desa'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('desa') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Kode POS</label>
	<div class="col-sm-7 col-md-7">
		 <input type="text" name="kodepos" value="{{$value->alamat->kodepos}}" class="form-control">
	</div>
	@if($errors->has('kodepos'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kodepos') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Alamat</label>
	<div class="col-sm-7 col-md-7">
		 <textarea class="form-control"  name="alamat">{{$value->alamat->alamat}}</textarea>
	</div>
		@if($errors->has('alamat'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('alamat') }}</small>
			</div>
   		 @endif
 </div>



  <div class="box-header with-border">
        <h3 class="box-title">Program yang dipilih</h3>
 </div>

 	<div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Program yang dipilih</label>
	<div class="col-sm-7 col-md-7">
			<label style="width: 140px;"><input type="radio" ng-model="vm.data.programIsSelected" name="tipe_paket" value="umroh"> Umroh</label>
		<label><input type="radio" ng-model="vm.data.programIsSelected" name="tipe_paket" value="haji"> Haji</label>

	</div>
	@if($errors->has('tipe_paket'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('tipe_paket') }}</small>
			</div>
   	@endif
  </div>
 <span ng-if="vm.data.programIsSelected">

 	<div class="form-group">
	<label class="col-sm-2 col-md-3 control-label" ng-if="vm.data.programIsSelected == 'umroh'">Paket Umroh</label>
	<label class="col-sm-2 col-md-3 control-label" ng-if="vm.data.programIsSelected == 'haji'">Paket Haji</label>

	<div class="col-sm-7 col-md-7">
		 <select name="harga_paket_id" ng-model="vm.data.paket" id="harga_paket_id" class="form-control">
		 	<option value="">Pilih</option>
		 	 <option ng-if="vm.data.programIsSelected == 'umroh'" ng-repeat="paket in vm.paket" value="@{{ paket.paket.id }},@{{ paket.tanggal_pemberangkatan }}" >@{{ paket.paket.nama_paket }} <i>(@{{ paket.tanggal_pemberangkatan }})</i> </option>

		 	<option ng-if="vm.data.programIsSelected == 'haji'" ng-repeat="paket in vm.paket" value="@{{ paket.paket_id }},@{{ paket.tahun }}" >@{{ paket.nama_paket }}</option>
		 </select>
	</div>
	@if($errors->has('harga_paket_id'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('harga_paket_id') }}</small>
			</div>
   	@endif
  </div>

   <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Paket Hotel</label>
	<div class="col-sm-7 col-md-7">
		 <select name="paket_hotel" class="form-control">
		 	<option value="">Pilih</option>
		 	<option @if($value->paket->paket_hotel == "ekonomi") selected @endif value="ekonomi">Ekonomi</option>
		 	<option  @if($value->paket->paket_hotel == "bisnis") selected @endif value="bisnis">Bisnis</option>
		 	<option  @if($value->paket->paket_hotel == "eksekutif") selected @endif value="eksekutif">Eksekutif</option>
		 </select>
	</div>
	@if($errors->has('paket_hotel'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('paket_hotel') }}</small>
			</div>
   	@endif
  </div>

    <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Pilihan Kamar</label>
	<div class="col-sm-7 col-md-7">
		 <select name="tipe_kamar" class="form-control">
		 	<option value="">Pilih</option>
		 	<option @if($value->paket->tipe_kamar == "double") selected @endif value="double">Double</option>
		 	<option @if($value->paket->tipe_kamar == "triple") selected @endif value="triple">Triple</option>
		 	<option @if($value->paket->tipe_kamar == "quad") selected @endif value="quad">Quad</option>
		 	<option @if($value->paket->tipe_kamar == "quint") selected @endif value="quad">Quint</option>
		 </select>
	</div>
	@if($errors->has('tipe_kamar'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('tipe_kamar') }}</small>
			</div>
   	@endif
  </div>
    	

  <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Pilihan Pesawat</label>
	<div class="col-sm-7 col-md-7">	

		 <select name="pesawat_id" class="form-control">
		 	<option value="">Pilih</option>
		 	@foreach($pesawat as $key => $valuePesawat)
		 		<option @if($value->paket->pesawat_id == $valuePesawat->id) selected @endif value="{{$valuePesawat->id}}">{{ucwords($valuePesawat->nama_pesawat)}}</option>
		 	@endforeach 
		 </select>
	</div>
	@if($errors->has('tipe_kamar'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('pesawat_id') }}</small>
			</div>
   	@endif
   </div>
   
   <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Tipe Pesawat</label>
	<div class="col-sm-7 col-md-7">
		 <select name="tipe_pesawat" class="form-control">
		 	<option value="">Pilih</option>
		 	<option @if($value->paket->tipe_pesawat == "ekonomi") selected @endif value="ekonomi">Ekonomi</option>
		 	<option @if($value->paket->tipe_pesawat == 'bisnis') selected @endif value="bisnis">Bisnis</option>
		 </select>
	</div>
	@if($errors->has('tipe_kamar'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('tipe_pesawat') }}</small>
			</div>
   	@endif
   </div>

   <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Dokumen</label>
	<div class="col-sm-7 col-md-7">
		<span style="margin-right:10px;" ng-repeat="dokumen in vm.dokumen">
			<input type="checkbox" name="dokumen[]" ng-checked="@{{dokumen.checked}}"   value="@{{dokumen.id}}"> @{{dokumen.nama_dokumen}}
		</span>
		
	</div>
   	
 <div class="form-group">
 	<div class="col-sm-2 col-md-3 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>

 </span>

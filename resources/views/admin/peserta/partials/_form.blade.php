
 {!! csrf_field() !!}
  <div class="box-header with-border">
        <h3 class="box-title">No. Identitas</h3>
 </div>

 <div class="form-group">
	<label class="col-md-3 control-label">No. KTP</label>
	<div class="col-md-7">
	@if(@$online != true)
		<input type="text" name="ktp" value="{{ old('ktp') }}"  class="form-control">
	@else
		<input type="text" name="no_ktp" value="{{ old('no_ktp') }}"  class="form-control">
	@endif
	</div>
	@if($errors->has('ktp'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('ktp') }}</small>
		</div>
	@endif
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">No. Passpor</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" name="no_passpor" value="{{old('no_passpor')}}"  class="form-control" >
	</div>
	@if($errors->has('no_passpor'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('no_passpor') }}</small>
		</div>
	@endif
 </div>

<div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Tempat Pembuatan</label>
	<div class="col-sm-7 col-md-7">
	@if(@$online != true)
		<input type="text" name="tempat_pembuatan_passpor" value="{{old('tempat_pembuatan_passpor')}}" class="form-control" >
	@else
		<input type="text" name="tempat_pembuatan" value="{{old('tempat_pembuatan')}}" class="form-control" >
	@endif
	</div>
	@if($errors->has('tempat_pembuatan_passpor'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tempat_pembuatan_passpor') }}</small>
		</div>
	@endif
 </div>

 <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Tanggal Pembuatan</label>
	<div class="col-sm-7 col-md-7">
	@if(@$online != true)
		<input type="text" id="tgl_pembuatan_passpor"  data-date-format="yyyy-mm-dd" name="tgl_pembuatan_passpor" value="{{old('tgl_pembuatan_passpor')}}" class="form-control">
	@else 
		<input type="text" id="tgl_pembuatan"  data-date-format="yyyy-mm-dd" name="tgl_pembuatan" value="{{old('tgl_pembuatan')}}" class="form-control">
	@endif
	</div>
	@if($errors->has('tgl_pembuatan_passpor'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tgl_pembuatan_passpor') }}</small>
		</div>
	@endif
  </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Tanggal Expired</label>
	<div class="col-sm-7 col-md-7">
	@if(@$online != true)
	    <input type="text" id="tgl_expired_passpor" data-date-format="yyyy-mm-dd" name="tgl_expired_passpor" value="{{old('tgl_expired_passpor')}}" class="form-control">
	@else
		<input type="text" id="tgl_expired" data-date-format="yyyy-mm-dd" name="tgl_expired" value="{{old('tgl_expired')}}" class="form-control">
	@endif
	</div>
	@if($errors->has('tgl_expired_passpor'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tgl_expired_passpor') }}</small>
		</div>
	@endif
  </div>

  <div class="box-header with-border">
        <h3 class="box-title">Biodata</h3>
 </div>


 <div class="form-group">
	<label class="col-md-3 control-label">Nama (Sesuai di passpor)</label>
	<div class="col-md-7">
		<input type="text" name="nama_peserta" value="{{old('nama_peserta')}}"    class="form-control">
	</div>
	@if($errors->has('nama_peserta'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('nama_peserta') }}</small>
		</div>
	@endif
 </div>

 <div class="form-group">
	<label class="col-md-3 control-label">Tempat Lahir</label>
	<div class="col-md-7">
		<input type="text"  name="tempat_lahir" value="{{old('tempat_lahir')}}"  class="form-control">
	</div>
	@if($errors->has('tempat_lahir'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tempat_lahir') }}</small>
		</div>
	@endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Tanggal Lahir</label>
	<div class="col-md-7">
        <input type="text" name="tgl_lahir" value="{{old('tgl_lahir')}}" id="tgl_lahir" class="form-control" data-date-format="yyyy-mm-dd">
	</div>
	@if($errors->has('tgl_lahir'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('tgl_lahir') }}</small>
		</div>
	@endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Nama Ayah Kandung</label>
	<div class="col-md-7">
		<input type="text" name="nama_ayah" value="{{old('nama_ayah')}}"  class="form-control">
	</div>
	@if($errors->has('nama_ayah'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('nama_ayah') }}</small>
		</div>
	@endif
 </div>

   <div class="form-group">
	<label class="col-md-3 control-label">No. Telepon</label>
	<div class="col-md-7">
		<input type="text" name="no_telp" value="{{old('no_telp')}}"  class="form-control">
	</div>
	@if($errors->has('no_telp'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('no_telp') }}</small>
		</div>
	@endif
 </div>

    <div class="form-group">
	<label class="col-md-3 control-label">No. Hp</label>
	<div class="col-md-7">
		<input type="text" name="no_hp" value="{{old('no_hp')}}"  class="form-control">
	</div>
	@if($errors->has('no_hp'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('no_hp') }}</small>
		</div>
	@endif
 </div>


  <div class="form-group">
	<label class="col-md-3 control-label">Pekerjaan</label>
	<div class="col-md-7">
		 <select name="pekerjaan" class="form-control">
		 	<option value="">Pilih</option>
			<option value="PNS" @if(old('pekerjaan') == "PNS") selected  @endif>PNS</option>
			<option value="TNI/POLRI" @if(old('pekerjaan') == "TNI/POLRI") selected  @endif>TNI/POLRI</option>
			<option value="Dagang" @if(old('pekerjaan') == "Dagang") selected  @endif>Dagang</option>
			<option value="Tani/Nelayan" @if(old('pekerjaan') == "Tani/Nelayan") selected  @endif>Tani/Nelayan</option>
			<option value="Swasta" @if(old('pekerjaan') == "Swasta") selected  @endif>Swasta</option>
			<option value="Ibu Rumah Tangga" @if(old('pekerjaan') == "Ibu Rumah Tangga") selected  @endif>Ibu Rumah Tangga</option>
			<option value="Pelajar/Mahasiswa"  @if(old('pekerjaan') == "Pelajar/Mahasiswa") selected  @endif>Pelajar/Mahasiswa</option>
			<option value="BUMN/BUMD" @if(old('pekerjaan') == "BUMN/BUMD") selected  @endif>BUMN/BUMD</option>
			<option value="Lain-lain" @if(old('pekerjaan') == "Lain-lain") selected  @endif>Lain-lain</option>
		 </select>
	</div>
	@if($errors->has('pekerjaan'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('pekerjaan') }}</small>
		</div>
	@endif
 </div>

    <div class="form-group">
	<label class="col-md-3 control-label">Pendidikan</label>
	<div class="col-md-7">
		<select class="form-control" name="pendidikan">
			<option value="">Pilih</option>
			<option value="SD"  @if(old('pendidikan') == "SD") selected  @endif>SD</option>
			<option value="SLTP" @if(old('pendidikan') == "SLTP") selected  @endif>SLTP</option>
			<option value="SLTA" @if(old('pendidikan') == "SLTP") selected  @endif>SLTA</option>
			<option value="SM/D1/D2/D3" @if(old('pendidikan') == "SM/D1/D2/D3") selected  @endif>SM/D1/D2/D3</option>
			<option value="S1" @if(old('pendidikan') == "S1") selected  @endif>S1</option>
			<option value="S2" @if(old('pendidikan') == "S2") selected  @endif>S2</option>
			<option value="S3" @if(old('pendidikan') == "S3") selected  @endif>S3</option>
		</select>
	</div>
	@if($errors->has('pendidikan'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('pendidikan') }}</small>
		</div>
	@endif
 </div>


   <div class="form-group">
	<label class="col-md-3 control-label">Golongan Darah</label>
	<div class="col-md-7">
		<select class="form-control" name="golongan_darah">
			<option value="">Pilih</option>
			<option value="A" @if(old('golongan_darah') == "A") selected  @endif>A</option>
			<option value="B" @if(old('golongan_darah') == "B") selected  @endif>B</option>
			<option value="AB" @if(old('golongan_darah') == "AB") selected  @endif>AB</option>
			<option value="O" @if(old('golongan_darah') == "O") selected  @endif>O</option>
		</select>
	</div>
	@if($errors->has('golongan_darah'))
		<div class="col-md-offset-3 col-md-7">
			<small style="color:red">{{ $errors->first('golongan_darah') }}</small>
		</div>
	@endif
 </div>


	  <div class="form-group">
		<label class="col-md-3 control-label">Ukuran Baju/Kerudung</label>
		<div class="col-md-7">
			<select class="form-control" name="ukuran_baju_kerudung">
				<option value="">Pilih</option>
				<option value="M" @if(old('ukuran_baju_kerudung') == "M") selected  @endif>M</option>
				<option value="L" @if(old('ukuran_baju_kerudung') == "L") selected  @endif>L</option>
				<option value="XL" @if(old('ukuran_baju_kerudung') == "XL") selected  @endif>XL</option>
				<option value="XXL" @if(old('ukuran_baju_kerudung') == "XXL") selected  @endif>XXL</option>
				<option value="XXXL" @if(old('ukuran_baju_kerudung') == "XXXL") selected  @endif>XXXL</option>
			</select>
		</div>
		@if($errors->has('ukuran_baju_kerudung'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('ukuran_baju_kerudung') }}</small>
			</div>
    	@endif
	 </div>

	 <div class="form-group">
	<label class="col-md-3 control-label">Photo</label>
	<div class="col-md-7">
		 <input type="file" name="photo" class="form-control">
	</div>
		@if($errors->has('photo'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('photo') }}</small>
			</div>
    	@endif
    </div>


  <div class="form-group">
	<label class="col-md-3 control-label">Jenis Kelamin</label>
	<div class="col-md-7">
		<label style="width: 140px;"><input type="radio" class="radio-inline" name="jk" @if(old('jk') == "L") checked="true"  @endif value="L"> Laki - Laki</label>
		<label ><input type="radio" class="radio-inline" name="jk" @if(old('jk') == "P") checked="true"  @endif value="P"> Perempuan</label>
	</div>
		@if($errors->has('jk'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('jk') }}</small>
			</div>
    	@endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Kewarganegaraan</label>
	<div class="col-md-7">
		<label style="width: 140px;"><input type="radio" name="kewarganegaraan" value="WNI" @if(old('kewarganegaraan') == "WNI") checked="true"  @endif> WNI</label>
		<label><input type="radio"  name="kewarganegaraan" value="Asing" @if(old('kewarganegaraan') == "Asing") checked="true"  @endif> Asing</label>
	</div>
	@if($errors->has('kewarganegaraan'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kewarganegaraan') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Pergi Haji/Umroh</label>
	<div class="col-md-7">
		<label style="width: 140px;"><input @if(old('pernah_haji_umroh') == "1") checked="true"  @endif type="radio" name="pernah_haji_umroh" value="1"> Pernah</label>
		<label><input @if(old('pernah_haji_umroh') == "0") checked="true"  @endif type="radio" name="pernah_haji_umroh" value="0"> Belum Pernah</label>
	</div>
	@if($errors->has('pernah_haji_umroh'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('pernah_haji_umroh') }}</small>
			</div>
    @endif
 </div>

@if(@$online != true)
  <div class="form-group">
	<label class="col-md-3 control-label">Kepala Keluarga(Mahram)</label>
	<div class="col-md-7">
		<label style="width: 140px;">
			<input type="radio"  ng-model="vm.data.mahram" name="kepala_keluarga" value="1"> Ya
		</label>
		<label>
			<input type="radio" ng-model="vm.data.mahram"  name="kepala_keluarga" value="0"> Bukan</label>
	</div>
	@if($errors->has('kepala_keluarga'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kepala_keluarga') }}</small>
			</div>
    @endif
 </div>


 <span ng-if="vm.data.mahram == '0'">
 	 <div class="form-group">
	<label class="col-md-3 control-label">Nama Mahram</label>
	<div class="col-md-7">

		   <input type="text"  ng-model="vm.selected" typeahead='mahram.namaandprov for mahram in vm.searchMahram($viewValue)' typeahead-on-select="vm.selectMahram($item);"   placeholder="Cari Pendaftar" class="form-control pull-right"  >
		   <input type="hidden" value="" name="mahram_id" id="mahram_id">
	</div>
	@if($errors->has('mahram_id'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('mahram_id') }}</small>
			</div>
    @endif
 </div>

 <div class="form-group">
	<label class="col-md-3 control-label">Hubungan Mahram</label>
	<div class="col-md-7">
		<select name="hubungan_mahram" class="form-control">
			<option value="">Pilih</option>
			<option value="anak">Anak</option>
			<option value="istri">Istri</option>
			<option value="ibu">Ibu</option>
			<option value="woman group">Woman Group</option>
			<option value="ayah mertua">Ayah Mertua</option>
			<option value="saudara kandung">Saudara Kandung</option>
			<option value="saudara seayah">Saudara Seayah</option>
			<option value="saudara seibu">Saudara Seibu</option>
			<option value="paman">Paman</option>
			<option value="kakek saudara">Kakek Saudara</option>
			<option value="anak saudari">Anak Saudari</option>
			<option value="saudara laki-laki">Saudara laki-laki</option>
			<option value="kakak laki-laki">Kakak laki-laki</option>
			<option value="kakak perempuan">Kakak Perempuan</option>
			<option value="adik laki-laki">Adik laki-laki</option>
			<option value="adik perempuan">Adik Perempuan</option>

		</select>
	</div>
	@if($errors->has('hubungan_mahram'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('hubungan_mahram') }}</small>
			</div>
    @endif
 </div>
 </span>

 <span ng-if="vm.data.mahram == '1'">
 	<div class="form-group">
	<label class="col-md-3 control-label">Sebagai</label>
	<div class="col-md-7">
		<select name="hubungan_mahram" class="form-control">
			<option value="">Pilih</option>
			<option value="single">Single</option>
			<option value="ayah">Ayah</option>
			<option value="suami">Suami</option>
			<option value="ayah mertua">Ayah Mertua</option>
			<option value="paman">Paman</option>
			<option value="kakek">Kakek</option>
			<option value="saudara seayah">Saudara Seayah</option>
			<option value="saudara seibu">Saudara Seibu</option>
			<option value="anak saudari">Anak Saudari</option>
			<option value="saudara laki-laki">Saudara laki-laki</option>
			<option value="kakak laki-laki">Kakak laki-laki</option>
			<option value="adik laki-laki">Adik laki-laki</option>
		</select>
	</div>
	@if($errors->has('hubungan_mahram'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('hubungan_mahram') }}</small>
			</div>
    @endif
 </div>
 </span>
@endif

 <div class="box-header with-border">
        <h3 class="box-title">Alamat Lengkap</h3>
 </div>

 <div class="form-group">
	<label class="col-md-3 control-label">Provinsi</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" class="form-control" name="provinsi" value="{{old('provinsi')}}">
	 
	</div>
	@if($errors->has('provinsi'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('provinsi') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group"  >
	<label class="col-md-3 control-label">Kota/Kabupaten</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" class="form-control" name="kabupaten" value="{{old('kabupaten')}}">
		 
	</div>
	@if($errors->has('kabupaten'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kabupaten') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group"  >
	<label class="col-md-3 control-label">Kecamatan</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" class="form-control" name="kecamatan" value="{{old('kecamatan')}}">
		 
	</div>
	@if($errors->has('kecamatan'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kecamatan') }}</small>
			</div>
    @endif
 </div>

   <div class="form-group"  >
	<label class="col-md-3 control-label">Desa</label>
	<div class="col-sm-7 col-md-7">
	    <input type="text" name="desa" class="form-control" value="{{old('desa')}}">
	 
	</div>
	@if($errors->has('desa'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('desa') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group"  >
	<label class="col-md-3 control-label">Kode POS</label>
	<div class="col-sm-7 col-md-7">
		 <input type="text" name="kodepos" class="form-control" value="{{old('kodepos')}}">
	</div>
	@if($errors->has('kodepos'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('kodepos') }}</small>
			</div>
    @endif
 </div>

  <div class="form-group">
	<label class="col-md-3 control-label">Alamat</label>
	<div class="col-sm-7 col-md-7">
		 <textarea class="form-control" name="alamat" value="{{old('alamat')}}"></textarea>
	</div>
		@if($errors->has('alamat'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('alamat') }}</small>
			</div>
   		 @endif
 </div>

 <div class="box-header with-border">
        <h3 class="box-title">Program yang dipilih</h3>
 </div>

 	<div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Program yang dipilih</label>
	<div class="col-sm-7 col-md-7">
			<label style="width: 140px;"><input type="radio" ng-model="vm.data.programIsSelected" name="tipe_paket" value="umroh"> Umroh</label>
		<label><input type="radio" ng-model="vm.data.programIsSelected" name="tipe_paket" value="haji"> Haji</label>

	</div>
	@if($errors->has('tipe_paket'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('tipe_paket') }}</small>
			</div>
   	@endif
  </div>
 <span ng-if="vm.data.programIsSelected">

 	<div class="form-group">
	<label class="col-sm-2 col-md-3 control-label" ng-if="vm.data.programIsSelected == 'umroh'">Paket Umroh</label>
	<label class="col-sm-2 col-md-3 control-label" ng-if="vm.data.programIsSelected == 'haji'">Paket Haji</label>

	<div class="col-sm-7 col-md-7">
		 <select name="harga_paket_id"   id="harga_paket_id" class="form-control">
		 	<option value="">Pilih</option>
		 	<option ng-if="vm.data.programIsSelected == 'umroh'" ng-repeat="paket in vm.paket" value="@{{ paket.paket.id }},@{{ paket.tanggal_pemberangkatan }}" >@{{ paket.paket.nama_paket }} <i>(@{{ paket.tanggal_pemberangkatan }})</i> </option>

		 	<option ng-if="vm.data.programIsSelected == 'haji'" ng-repeat="paket in vm.paket" value="@{{ paket.paket_id }},@{{ paket.tahun }}" >@{{ paket.nama_paket }}</option>
		  
		 </select>
	</div>
	@if($errors->has('harga_paket_id'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('harga_paket_id') }}</small>
			</div>
   	@endif
  </div>

   <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Paket Hotel</label>
	<div class="col-sm-7 col-md-7">
		 <select name="paket_hotel" class="form-control">
		 	<option value="">Pilih</option>
		 	<option @if(old('paket_hotel') == "ekonomi") selected @endif value="ekonomi">Ekonomis</option>
		 	<option @if(old('paket_hotel') == "bisnis") selected @endif value="bisnis">Bisnis</option>
		 	<option @if(old('paket_hotel') == "eksekutif") selected @endif value="eksekutif">Eksekutif</option>
		 </select>
	</div>
	@if($errors->has('paket_hotel'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('paket_hotel') }}</small>
			</div>
   	@endif
  </div>

    <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Pilihan Kamar</label>
	<div class="col-sm-7 col-md-7">
		 <select name="tipe_kamar" class="form-control">
		 	<option value="">Pilih</option>
		 	<option value="double">Double</option>
		 	<option value="triple">Triple</option>
		 	<option value="quad">Quad</option>
		 	<option value="quint">Quint</option>
		 </select>
	</div>
	@if($errors->has('tipe_kamar'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('tipe_kamar') }}</small>
			</div>
   	@endif
  </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Pilihan Pesawat</label>
	<div class="col-sm-7 col-md-7">
		 <select name="pesawat_id" class="form-control">
		 	<option value="">Pilih</option>
		 	@foreach($pesawat as $key => $value)
		 		<option @if(old('pesawat_id') == $value->id) selected @endif value="{{$value->id}}">{{ucwords($value->nama_pesawat)}}</option>
		 	@endforeach 
		 </select>
	</div>
	@if($errors->has('tipe_kamar'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('pesawat_id') }}</small>
			</div>
   	@endif
   </div>
   
    <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Tipe Pesawat</label>
	<div class="col-sm-7 col-md-7">
		 <select name="tipe_pesawat" class="form-control">
		 	<option value="">Pilih</option>
		 	<option @if(old('tipe_pesawat') == "ekonomi") selected @endif  value="ekonomi">Ekonomi</option>
		 	<option @if(old('tipe_pesawat') == "bisnis") selected @endif value="bisnis">Bisnis</option>
		 </select>
	</div>
	@if($errors->has('tipe_kamar'))
			<div class="col-md-offset-3 col-md-7">
				<small style="color:red">{{ $errors->first('tipe_pesawat') }}</small>
			</div>
   	@endif
   </div>

@if(@$online != true)
    <div class="form-group">
	<label class="col-sm-2 col-md-3 control-label">Dokumen</label>
	<div class="col-sm-7 col-md-7">
			
		<span style="margin-right:10px;" ng-repeat="dokumen in vm.dokumen">
			<input type="checkbox" name="dokumen[]"   value="@{{dokumen.id}}"> @{{dokumen.nama_dokumen}}
		</span>
	</div>
   </div>
@endif



 <div class="form-group">
 	<div class="col-sm-2 col-md-3 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>

 </span>

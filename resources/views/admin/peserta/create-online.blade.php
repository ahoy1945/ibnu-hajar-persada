@extends('admin.layouts.pendaftaran-online')

@section('title', 'Pendaftaran Online')

@section('content')
    <form ng-controller="PendaftaranController as vm" enctype="multipart/form-data" action="{{route('admin.daftar.online.store')}}" method="POST"  class="col-md-12 form-horizontal">
     <div class="box box-info">
               
                <div class="box-body"> 
                    <center>
                      <img src="/dist/img/logo.png"  ><br>
                      <h2 class="box-title">Pendaftaran Online</h2>
                    </center>
                  @include('admin.alert.alert')
                  @include('admin.peserta.partials._form')		
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      var tgl_pembuatan_passpor = $("#tgl_pembuatan_passpor");
      var tgl_expired_passpor   = $("#tgl_expired_passpor");
      var tgl_lahir             = $("#tgl_lahir");
      
      tgl_pembuatan_passpor.datepicker().on("changeDate", function() {
        tgl_pembuatan_passpor.datepicker("hide");
      });

      tgl_expired_passpor.datepicker().on("changeDate", function(){
        tgl_expired_passpor.datepicker("hide");
      });

      tgl_lahir.datepicker().on("changeDate", function() {
        tgl_lahir.datepicker("hide");
      });
     
    });
  </script>
@stop
@extends('admin.layouts.default')

@section('title', 'Data Pendaftar Online')

@section('content')

	@include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Data Pendaftar Online (WEB)</h3>
                      &nbsp;
                      
                    </div>
                  
                  
                     <div class="col-md-4 ">
                      <form action="{{ route('admin.daftar.online.search') }}" method="GET">
                       <div class="input-group input-group-sm  ">
                           <input type="text" value="@if(isset($keyword)){{$keyword}}@endif" name="keyword" class="form-control">
                             
                            <span class="input-group-btn">
                            <button type="submit"  class="btn btn-info btn-flat" type="button">Search</button>
                           </span>
                      </form>

                      </div>
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                      
                        <th width="20" >#</th>
                        <th width="200">Nama Jamaah</th>
                        <th></th>
                        <th width="50">Tipe</th>
                        <th width="170">Pilihan Paket</th>
                        <th width="130">Tgl Berangkat</th>
                      
                        <th width="20">JK</th>
                        <th>No. HP</th>
                        <th>Agen</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 1; ?>
                      @foreach($pendaftar as $daftar)
                        <tr>
							<td>
								@if($page != "1")
							      {{ ($i++*$page)+1 }}
							  	@else
							      {{$i++*$page}}
							  	@endif
							</td>
							<td> {{ $daftar->nama_peserta }} </td>
							<td>
                            	<img src=" {{fileExist($daftar->photo)}}" width="120">
                          	</td> 
	                        <td>{{ ucwords($daftar->tipe_paket) }}</td>
							<td>
								@foreach($hargapaket as $harga)
									@if($harga->id === $daftar->harga_paket_id)
									    Paket:  <strong>{{ $harga->nama_paket }}  </strong>
									@endif
								@endforeach
								<br>
	                            Paket Hotel: <strong>{{ ucwords($daftar->paket_hotel) }}</strong><br>
	                            Tipe Kamar: <strong>{{ ucwords($daftar->tipe_kamar) }}</strong>
							</td>
							<td>{{ formatDate($daftar->tgl_berangkat) }}</td>
							<td>{{ $daftar->jenis_kelamin }}</td>
							<td>{{ $daftar->no_hp }}</td>
							<td>Online web</td>
							<td>
								<form method="POST" action="{{ route('admin.daftar.online.destroy', $daftar->id) }}">
									{!! csrf_field() !!}
									{!! method_field('DELETE') !!}
									<a href="{{route('admin.daftar.online.show', $daftar->id)}}" class="btn btn-primary"> <i class="fa fa-eye"></i> </a>
									<a class="btn btn-warning" href="{{route('admin.daftar.online.move', $daftar->id)}}"><i class="fa fa-sign-out"></i></a>
									<button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus  {{ $daftar->name }} ?');"><i class="fa fa-trash"></i></button>
								</form>
                          	</td>
                        </tr>
                      @endforeach
              
                    </tbody>
                  </table>
                   {!! $pendaftar->render() !!}
                  @include('admin.peserta.partials._show_pendaftar')
                </div><!-- /.box-body -->
              </div><!-- /.box -->

@stop
@extends('admin.layouts.default')

@section('title', 'Tambah Tabungan')

@section('content')
	<form action="{{route('admin.tabungan.update', $data->id)}}" ng-controller="TabunganController as vm" method="POST"  class="col-md-12 form-horizontal">
		 <div class="box box-info">
    
                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Tambah Tabungan</h3>
                  </div>
                  
                  @include('admin.alert.alert')
                  @include('admin.alert.form_errors')
                 
                  @include('admin.tabungan.partials._form-edit')		
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

 
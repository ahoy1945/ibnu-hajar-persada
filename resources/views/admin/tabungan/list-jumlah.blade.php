@extends('admin.layouts.default')

@section('title', 'Jumlah Tabungan')

@section('content')

  <div class="col-md-12">
      @include('admin.alert.alert')
      <div class="box box-info">
         <div class="box-header">
             <div class="col-md-4">
               <h3 class="box-title">Jumlah Tabungan</h3>
               &nbsp;
               <a class="text-center" href="{{route('admin.tabungan.create')}}">
                   <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                 </a>
             </div>


              <div class="col-md-4 col-md-push-4">
               <form action="#" method="GET">
                <div class="input-group input-group-sm">

               </form>

               </div>


           </div>

           <div class="box-body">
             <table class="table table-bordered table-hover">
               <thead>
                 <tr>
                   <th>#</th>
                   <th>Nama</th>
                   <th>Jumlah</th>
                
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                  <?php $i = 1 ?>
                 @foreach($tabunganJumlah as $data)
                   <tr>
                     <td>
                          @if($page != "1")
                              {{ ($page*10+$i)-10 }}
                              <?php $i++; ?>
                          @else
                              {{$i++*$page}}
                          @endif
                     </td>
                     <td>{{$data['tabungan']['nama']}}</td>
                     <td> {{ number_format($data['jumlah'],0,",",".") }}</td>
                     <td>
                         <form method="POST" action="{{route('admin.tabungan.destroy', $data['tabungan']['id'])}}">
                           {!! csrf_field() !!}
                           {!! method_field('DELETE') !!}
                        
                         <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus  ?');"><i class="fa fa-trash"></i></button>
                         </form>
                     </td>
                   </tr>
                  @endforeach
               </tbody>
             </table>
                {!!$tabunganJumlah->render()!!}

           </div><!-- /.box-body -->

  </div>

@stop

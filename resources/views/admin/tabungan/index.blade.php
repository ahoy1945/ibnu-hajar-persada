@extends('admin.layouts.default')

@section('title', 'Dokumen')

@section('content')

  <div class="col-md-12">
      @include('admin.alert.alert')
      <div class="box box-info">
         <div class="box-header">
             <div class="col-md-4">
               <h3 class="box-title">Tabungan</h3>
               &nbsp;
               <a class="text-center" href="{{route('admin.tabungan.create')}}">
                   <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                 </a>
             </div>


              <div class="col-md-4 col-md-push-4">
               <form action="#" method="GET">
                <div class="input-group input-group-sm">

               </form>

               </div>


           </div>

           <div class="box-body">
             <table class="table table-bordered table-hover">
               <thead>
                 <tr>
                   <th>#</th>
                   <th>Nama</th>
                   <th>Tipe</th>
                   <th>Jumlah</th>
                   <th>Tanggal</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                  <?php $i = 1 ?>
                 @foreach($tabunganSaldo as $data)
                   <tr>
                     <td>
                          @if($page != "1")
                              {{ ($page*10+$i)-10 }}
                              <?php $i++; ?>
                          @else
                              {{$i++*$page}}
                          @endif
                     </td>
                     <td>{{$data->tabungan->nama}}</td>
                     <td>{{$data->tipe}}</td>
                     <td>{{$data->jumlah}}</td>
                     <td>{{formatDate($data->created_at)}}</td>
                     <td>
                         <form method="POST" action="{{route('admin.tabungan.destroy', $data->id)}}">
                           {!! csrf_field() !!}
                           {!! method_field('DELETE') !!}
                         <a href="{{route('admin.tabungan.edit', $data->id)}}" class="btn btn-primary"> <i class="fa fa-edit"></i></a>
                         <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus  ?');"><i class="fa fa-trash"></i></button>
                         </form>
                     </td>
                   </tr>
                  @endforeach
               </tbody>
             </table>
                {!! $tabunganSaldo->render() !!}

           </div><!-- /.box-body -->

  </div>

@stop


 {!! csrf_field() !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Atas Nama</label>
	<div class="col-md-7">
		  <input type="text" readonly value="{{$data->tabungan->nama}}" class="form-control">
		  <input type="hidden"  name="tabungan_id" value="{{$data->tabungan_id}}" id="tabungan_id">
	</div>
 </div>
 {{ method_field('PATCH') }}
 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Tipe</label>
	<div class="col-sm-7 col-md-7">
		 <select name="tipe" class="form-control">
		 	<option value="debit" @if($data->tipe == 'debit') selected @endif>Debit</option>
		 	<option value="kredit" @if($data->tipe == 'debit') selected @endif>Kredit</option>
		 </select>
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Jumlah</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" name="jumlah" value="{{$data->jumlah}}" class="form-control"> 
	</div>
 </div>

 

 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
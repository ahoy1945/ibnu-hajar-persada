
 {!! csrf_field() !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Atas Nama</label>
	<div class="col-md-7">
		 <input type="text" autocomplete="off" name="nama"  ng-model="vm.nama" typeahead='nama.nama for nama in vm.search($viewValue)' typeahead-on-select="vm.select($item);"   placeholder="Cari Pendaftar" class="form-control pull-right"  >
		  <input type="hidden" value="" name="tabungan_id" id="tabungan_id">
	</div>
 </div>

 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Tipe</label>
	<div class="col-sm-7 col-md-7">
		 <select name="tipe" class="form-control">
		 	<option value="debit">Debit</option>
		 	<option value="kredit">Kredit</option>
		 </select>
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Jumlah</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" name="jumlah" class="form-control"> 
	</div>
 </div>

 

 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
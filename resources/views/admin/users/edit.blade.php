@extends('admin.layouts.default')

@section('title', 'Edit User')

@section('content')
	
	<form action="{{route('admin.users.update', $user->id)}}" method="POST" enctype="multipart/form-data" class="col-md-12 form-horizontal"  >
		 <div class="box box-info">
    
                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                  </div>
                  
                  @include('admin.alert.form_errors')
                  @include('admin.users.partials.form-update')		
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

@section('script')
    
    

@stop
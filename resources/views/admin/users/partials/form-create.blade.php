
 {!! csrf_field() !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Nama</label>
	<div class="col-md-7">
		<input type="text" name="name"  class="form-control" id="inputEmail3" placeholder="John Doe">
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Email</label>
	<div class="col-sm-7 col-md-7">
		<input type="email" name="email"  class="form-control" id="inputEmail3" placeholder="johndoe@gmail.com">
	</div>
 </div>

<div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Password</label>
	<div class="col-sm-7 col-md-7">
		<input type="password" name="password"  class="form-control" id="inputEmail3" placeholder="******">
	</div>
 </div>


  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Role</label>
	<div class="col-sm-7 col-md-7">
		<select name="role_id" class="form-control">
			<option value="">Pilih</option>
			@foreach($roles as $role)
				<option value="{{ $role->id }}">{{ ucwords($role->nama) }}</option>
			@endforeach
		</select>	
	</div>
  </div>


 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
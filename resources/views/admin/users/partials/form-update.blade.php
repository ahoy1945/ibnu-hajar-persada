
 {!! csrf_field() !!}
 {!! method_field('PATCH') !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Nama</label>
	<div class="col-md-7">
		<input type="text" name="name" value="{{ $user->name }}" class="form-control" id="inputEmail3" placeholder="John Doe">
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Email</label>
	<div class="col-sm-7 col-md-7">
		<input type="email" name="email" value="{{ $user->email }}" class="form-control" id="inputEmail3" placeholder="johndoe@gmail.com">
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label" autocomplete="off">Password (Kosongkan, jika tidak ingin diganti)</label>
	<div class="col-sm-7 col-md-7">
		<input type="password" name="password" value="" class="form-control" id="inputEmail3">
	</div>
 </div>

 

 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
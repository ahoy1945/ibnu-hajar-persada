@extends('admin.layouts.default')

@section('title', 'User')

@section('content')
	<div class="col-md-12">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Data User</h3>
                      &nbsp;
                      <a class="text-center" href="{{ route('admin.users.create') }}">
                          <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                        </a>
                    </div>
                  
                  
                     <div class="col-md-4 col-md-push-4">
                      <form action="#" method="GET">
                       <div class="input-group input-group-sm">

                      </form>

                      </div>
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                      
                  
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                       
						            <th></th>
                      </tr>
                    </thead>
                    <tbody>
                   
					          @foreach($users as $user)
                         <tr>
                            
                           <td>{{ $user->name }}</td>
                           <td>{{ $user->email }}</td>
                           <td>{{ $user->roles->nama }}</td>
                          
                           <td>
                              <form method="POST" action="{{ route('admin.users.destroy', $user->id) }}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                              <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>
                              <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus staff {{ $user->name }} ?');"><i class="fa fa-trash"></i></button>
                              </form>
                           </td>
                         </tr> 
                        
                    @endforeach  

                     
                    </tbody>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
@stop
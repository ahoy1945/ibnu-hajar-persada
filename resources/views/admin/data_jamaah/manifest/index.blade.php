@extends('admin.layouts.default')

@section('title', 'Manifest')

@section('content')
  <div class="col-md-12" ng-controller="PendaftaranController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-2">
                      <h3 class="box-title">Manifest</h3>
                      &nbsp;
                    </div>

                    <div class="col-md-8 ">
                      <form class="form-inline" form action="{{ route('admin.data_jamaah.manifest.index') }}" method="GET">
                        <div class="form-group">
                          <select class="form-control" name="tipe">
                            <option value="">Pilih</option>
                            <option value="haji">Haji</option>
                            <option value="umroh">Umroh</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" name="date">
                            <option value="">Pilih</option>
                            @foreach($date as $date)
                              <option value="{{ $date->tanggal_pemberangkatan }}">{{ formatDate($date->tanggal_pemberangkatan) }}</option>
                            @endforeach
                          </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                      </form>

                     </div>

                     <div class="col-md-2 ">
                       <form method="POST" action="{{ route('admin.data_jamaah.manifest.export') }}">
                         {{ csrf_field() }}
                         <input type="hidden" name="excel" value="excel" >
                         <input type="hidden" name="date" value="{{ $query['date'] }}" >
                         <input type="hidden" name="tipe"  value="{{ $query['tipe'] }}">
                        <span class="input-group-btn pull-left">
                              <button type="submit" class="btn btn-warning">

                                <i class="fa fa-file-excel-o"></i>

                              </button>

                         </form>
                     </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <!-- <th>NO</th> -->
                        <th>Name</th>
                        <th>Place of birth</th>
                        <th>Date of birth</th>
                        <th>Age</th>
                        <th>Sex</th>
                        <th>Passport</th>
                        <th>Address</th>
                        <th>Status</th>
                        <th>Plane</th>
                        <th>Hotel</th>
                        <th>Type</th>
                        <th>Berangkat</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 1; ?>
                      @foreach($manifest as $key => $data)
                        <?php $hotel = ['ekonomis','bisnis','eksekutif'];
                        $bintang = array_search($data['paket_hotel'], $hotel) + 3; 
                        $jenis_kelamin = ($data['jenis_kelamin'] == 'L') ? 'M' : 'F';?>
                              <tr>
                                <!-- <td>{{ $i++ }}</td> -->
                                <td>{{ $data['nama_peserta'] }}</td>
                                <td>{{ $data['tempat_lahir'] }}</td>
                                <td>{{ formatDate($data['tgl_lahir']) }}</td>
                                <td>{{ age($data['tgl_lahir']) }}</td>
                                <td>{{ $jenis_kelamin }}</td>
                                <td>
                                  <b>No : </b> {{ $data['no_passpor'] }} <br>
                                  <b>Issue : </b> {{ formatDate($data['tgl_pembuatan']) }} <br>
                                  <b>Exp : </b>{{ formatDate($data['tgl_expired']) }}<br>
                                  <b>Iss Office : </b>{{ $data['tempat_pembuatan'] }}
                                </td>
                              
                                <td>
                                  <b>Province :</b>{{ $data['provinsi'] }} <br>
                                  <b>District :</b>{{ $data['kabupaten'] }} <br>
                                  <b>Postal Code :</b>{{ $data['kodepos'] }} <br>
                                </td>
                                <td>{{ $data['hubungan_mahram'] }}</td>
                                <td>{{ $data['tipe_pesawat'] }}</td>
                                <td>{{ $bintang }}</td>
                                <td>
                                  {{ $data['tipe_paket'] }}<br>
                                  No. Porsi: {{ $data['no_porsi'] or '-' }}
                                </td>
                                <td>{{ formatDate($data['tgl_berangkat']) }}</td>
                                <td>
                                   <form method="POST" action="{{ route('admin.data_jamaah.manifest.destroy', $data['id']) }}">
                                      {!! csrf_field() !!}
                                      {!! method_field('DELETE') !!}
                                    {{-- <a href="{{ route('admin.data_jamaah.manifest.show', $data['id']) }}" class="btn btn-warning"> <i class="fa fa-eye"></i> </a> --}}
                                    <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus {{ $data['nama_peserta'] }}?');"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                              </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {!! $manifest->render() !!}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
@stop

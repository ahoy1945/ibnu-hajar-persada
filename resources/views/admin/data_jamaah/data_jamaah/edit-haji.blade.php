@extends('admin.layouts.default')

@section('title', 'Edit Pembayaran ')

@section('content')
	
	<form action="{{route('admin.data-jamaah.update', $jamaah->id)}}" enctype="multipart/form-data" method="POST"  class="col-md-12 form-horizontal" >
		 <div class="box box-info">

                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Edit Data {{$jamaah->nama_peserta}}</h3>
                  </div>
                  @include('admin.data_jamaah.data_jamaah.partials.form-edit-haji')		
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

@section('script')
  <script type="text/javascript">
      $(document).ready(function() {
        var tgl_bayar = $('#tgl_bayar');
      
        tgl_bayar.datepicker();
      });
  </script>
    

@stop
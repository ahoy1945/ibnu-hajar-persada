@extends('admin.layouts.default')

@section('title', 'Data Jamaah Haji')
 
@section('content')
  <div class="col-md-12" ng-controller="PendaftaranController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                     
                      
                    </div>
                  
                  
                       <div class="col-md-6 col-md-pull-1">
                      <form action="{{ route('admin.data-jamaah.index') }}" method="GET">
                       <div class="input-group input-group-sm">
                          
                            <select name="tgl_berangkat" class="form-control">
                              <option >Tahun Daftar</option>
                              @foreach($listYears as $year)
                                <option value="{{$year}}">{{$year}}</option>
                              @endforeach
                           </select>


                           
                          <span class="input-group-btn">
                             
                          </span>
                           <input type="text"  name="keyword" class="form-control" placeholder="Nama Jamaah">

                           <input type="hidden" name="tipe" value="{{$parameters['tipe']}}">
                           
                          <span class="input-group-btn">
                             
                          </span>

                          <span class="input-group-btn">
                             
                          </span>

                           
                          <span class="input-group-btn">
                            <button type="submit"   class="btn btn-info btn-flat" type="button">Search</button>
                          </span>

                         
                      </form>
                      
                      </div>

                      
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th></th>
                        <th>No Porsi</th>
                        <th>Nama</th>
                        <th>Tahun Daftar</th>
                        <th>Paket</th>
                        <th>Hub</th>
                        <th>Asal</th>
                        <th>Action</th>
                      </tr>
                     
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($dataJamaah as $jamaah)
                         <tr>
                          <td>
                              @if($parameters['page'] != "1")
                              {{ ($i++*$parameters['page'])+1 }}
                              @else
                                  {{$i++*$parameters['page']}}
                              @endif
                          </td>
                          <td><img width="120" src="{{fileExist($jamaah['photo'])}}"></td> 
                          <td>
                            @if($jamaah['no_porsi'])
                              {{$jamaah['no_porsi']}}
                            @else
                              -
                            @endif
                          </td>
                          <td>
                            {{$jamaah['nama_peserta']}}
                          </td>
                          <td>{{substr($jamaah['tgl_daftar'], 0, 4)}}</td>
                          <td>{{$jamaah['nama_paket']}}</td>
                           @if($jamaah['mahram'])
                             <td style="vertical-align:middle;text-align:center;" rowspan="{{$jamaah['td']}}">
                            @if($jamaah['td'] > 1) 
                              Kel
                            @else
                              -
                            @endif
                          </td>
                          @endif
                          <td>{{ucwords($jamaah['alamat'])}}</td>
                          <td>
                             <a href="" ng-click="vm.showDetail({{ $jamaah['id'] }})" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                             <a href="{{route('admin.data-jamaah.edit', $jamaah['id'])}}" class="btn btn-warning "><i class="fa fa-pencil"></i></a>
                          </td>
                        </tr>
                     
                        @endforeach
                    </tbody>
                  </table>
                   
                     @include('admin.peserta.partials._show_pendaftar') 
                    
                </div><!-- /.box-body -->
              </div><!-- /.box -->
       @include("admin.pembayaran.partials.show")           
   </div>
    
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $("#tanggal").datepicker({
        format: " yyyy", // Notice the Extra space at the beginning
        viewMode: "years", 
        minViewMode: "years"
      });
    });
  </script> 
@stop 

@section('style')
  <style>
    th {
      text-align: center;
      vertical-align: middle !important;
      white-space: nowrap !important;
    }
    .table-bordered>tbody>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #95a5a6 !important;
    }
  </style>
@stop
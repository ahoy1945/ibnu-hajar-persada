
 {!! csrf_field() !!}
 {!! method_field('PATCH') !!}

 <div class="form-group">
	<label class="col-md-2 control-label">No Porsi</label>
	<div class="col-md-7">
		<input type="text" name="no_porsi"  value="{{ $jamaah->no_porsi }}"  class="form-control">
	</div>

	 
 </div>

 <div class="form-group">
	<label class="col-md-2 control-label">Attach(Kosongkan jika tidak diganti)</label>
	<div class="col-md-7">
		<input type="file" name="attach_file" class="form-control">
	</div>
	 
 </div>

 

 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-4">
 		<div class="col-md-6">
 			<button type="submit" class="btn btn-primary">Simpan</button>
 		</div>
 	</div>
 </div>
                  
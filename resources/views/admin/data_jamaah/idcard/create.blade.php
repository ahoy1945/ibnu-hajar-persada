@extends('admin.layouts.default')

@section('title', 'Cetak')
 
@section('content')
  <div class="col-md-12" ng-controller="IdCardController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                     
                      
                    </div>
                  
                        
                       <div class="col-md-6 col-md-pull-1">
                      <form action="{{ route('admin.print-idcard.create') }}" method="GET">
                       <div class="input-group input-group-sm">
                          <select name="tipe_paket" class="form-control" ng-model="vm.input.tipe_paket">
                              <option value="umroh">Umroh</option>
                              <option value="haji">Haji</option>
                          </select>
                          
                           <span class="input-group-btn">
                             
                          </span>
                          
                          <select ng-if="vm.input.tipe_paket == 'umroh'" name="tgl_berangkat" class="form-control">
                            @foreach($tglPemberangkatan as $key => $value)
                                <option @if(isset($parameters['tgl_berangkat']) && $parameters['tgl_berangkat'] == $value) selected @endif value="{{$value}}">{{formatDate($value)}}</option>
                            @endforeach
                          </select>

                           <select ng-if="vm.input.tipe_paket == 'haji'" name="tgl_berangkat" class="form-control">
                            @foreach($rangeYears as $key => $year)
                                <option @if(isset($parameters['tgl_berangkat']) && $parameters['tgl_berangkat'] == $year) selected @endif value="{{$year}}">{{$year}}</option>
                            @endforeach
                          </select>

                          


                           
                          <span class="input-group-btn">
                             
                          </span>
                           <input type="text"  name="keyword" class="form-control" placeholder="Nama Jamaah">

                           
                          <span class="input-group-btn">
                             
                          </span>

                          <span class="input-group-btn">
                             
                          </span>

                           
                          <span class="input-group-btn">
                            <button type="submit"   class="btn btn-info btn-flat" type="button">Search</button>
                          </span>

                         
                      </form>
                      
                      </div>

                      
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Tipe</th>
                        <th>Tgl Berangkat</th>
                        <th>Asal</th>
                        <th width="150">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @if(!empty($jamaah))
                        @foreach($jamaah as $key => $value)
                          <tr>
                            <td>{{$value->nama_peserta}}</td>
                            <td>{{$value->tipe_paket}}</td>
                            <td>{{formatDate($value->tgl_berangkat)}}</td>
                            <td>@if(isset($value->alamat->provinsi))
                                  {{$value->alamat->provinsi}}
                                @else
                                  -
                                @endif
                              </td>
                            <td>
                                 <form style="float:left;"   class="form-horizontal" method="GET" action="{{route('admin.print-idcard.print')}}">
                              <input type="hidden" name="tipe_paket" value="@if(isset($tipe_paket)){{$tipe_paket}}@endif">
                              <input type="hidden" name="tgl_berangkat" value="@if(isset($tgl_berangkat)){{$tgl_berangkat}}@endif">
                              <input type="hidden" name="keyword" value="@if(isset($keyword)){{$keyword}}@endif">
                              <input type="hidden" value="idcard" name="tipe_cetak">
                              <input type="hidden" value="{{$value->peserta_id_peserta}}" name="id_peserta">
                              <button class="btn btn-primary">IDCard</button>
                            </form>
                            <form style="float:right;" class="form-horizontal" method="GET" action="{{route('admin.print-idcard.print')}}">
                              <input type="hidden" name="tipe_paket" value="@if(isset($tipe_paket)){{$tipe_paket}}@endif">
                              <input type="hidden" name="tgl_berangkat" value="@if(isset($tgl_berangkat)){{$tgl_berangkat}}@endif">
                              <input type="hidden" name="keyword" value="@if(isset($keyword)){{$keyword}}@endif">
                              <input type="hidden" value="koper" name="tipe_cetak">
                               <input type="hidden" value="{{$value->peserta_id_peserta}}" name="id_peserta">
                              <button class="btn btn-primary">Koper</button>
                            </form>
                            </td>
                          </tr>
                       @endforeach
                        <tr>
                          <td colspan="5" style="text-align:center;">
                            <div  style="width:300px;background:red;margin:0 auto;">
                               <form style="float:left;" class="form-horizontal" method="GET" action="{{route('admin.print-idcard.print')}}">
                              <input type="hidden" name="tipe_paket" value="@if(isset($tipe_paket)){{$tipe_paket}}@endif">
                              <input type="hidden" name="tgl_berangkat" value="@if(isset($tgl_berangkat)){{$tgl_berangkat}}@endif">
                              <input type="hidden" name="keyword" value="@if(isset($keyword)){{$keyword}}@endif">
                              <input type="hidden" value="idcard" name="tipe_cetak">
                              <button class="btn btn-primary">Cetak Semua IDCard</button>
                            </form>
                             <form style="float:right;" class="form-horizontal" method="GET" action="{{route('admin.print-idcard.print')}}">
                              <input type="hidden" name="tipe_paket" value="@if(isset($tipe_paket)){{$tipe_paket}}@endif">
                              <input type="hidden" name="tgl_berangkat" value="@if(isset($tgl_berangkat)){{$tgl_berangkat}}@endif">
                              <input type="hidden" name="keyword" value="@if(isset($keyword)){{$keyword}}@endif">
                              <input type="hidden" value="koper" name="tipe_cetak">
                              <button class="btn btn-primary">Cetak Semua Koper</button>
                            </form>
                            </div>
                          </td>
                        </tr>
                        @endif
                    </tbody>
                  </table>
                   @if(!empty($jamaah))
                     {!! $jamaah->appends(['tipe_paket' => $parameters['tipe_paket'], 'keyword' => $parameters['keyword'], 'tgl_berangkat' => $parameters['tgl_berangkat']])->render() !!} 
                   @endif                     
                </div><!-- /.box-body -->
              </div><!-- /.box -->           
   </div>
    
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $("#tanggal").datepicker();
    });

    
  </script> 
@stop 

@section('style')
  <style>
    th {
      text-align: center;
      vertical-align: middle !important;
      white-space: nowrap !important;
    }
    .table-bordered>tbody>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #95a5a6 !important;
    }
  </style>
@stop
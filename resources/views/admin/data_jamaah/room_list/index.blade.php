@extends('admin.layouts.default')

@section('title', 'Room List & Bus')

@section('content')
<style type="text/css">
  .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid rgba(66, 56, 56, 0.78) !important;
  }
</style>

  <div class="col-md-12">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-2">
                      <h3 class="box-title">Room List & Bus</h3>
                      &nbsp;

                    </div>


                    <div class="col-md-8 ">
                      <form class="form-inline" form action="{{ route('admin.data_jamaah.room_list.index') }}" method="GET">
                        <div class="form-group">
                          <select class="form-control" name="tipe">
                            <option value="Umroh">Umroh</option>
                            <option value="Haji">Haji</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" name="date">
                            @foreach($date as $value)
                              <option value="{{ $value->tanggal_pemberangkatan }}">{{ formatDate($value->tanggal_pemberangkatan) }}</option>
                            @endforeach
                          </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                      </form>

                     </div>


                     <div class="col-md-2 ">
                        <button class="excel btn btn-warning">
                          <i class="fa fa-file-excel-o"></i>
                        </button>
                        {{-- <form method="POST" action="{{ route('admin.data_jamaah.room_list.print') }}">
                         {{ csrf_field() }}
                         <input type="hidden" name="date" value="{{ $query['date'] }}" >
                         <input type="hidden" name="tipe"  value="{{ $query['tipe'] }}">
                        <span class="input-group-btn pull-left">
                              <button type="submit" class="btn btn-warning">

                                <i class="fa fa-file-pdf-o"></i>

                              </button>

                         </form> --}}

                       {{--  <a href="{{ route('admin.data_jamaah.room_list.print') }}" class="btn btn-warning">
                          <i class="fa fa-file-pdf-o"></i>
                        </a> --}}
                     </div>

                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>

                        <th width="20">No</th>
                        <th >Nama</th>
                        <th >Age</th>
                        <th >JK</th>
                        <th >Tgl Berangkat</th>
                        <th >Tipe Paket</th>
                        <th >Bus</th>
                        <th >Room</th>
                        <th width="40">Option</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1; ?>
                      @foreach($roomList as $value)
                        @foreach($value as $key)
                          <?php $singleRow = count($key);?>
                          @foreach($key as $room)
                          <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $room['nama_peserta'] }}</td>
                            <td>{{ age($room['tgl_lahir']) }}</td>
                            <td>{{ $room['jenis_kelamin'] }}</td>
                            <td>{{ formatDate($room['tgl_berangkat']) }}</td>
                            <td>{{ $room['tipe_paket'] }}</td>
                            <td>{{ $room['bus'] }}</td>
                            @if (isset($room['row_span'])) 
                              <td  style=" vertical-align: middle;" rowspan="{{  $room['row_span'] }}">{{ $room['tipe_kamar'] }}</td>
                            @elseif($singleRow == 1)
                              <td rowspan="1">{{ $room['tipe_kamar'] }}</td>
                            @endif
                            <td>
                                <a href="{{ route('admin.data_jamaah.room_list.edit', $room['id']) }}" class="btn btn-primary"> <i class="fa fa-edit"></i> </a>
                            </td>
                          </tr>
                          @endforeach
                        @endforeach
                      @endforeach
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
@stop

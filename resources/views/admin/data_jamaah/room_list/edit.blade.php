@extends('admin.layouts.default')

@section('title', 'Edit Room & Bus')

@section('content')

<form action="{{ route('admin.data_jamaah.room_list.update', $data[0]['id']) }}" method="POST"  class="col-md-12 form-horizontal">
   <div class="box box-info">

          <div class="box-body">
             <div class="box-header with-border">
              <h3 class="box-title">Edit Room/Bus</h3>
            </div>
                  @include('admin.alert.form_errors')
                  {!! csrf_field() !!}
                  {{method_field('PATCH')}}
                  <div class="form-group">
                   <label class="col-md-2 control-label">Nama Peserta</label>
                   <div class="col-md-7">
                     <input type="text" name="nama_peserta" value="{{ $data[0]['nama_peserta'] }}" class="form-control" disabled>
                   </div>
                  </div>

                  <div class="form-group">
                   <label class="col-md-2 control-label">Tanggal Lahir</label>
                   <div class="col-md-7">
                     <input type="text" name="tgl_lahir" value="{{ formatDate($data[0]['tgl_lahir']) }}" class="form-control" disabled>
                   </div>
                  </div>

                   <div class="form-group">
                   <label class="col-md-2 control-label">Tipe Paket</label>
                   <div class="col-md-7">
                     <input type="text" name="tipe_paket" value="{{ $data[0]['tipe_paket'] }}" class="form-control" disabled>
                   </div>
                  </div>

                  <div class="form-group">
                   <label class="col-md-2 control-label">Bus</label>
                   <div class="col-md-7">
                      <select  name='bus' class="form-control">
                        <option value="">Pilih</option>
                        <option value="bus 1"
                        @if($data[0]['bus'] == "bus 1") selected @endif>Bus 1</option>
                        <option value="bus 2"
                        @if($data[0]['bus'] == "bus 2") selected @endif>Bus 2</option>
                        <option value="bus 3"
                        @if($data[0]['bus'] == "bus 3") selected @endif>Bus 3</option>
                        <option value="bus 4"
                        @if($data[0]['bus'] == "bus 4") selected @endif>Bus 4</option>
                        <option value="bus 5"
                        @if($data[0]['bus'] == "bus 5") selected @endif>Bus 5</option>                        
                        <option value="bus 6"
                        @if($data[0]['bus'] == "bus 6") selected @endif>Bus 6</option>
                        <option value="bus 7"
                        @if($data[0]['bus'] == "bus 7") selected @endif>Bus 7</option>
                        <option value="bus 8"
                        @if($data[0]['bus'] == "bus 8") selected @endif>Bus 8</option>
                        <option value="bus 9"
                        @if($data[0]['bus'] == "bus 9") selected @endif>Bus 9</option>
                      </select>
                   </div>
                  </div> 

                  <div class="form-group">
                   <label class="col-md-2 control-label">Jenis Room</label>
                   <div class="col-md-7">
                      <select  name='tipe_kamar' class="form-control">
                        <option value="double"
                        @if($data[0]['tipe_kamar'] == "double") selected @endif>Double</option>
                        <option value="triple"
                        @if($data[0]['tipe_kamar'] == "triple") selected @endif>Triple</option>
                        <option value="quad"
                        @if($data[0]['tipe_kamar'] == "quad") selected @endif>Quad</option>                        
                        <option value="quint"
                        @if($data[0]['tipe_kamar'] == "quint") selected @endif>Quint</option>
                      </select>
                   </div>
                  </div> 

                  <div class="form-group">
                    <div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>

          </div><!-- /.box-body -->

          </div><!-- /.box-body -->
        </div><!-- /.box -->
</form>

@stop

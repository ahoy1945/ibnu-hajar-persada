 <!DOCTYPE html>
<html>
<head>
  <title>Print Setting Room</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" href="/css/AdminLTE.min.css">
  <style type="text/css">
  * {
    box-sizing: border-box;
  }
   body {
    width: 100%;
    font-family: arial;
   }
   .container {
    width: 100%;
    margin: 0 auto;
    
   }

   .container .header {
    text-align: left;
   }

   .container .header img {
    width: 300px;
   }

   .data-list td, .data-list th {
    border: 1px solid black;
   }
   th {
    border-right: 1px solid black;
   }

   tbody tr:last-child td {
     
   }
   th:last-child, td:last-child {
    
   }

   

   table.data-list  {
     
    
    text-align: left;
    border-spacing: 0px;
    border: 1px solid black;
   }

   table.tanda-tangan {
    text-align: center;
   }
  </style>
<body>
<div class="container">
    <div class="header">
      <h2>Room List {{ $dateTime['tipe'] }} {{ formatDate($dateTime['date']) }}<br>PT. Ibnu Hajar Persada</h2>   
       
    </div>
    <div class="body-table">
     <table border="0"  class="data-list" align="center" width="530">
      <thead>
        <tr>

          <th width="20">No</th>
          <th >Nama</th>
          <th width="25">Age</th>
          <th width="25">JK</th>
          <th >Tgl Berangkat</th>
          <th >Tipe Paket</th>
          <th >Bus</th>
          <th >Room</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=1; ?>
        @foreach($data as $value)
          @foreach($value as $key)
            <?php $singleRow = count($key);?>
            @foreach($key as $room)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $room['nama_peserta'] }}</td>
              <td>{{ age($room['tgl_lahir']) }}</td>
              <td>{{ $room['jenis_kelamin'] }}</td>
              <td>{{ formatDate($room['tgl_berangkat']) }}</td>
              <td>{{ $room['tipe_paket'] }}</td>
              <td>{{ $room['bus'] }}</td>
              @if (isset($room['row_span'])) 
                <td  style=" vertical-align: middle;" rowspan="{{  $room['row_span'] }}">{{ $room['tipe_kamar'] }}</td>
              @elseif($singleRow == 1)
                <td rowspan="1">{{ $room['tipe_kamar'] }}</td>
              @endif
            </tr>
            @endforeach
          @endforeach
        @endforeach
      </tbody>
    </table>
    </div>
</div>
</body>
</html>

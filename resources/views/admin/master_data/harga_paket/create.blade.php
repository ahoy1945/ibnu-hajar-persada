@extends('admin.layouts.default')

@section('title', 'Peserta')

@section('content')

<form action="/harga_paket" method="POST"  class="col-md-12 form-horizontal">
   <div class="box box-info">

          <div class="box-body">
             <div class="box-header with-border">
              <h3 class="box-title">MASTER DATA - Harga Paket</h3>
            </div>
                  @include('admin.alert.form_errors')
                  {!! csrf_field() !!}
                  <div class="form-group">
                	<label class="col-md-2 control-label">Tipe Paket</label>
                	<div class="col-md-7">
                		<select  name='tipe_paket' value="{{ old('tipe_paket') }}" class="form-control">
                			<option value="">Pilih</option>
                			<option value="Haji"
                      @if(old("tipe_paket") == "Haji") selected @endif>Haji</option>
                			<option value="Umroh"
                      @if(old("tipe_paket") == "Umroh") selected @endif>Umroh</option>
                		</select>
                	</div>
                 </div>

                  <div class="form-group">
                   <label class="col-md-2 control-label">Nama Paket</label>
                   <div class="col-md-7">
                     <input type="text" name="nama_paket" value="{{ old('nama_paket') }}" class="form-control">
                   </div>
                  </div>

                  <div class="form-group">
                   <label class="col-md-2 control-label">Harga Paket<br>(USD)</label>
                   <div class="col-md-7">
                     <input type="text" name="harga_paket" value="{{ old('harga_paket') }}" placeholder="1000" class="form-control">
                   </div>
                  </div>

                <div class="box-header with-border">
                    <h3 class="box-title">Kamar</h3>
                </div>

                <div class="form-group">
                   <label class="col-md-2 control-label">Double(USD)</label>
                   <div class="col-md-7">
                    <input type="text" name="kamar_harga[]" value="0"  class="form-control">
                    <input type="hidden" name="kamar_nama[]"  value="double" class="form-control">
            
                   </div>
                  </div>

                <div class="form-group">
                   <label class="col-md-2 control-label">Triple(USD)</label>
                   <div class="col-md-7">
                    <input type="text" name="kamar_harga[]" value="0"   class="form-control">
                    <input type="hidden" name="kamar_nama[]"  value="triple" class="form-control">
            
                   </div>
                </div>

                <div class="form-group">
                   <label class="col-md-2 control-label">Quad(USD)</label>
                   <div class="col-md-7">
                    <input type="text" name="kamar_harga[]" value="0"   class="form-control">
                    <input type="hidden" name="kamar_nama[]"  value="quad" class="form-control">
            
                   </div>
                </div>

                <div class="form-group">
                   <label class="col-md-2 control-label">Quint(USD)</label>
                   <div class="col-md-7">
                    <input type="text" name="kamar_harga[]"  value="0"  class="form-control">
                    <input type="hidden" name="kamar_nama[]"  value="quint" class="form-control">
            
                   </div>
                </div>

                  <div class="form-group">
                  	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                  		<button type="submit" class="btn btn-primary">Submit</button>
                  	</div>
                  </div>

          </div><!-- /.box-body -->

          </div><!-- /.box-body -->
        </div><!-- /.box -->
</form>

@stop

@extends('admin.layouts.default')

@section('title', 'Dokumen')

@section('content')

  <div class="col-md-12">
      @include('admin.alert.alert')
      <div class="box box-info">
         <div class="box-header">
             <div class="col-md-4">
               <h3 class="box-title">MASTER DATA - Harga Paket</h3>
               &nbsp;
               <a class="text-center" href="/harga_paket/create">
                   <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                 </a>
             </div>


              <div class="col-md-4 col-md-push-4">
               <form action="#" method="GET">
                <div class="input-group input-group-sm">

               </form>

               </div>


           </div>

           <div class="box-body">
             <table class="table table-bordered table-hover">
               <thead>
                 <tr>
                   <th>Tipe Paket</th>
                   <th>Nama Paket</th>
                   <th>Harga Paket</th>
                   <th>Pilihan</th>
                 </tr>
               </thead>
               <tbody>
                 @foreach($data as $paket)
                   <tr>
                     <td>{{ $paket['tipe_paket'] }}</td>
                     <td>{{ $paket['nama_paket'] }}</td>
                     <td>${{ $paket['harga_paket'] }}</td>
                     <td>
                         <form method="POST" action="/harga_paket/{{ $paket['id'] }}">
                           {!! csrf_field() !!}
                           {!! method_field('DELETE') !!}
                         <a href="/harga_paket/{{ $paket['id'] }}/edit" class="btn btn-primary"> <i class="fa fa-edit"></i></a>
                         <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus {{ $paket['nama_paket'] }} ?');"><i class="fa fa-trash"></i></button>
                         </form>
                     </td>
                   </tr>
                  @endforeach
               </tbody>
             </table>

           </div><!-- /.box-body -->

  </div>

@stop

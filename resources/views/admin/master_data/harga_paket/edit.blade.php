@extends('admin.layouts.default')

@section('title', 'Edit Harga Paket')

@section('content')
 <div class="col-md-12 form-horizontal">
	 <div class="box box-info">
	    <div class="box-body">
	      <div class="box-header with-border">
	        <h3 class="box-title">Edit Harga Paket</h3>
	      </div>

        <div class="row">
        	<div class="col-md-3 col-md-push-9">

        	</div>
          <form  action="/harga_paket/{{$paket['id']}}" method="post">
          	<div class="col-md-9 col-md-pull-3">
                   @include('admin.alert.form_errors')
                   {!! csrf_field() !!}
                  
                   <div class="form-group">
                   <label class="col-md-2 control-label">Tipe Paket</label>
                   <div class="col-md-9">
                     <select  name='tipe_paket' class="form-control">
                       <option value="">Pilih</option>
                       <option value="Haji"
                       @if(old("tipe_paket") == "haji" || $paket['tipe_paket'] == 'haji') selected @endif>Haji</option>
                       <option value="Umroh"
                       @if(old("tipe_paket") == "umroh" || $paket['tipe_paket'] == 'umroh') selected @endif>Umroh</option>
                     </select>
                   </div>
                  </div>

                 <div class="form-group">

          				<label class="col-sm-2 col-md-2 control-label">Nama Paket</label>
          				<div class="col-sm-9 col-md-9">
          					 <input type="text" name="nama_paket" class="form-control"
                     value="@if(old('nama_paket')){{ trim(old('nama_paket')) }}@else{{ trim($paket['nama_paket'])}}@endif">
          				</div>
          			 </div>

          			 <div class="form-group">
          				<label class="col-sm-2 col-md-2 control-label">Harga Paket<br>($USD)</label>
          				<div class="col-sm-9 col-md-9">
          					 <input type="text" name="harga_paket" class="form-control"
                     value="@if(old('harga_paket')){{ trim(old('harga_paket')) }}@else{{ trim($paket['harga_paket']) }}@endif">
          				</div>
          			 </div>

                <div class="box-header with-border">
                    <h3 class="box-title">Kamar</h3>
                </div>

                 <div class="form-group">
                   <label class="col-md-2 control-label">Double(USD)</label>
                   <div class="col-md-7">
                    <input type="text" name="kamar_harga[]" @if(count($paket->hotel)) value="{{$paket->hotel[0]->harga}}" @else value="0" @endif  class="form-control">
                    <input type="hidden" name="kamar_nama[]"  value="double" class="form-control">
            
                   </div>
                  </div>

                <div class="form-group">
                   <label class="col-md-2 control-label">Triple(USD)</label>
                   <div class="col-md-7">
                    <input type="text" name="kamar_harga[]" @if(count($paket->hotel)) value="{{$paket->hotel[1]->harga}}" @else value="0" @endif    class="form-control">
                    <input type="hidden" name="kamar_nama[]"  value="triple" class="form-control">
            
                   </div>
                </div>

                <div class="form-group">
                   <label class="col-md-2 control-label">Quad(USD)</label>
                   <div class="col-md-7">
                    <input type="text" name="kamar_harga[]" @if(count($paket->hotel)) value="{{$paket->hotel[2]->harga}}" @else value="0" @endif    class="form-control">
                    <input type="hidden" name="kamar_nama[]"  value="quad" class="form-control">
            
                   </div>
                </div>

                <div class="form-group">
                   <label class="col-md-2 control-label">Quint(USD)</label>
                   <div class="col-md-7">
                    <input type="text" name="kamar_harga[]" @if(count($paket->hotel)) value="{{$paket->hotel[3]->harga}}" @else value="0" @endif    class="form-control">
                    <input type="hidden" name="kamar_nama[]"  value="quint" class="form-control">
            
                   </div>
                </div>

                 <div class="form-group">
                  <div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                 </div>
          	</div>
          </div><!--  div row -->
      </form>

	 	</div><!-- /.box-body -->
	 </div><!-- /.box -->
 </div>
@stop

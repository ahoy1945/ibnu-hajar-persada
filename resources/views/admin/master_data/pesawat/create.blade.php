@extends('admin.layouts.default')

@section('title', 'Pesawat')

@section('content')

<form action="{{ route('admin.master_data.pesawat.store') }}" method="POST"  class="col-md-12 form-horizontal">
   <div class="box box-info">

          <div class="box-body">
             <div class="box-header with-border">
              <h3 class="box-title">MASTER DATA - Pesawat</h3>
            </div>
                  @include('admin.alert.form_errors')
                  {!! csrf_field() !!}

                  <div class="form-group">
                   <label class="col-md-2 control-label">Kode Pesawat</label>
                   <div class="col-md-7">
                     <input type="text" name="kode_pesawat" value="{{ old('kode_pesawat') }}" class="form-control">
                   </div>
                  </div>

                  <div class="form-group">
                   <label class="col-md-2 control-label">Nama Pesawat</label>
                   <div class="col-md-7">
                     <input type="text" name="nama_pesawat" value="{{ old('nama_pesawat') }}" class="form-control">
                   </div>
                  </div>

                  <div class="form-group">
                  	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                  		<button type="submit" class="btn btn-primary">Submit</button>
                  	</div>
                  </div>

          </div><!-- /.box-body -->

          </div><!-- /.box-body -->
        </div><!-- /.box -->
</form>

@stop

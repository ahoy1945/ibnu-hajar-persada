@extends('admin.layouts.default')

@section('title', 'Edit Pesawat')

@section('content')
 <div class="col-md-12 form-horizontal">
	 <div class="box box-info">
	    <div class="box-body">
	      <div class="box-header with-border">
	        <h3 class="box-title">Edit Pesawat</h3>
	      </div>

        <div class="row">
        	<div class="col-md-3 col-md-push-9">

        	</div>
          <form  action="{{ route('admin.master_data.pesawat.update', $data->id) }}" method="post">
          	<div class="col-md-9 col-md-pull-3">
                   @include('admin.alert.form_errors')
                   {!! csrf_field() !!}
                   {!! method_field('PATCH') !!}

                   <div class="form-group">
                    <label class="col-md-2 control-label">Kode Pesawat</label>
                    <div class="col-md-7">
                      <input type="text" name="kode_pesawat"
                      value="@if(old('kode_pesawat')){{old('kode_pesawat')}}@else{{ $data->kode_pesawat }}@endif" class="form-control">
                    </div>
                   </div> 

                   <div class="form-group">
                    <label class="col-md-2 control-label">Nama Pesawat</label>
                    <div class="col-md-7">
                      <input type="text" name="nama_pesawat"
                      value="@if(old('nama_pesawat')){{old('nama_pesawat')}}@else{{ $data->nama_pesawat }}@endif" class="form-control">
                    </div>
                   </div>

                 <div class="form-group">
                  <div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                 </div>
          	</div>
          </div><!--  div row -->
      </form>

	 	</div><!-- /.box-body -->
	 </div><!-- /.box -->
 </div>
@stop

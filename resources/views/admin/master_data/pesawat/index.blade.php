@extends('admin.layouts.default')

@section('title', 'Pesawat')

@section('content')

  <div class="col-md-12">
      @include('admin.alert.alert')
      <div class="box box-info">
         <div class="box-header">
             <div class="col-md-4">
               <h3 class="box-title">MASTER DATA - Pesawat</h3>
               &nbsp;
               <a class="text-center" href="{{ route('admin.master_data.pesawat.create') }}">
                   <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                 </a>
             </div>


              <div class="col-md-4 col-md-push-4">
               <form action="#" method="GET">
                <div class="input-group input-group-sm">

               </form>

               </div>


           </div>

           <div class="box-body">
             <table class="table table-bordered table-hover">
               <thead>
                 <tr>
                   <th>Kode</th>
                   <th>Nama Pesawat</th>
                   <th>Pilihan</th>
                 </tr>
               </thead>
               <tbody>
                 @foreach($data as $pesawat)
                   <tr>
                     <td>{{ $pesawat->kode_pesawat }}</td>
                     <td>{{ $pesawat->nama_pesawat }}</td>
                     <td>
                         <form method="POST" action="{{ route('admin.master_data.pesawat.delete', $pesawat->id) }}">
                           {!! csrf_field() !!}
                           {!! method_field('DELETE') !!}
                         <a href="{{ route('admin.master_data.pesawat.edit' , $pesawat->id) }}" class="btn btn-primary"> <i class="fa fa-edit"></i></a>
                         <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus {{ $pesawat->nama_pesawat }} ?');"><i class="fa fa-trash"></i></button>
                         </form>
                     </td>
                   </tr>
                  @endforeach
               </tbody>
             </table>

           </div><!-- /.box-body -->

  </div>

@stop

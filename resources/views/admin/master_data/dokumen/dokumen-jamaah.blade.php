@extends('admin.layouts.default')

@section('title', 'Dokumen Jamaah')

@section('content')
	<div class="col-md-12" ng-controller="DokumenController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Dokumen Jamaah {{ ucwords($tipe_paket) }} </h3>
                      &nbsp;
                      
                    </div>
                  
                  
                       <div class="col-md-6 col-md-pull-1">
                      <form action="{{ route('admin.dokumen.data-jamaah') }}" method="GET">
                       <div class="input-group input-group-sm">
                          <input type="hidden" name="tipe" value="@if($tipe_paket){{$tipe_paket}}@endif">
                           <select name="tanggal" class="form-control">
                            @foreach($tglPemberangkatan as $key => $value)

                                <option value="{{$value->tanggal_pemberangkatan}}">{{formatDate($value->tanggal_pemberangkatan)}}</option>
                            @endforeach
                          </select>
 
                          
                           
                          <span class="input-group-btn">
                             
                          </span>
                           
                           
                          <span class="input-group-btn">
                             
                          </span>

                           
                          <span class="input-group-btn">
                            <button type="submit"   class="btn btn-info btn-flat" type="button">Search</button>
                          </span>

                         
                      </form>

                      <form method="GET"  action="{{route('admin.dokumen.excel')}}">
                         <input type="hidden" name="tanggal" @if(isset($tanggal)) value="{{$tanggal}}" @endif>
                        
                          <input type="hidden" name="tipe" value="@if($tipe_paket){{$tipe_paket}}@endif">
                         <button class="btn btn-warning" style="margin-top:-20px;margin-left:30px;">
                           <i class="fa fa-file-excel-o"></i>
                         </button>
                        

                        </form>  
                      </div>

                      
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th rowspan="2"  width="200">Nama</th>
                        <th rowspan="2"  width="10">U</th>
                        <th rowspan="2"  width="20">Tgl Berangkat</th>
                        <th rowspan="2"  width="20">JK</th>
                        <th rowspan="2">Alamat</th>
                        <th colspan="{{ $documents->count() }}">Dokumen</th>
                         
                      </tr>
                      <tr>
                         @foreach($documents  as $document)
                            <th width="10">{{ $document->nama_dokumen }}</th>
                         @endforeach
                      </tr>
                    </thead>
                    <tbody>
                     
                       @if(count($jamaah) > 0)  
                          @foreach($jamaah as $value)
                            <tr>
                              <td>{{ $value->nama_peserta }}</td>
                              <td>{{ age($value->tgl_lahir) }}</td>
                              <td>{{ formatDate($value->paket->tgl_berangkat) }}</td>
                              <td>{{ $value->jenis_kelamin }}</td>
                              <td>{{ $value->alamat->alamat }} Desa
                                  {{ $value->alamat->desa }} Kec
                                  {{ $value->alamat->kecamatan}}
                                  {{ $value->alamat->kabupaten }}
                                  {{ $value->alamat->provinsi }} 
                              </td>
                              @foreach($documents as $document)
                                @if(in_array($document->id, $value->documents_group))
                                   <td style="text-align:center;"><input  ng-click="vm.updateDokumenCheck('{{$value['id']}}', '{{$document['id']}}')" type="checkbox" checked name="barang"></td>
                                @else
                                    <td style="text-align:center;"><input  ng-click="vm.updateDokumenCheck('{{$value['id']}}', '{{$document['id']}}')" type="checkbox"   name="barang"></td>
                                @endif
                                 
                              @endforeach
                            </tr>
                          @endforeach
                          
                       @else
                          <tr>
                            <td colspan="{!! $documents->count() + 5 !!}" align="center">Tidak ada data</td>
                          </tr>
                       @endif 
                         
                       
                    </tbody>
                  </table>
                   @if(!empty($jamaah))
                      {!! $jamaah->appends(['tipe' => $tipe_paket])->render() !!}
                   @endif 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
       @include("admin.inventory.partials.modal")           
   </div>
    
@stop

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $("#tanggal").datepicker();
    });
  </script> 
@stop

@section('style')
  <style>
    th {
      text-align: center;
      vertical-align: middle !important;
      white-space: nowrap !important;
    }
    .table-bordered>tbody>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #95a5a6 !important;
    }
  </style>
@stop
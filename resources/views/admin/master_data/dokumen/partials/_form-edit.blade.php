
 {!! csrf_field() !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Nama Dokumen</label>
	<div class="col-md-7">
		<input type="text" value="{{ $dokumen->nama_dokumen }}" name="nama_dokumen"  class="form-control">
	</div>
 </div>
 {{ method_field('PATCH') }}

<div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Tipe</label>
	<div class="col-sm-7 col-md-7">
		<select name="tipe" class="form-control">
			<option value="">Pilih</option>
			<option @if($dokumen->tipe === 'umroh') selected  @endif value="umroh">Umroh</option>
			<option @if($dokumen->tipe === 'haji') selected  @endif value="haji">Haji</option>
		</select>
	</div>
 </div>

 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
@extends('admin.layouts.default')

@section('title', 'Harga Paket')

@section('content')

  <div class="col-md-12">
      @include('admin.alert.alert')
      <div class="box box-info">
         <div class="box-header">
             <div class="col-md-4">
               <h3 class="box-title">MASTER DATA - Dokumen</h3>
               &nbsp;
               <a class="text-center" href="{{ route('admin.dokumen.create') }}">
                   <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                 </a>
             </div>


              <div class="col-md-4 col-md-push-4">
               <form action="#" method="GET">
                <div class="input-group input-group-sm">

               </form>

               </div>


           </div>

           <div class="box-body">
             <table class="table table-bordered table-hover">
               <thead>
                 <tr>
                   <th>Nama Dokumen</th>
                   <th>Tipe</th>
                    <th></th>
                 </tr>
               </thead>
               <tbody>
                  @foreach($documents as $document)
                    <tr>
                      <td>{{ $document->nama_dokumen }}</td>
                      <td>{{ ucwords($document->tipe) }}</td>
                       <td>
                         <form method="POST" action="{{ route('admin.dokumen.destroy', $document->id) }}">
                           {!! csrf_field() !!}
                           {!! method_field('DELETE') !!}
                         <a href="{{ route('admin.dokumen.edit', $document->id) }}" class="btn btn-primary"> <i class="fa fa-edit"></i></a>
                         <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus {{ $document->nama_dokumen }} ?');"><i class="fa fa-trash"></i></button>
                         </form>
                     </td>
                    </tr>
                  @endforeach
               </tbody>
             </table>
             {!! $documents->render() !!}

           </div><!-- /.box-body -->

  </div>

@stop

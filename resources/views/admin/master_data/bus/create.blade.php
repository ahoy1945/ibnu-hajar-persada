@extends('admin.layouts.default')

@section('title', 'Tambah Bus')

@section('content')

<form action="{{ route('admin.master_data.bus.store') }}" method="POST"  class="col-md-12 form-horizontal">
   <div class="box box-info">

          <div class="box-body">
             <div class="box-header with-border">
              <h3 class="box-title">MASTER DATA - BUS</h3>
            </div>
                  @include('admin.alert.form_errors')
                  {!! csrf_field() !!}
                  <div class="form-group">
                	<label class="col-md-2 control-label">Tipe Paket</label>
                	<div class="col-md-7">
                		<select  name='tipe_paket' value="{{ old('tipe_paket') }}" class="form-control">
                			<option value="">Pilih</option>
                			<option value="Haji"
                      @if(old("tipe_paket") == "Haji") selected @endif>Haji</option>
                			<option value="Umroh"
                      @if(old("tipe_paket") == "Umroh") selected @endif>Umroh</option>
                		</select>
                	</div>
                 </div>

                 <div class="form-group">
                 <label class="col-md-2 control-label">Nama Bus</label>
                 <div class="col-md-7">
                   <input type="text" name="nama_bus" value="{{ old('nama_bus') }}" placeholder="Bus 1" class="form-control">
                 </div>
                </div> 

                <div class="form-group">
                 <label class="col-md-2 control-label">Max Jamaah</label>
                 <div class="col-md-7">
                   <input type="text" name="max_jamaah" value="{{ old('max_jamaah') }}" placeholder="50" class="form-control">
                 </div>
                </div>

                  <div class="form-group">
                  	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                  		<button type="submit" class="btn btn-primary">Submit</button>
                  	</div>
                  </div>

          </div><!-- /.box-body -->

          </div><!-- /.box-body -->
        </div><!-- /.box -->
</form>

@stop

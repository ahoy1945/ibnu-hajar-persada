@extends('admin.layouts.default')

@section('title', 'Dokumen Jamaah')

@section('content')
	<div class="col-md-12" ng-controller="DokumenController as vm">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Laporan Pemasukan PerBank</h3>
                      &nbsp;
                      
                    </div>
                  
                  
                       <div class="col-md-6 col-md-pull-1">
                      <form action="{{ route('admin.rekening.laporan') }}" method="GET">
                       <div class="input-group input-group-sm">
                          <input type="hidden" name="tipe">
                           <select name="tanggal" class="form-control">
                            @foreach($tglPemberangkatan as $key => $value)

                                <option value="{{$value}}">{{formatDate($value)}}</option>
                            @endforeach
                          </select>
 
                          
                           
                          <span class="input-group-btn">
                             
                          </span>
                           
                           
                          <span class="input-group-btn">
                             
                          </span>

                           
                          <span class="input-group-btn">
                            <button type="submit"   class="btn btn-info btn-flat" type="button">Search</button>
                          </span>

                         
                      </form>
                      <form method="POST" action=" ">
                        {{ csrf_field() }}
                        <input type="hidden" name="from"  >
                        <input type="hidden" name="to"   >
                        <input type="hidden" name="type" >

                        </form>  
                      </div>

                      
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th width="200">Bank</th>
                        <th width="200">No Rek</th>
                        <th width="200"> Mata Uang</th>
                        <th>Jumlah</th>  
                      </tr>
                      
                    </thead>
                    <tbody>
                        @if(!empty($saldoRekening['data'])) 
                           @foreach($saldoRekening['data'] as $key => $value)
                            <tr>
                              <td>{{$value['bank']}}</td>
                              <td>{{$value['no_rek']}}</td>
                              <td>{{strtoupper($value['mata_uang'])}}</td>
                              <td>@if($value['mata_uang'] == 'idr') Rp. @else $ @endif
                               {{ number_format($value['jumlah'],0,",",".") }}   
                              </td>
                            </tr>
                        @endforeach
                        @else
                          <tr>
                            <td colspan="3" style="text-align:center;">Tidak ada</td>
                          </tr>
                        @endif
                       
                    </tbody>
                  </table>
                  <br>
                   <table style="font-weight: bold;">
                     <tr>
                       <td width="100">Total USD</td>
                       <td width="20">:</td>
                       <td width="150">${{number_format($saldoRekening['totalUsd'],0,",",".")}}</td>
                       <td width="100">Total IDR</td>
                       <td width="20">:</td>
                       <td>Rp.{{number_format($saldoRekening['totalIdr'],0,",",".")}}</td>
                     </tr>
                     
                   </table>                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
                
   </div>
    
@stop

@section('style')
  <style>
    th {
      text-align: center;
      vertical-align: middle !important;
      white-space: nowrap !important;
    }
    .table-bordered>tbody>tr>td, .table-bordered>thead>tr>th {
      border: 1px solid #95a5a6 !important;
    }
  </style>
@stop
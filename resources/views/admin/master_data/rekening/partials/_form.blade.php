
 {!! csrf_field() !!}
 <div class="form-group">
	<label class="col-md-2 control-label">Atas Nama</label>
	<div class="col-md-7">
		<input type="text" name="atas_nama"  class="form-control">
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">No Rek</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" name="no_rek"  class="form-control">
	</div>
 </div>

<div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Bank</label>
	<div class="col-sm-7 col-md-7">
		<select name="bank" class="form-control">
			<option value="">Pilih</option>
			<option value="Mandiri">Mandiri</option>
			<option value="BCA">BCA</option>
			<option value="BNI">BNI</option>
			<option value="BRI">BRI</option>
			<option value="BSM">BSM</option>
		</select>
	</div>
 </div>

 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Bank</label>
	<div class="col-sm-7 col-md-7">
		<select name="mata_uang" class="form-control">
			<option value="">Pilih</option>
		    <option value="idr">Rupiah</option>
		    <option value="usd">Dollar</option>
		</select>
	</div>
 </div>

 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
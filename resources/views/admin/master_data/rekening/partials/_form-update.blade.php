
 {!! csrf_field() !!}
  {!! method_field('PATCH') !!}

 <div class="form-group">
	<label class="col-md-2 control-label">Atas Nama</label>
	<div class="col-md-7">
		<input type="text" name="atas_nama" value="{{ $rekening->atas_nama }}"  class="form-control">
	</div>
 </div>

  <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">No Rek</label>
	<div class="col-sm-7 col-md-7">
		<input type="text" name="no_rek" value="{{ $rekening->no_rek }}"  class="form-control">
	</div>
 </div>

<div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Bank</label>
	<div class="col-sm-7 col-md-7">
		<select name="bank" class="form-control">
			<option value="">Pilih</option>
			<option @if($rekening->bank === 'Mandiri') selected  @endif value="Mandiri">Mandiri</option>
			<option  @if($rekening->bank === 'BCA') selected  @endif value="BCA">BCA</option>
			<option  @if($rekening->bank === 'BNI') selected  @endif value="BNI">BNI</option>
			<option  @if($rekening->bank === 'BRI') selected  @endif value="BRI">BRI</option>
			<option  @if($rekening->bank === 'BSM') selected  @endif value="BSM">BSM</option>
		</select>
	</div>
 </div>

 <div class="form-group">
	<label class="col-sm-2 col-md-2 control-label">Bank</label>
	<div class="col-sm-7 col-md-7">
		<select name="mata_uang" class="form-control">
			<option value="">Pilih</option>
		    <option @if($rekening->mata_uang == 'idr') selected @endif value="idr">Rupiah</option>
		    <option @if($rekening->mata_uang == 'usd') selected @endif value="usd">Dollar</option>
		</select>
	</div>
 </div>

 <div class="form-group">
 	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
 		<button type="submit" class="btn btn-primary">Submit</button>
 	</div>
 </div>
                  
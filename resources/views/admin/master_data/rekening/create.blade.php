@extends('admin.layouts.default')

@section('title', 'Tambah Rekening')

@section('content')
	<form action="{{route('admin.rekening.store')}}" method="POST"  class="col-md-12 form-horizontal">
		 <div class="box box-info">
    
                <div class="box-body">
                   <div class="box-header with-border">
                    <h3 class="box-title">Tambah Rekening</h3>
                  </div>
                  
                  @include('admin.alert.alert')
                  @include('admin.alert.form_errors')
                 
                  @include('admin.master_data.rekening.partials._form')		
                </div><!-- /.box-body -->

                </div><!-- /.box-body -->
              </div><!-- /.box -->
	</form>
@stop

 
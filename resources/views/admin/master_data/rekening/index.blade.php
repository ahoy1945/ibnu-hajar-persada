@extends('admin.layouts.default')

@section('title', 'Rekening')

@section('content')

  <div class="col-md-12">
      @include('admin.alert.alert')
      <div class="box box-info">
         <div class="box-header">
             <div class="col-md-4">
               <h3 class="box-title">Rekening</h3>
               &nbsp;
               <a class="text-center" href="{{ route('admin.rekening.create') }}">
                   <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                 </a>
             </div>


              <div class="col-md-4 col-md-push-4">
               <form action="#" method="GET">
                <div class="input-group input-group-sm">

               </form>

               </div>


           </div>

           <div class="box-body">
             <table class="table table-bordered table-hover">
               <thead>
                 <tr>
                   <th>Atas Nama</th>
                   <th>No. Rekening</th>
                   <th>Mata Uang</th>
                   <th>Bank</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 @foreach($rekenings as $rekening)
                   <tr>
                      <td>{{ $rekening->atas_nama }}</td>
                      <td>{{ $rekening->no_rek }}</td>
                      <td>{{ strtoupper($rekening->mata_uang) }}</td>
                      <td>{{ $rekening->bank }}</td> 
                      <td>
                        <form method="POST" action="{{ route('admin.rekening.destroy', $rekening->id) }}">
                        {!! csrf_field() !!}
                        {!! method_field('DELETE') !!}
                      <a href="{{ route('admin.rekening.edit', $rekening->id) }}" class="btn btn-warning"> <i class="fa fa-pencil"></i> </a>
                      <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus Rekening {{ $rekening->no_rek }} ?');"><i class="fa fa-trash"></i></button>
                </form>
                      </td>
                   </tr>
                    
                   
                 @endforeach 
               </tbody>
             </table>

           </div><!-- /.box-body -->

  </div>

@stop

@extends('admin.layouts.default')

@section('title', 'Kuota')

@section('content')

  <div class="col-md-12">
      @include('admin.alert.alert')
      <div class="box box-info">
         <div class="box-header">
             <div class="col-md-4">
               <h3 class="box-title">MASTER DATA - Maks Kuota</h3>
               &nbsp;
               <a class="text-center" href="{{ route('admin.master_data.kuota.create') }}">
                   <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                 </a>
             </div>


              <div class="col-md-4 col-md-push-4">
               <form action="#" method="GET">
                <div class="input-group input-group-sm">

               </form>

               </div>


           </div>

           <div class="box-body">
             <table class="table table-bordered table-hover">
               <thead>
                 <tr>
                   <th>Tipe Paket</th>
                   <th>Tanggal</th>
                   <th>Kuota Jamaah</th>
                   <th>Pilihan</th>
                 </tr>
               </thead>
               <tbody>
                  @foreach($data as $kuota)
                   <tr>
                     <td>{{ $kuota->tipe_paket }}</td>
                     <td>{{ formatDate($kuota->tgl_berangkat) }}</td>
                     <td>{{ $kuota->max_kuota }} Jamaah</td>
                     <td>
                         <form method="POST" action="{{ route('admin.master_data.kuota.delete' , $kuota->id) }}">
                           {!! csrf_field() !!}
                           {!! method_field('DELETE') !!}
                         <a href="{{ route('admin.master_data.kuota.edit' , $kuota->id) }}" class="btn btn-primary"> <i class="fa fa-edit"></i></a>
                         <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus data ini?');"><i class="fa fa-trash"></i></button>
                         </form>
                     </td>
                   </tr>
                   @endforeach
               </tbody>
             </table>

           </div><!-- /.box-body -->

  </div>

@stop

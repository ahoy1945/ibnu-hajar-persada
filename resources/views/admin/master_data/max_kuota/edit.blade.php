@extends('admin.layouts.default')

@section('title', 'Edit Kuota')

@section('content')

<form action="{{ route('admin.master_data.kuota.update', $data->id) }}" method="POST"  class="col-md-12 form-horizontal">
   <div class="box box-info">

          <div class="box-body">
             <div class="box-header with-border">
              <h3 class="box-title">MASTER DATA - Maks Kuota</h3>
            </div>
                  @include('admin.alert.form_errors')
                  {!! csrf_field() !!}
                  {!! method_field('PATCH') !!}
                  <div class="form-group">
                	<label class="col-md-2 control-label">Tipe Paket</label>
                	<div class="col-md-7">
                		<select  name='tipe_paket' value="{{ old('tipe_paket') }}" class="form-control">
                			<option value="">Pilih</option>
                			<option value="haji"
                      @if(old("tipe_paket") == "haji" || $data->tipe_paket == 'haji') selected @endif>Haji</option>
                			<option value="umroh"
                      @if(old("tipe_paket") == "umroh" || $data->tipe_paket == 'umroh') selected @endif>Umroh</option>
                		</select>
                	</div>
                 </div>

                  <div class="form-group">
                   <label class="col-md-2 control-label">Tanggal</label>
                   <div class="col-md-7">
                      <select  name='tgl_berangkat' value="{{ old('tipe_paket') }}" class="form-control">
                        <option value="">Pilih</option>
                        @foreach($tglBerangkat as $tanggal)
                          <option value="{{ $tanggal->tanggal_pemberangkatan }}" @if(old("tgl_berangkat") == $tanggal->tanggal_pemberangkatan || $data->tgl_berangkat == $tanggal->tanggal_pemberangkatan) selected @endif>
                            {{ formatDate($tanggal->tanggal_pemberangkatan) }}
                          </option>
                        @endforeach
                    </select>
                   </div>
                  </div>

                   <div class="form-group">
                   <label class="col-md-2 control-label">Max Kuota</label>
                   <div class="col-md-7">
                     <input type="text" name="max_kuota" value="{{ $data->max_kuota or old('max_kuota') }}" placeholder="50" class="form-control">
                   </div>
                  </div>

                  <div class="form-group">
                  	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                  		<button type="submit" class="btn btn-primary">Submit</button>
                  	</div>
                  </div>

          </div><!-- /.box-body -->

          </div><!-- /.box-body -->
        </div><!-- /.box -->
</form>

@stop

@extends('admin.layouts.default')

@section('title', 'Tanggal Pemberangkatan')

@section('content')

<form action="/tanggal_pemberangkatan" ng-controller="PemberangkatanController as vm" method="POST"  class="col-md-12 form-horizontal">
   <div class="box box-info">

          <div class="box-body">
             <div class="box-header with-border">
              <h3 class="box-title">MASTER DATA - Tgl Pemberangkatan</h3>
            </div>
                  @include('admin.alert.form_errors')
                  {!! csrf_field() !!}

                    <div class="form-group">
                    <label class="col-md-2 control-label">Tipe</label>
                    <div class="col-md-7">
                        <select ng-model="vm.input.tipe" name="tipe" class="form-control">
                          <option value=""></option>
                          <option value="umroh">Umroh</option>
                          <option value="haji">Haji</option>
                        </select>
                    </div>
                  </div>

                  <div class="form-group" ng-if="vm.input.tipe">
                  <label class="col-md-2 control-label">Pemberangkatan</label>
                  <div class="col-md-7">
                    <select  name='paket_id' class="form-control">
                      <option value="">Pilih</option>
                      
                      <option ng-if="vm.input.tipe" ng-repeat="hargapaket in vm.hargapaket" value="@{{hargapaket.id}}">@{{hargapaket.nama_paket}}</option>
                  
                    </select>
                  </div>
                 </div>  

                     

                  <div class="form-group" ng-if="vm.input.tipe == 'umroh'">
                    <label class="col-md-2 control-label">Tanggal</label>
                    <div class="col-md-7">
                      <input type="text"  data-date-format="yyyy-mm-dd" name="tanggal_pemberangkatan" value="{{ old('tanggal_pemberangkatan') }}" id="tanggal_pemberangkatan" ng-click="vm.openDatepicker();" class="form-control">
                    </div>
                  </div>

                     <div class="form-group" ng-if="vm.input.tipe == 'haji'">
                    <label class="col-md-2 control-label">Tahun</label>
                    <div class="col-md-7">
                       <select  class="form-control" name="tanggal_pemberangkatan">
                          <option ng-repeat="date in vm.date" value="@{{date}}-12-31">@{{date}}</option>
                        </select> 
                   
                    </div>
                  </div>
          
                  <div class="form-group">
                  	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                  		<button type="submit" class="btn btn-primary">Submit</button>
                  	</div>
                  </div>

          </div><!-- /.box-body -->

          </div><!-- /.box-body -->
        </div><!-- /.box -->
</form>
@stop


@section('script')
  <script type="text/javascript">
      $(document).ready(function() {
          var tanggal_pemberangkatan = $('#tanggal_pemberangkatan');

              tanggal_pemberangkatan.datepicker(); 
      });

  </script>
@stop
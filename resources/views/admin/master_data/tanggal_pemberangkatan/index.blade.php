@extends('admin.layouts.default')

@section('title', 'Tanggal Pemberangkatan')

@section('content')

  <div class="col-md-12">
      @include('admin.alert.alert')
      <div class="box box-info">
         <div class="box-header">
             <div class="col-md-4">
               <h3 class="box-title">MASTER DATA - Tgl Pemberangkatan</h3>
               &nbsp;
               <a class="text-center" href="/tanggal_pemberangkatan/create">
                   <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                 </a>
             </div>


              <div class="col-md-4 col-md-push-4">
               <form action="#" method="GET">
                <div class="input-group input-group-sm">

               </form>

               </div>


           </div>

           <div class="box-body">
             <table class="table table-bordered table-hover">
               <thead>
                 <tr>
                   <th>Tanggal Pemberangkatan</th>
                   <th>Tipe</th>
                   <th>Nama Pemberangkatan</th>
                   <th>Pilihan</th>
                 </tr>
               </thead>
               <tbody>
                 @foreach($data as $tanggal)
                   <tr>
                     <td>{{ formatDate($tanggal['tanggal_pemberangkatan']) }}</td>
                     <td>{{ $tanggal->tipe }}</td> 
                     <td>{{ $tanggal->paket->nama_paket }}</td>
                     <td>
                         <form method="POST" action="/tanggal_pemberangkatan/{{ $tanggal['id'] }}">
                           {!! csrf_field() !!}
                           {!! method_field('DELETE') !!}
                         <a href="/tanggal_pemberangkatan/{{ $tanggal['id'] }}/edit" class="btn btn-primary"> <i class="fa fa-edit"></i></a>
                         <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus {{ $tanggal['nama_pemberangkatan'] }} ?');"><i class="fa fa-trash"></i></button>
                         </form>
                     </td>
                   </tr>
                  @endforeach
               </tbody>
             </table>

           </div><!-- /.box-body -->

  </div>

@stop

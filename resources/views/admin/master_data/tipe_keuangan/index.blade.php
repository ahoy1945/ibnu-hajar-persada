@extends('admin.layouts.default')

@section('title', 'Harga tipe')

@section('content')

  <div class="col-md-12">
      @include('admin.alert.alert')
      <div class="box box-info">
         <div class="box-header">
             <div class="col-md-4">
               <h3 class="box-title">MASTER DATA - Tipe Keuangan</h3>
               &nbsp;
               <a class="text-center" href="/tipe_keuangan/create">
                   <button class="btn btn-info btn-flat"><i class="fa fa-plus"></i></button>
                 </a>
             </div>


              <div class="col-md-4 col-md-push-4">
               <form action="#" method="GET">
                <div class="input-group input-group-sm">

               </form>

               </div>


           </div>

           <div class="box-body">
             <table class="table table-bordered table-hover">
               <thead>
                 <tr>
                   <th>Tipe Keuangan</th>
                   <th>Nama Tipe</th>
                   <th>Pilihan</th>
                 </tr>
               </thead>
               <tbody>
                 @foreach($data as $tipe)
                   <tr>
                     <td>{{ $tipe['tipe_keuangan'] }}</td>
                     <td>{{ $tipe['nama_tipe'] }}</td>
                     <td>
                         <form method="POST" action="/tipe_keuangan/{{ $tipe['id'] }}">
                           {!! csrf_field() !!}
                           {!! method_field('DELETE') !!}
                         <a href="/tipe_keuangan/{{ $tipe['id'] }}/edit" class="btn btn-primary"> <i class="fa fa-edit"></i></a>
                         <button class="btn btn-danger" onclick="return confirm('yakin ingin menghapus {{ $tipe['nama_tipe'] }} ?');"><i class="fa fa-trash"></i></button>
                         </form>
                     </td>
                   </tr>
                  @endforeach
               </tbody>
             </table>

           </div><!-- /.box-body -->

  </div>

@stop

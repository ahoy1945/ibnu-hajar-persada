@extends('admin.layouts.default')

@section('title', 'Edit Harga Paket')

@section('content')
 <div class="col-md-12 form-horizontal">
	 <div class="box box-info">
	    <div class="box-body">
	      <div class="box-header with-border">
	        <h3 class="box-title">Edit Tipe Keuangan</h3>
	      </div>

        <div class="row">
        	<div class="col-md-3 col-md-push-9">

        	</div>
          <form  action="/tipe_keuangan/{{$data[0]['id']}}" method="post">
          	<div class="col-md-9 col-md-pull-3">
                   @include('admin.alert.form_errors')
                   {!! csrf_field() !!}

                   <div class="form-group">
                 	<label class="col-md-2 control-label">Tipe Keuangan</label>
                 	<div class="col-md-7">
                 		<select  name='tipe_keuangan' class="form-control">
                 			<option value="">Pilih</option>
                 			<option value="Pemasukan"
                      @if(old("tipe_keuangan") == "Pemasukan" || $data[0]['tipe_keuangan'] == 'Pemasukan') selected @endif>Pemasukan</option>
                 			<option value="Pengeluaran"
                      @if(old("tipe_keuangan") == "Pengeluaran" || $data[0]['tipe_keuangan'] == 'Pengeluaran') selected @endif>Pengeluaran</option>
                 		</select>
                 	</div>
                  </div>

                   <div class="form-group">
                    <label class="col-md-2 control-label">Nama Tipe</label>
                    <div class="col-md-7">
                      <input type="text" name="nama_tipe"
                      value="@if(old('nama_tipe')){{old('nama_tipe')}}@else{{ $data[0]['nama_tipe'] }}@endif" class="form-control">
                    </div>
                   </div>

                 <div class="form-group">
                  <div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                 </div>
          	</div>
          </div><!--  div row -->
      </form>

	 	</div><!-- /.box-body -->
	 </div><!-- /.box -->
 </div>
@stop

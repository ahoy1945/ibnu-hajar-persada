@extends('admin.layouts.default')

@section('title', 'Peserta')

@section('content')

<form action="/tipe_keuangan" method="POST"  class="col-md-12 form-horizontal">
   <div class="box box-info">

          <div class="box-body">
             <div class="box-header with-border">
              <h3 class="box-title">MASTER DATA - Tipe Keuangan</h3>
            </div>
                  @include('admin.alert.form_errors')
                  {!! csrf_field() !!}
                  <div class="form-group">
                	<label class="col-md-2 control-label">Tipe Keuangan</label>
                	<div class="col-md-7">
                		<select  name='tipe_keuangan' class="form-control">
                			<option value="">Pilih</option>
                			<option value="Pemasukan"
                      @if(old("tipe_keuangan") == "Pemasukan") selected @endif>Pemasukan</option>
                			<option value="Pengeluaran"
                      @if(old("tipe_keuangan") == "Pengeluaran") selected @endif>Pengeluaran</option>
                		</select>
                	</div>
                 </div>

                  <div class="form-group">
                   <label class="col-md-2 control-label">Nama Tipe</label>
                   <div class="col-md-7">
                     <input type="text" name="nama_tipe" value="{{ old('nama_tipe') }}" class="form-control">
                   </div>
                  </div>

                  <div class="form-group">
                  	<div class="col-sm-2 col-md-2 col-sm-offset-2 col-md-offset-2">
                  		<button type="submit" class="btn btn-primary">Submit</button>
                  	</div>
                  </div>

          </div><!-- /.box-body -->

          </div><!-- /.box-body -->
        </div><!-- /.box -->
</form>

@stop

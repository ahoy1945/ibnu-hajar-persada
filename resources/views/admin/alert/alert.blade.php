@if(Session::has('success_message'))
	 <div class="alert alert-success alert-dismissable">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	    <h4> <i class="icon fa fa-check"></i>{{ Session::get('success_message') }}</h4>
    </div>
@endif

@if(Session::has('error_message'))
	 <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4> <i class="icon fa fa-ban"></i>{{ Session::get('error_message') }}</h4>
    </div>
@endif

@extends('admin.layouts.default')

@section('title', 'Pengaturan')

@section('content')
	<div class="col-md-12">
            @include('admin.alert.alert')
             <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-4">
                      <h3 class="box-title">Pengaturan</h3>
                      &nbsp;
                     
                    </div>
                  
                  
                     <div class="col-md-4 col-md-push-4">
                      <form action="#" method="GET">
                       <div class="input-group input-group-sm">

                      </form>

                      </div>
                       
                    
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                      
                  
                        <th width="400">Label</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody>
                   
					          @foreach($settings as $setting)
                      <form method="POST" action="{{ route('admin.settings.store') }}">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $setting->id }}" name="id[]">
                        <tr>
                          <td>{{ $setting->label }}</td>
                          <td>
                            @if($setting->type_input === "textarea" )
                                <textarea name="meta_value[]" required class="form-control">{{ $setting->meta_value }}</textarea>
                            @else
                                <input name="meta_value[]" required class="form-control" type="{{ $setting->type_input }}" value="{{ $setting->meta_value}}">
                            @endif
                          </td>
                      
                      </tr>
                    @endforeach
                      <tr>
                        <td colspan="2" style="text-align:center"><button type="submit" class="btn btn-primary">Simpan</button></td>
                      </tr>
                      </form>
                    </tbody>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
@stop
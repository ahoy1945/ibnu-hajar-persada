<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefDokumen extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ref_dokumen';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nama_dokumen', 'tipe'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     * 
     * @var array
     */
    protected $dates = [];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefMaxKuota extends Model
{
    protected $table = 'ref_max_kuota';

    protected $fillable = ['tipe_paket','tgl_berangkat', 'max_kuota'];
}

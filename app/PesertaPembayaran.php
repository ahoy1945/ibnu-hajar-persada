<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class PesertaPembayaran extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'peserta_pembayaran';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['peserta_id','no_kwitansi', 'diskon', 'tipe_keuangan_id', 'rekening_id' , 'pembayaran', 'mata_uang', 'jumlah', 'keterangan', 'tgl_bayar', 'jumlah_dollar'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     * 
     * @var array
     */
    protected $dates = [];

    /**
     * Pembayaran belongsTo Peserta
     * 
     * @return Relationship
     */
    public function peserta()
    {
    	return $this->belongsTo(\App\Peserta::class, 'peserta_id');
    }

    /**
     * Pembayaran belongsTo Rekening
     * 
     * @return Relationship
     */
    public function rekening()
    {
    	return $this->belongsTo(\App\Rekening::class, 'rekening_id');
    }

    public function tipe_keuangan()
    {
        return $this->belongsTo(\App\RefTipeKeuangan::class, 'tipe_keuangan_id');
    }

    public function konversi()
    {
        return $this->hasMany(\App\PesertaPembayaranKonversi::class, 'peserta_pembayaran_id');
                    
    }



}

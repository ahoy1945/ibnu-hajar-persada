<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesertaPembayaranKonversi extends Model
{
    protected $table = 'peserta_pembayaran_konversi';

    protected $fillable = ['peserta_pembayaran_id', 'kurs', 'tgl_konversi'];

    protected $guarded = [];

 

    protected $dates = [];

    public function pembayaran()
    {
    	return $this->belongsTo(\App\PesertaPembayaran::class, 'peserta_pembayaran_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesertaDokumen extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'peserta_dokumen';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['peserta_id', 'dokumen_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     * 
     * @var array
     */
    protected $dates = [];
}

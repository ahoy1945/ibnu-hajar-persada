<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefPesawat extends Model
{
    protected $table = 'ref_pesawat';

    protected $fillable = ['kode_pesawat', 'nama_pesawat'];
}

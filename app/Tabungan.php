<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tabungan extends Model
{
    protected $table = 'tabungan';

    protected $fillable = ['nama'];

    protected $guarded = [];

    protected $dates = [];

    public function saldo()
    {
    	return $this->hasMany(\App\TabunganSaldo::class, 'tabungan_id');
    }
}

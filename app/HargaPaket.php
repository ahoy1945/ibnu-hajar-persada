<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HargaPaket extends Model
{
    protected $table = 'ref_harga_paket';

    protected $fillable = ['tipe_paket','nama_paket','harga_paket'];

    public function hotel()
    {
    	return $this->hasMany(\App\Pakethotel::class, 'paket_id');
    }
}

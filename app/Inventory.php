<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inventory';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nama', 'jumlah', 'ket', 'tipe'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     * 
     * @var array
     */
    protected $dates = [];

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function inventoryHistories()
    {
        return $this->hasMany('App\InventoryHistory', 'inventory_id');
    }
    
    public function peserta()
    {
        return $this->belongsToMany('App\Peserta', 'peserta_inventory', 'inventory_id', 'peserta_id')->withTimestamps(); ;
    }
}

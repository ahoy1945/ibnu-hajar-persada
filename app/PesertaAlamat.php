<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesertaAlamat extends Model
{
    protected $table = 'peserta_alamat';

    protected $fillable = [
      'peserta_id',
      'alamat',
      'desa',
      'kecamatan',
      'kabupaten',
      'provinsi',
      'kodepos'
    ];

   

    
}

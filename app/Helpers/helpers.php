<?php

use Illuminate\Filesystem\Filesystem;
use Jenssegers\Date\Date;

/**
* @param date
* @return integer (age)
*/
function age($date) {
  return date_diff(date_create($date), date_create('today'))->y;
}

function dataRekening () {
    return ;
}

/**
* @param string
* @return string
*/
function nameStatus($status) {
  $listMahram = [
    'single' => 'Single',
    'woman group' => 'Woman Group',
    'ibu' => 'Mother',
    'suami' => 'Husband',
    'ayah' => 'Father',
    'istri' => 'Wife',
    'anak' => 'Child',
    'paman' => 'Uncle',
    'ayah mertua' => 'Father-in-law',
    'saudara kandung' => 'Blood brother',
    'saudara seayah' => 'Half brother',
    'saudara seibu' => 'Half brother',
    'kakek saudara' => 'Grandfather brother',
    'anak saudari' => 'Kid brother',
    'kakak laki-laki' => 'Older Brother',
    'kakak perempuan' => 'Older Sister',
    'adik laki-laki' => 'Younger Brother',
    'adik perempuan' => 'Younger Sister',
    'saudara laki-laki' => 'Brother',
    'saudara perempuan' => 'Sister',
    '' => '-',
  ];
  return $listMahram[$status];
}

/**
* @param 1 integer
* @param 2 array
* @param 3 integer
* @return array
*/
function arrayPagination($page = null, $collection, $perPage) {
  $page = $page < 1 ? 1 : $page;
  $start = ($page - 1) * $perPage;
  $offset = $perPage;
  $outArray = array_slice($collection, $start, $offset);
  return $outArray;
}

/**
* @param date
* @return date
*/
function formatDate($date) {
    if ($date != '0000-00-00') {
        Date::setLocale('id');
        return Date::parse($date)->format('d F Y');
    }

    return "-";
}

/**
 * this function for edit jamaah in menu peserta
 */
function editDate($date) {
    if ($date != '0000-00-00') {
        return $date;
    }

    return null;
}

function doubleFamily($var) {
    $newArray = array_merge($var, $var['paket']);
    unset($var['paket']);
    return ($newArray['tipe_kamar'] == 'double');
}

function tripleFamily($var) {
    $newArray = array_merge($var, $var['paket']);
    unset($newArray['paket']);
    return ($newArray['tipe_kamar'] == 'triple');   
}

function quadFamily($var) {
    $newArray = array_merge($var, $var['paket']);
    unset($newArray['paket']);
    return ($newArray['tipe_kamar'] == 'quad');
}

function quintFamily($var) {
    $newArray = array_merge($var, $var['paket']);
    unset($newArray['paket']);
    return ($newArray['tipe_kamar'] == 'quint');
}

// setting room in pagination and excel
/**
* @param array
* @return array
*/
function setDataRoom($dataRoom) {
   $roomList = [];
    $i=0; $j=0; $k=0; 
    $l=0; $m=0; $n=0; $o=0;

    foreach ($dataRoom as $key => $room) {
        if ($room['type'] == 'double_family') {
            if (!isset($roomList['double_family'][$i])) {
                 $roomList['double_family'][$i][] = $room;
            } else {
                if (count($roomList['double_family'][$i]) < 2 ) {
                    $roomList['double_family'][$i][] = $room;
                    $roomList['double_family'][$i][0]['row_span'] = count($roomList['double_family'][$i]);
                } else {
                    $roomList['double_family'][$i+1][] = $room;
                    $i+=1;
                }
            }                
        } else if ($room['type'] == 'triple_family') {
            if (!isset($roomList['triple_family'][$i])) {
                 $roomList['triple_family'][$i][] = $room;
            } else {
                if (count($roomList['triple_family'][$i]) < 3 ) {
                    $roomList['triple_family'][$i][] = $room;
                    $roomList['triple_family'][$i][0]['row_span'] = count($roomList['triple_family'][$i]);
                } else {
                    $roomList['triple_family'][$i+1][] = $room;
                    $i+=1;
                }
            }
        } else if ($room['type'] == 'quad_family') {
            if (!isset($roomList['quad_family'][$i])) {
                 $roomList['quad_family'][$i][] = $room;
            } else {
                if (count($roomList['quad_family'][$i]) < 4 ) {
                    $roomList['quad_family'][$i][] = $room;
                    $roomList['quad_family'][$i][0]['row_span'] = count($roomList['quad_family'][$i]);
                } else {
                    $roomList['quad_family'][$i+1][] = $room;
                    $i+=1;
                }
            }
        } else if ($room['type'] == 'quint_family') {
            if (!isset($roomList['quint_family'][$i])) {
                 $roomList['quint_family'][$i][] = $room;
            } else {
                if (count($roomList['quint_family'][$i]) < 5 ) {
                    $roomList['quint_family'][$i][] = $room;
                    $roomList['quint_family'][$i][0]['row_span'] = count($roomList['quint_family'][$i]);
                } else {
                    $roomList['quint_family'][$i+1][] = $room;
                    $i+=1;
                }
            }
        }else if ($room['type'] == 'double_man') {
            if (!isset($roomList['double_man'][$j])) {
                 $roomList['double_man'][$j][] = $room;
            } else {
                if (count($roomList['double_man'][$j]) < 2 ) {
                    $roomList['double_man'][$j][] = $room;
                    $roomList['double_man'][$j][0]['row_span'] = count($roomList['double_man'][$j]);
                } else {
                    $roomList['double_man'][$j+1][] = $room;
                    $j+=1;
                }
            }
        } else if ($room['type'] == 'double_woman') {
            if (!isset($roomList['double_woman'][$k])) {
                 $roomList['double_woman'][$k][] = $room;
            } else {
                if (count($roomList['double_woman'][$j]) < 2 ) {
                    $roomList['double_woman'][$k][] = $room;
                    $roomList['double_woman'][$k][0]['row_span'] = count($roomList['double_woman'][$k]);
                } else {
                    $roomList['double_woman'][$k+1][] = $room;
                    $k+=1;
                }
            }
        } else if ($room['type'] == 'triple_man') {
            if (!isset($roomList['triple_man'][$l])) {
                 $roomList['triple_man'][$l][] = $room;
            } else {
                if (count($roomList['triple_man'][$l]) < 3 ) {
                    $roomList['triple_man'][$l][] = $room;
                    $roomList['triple_man'][$l][0]['row_span'] = count($roomList['triple_man'][$l]);
                } else {
                    $roomList['triple_man'][$l+1][] = $room;
                    $l+=1;
                }
            }
        } else if ($room['type'] == 'triple_woman') {
            if (!isset($roomList['triple_woman'][$m])) {
                 $roomList['triple_woman'][$m][] = $room;
            } else {
                if (count($roomList['triple_woman'][$m]) < 3 ) {
                    $roomList['triple_woman'][$m][] = $room;
                    $roomList['triple_woman'][$m][0]['row_span'] = count($roomList['triple_woman'][$m]);
                } else {
                    $roomList['triple_woman'][$m+1][] = $room;
                    $m+=1;
                }
            }
        } else if ($room['type'] == 'quad_man') {
            if (!isset($roomList['quad_man'][$n])) {
                 $roomList['quad_man'][$n][] = $room;
            } else {
                if (count($roomList['quad_man'][$n]) < 4 ) {
                    $roomList['quad_man'][$n][] = $room;
                    $roomList['quad_man'][$n][0]['row_span'] = count($roomList['quad_man'][$n]);
                } else {
                    $roomList['quad_man'][$n+1][] = $room;
                    $n+=1;
                }
            }
        } else if ($room['type'] == 'quad_woman') {
            if (!isset($roomList['quad_woman'][$o])) {
                 $roomList['quad_woman'][$o][] = $room;
            } else {
                if (count($roomList['quad_woman'][$o]) < 4 ) {
                    $roomList['quad_woman'][$o][] = $room;
                    $roomList['quad_woman'][$o][0]['row_span'] = count($roomList['quad_woman'][$o]);
                } else {
                    $roomList['quad_woman'][$o+1][] = $room;
                    $o+=1;
                }
            }
        } else if ($room['type'] == 'quint_man') {
                if (!isset($roomList['quint_man'][$n])) {
                     $roomList['quint_man'][$n][] = $room;
                } else {
                    if (count($roomList['quint_man'][$n]) < 5 ) {
                        $roomList['quint_man'][$n][] = $room;
                        $roomList['quint_man'][$n][0]['row_span'] = count($roomList['quint_man'][$n]);
                    } else {
                        $roomList['quint_man'][$n+1][] = $room;
                        $n+=1;
                    }
                }
        } else if ($room['type'] == 'quint_woman') {
            if (!isset($roomList['quint_woman'][$o])) {
                 $roomList['quint_woman'][$o][] = $room;
            } else {
                if (count($roomList['quint_woman'][$o]) < 5 ) {
                    $roomList['quint_woman'][$o][] = $room;
                    $roomList['quint_woman'][$o][0]['row_span'] = count($roomList['quint_woman'][$o]);
                } else {
                    $roomList['quint_woman'][$o+1][] = $room;
                    $o+=1;
                }
            }
        }
    }
    return $roomList;
}


function fileExist($path) {
    $filesystem = new Filesystem;
     
    if($filesystem->exists(public_path().$path) && !empty($path)) {

     
        return $path;
    } 

    return '/attachs/no-avatar.png';
}

function monthToString($month)
{
    switch ($month) {
        case '01':
            return 'Januari';
            break;

        case '02':
            return 'Februari';
            break;
        case '03':
            return 'Maret';
            break;    
        
        case '04':
            return 'April';
            break;    

        case '05':
            return 'Mei';
            break;    
        
        case '06':
            return 'Juni';
            break;

        case '07':
            return 'Juli';
            break;

        case '08':
            return 'Agustus';
            break;        
        
        case '09':
            return 'September';
            break;

        case '10':
            return 'Oktober';
            break;

        case '11':
            return 'November';
            break;
            
        case '12':
            return 'Desember';
            break;    

        }
}

function monthToStringShort($month)
{
    switch ($month) {
        case '01':
            return 'Jan';
            break;
        
        case '02':
            return 'Feb';
            break;
        case '03':
            return 'Mar';
            break;    
        
        case '04':
            return 'Apr';
            break;    

        case '05':
            return 'Mei';
            break;    
        
        case '06':
            return 'Jun';
            break;

        case '07':
            return 'Jul';
            break;

        case '08':
            return 'Agu';
            break;        
        
        case '09':
            return 'Sep';
            break;

        case '10':
            return 'Okt';
            break;

        case '11':
            return 'Nov';
            break;
            
        case '12':
            return 'Des';
            break;    

        }
}

function removeCity($city) {
    $string = str_replace("Kabupaten", "", $city);
    $string = ltrim(str_replace("Kota", "", $string));

    return $string;

}

function ifIsset($array = [], $index){
    return (isset($array[$index]) ? $array[$index] : '' );
}

function tglPemberangkatanList()
{
    $newArray = [];
    
    foreach(\App\TanggalPemberangkatan::all() as $key => $value ) {
        if(!in_array($value->tanggal_pemberangkatan, $newArray)) {
            array_push($newArray, $value->tanggal_pemberangkatan);
        }
    }

    return $newArray;
}

function rangeYears()
{
    $currentYear = date("Y");
    $firstYear = $currentYear - 5;
    $lastYear = $currentYear + 5;
    $listYears = [];
    for($i = $firstYear; $i <= $lastYear; $i++) {
        array_push($listYears, $i);
    }

    return $listYears;
}

function pemberangkatan()
{
    $pemberangkatan = \App\TanggalPemberangkatan::with('paket')->get()->toArray();
    
    $paketHajiArray = [];
    $i = 0;   

    foreach(\App\HargaPaket::where('tipe_paket', 'haji')->get() as $key => $value) {
        $paketHajiArray[$i]['id'] = $value->id;
        $paketHajiArray[$i]['paket_id'] = $value->id;
        $paketHajiArray[$i]['tipe'] = $value->tipe_paket;
        $paketHajiArray[$i]['nama_paket'] = $value->nama_paket; 
        $paketHajiArray[$i]['tahun'] = date('Y')."-01-01"; 
        $i++;
    }

    return array_merge($pemberangkatan, $paketHajiArray);
}
?>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pakethotel extends Model
{
    protected $fillable = ['kamar', 'harga', 'paket_id'];

    protected $guarded = [];

    protected $table = 'ref_harga_paket_hotel';
}

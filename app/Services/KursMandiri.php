<?php 

namespace App\Services;

use Goutte\Client;

class KursMandiri
{
	const API_URL = "http://www.bankmandiri.co.id/resource/kurs.asp?row=2";

	/**
	 * @var Goutte\Client
	 */
	protected $http;

	protected $kurs;

	/**
	 * Create a new instance
	 */
	public function __construct(Client $http)
	{
		
	}

	public static function  get($kurs = 'USD', $http = 'GET')
	{
		$client = new Client();
		$crawler = $client->request($http, self::API_URL);
		$statusCode = $client->getResponse()->getStatus();

		if($statusCode === 200) {
			 $content = $crawler->filter('.tbl-view  tr')->siblings()->each(function($node, $i) use ($kurs, $client){
	            if($node->attr('class') !== 'zebra') {
	                if($node->filter('td')->eq(1)->text() === $kurs) {
	                    $result = explode(",",$node->filter('td')->eq(4)->text())[0];
	                    $result = str_replace(".","", $result);

	                    return $result;
	                }
	                    
	            }
      	 	 });

      	 	 return array_filter($content)[14];
		}
		
	}
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class TabunganSaldo extends Model
{
    protected $table = 'tabungan_saldo';

    protected $fillable = ['tabungan_id', 'tipe', 'jumlah'];

    public function tabungan()
    {
    	return $this->belongsTo(\App\Tabungan::class, 'tabungan_id');
    }

    public function listTabungan($request, $perPage = 10)
    {
        $page = ($request->input('page') ? $request->input('page') : 1 );
    	$countDebit = static::selectRaw('*, sum(jumlah) as jumlah')->groupBy('tabungan_id')->where('tipe','debit')->get();

    	$debit =  static::selectRaw('*, sum(jumlah) as jumlah')->groupBy('tabungan_id')->where('tipe','debit')->take($perPage)->skip($perPage * ($page-1))->with('tabungan')->get('sum', 'tabungan_id');
        $kredit =  static::selectRaw('*, sum(jumlah) as jumlah')->groupBy('tabungan_id')->where('tipe','kredit')->get('sum', 'tabungan_id');

        $i = 0;
        foreach($debit as $key1 => $value1) {
            foreach($kredit as $key2 => $value2) {
                if($value1->tabungan_id == $value2->tabungan_id) {
                    $debit[$i]['jumlah'] = $value1->jumlah - $value2->jumlah;
                }
            }
            $i++;
        }

        return new LengthAwarePaginator($debit, count($countDebit), $perPage, $page, ['path' => $request->url()]);	 

     }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryHistory extends Model
{
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inventory_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['inventory_id', 'masuk', 'keluar', 'sisa', 'keterangan', 'tanggal'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     * 
     * @var array
     */
    protected $dates = [ ];

    
   
}

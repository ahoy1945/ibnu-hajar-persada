<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesertaIdentitas extends Model
{
    protected $table = 'peserta_identitas';

    protected $fillable = [
      'peserta_id',
      'no_ktp',
      'no_passpor',
      'tempat_pembuatan',
      'tgl_pembuatan',
      'tgl_expired'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefBus extends Model
{
    protected $table = 'ref_bus';

    protected $fillable = ['tipe_paket', 'nama_bus', 'max_jamaah'];
}

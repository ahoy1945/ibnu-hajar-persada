<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RefTipeKeuanganRequest;
use App\Repositories\MasterData\TipeKeuangan\TipeKeuanganRepository;
use App\Http\Controllers\Controller;

class RefTipeKeuanganController extends Controller
{

  /**
   * @var App\Repositories\MasterData\TipeKeuangan
   */
    protected $tipeKeuangan;

    public function __construct(TipeKeuanganRepository $tipeKeuangan)
    {
      $this->tipeKeuangan = $tipeKeuangan;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->tipeKeuangan->all();
        // echo "<pre>".print_r(compact('data'),1)."</pre>";
        return view('admin.master_data.tipe_keuangan.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master_data.tipe_keuangan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefTipeKeuanganRequest $request)
    {
        // echo "<pre>".print_r($request->all(),1)."</pre>";
        if(! $this->tipeKeuangan->create($request->all())) {
            return redirect('/tipe_keuangan')->with('error_message', 'Gagal menyimpan tipe keuangan');
        }

        return redirect('/tipe_keuangan')->with('success_message', 'Berhasil menyimpan tipe keuangan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = $this->tipeKeuangan->edit($id);
        // echo "<pre>".print_r($data,1)."</pre>";
        return view('admin.master_data.tipe_keuangan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RefTipeKeuanganRequest $request, $id)
    {

      $data = [
        'tipe_keuangan' => $request->all()['tipe_keuangan'],
        'nama_tipe' => $request->all()['nama_tipe'],
      ];
        if($this->tipeKeuangan->update($data, $id)) {
          return redirect('/tipe_keuangan')->with('success_message', 'Berhasil update tipe keuangan');
        } else {
          return redirect('/tipe_keuangan')->with('error_message', 'Gagal update tipe keuangan');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->tipeKeuangan->delete($id)) {
          return redirect()->back()->with('error_message', 'Gagal menghapus tipe keuangan');
        }

        return redirect()->back()->with('success_message', 'Berhasil menghapus tipe keuangan');
    }
}

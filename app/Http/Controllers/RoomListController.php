<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TanggalPemberangkatan as Date;
use Maatwebsite\Excel\Excel;
use Barryvdh\DomPDF\PDF;

use App\Repositories\DataJamaah\RoomList\RoomListRepository;

class RoomListController extends Controller
{

    /**
    * App\Repositories\Datajamaah\Setting\RoomListRepository
    */
    protected $peserta;

    protected $excel;

    protected $request;

    protected $pdf;

    public function __construct(RoomListRepository $peserta, Excel $excel, Request $request, PDF $pdf)
    {
      $this->peserta = $peserta;
      $this->excel = $excel;
      $this->request = $request;
      $this->pdf = $pdf;
      // echo "<pre>".print_r($this->request->all() ,1)."</pre>";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dataRoom = $this->peserta->all($request->all());
        $date = Date::all();
        // echo "<pre>".print_r($dataRoom ,1)."</pre>";
        // die();
        $roomList = setDataRoom($dataRoom);
        $query = $request->only('date', 'tipe');
        return view('admin.data_jamaah.room_list.index', compact('roomList', 'date', 'query'));
    }

    public function printRoomPDF()
    {
        $getData = $this->peserta->all($this->request->all());
        $data = setDataRoom($getData);
        $dateTime = $this->request->all();

        $dateDb = Date::all()->toArray();
        //jk data kosong
        if (empty($dateTime['date']) && empty($dateTime['tipe'])) {
            $dateTime['date'] = $dateDb[0]['tanggal_pemberangkatan'];
            $dateTime['tipe'] = 'Umroh';
        }
        $pdf = $this->pdf->loadView('admin.data_jamaah.room_list.room_pdf', compact('data', 'dateTime'));

        return $pdf->stream();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->peserta->edit($id);
        // echo "<pre>".print_r($data ,1)."</pre>";
        // die();
        return view('admin.data_jamaah.room_list.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( !$this->peserta->update($request->only('bus','tipe_kamar'), $id)) {
          return redirect('/room_list')->with('error_message', 'Gagal update');
        }

        return redirect('/room_list')->with('success_message', 'Berhasil update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

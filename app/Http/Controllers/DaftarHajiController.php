<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\DaftarHajiRequest;
use App\Repositories\MasterData\DaftarHaji\DaftarHajiRepository;
use App\Http\Controllers\Controller;

class DaftarHajiController extends Controller
{

  /**
   * @var App\Repositories\MasterData\daftarHaji
   */
    protected $daftarHaji;

    public function __construct(DaftarHajiRepository $daftarHaji)
    {
      $this->daftarHaji = $daftarHaji;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         $data = $this->daftarHaji->all();
         return view('admin.master_data.daftar_haji.index',compact('data'));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         return view('admin.master_data.daftar_haji.create');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(DaftarHajiRequest $request)
     {
         // echo "<pre>".print_r($request->all(),1)."</pre>";
         if(! $this->daftarHaji->create($request->all())) {
             return redirect('/daftar_haji')->with('error_message', 'Gagal menyimpan daftar haji');
         }

         return redirect('/daftar_haji')->with('success_message', 'Berhasil menyimpan daftar haji');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         $data['data'] = $this->daftarHaji->edit($id);
         // echo "<pre>".print_r($data,1)."</pre>";
         return view('admin.master_data.daftar_haji.edit', $data);
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(DaftarHajiRequest $request, $id)
     {

       $data = [
         'nama_haji' => $request->all()['nama_haji'],
         'harga_haji' => $request->all()['harga_haji'],
       ];
         if($this->daftarHaji->update($data, $id)) {
           return redirect('/daftar_haji')->with('success_message', 'Berhasil update daftar haji');
         } else {
           return redirect('/daftar_haji')->with('error_message', 'Gagal update daftar haji');
         }


     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         if(! $this->daftarHaji->delete($id)) {
           return redirect()->back()->with('error_message', 'Gagal menghapus daftar haji');
         }

         return redirect()->back()->with('success_message', 'Berhasil menghapus daftar haji');
     }
 }

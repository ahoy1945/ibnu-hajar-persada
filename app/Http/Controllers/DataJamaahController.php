<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\Peserta\PesertaRepository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;

class DataJamaahController extends Controller
{
    /**
     * @var Intervention\Image\ImageManager
     */
    protected $image;

    /**
     * @var Illuminate\Filesystem\Filesystem;
     */
    protected $filesystem;

    /**
     * @var App\Repositories\Peserta\EloquentPesertaRepository
     */
    protected $peserta;

    /**
     * Create a new instance
     */
    public function __construct(PesertaRepository $peserta, ImageManager $image, Filesystem $filesystem)
    {
        $this->peserta = $peserta;
        $this->image = $image;
        $this->filesystem = $filesystem;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dataJamaah = [];
        $page = $request->input('page') ? $request->input('page') : 1;
        $tipe = $request->input('tipe');
        $listYears = rangeYears();
        $parameters = $request->except('page');
        $parameters = array_add($parameters, 'page', $page);
        $tglPemberangkatan = tglPemberangkatanList();
        if(!empty($request->input('tgl_berangkat'))) {
            $dataJamaah = $this->peserta->getCollectionForDataJamaah($request->all());  
        }


        return view("admin.data_jamaah.data_jamaah.index-{$tipe}", compact('parameters', 'dataJamaah', 'tglPemberangkatan', 'listYears'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jamaah = $this->peserta->getItemBy('id', $id);
        $tipe = $jamaah->tipe_paket;
         
        return view("admin.data_jamaah.data_jamaah.edit-{$tipe}", compact('jamaah'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->only('no_porsi');
        $jamaah = $this->peserta->getItemBy('id', $id);

        if($request->hasFile('attach_file')) {
            $path = $this->generatePhoto($request->file('attach_file'), $jamaah);

            $input = array_add($input, 'attach_file', $path);
        }
        
        if(! \App\Peserta::find($id)->update($input)) {
            return redirect()->route('admin.data_jamaah.index', ['tipe' => $jamaah->tipe_paket])->with('error_message', 'Gagal mengupdate');
        }

        return redirect()->route('admin.data-jamaah.index', ['tipe' => $jamaah->tipe_paket])->with('success_message', 'Berhasil mengupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generatePhoto($photo, $data)
    {
        
        $filename = date('YmdHis').'-'.snake_case($data->nama_peserta).".".$this->filesystem->extension($photo->getClientOriginalName());
        $path = public_path('attachs/').$filename;
        
        $this->image->make($photo->getRealPath())->save($path);
         
        return "/attachs/".$filename;
        
    }
}

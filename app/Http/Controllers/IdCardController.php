<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Peserta;
use App\Repositories\Peserta\PesertaRepository;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\PDF;
use App\TanggalPemberangkatan;
use JavaScript;

class IdCardController extends Controller
{
    /**
     * @var Barryvdh\DomPDF\PDF
     */
    protected $pdf;

    /**
     * @var App\Repositories\Peserta\PesertaRepository
     */
    protected $jamaah;

	/**
	 * Create a new instance
	 */
    public function __construct(PesertaRepository $jamaah, PDF $pdf)
    {
        $this->jamaah = $jamaah;
        $this->pdf = $pdf;
    }

    public function create(Request $request)
    {
        $jamaah = [];
        if($request->has('keyword') || $request->has('tipe_paket') || $request->has('tgl_berangkat')) {
            $jamaah = $this->jamaah->searchIdCard($request, 'all');
        }

        $keyword = $request->input('keyword');
        $tgl_berangkat = $request->input('tgl_berangkat');
        $tipe_paket = $request->input('tipe_paket');
        $parameters = $request->all();
        $tglPemberangkatan = tglPemberangkatanList();
        $rangeYears = rangeYears();
        JavaScript::put(['parameters' => $parameters]);

        return view('admin.data_jamaah.idcard.create', compact('jamaah', 'tglPemberangkatan' , 'keyword', 'tgl_berangkat', 'tipe_paket', 'parameters', 'rangeYears'));
    }

    public function searchIdCard(Request $request)
    {
        $jamaah = $this->jamaah->searchIdCard($request);
       
        return response()->json($jamaah, 200);
    }

    public function cetak(Request $request)
    {  

        if($request->input('tipe_cetak') == "idcard") {
            $jamaah = array_chunk($this->jamaah->searchIdCard($request, 'print')->toArray(), 5);    
            $pdf = $this->pdf->setPaper('a4')->setOrientation('landscape')->loadView('admin.data_jamaah.idcard.template-idcard', compact('jamaah'));
             
        } else {

            $jamaah = array_chunk($this->jamaah->searchIdCard($request, 'print')->toArray(), 2); 
            $pdf = $this->pdf->setPaper('a4')->setOrientation('landscape')->loadView('admin.data_jamaah.idcard.template-koper', compact('jamaah'));
        }

        return $pdf->stream();     
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RefMaxKuotaRequest;
use App\Http\Controllers\Controller;
use App\RefMaxKuota as Kuota;
use App\TanggalPemberangkatan;

class RefMaxKuotaController extends Controller
{

    protected $kuota;

    protected $tglBerangkat;

    public function __construct(Kuota $kuota, TanggalPemberangkatan $tanggal)
    {
        $this->kuota = $kuota;
        $this->tglBerangkat = $tanggal;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->kuota->get();
        // echo "<pre>".print_r($data ,1)."</pre>";
        return view('admin.master_data.max_kuota.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tglBerangkat = $this->tglBerangkat->get();
        return view('admin.master_data.max_kuota.create', compact('tglBerangkat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefMaxKuotaRequest $request)
    {
        $data = $request->except('_token');
        if (! $this->kuota->create($data)) {
            return redirect()->route('admin.master_data.kuota.index')->with('error_message', 'Gagal menyimpan data');
        }

        return redirect()->route('admin.master_data.kuota.index')->with('success_message', 'Sukses menyimpan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->kuota->find($id);
        $tglBerangkat = $this->tglBerangkat->get();
        return view('admin.master_data.max_kuota.edit' , compact('data','tglBerangkat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RefMaxKuotaRequest $request, $id)
    {
        $data = $request->except('_token', '_method');
        
        if(empty($id) || is_null($id))
        {
            throw new InvalidArgumentException("Invalid ID");
            $update = false;
        }

        $kuota = $this->kuota->find($id);

        if(empty($kuota) || is_null($kuota))
        {
            throw new InvalidArgumentException("Item not found");
            $update = false;
        }

        try {

            $update = $kuota->where('id', '=', $id)->update($data);

        } catch (Exception $e) {

            throw new RuntimeException($e->getMessage(), 1);
            $update = false;
        }

        if(! $update) {
            return redirect()->route('admin.master_data.kuota.index')->with('error_message', 'Gagal update data');
        }
        return redirect()->route('admin.master_data.kuota.index')->with('success_message', 'sukses update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(is_null($id) || empty($id))
        {
            throw new InvalidArgumentException('Invalid ID');
        }

        if(! $this->kuota->where('id', $id)->first()->delete()) {
            return redirect()->route('admin.master_data.kuota.index')->with('error_message', 'Gagal menghapus data');
        }
        return redirect()->route('admin.master_data.kuota.index')->with('success_message', 'sukses menghapus data');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Requests\ProfileRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EditProfileController extends Controller
{
    protected $model;

    public function __construct(User $model) 
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo "<pre>".print_r(Auth::user() ,1)."</pre>";
        $user = Auth::user();
        return view('admin.edit_profile.index', compact('user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request, $id)
    {   
        $data = $request->except('_token','_method');
        $data['password'] = bcrypt($request->input('password'));
        if(empty($request->input('password'))) {
            $data = $request->except('_token','_method','password');
            unset($data['password']);
        }

        if(empty($id) || is_null($id)) {
          throw new InvalidArgumentException("Invalid ID");
        }

        $model = $this->model->find($id);

        if(empty($model) || is_null($model))
        {
          throw new InvalidArgumentException("Item not found");

        }


        try {

        $update = $model->where('id', '=', $id)->update($data);

        } catch (Exception $e) {

          throw new RuntimeException($e->getMessage(), 1);

        }

        //check
        if(! $update) {
            return redirect()->route('admin.profile.index')->with('error_message', 'Gagal mengupdate profile');
        }

        return redirect()->route('admin.profile.index')->with('success_message', 'Berhasil mengupdate profile');
    }
 

        // return $update;

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

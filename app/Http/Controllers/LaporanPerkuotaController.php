<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Peserta;
use App\PesertaPaket as Paket;
use App\TanggalPemberangkatan as Date;
use App\RefMaxKuota as Kuota;

class LaporanPerkuotaController extends Controller
{

    protected $peserta;

    protected $paket;

    protected $kuota;

    public function __construct(Peserta $peserta, Paket $paket, Kuota $kuota)
    {
        $this->peserta = $peserta;
        $this->paket = $paket;
        $this->kuota = $kuota;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // echo "<pre>".print_r($request->all() ,1)."</pre>";
        $tgl_berangkat = $request->input('tgl_berangkat');
        $tipe = $request->input('tipe_paket');
        $tipe_kamar = $request->input('tipe_kamar');

        $data = $this->getData($tgl_berangkat, $tipe, $tipe_kamar);
        $total = $data->count();
        $date = Date::all();

        $kuota = $this->kuota->get();  

        return view('admin.laporan_perkuota.index', compact('date', 'data', 'kuota', 'total', 'tgl_berangkat', 'tipe' ,'tipe_kamar'));
    }

    public function getData ($tgl_berangkat, $tipe, $tipe_kamar, $paginate = 10) 
    {
         $query = $this->peserta->join('peserta_identitas', 'peserta.id', '=', 'peserta_identitas.peserta_id')
                             ->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
                             ->join('ref_harga_paket', 'peserta_paket.harga_paket_id', '=', 'ref_harga_paket.id')
                             ->where(function($query) use ($tgl_berangkat, $tipe, $tipe_kamar){
                               if (!empty($tgl_berangkat)) {
                                 $query->where('peserta_paket.tgl_berangkat', $tgl_berangkat);
                               }
                               if (!empty($tipe)) {
                                 $query->where('peserta.tipe_paket', $tipe);
                               }
                               if (!empty($tipe_kamar)) {
                                 $query->where('peserta_paket.tipe_kamar', $tipe_kamar);
                               }
                             })
                             ->paginate($paginate);

        return $query;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Repositories\Role\RoleRepository;
use App\Repositories\User\UserRepository;

class UsersController extends Controller
{
    /**
     * App\Role
     */
    protected $role;

    /**
     * App\User
     */
    protected $user;
    
    /**
     * Create a new instance
     * 
     * @param Role $role 
     * @param User $user 
     */
    public function __construct(UserRepository $user, RoleRepository $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->role->all();
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if(! $this->user->create($request->except('_token'))) {
            return redirect()->route('admin.users.index')->with('error_message', 'Gagal menyimpan');
        }

        return redirect()->route('admin.users.index')->with('success_message', 'Berhasil menyimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->getItemBy('id', $id);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_token', '_method', 'newpassword');

        if(!empty($request->input('password'))) {
            $input = array_add($input, 'password', $request->input('password'));
        }

        if(! \App\User::find($id)->update($input)) {
            return redirect()->route('admin.users.index')->with('error_message', 'User gagal diupdate');
        }

        return redirect()->route('admin.users.index')->with('success_message', 'User berhasil diupdate');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->user->delete($id)) {
            return redirect()->back()->with('error_message', 'User gagal di delete');
        }

        return redirect()->back()->with('success_message', 'User berhasil di delete');

    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\Keuangan\EloquentKeuanganRepository;
use App\Repositories\Peserta\EloquentPesertaRepository;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

class DashboardController extends Controller
{
    /**
     * @var App\Keuangan
     */
    protected $keuangan;
    
    /**
     * @var App\Repositories\Peserta\EloquentPesertaRepository;
     */
    protected $peserta;

    /**
     * @var  excel
     */
    protected $excel;

    /**
     * Create a new instance
     *
     */
    public function __construct(EloquentPesertaRepository $peserta, EloquentKeuanganRepository $keuangan, Excel $excel)
    {
        $this->peserta = $peserta;
        $this->keuangan = $keuangan;
        $this->excel = $excel;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jamaahPerbulan = $this->peserta->totalJamaahPerbulan();
        $jamaahPertahun = $this->peserta->totalJamaahPertahun();
        $jamaahHajiPertahun = $this->peserta->totalJamaahHajiPertahun();
        $totalPemasukan = $this->keuangan->totalPemasukanPerbulan();
        $totalPemasukanPerTahun = $this->keuangan->totalPemasukanPerTahun();
        $totalPengeluaranPerbulan = $this->keuangan->totalPengeluaranPerbulan();
        $totalPengeluaranPerTahun = $this->keuangan->totalPengeluaranPerTahun();
      
        return view('admin.dashboard.index', compact('jamaahPerbulan', 'totalPemasukan','jamaahHajiPertahun', 'jamaahPertahun', 'totalPemasukanPerTahun', 'totalPengeluaranPerbulan', 'totalPengeluaranPerTahun'));
    }

    /**
     * [exportPemasukanPerbulan description]
     * @return [excel] [description]
     */
    public function exportPemasukanPerbulan()
    {
        $data = $this->keuangan->totalPemasukanPerbulan();
        $this->incomePerbulan($data, 'Pemasukan Perbulan');
    }

    public function exportPengeluaranPerbulan()
    {
        $data = $this->keuangan->totalPengeluaranPerbulan();
        $this->incomePerbulan($data, 'Pengeluaran Perbulan');
    }    

    public function exportPemasukanPertahun()
    {
        $data = $this->keuangan->totalPemasukanPertahun();
        $this->incomePertahun($data, 'Pemasukan Pertahun');
    }    

    public function exportPengeluaranPertahun()
    {
        $data = $this->keuangan->totalPengeluaranPertahun();
        $this->incomePertahun($data, 'Pengeluaran Pertahun');
    }

    public function exportJamaahPerbulan()
    {
        $data = $this->peserta->totalJamaahPerbulan();
        $this->totalJamaah($data, 'Bulan', 'Jamaah Perbulan');
    }

    public function exportJamaahPertahun()
    {
        $data = $this->peserta->totalJamaahPertahun();
        $this->totalJamaah($data, 'Tahun', 'Jamaah Pertahun');
    }

    /**
     * [pemasukanPengelualaranPerbulan description]
     * @param  [array] $data  [description]
     * @param  [string] $title [description]
     * @return [excel report]        [description]
     */
    protected function incomePerbulan ($data, $title)
    {

        $this->excel->create($title, function($excel) use ($data, $title) {
          $excel->sheet('Data Keuangan', function($sheet) use ($data, $title) {
              $row = 2;
              $no = 1;

              $sheet->mergeCells("A1:C1");

              $sheet->row(1, [
                      $title
                  ]);
              $sheet->setHeight(1, 30);

              $sheet->row($row, array(
                  'No',
                  'Bulan',
                  'Nilai (Rp)',
              ));

                foreach($data as $key => $value) {

                    $sheet->row(++$row, [
                            $no,
                            monthToString(substr($key,5, 6))." ".substr($key,0, 4),
                            number_format($value,0,",","."),
                          ]);

                    $no++;
                }

                $sheet->row($no+2, [
                    $no,
                    'Total',
                    number_format(array_sum($data),0,",","."),
                ]);

               $sheet->setBorder("A1:C{$row}", 'thin');

               $sheet->cells("A1:C{$row}", function($cells) {
                      $cells->setAlignment('right');
                      $cells->setValignment('middle');
                });


          });
        })->export('xlsx');
    } 

    /**
     * [pemasukanPengelualaranPerbulan description]
     * @param  [array] $data  [description]
     * @param  [string] $title [description]
     * @return [excel report]        [description]
     */
    protected function incomePertahun ($data, $title)
    {

        $this->excel->create($title, function($excel) use ($data, $title) {
          $excel->sheet('Data Keuangan', function($sheet) use ($data, $title) {
              $row = 2;
              $no = 1;

              $sheet->mergeCells("A1:C1");

              $sheet->row(1, [
                      $title
                  ]);
              $sheet->setHeight(1, 30);

              $sheet->row($row, array(
                  'No',
                  'Tahun',
                  'Nilai (Rp)',
              ));

                foreach($data as $key => $value) {

                    $sheet->row(++$row, [
                            $no,
                            monthToString(substr($key,5, 6))." ".substr($key,0, 4),
                            number_format($value,0,",","."),
                          ]);

                    $no++;
                }

               $sheet->setBorder("A1:C{$row}", 'thin');

               $sheet->cells("A1:C{$row}", function($cells) {
                      $cells->setAlignment('right');
                      $cells->setValignment('middle');
                });


          });
        })->export('xlsx');
    }

    protected function totalJamaah($data, $tipe, $title) {
            
            $this->excel->create($title, function($excel) use ($data, $title, $tipe) {
            $excel->sheet('Data Keuangan', function($sheet) use ($data, $title, $tipe) {
              $row = 2;
              $no = 1;

              $sheet->mergeCells("A1:F1");

              $sheet->row(1, [
                      $title
                  ]);
              $sheet->setHeight(1, 30);

              $sheet->row($row, array(
                  'No',
                  $tipe,
                  'Umroh (langsung)',
                  'Haji (langsung)',                  
                  'Umroh (agen)',
                  'Haji (agen)',
              ));

                foreach($data as $key => $value) {

                    $sheet->row(++$row, [
                            $no,
                            monthToString(substr($key,5, 6))." ".substr($key,0, 4),
                            $value['umroh']['langsung'],
                            $value['haji']['langsung'],
                            $value['umroh']['agen'],
                            $value['haji']['agen'],
                          ]);

                    $no++;
                }

               $sheet->setBorder("A1:F{$row}", 'thin');

               $sheet->cells("A1:F{$row}", function($cells) {
                      $cells->setAlignment('right');
                      $cells->setValignment('middle');
                });


          });
        })->export('xlsx');
    }

}

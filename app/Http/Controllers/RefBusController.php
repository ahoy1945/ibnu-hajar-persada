<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RefBusRequest;
use App\Http\Controllers\Controller;
use App\RefBus as Bus;

class RefBusController extends Controller
{

    protected $bus;


    public function __construct(Bus $bus)
    {
        $this->bus = $bus;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->bus->get();
        // echo "<pre>".print_r($data ,1)."</pre>";
        return view('admin.master_data.bus.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master_data.bus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefBUsRequest $request)
    {
        $data = $request->except('_token');
        if (! $this->bus->create($data)) {
            return redirect()->route('admin.master_data.bus.index')->with('error_message', 'Gagal menyimpan data');
        }

        return redirect()->route('admin.master_data.bus.index')->with('success_message', 'Sukses menyimpan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->bus->find($id);
        return view('admin.master_data.bus.edit' , compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RefBUsRequest $request, $id)
    {
        $data = $request->except('_token', '_method');
        
        if(empty($id) || is_null($id))
        {
            throw new InvalidArgumentException("Invalid ID");
            $update = false;
        }

        $bus = $this->bus->find($id);

        if(empty($bus) || is_null($bus))
        {
            throw new InvalidArgumentException("Item not found");
            $update = false;
        }

        try {

            $update = $bus->where('id', '=', $id)->update($data);

        } catch (Exception $e) {

            throw new RuntimeException($e->getMessage(), 1);
            $update = false;
        }

        if(! $update) {
            return redirect()->route('admin.master_data.bus.index')->with('error_message', 'Gagal update data');
        }
        return redirect()->route('admin.master_data.bus.index')->with('success_message', 'sukses update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(is_null($id) || empty($id))
        {
            throw new InvalidArgumentException('Invalid ID');
        }

        if(! $this->bus->where('id', $id)->first()->delete()) {
            return redirect()->route('admin.master_data.bus.index')->with('error_message', 'Gagal menghapus data');
        }
        return redirect()->route('admin.master_data.bus.index')->with('success_message', 'sukses menghapus data');
    }
}

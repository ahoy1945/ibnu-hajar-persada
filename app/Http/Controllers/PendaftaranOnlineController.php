<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManager;
use Illuminate\Http\Request;
use JavaScript;

use App\Http\Controllers\Controller;
use App\Http\Requests\PesertaOnlineRequest;
use App\Http\Requests\PesertaRequest;
use App\HargaPaket;
use App\TanggalPemberangkatan;
use App\RefPesawat;
use App\RefDokumen;
use App\RefBus;
use App\Provinsi;
use App\Kecamatan;
use App\Kabupaten;
use App\Desa;
use App\Repositories\PesertaOnline\PesertaOnlineRepository;
use Illuminate\Filesystem\Filesystem;


class PendaftaranOnlineController extends Controller
{
  /**
   * @var App\RefDokumen
   */
  protected $refDokumen;

  /**
   * App\RefBus
   * @var [type]
   */
  protected $bus;
  
  /**
   * @var App\RefPesawat
   */
  protected $pesawat;
  
  /**
   * @var Intervention\Image\ImageManager
   */
  protected $image;

  /**
   * @var App\TanggalPemberangkatan
   */
  protected $tanggalPemberangkatan;

  /**
   * @var App\Repositories\Peserta\PesertaOnlineRepository
   */
  protected $peserta;

  /**
   * @var App\HargaPaket
   */
  protected $hargaPaket;


  protected $filesystem;


  /**
   * @param PesertaOnlineRepository $peserta
   */
  public function __construct(PesertaOnlineRepository $peserta, 
    HargaPaket $hargaPaket, 
    TanggalPemberangkatan $tanggalPemberangkatan,
    ImageManager $image, 
    RefPesawat $pesawat, 
    RefBus $bus, 
    RefDokumen $refDokumen,
    Filesystem $filesystem)
  {
    $this->peserta = $peserta;
    $this->hargaPaket = $hargaPaket;
    $this->tanggalPemberangkatan = $tanggalPemberangkatan;
    $this->image = $image;
    $this->pesawat = $pesawat;
    $this->bus = $bus;
    $this->refDokumen = $refDokumen;
    $this->filesystem = $filesystem;
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $page = ($request->input('page') ? $request->input('page') : 1 );

      $pendaftar = $this->peserta->all();
      $hargapaket = $this->hargaPaket->all();
      
      return view('admin.peserta.pendaftar-online', compact('pendaftar', 'hargapaket', 'page'));
    }

    public function pendaftarSearch(Request $request)
    {

      $pendaftar = $this->peserta->search($request->input('keyword'));
      $page = ($request->input('page') ? $request->input('page') : 1);
      $keyword = $request->input('keyword');
      $hargapaket = $this->hargaPaket->all();

      return view('admin.peserta.pendaftar-online', compact('pendaftar', 'keyword', 'hargapaket', 'page'));
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $pemberangkatan = $this->tanggalPemberangkatan->with('paket')->where('tanggal_pemberangkatan', '>=', date('Y-m-d'))->get();
      $dokumen =  $this->refDokumen->all();
      $pesawat = $this->pesawat->all();
      JavaScript::put(['tgl_berangkat' => $pemberangkatan, 'dokumen' => $this->refDokumen->all()]);
      $online = true;

      return view('admin.peserta.create-online', compact('pesawat', 'online'));
    }


    public function store(PesertaOnlineRequest $request)
    { 
       
      $input = $request->except('_token', 'harga_paket_id', 'tgl_berangkat', 'photo');
      $splitPaketId = explode(',', $request->input('harga_paket_id'));
      
      if($request->hasFile('photo')) {
          $urlPhoto = $this->generatePhoto($request->file('photo')); 
          $input = array_add($input, 'photo', $urlPhoto);
      }
      

      $input = array_add($input, 'harga_paket_id', $splitPaketId[0]);
      $input = array_add($input, 'tgl_berangkat', $splitPaketId[1]);

      $store = $this->peserta->create($input);

      if($store) return redirect()->back()->with('error_message', 'Gagal menyimpan');

      return redirect()->back()->with('success_message', 'Berhasil mendaftar'); 
    }

    public function searchMahram(Request $request)
    {
      $data = $this->peserta->searchMahram($request->input('keyword'));
      return response()->json($data, 200);
    }

    public function destroy($id)
    {
      if(! $this->peserta->delete($id)) {
          return redirect()->back()->with('error_message', 'Gagal menghapus');
      }

      return redirect()->back()->with('success_message', 'Berhasil menghapus');
    }

    public function show($id)
    {
      
      $peserta = $this->peserta->find($id)->toArray();
      // echo "<pre>".print_r($data ,1)."</pre>";
      return view('admin.peserta.show-pendaftar-online', compact('peserta'));
    }

    public function search(Request $request)
    {
      $data = $this->peserta->searchPeserta($request->input('keyword'));

      return response()->json($data, 200);
    }

    public function generatePhoto($photo)
    {
        $filename = date('YmdHis').'-'. $photo->getClientOriginalName();
        $path = public_path('attachs/').$filename;
        
        $this->image->make($photo->getRealPath())
              ->resize(116, 156)
              ->save($path);

        return "/attachs/".$filename;
        
    }

  public function move($id)
  {
    $pesawat = $this->pesawat->all();
    $value = $this->peserta->getItemBy('id', $id);
    $pemberangkatan = $this->tanggalPemberangkatan->with('paket')->where('tanggal_pemberangkatan', '>=', date('Y-m-d'))->get();
    $pendaftaranOnline = true;

    JavaScript::put([
        'jamaah' =>  $value,
        'tgl_berangkat' => $pemberangkatan,
        'dokumen' => $this->refDokumen->all(),
    ]);
    
    return view('admin.peserta.edit', compact('value', 'pesawat', 'id', 'pendaftaranOnline'));
  }

  public function moveStore(PesertaRequest $request)
  {

    $input = $request->except('_token', 'harga_paket_id', 'tgl_berangkat', 'photo');
    $splitPaketId = explode(',', $request->input('harga_paket_id'));

    $input = array_add($input, 'harga_paket_id', $splitPaketId[0]);
    $input = array_add($input, 'tgl_berangkat', $splitPaketId[1]);
    

    if($request->hasFile('photo')) {
        //delete old photo
        $jamaah =  $this->peserta->find($request->input('id'));
        if($this->filesystem->exists(public_path().$jamaah->photo)) {
             $this->filesystem->delete(public_path().$jamaah->photo);
        }

        $urlPhoto = $this->generatePhoto($request->file('photo')); 
        $input = array_add($input, 'photo', $urlPhoto);
    }
    
    $move = $this->peserta->moveToPeserta($input);
    
    if(! $this->peserta->deleteOldData($request->input('id'))) {
        return redirect()->route('admin.daftar.online.index')->with('error_message', 'Gagal hapus data lama');
    }
    return redirect()->route('admin.daftar.online.index')->with('success_message', 'Sukses komfirmasi data');
  }


}

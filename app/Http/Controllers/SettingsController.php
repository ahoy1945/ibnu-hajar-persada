<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Setting;

class SettingsController extends Controller
{
	/**
	 * @var App\Setting
	 */
	protected $setting;

	/**
	 * Create a new instance
	 * 
	 * @param Setting $setting 
	 */
	public function __construct(Setting $setting)
	{
		$this->setting = $setting;
	}

	/**
	 * @return response
	 */
    public function index()
    {
    	$settings =  $this->setting->all();

    	return view('admin.settings.index', compact('settings'));
    }

    public function store(Request $request)
    {
    	 
    	for($i = 0; $i < count($request->input('id')); $i++) {
    		 
    		$this->setting->find($request->input('id')[$i])->update(['meta_value' => $request->input('meta_value')[$i]]);
    	}	

    	return redirect()->back()->with('success_message', 'Terupdate');

    }
}

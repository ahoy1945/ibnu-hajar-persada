<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\HargaPaketRequest;
use App\Repositories\MasterData\HargaPaket\HargaPaketRepository;
use App\Http\Controllers\Controller;

class HargaPaketController extends Controller
{

  /**
   * @var App\Repositories\MasterData\HargaPaket
   */
    protected $hargaPaket;

    /**
     * Create a new instance
     * 
     * @param HargaPaketRepository $hargaPaket [description]
     */
    public function __construct(HargaPaketRepository $hargaPaket)
    {
      $this->hargaPaket = $hargaPaket;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->hargaPaket->all();
        // echo "<pre>".print_r(compact('data'),1)."</pre>";
        return view('admin.master_data.harga_paket.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master_data.harga_paket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HargaPaketRequest $request)
    {
        $dataPakethotel = $this->arraySortPaketHotel($request->input('kamar_harga'), $request->input('kamar_nama') );
        
        if(! $inserted =  $this->hargaPaket->create($request->all())) {
             

            return redirect('/harga_paket')->with('error_message', 'Gagal menyimpan harga paket');
        }

        foreach($dataPakethotel as $createDataPakethotel) {

                $inserted->hotel()->create(['kamar' => $createDataPakethotel['kamar'], 'harga' => $createDataPakethotel['harga']]);
        }

        return redirect('/harga_paket')->with('success_message', 'Berhasil menyimpan harga paket');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paket = \App\HargaPaket::whereId($id)->with('hotel')->first();

        return view('admin.master_data.harga_paket.edit', compact('paket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HargaPaketRequest $request, $id)
    {
       
      $data = [
        'tipe_paket' => $request->all()['tipe_paket'],
        'nama_paket' => $request->all()['nama_paket'],
        'harga_paket' => $request->all()['harga_paket']
      ];

       $dataPakethotel = $this->arraySortPaketHotel($request->input('kamar_harga'), $request->input('kamar_nama') );
  
        if($this->hargaPaket->update($data, $id)) {
            $paket = \App\HargaPaket::whereId($id)->with('hotel')->first();
            $paket->hotel()->delete();      

            foreach($dataPakethotel as $createDataPakethotel) {

                 $paket->hotel()->create(['kamar' => $createDataPakethotel['kamar'], 'harga' => $createDataPakethotel['harga']]);
            }


          return redirect('/harga_paket')->with('success_message', 'Berhasil update harga paket');
        } 
        
        return redirect('/harga_paket')->with('error_message', 'Gagal update harga paket');
        


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->hargaPaket->delete($id)) {
          return redirect()->back()->with('error_message', 'Gagal menghapus harga paket');
        }

        return redirect()->back()->with('success_message', 'Berhasil menghapus harga paket');
    }

    public function apiPaket(Request $request) {
        $paket = $this->hargaPaket->getCollectionBy('tipe_paket', $request->input('type'));

        return response()->json($paket, 200);
    }

    private function arraySortPaketHotel($kamarHarga = [], $kamarNama = [])
    {
        $data = [];
        $i = 0;
       
        foreach($kamarHarga as $key1 => $value1) {

            foreach($kamarNama as $key2 => $value2) {
                if($key1 == $key2) {
                    $data[$i]['kamar'] = $value2;
                    $data[$i]['harga'] = $value1;
                    $i++;
                    
                }
            }
        }

        return $data;

    }
}

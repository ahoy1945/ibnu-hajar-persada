<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\DaftarUmrahRequest;
use App\Repositories\MasterData\DaftarUmrah\DaftarUmrahRepository;
use App\Http\Controllers\Controller;

class DaftarUmrahController extends Controller
{

  /**
   * @var App\Repositories\MasterData\daftarUmrah
   */
    protected $daftarUmrah;

    public function __construct(DaftarUmrahRepository $daftarUmrah)
    {
      $this->daftarUmrah = $daftarUmrah;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         $data = $this->daftarUmrah->all();
         return view('admin.master_data.daftar_umrah.index',compact('data'));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         return view('admin.master_data.daftar_umrah.create');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(DaftarUmrahRequest $request)
     {
         // echo "<pre>".print_r($request->all(),1)."</pre>";
         if(! $this->daftarUmrah->create($request->all())) {
             return redirect('/daftar_umrah')->with('error_message', 'Gagal menyimpan daftar umrah');
         }

         return redirect('/daftar_umrah')->with('success_message', 'Berhasil menyimpan daftar umrah');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         $data['data'] = $this->daftarUmrah->edit($id);
         // echo "<pre>".print_r($data,1)."</pre>";
         return view('admin.master_data.daftar_umrah.edit', $data);
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(DaftarUmrahRequest $request, $id)
     {

       $data = [
         'nama_umrah' => $request->all()['nama_umrah'],
         'harga_umrah' => $request->all()['harga_umrah'],
       ];
         if($this->daftarUmrah->update($data, $id)) {
           return redirect('/daftar_umrah')->with('success_message', 'Berhasil update daftar umrah');
         } else {
           return redirect('/daftar_umrah')->with('error_message', 'Gagal update daftar umrah');
         }


     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         if(! $this->daftarUmrah->delete($id)) {
           return redirect()->back()->with('error_message', 'Gagal menghapus daftar umrah');
         }

         return redirect()->back()->with('success_message', 'Berhasil menghapus daftar umrah');
     }
 }

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Desa;
use App\Kabupaten;
use App\Provinsi;
use App\Kecamatan;

class LokasiController extends Controller
{
    protected $kecamatan;

    protected $desa;

    protected $provinsi;

    protected $kabupaten;

    public function __construct(Request $request, Kabupaten $kabupaten, Desa $desa, Provinsi $provinsi, Kecamatan $kecamatan)
    {
    	$this->kecamatan = $kecamatan;
    	$this->desa = $desa;
    	$this->provinsi = $provinsi;
    	$this->kabupaten = $kabupaten;
    	$this->request = $request;
    }

    public function desa($id)
    {
    	$keyword = $this->request->input('keyword');

    	$data = $this->desa->where('district_id', $id)->where('name', 'LIKE', "%{$keyword}%")->get();

    	return response()->json($data, 200);
    }

    public function kecamatan($id)
    {
    	$data = $this->kecamatan->where('regency_id', $id)->get();

    	return response()->json($data, 200);
    }

    public function kabupaten($id)
    {
    	$data = $this->kabupaten->where('province_id', $id)->get();

    	return response()->json($data, 200);
    }

    public function provinsi()
    {
    	$data = $this->provinsi->all();

    	return response()->json($data, 200);
    }
}

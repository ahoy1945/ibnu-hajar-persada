<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\AgenRequest;
use App\Repositories\Agen\AgenRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;

class AgenController extends Controller
{

    /**
     * App\Repositories\User\UserRepository
     */
    protected $user;

    /**
     * @var App\Repositories\Agen\AgenRepository
     */
    protected $agen;

    /**
     * create a new instance
     */
    public function __construct(AgenRepository $agen,UserRepository $user)
    {
        $this->agen = $agen;
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $agens =  $this->agen->all();
        $page = ($request->input('page') ? $request->input('page') : "1");
        
        return view('admin.agen.index', compact('agens', 'page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.agen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgenRequest $request)
    {
        $store = $this->agen->create($request->all());

        if(! $store) {
            return redirect()->route('admin.agen.index')->with('error_message', 'Gagal menyimpan');   
        }

        return redirect()->route('admin.agen.index')->with('success_message', 'Berhasil menyimpan'); 



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if( ! $agen = $this->user->getItemBy('id', $id)) {
            return redirect()->back()->with('error_message', 'Agen tidak ditemukan');
        }
       
        return view('admin.agen.show', compact('agen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $agen = \App\User::with('agen')->find($id);

        return view('admin.agen.edit', compact('agen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $input = $request->all();

        $userInput = ['name' => $input['name'], 'email' => $input['email']];
        if(!empty($input['password'])) {
            
            $userInput = array_add($userInput, 'password', $input['password']);
        }
        
        $agenInput = [
            'no_hp' => $input['no_hp'],
            'no_rek' => json_encode(['no_rek' => $input['no_rek'], 'bank' => $input['bank'], 'nama_rek' => $input['nama_rek']]),
            'alamat' => $input['alamat'],
            'desa' => $input['desa'],
            'kecamatan' => $input['kecamatan'],
            'kabupaten' => $input['kabupaten'],
            'provinsi' => $input['provinsi']

        ];


        $user = \App\User::with('agen')->find($id);
        $userUpdate = $user->update($userInput);
        $agenUpdate = $user->agen()->update($agenInput);

        if(! $userUpdate) {
            return redirect()->route('admin.agen.index')->with('error_message', 'Gagal mengupdate agen'); 
        }

        return redirect()->route('admin.agen.index')->with('success_message', 'Berhasil mengupdate agen'); 



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->user->getItemBy('id', $id)->delete()) {
            return redirect()->back()->with('error_message', 'Gagal menghapus');
        }

        return redirect()->back()->with('success_message', 'Berhasil menghapus');
    }
}

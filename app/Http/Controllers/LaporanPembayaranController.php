<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\PesertaPembayaran;
use App\Repositories\PesertaPembayaran\PesertaPembayaranRepository;
use DB;
use Maatwebsite\Excel\Excel;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\PDF;
use JavaScript;

class LaporanPembayaranController extends Controller
{
     /**
     * @var Barryvdh\DomPDF\PDF
     */
    protected $pdf;
    

	/**
	 * @var PesertaPembayaranRepository
	 */
	protected $pesertaPembayaran;

    /**
     * @var Maatwebsite\Excel\Excel
     */
    protected $excel;

	/**
	 * Create a new instance
	 * 
	 * @param PesertaPembayaranRepository $pesertaPembayaran [description]
	 */
    public function __construct(PesertaPembayaranRepository $pesertaPembayaran, Excel $excel, PDF $pdf)
    {
    	$this->pesertaPembayaran = $pesertaPembayaran;
        $this->pdf  = $pdf;
        $this->excel = $excel; 
    }

    public function index(Request $request)
    {   
        $page = ($request->input('page') ? $request->input('page') : 1);
        $parameters = $request->all();
        $parameters = array_add($parameters, 'page', $page);
        $tglPemberangkatan = tglPemberangkatanList();   
    	$jamaah =  $this->pesertaPembayaran->laporanUmmuAhmadPrint($request);
        $rangeYears = rangeYears();  
        JavaScript::put(['parameters' => $parameters]);  
        //return $jamaah;
    	return view("admin.pembayaran.laporan-{$request->input('tipe')}", compact('jamaah', 'parameters', 'tglPemberangkatan', 'rangeYears'));
    }

    public function printUmmuAhmad(Request $request)
    {
        $jamaah =  $this->pesertaPembayaran->laporanUmmuAhmadPrint($request);        
        $parameters = $request->all();
        $settings = \App\Setting::all();
        $pdf = $this->pdf->setOrientation('landscape')->loadView('admin.pembayaran.laporan-ummuahmad-print', compact('jamaah', 'parameters', 'settings'));
        return $pdf->stream('laporan-ummu-ahmad.pdf');
    }

    public function exportExcel(Request $request)
    {   
         $data = $this->keuangan->export($this->request->all())->toArray();
        $last = (count($data)-1);

        $this->excel->create("Laporan Keuangan {$this->request->input('from')} {$this->request->input('to')}", function($excel) use ($data, $last) {
            $excel->sheet('Data Keuangan', function($sheet) use ($data, $last) {
                $row = 2;
                $no = 1;

                $sheet->mergeCells("A1:F1");

                $sheet->row(1, [
                        "Laporan Keuangan {$this->request->input('from')} {$this->request->input('to')}"
                    ]);
                $sheet->setHeight(1, 30);

                $sheet->row($row, array(
                    'No',
                    'Tanggal',
                    'Keterangan',
                    'Debit(Rp)',
                    'kredit(Rp)',
                    'Saldo(Rp)'
                ));

                foreach($data as $key => $value) {
                    $sum = 0;
                    for ($j=$last; $j >= $key ; $j--) {
                          $sum += ($data[$j]['debit'] - $data[$j]['kredit']);
                    }

                    $sheet->row(++$row, [
                            $no,
                            Date::parse($value['tanggal'])->format('d M Y'),
                            $value['keterangan'],
                            $value['debit'],
                            $value['kredit'],
                            $sum
                        ]);

                    $no++;
                }

                 $sheet->setBorder("A1:F{$row}", 'thin');

                 $sheet->cells("A1:F{$row}", function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('middle');
                    });


            });
        })->export('xlsx');
    }

}

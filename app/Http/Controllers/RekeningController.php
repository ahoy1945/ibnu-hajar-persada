<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\RekeningRequest;
use App\PesertaPaket;
use App\Rekening;
use Illuminate\Http\Request;

class RekeningController extends Controller
{
    /**
     * @var App\Rekening
     */
    protected $rekening;

    /**
     * @var App\PesertaPaket
     */
    protected $pesertaPaket;

    /**
     * Create a new instancce
     * 
     * @param  Rekening $rekening
     */
    public function __construct(Rekening $rekening, PesertaPaket $pesertaPaket)
    {
        $this->rekening = $rekening;
        $this->pesertaPaket = $pesertaPaket;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rekenings = $this->rekening->orderBy('id', 'DESC')->paginate(10);

        return view('admin.master_data.rekening.index', compact('rekenings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master_data.rekening.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RekeningRequest $request)
    {
        if(! $this->rekening->create($request->all())) {
            return redirect()->route('admin.rekening.index')->with('error_message', 'Gagal membuat data');
        }

        return redirect()->route('admin.rekening.index')->with('success_message', 'Berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rekening = $this->rekening->find($id);
        
        return view('admin.master_data.rekening.edit', compact('rekening'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RekeningRequest $request, $id)
    {
        $data = $request->except('_token');

        if(! $this->rekening->find($id)->update($data)) {
            return redirect()->route('admin.rekening.index')->with('error_message', 'Gagal mengupdate');
        }

        return redirect()->route('admin.rekening.index')->with('success_message', 'Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->rekening->find($id)->delete()) {
            return redirect()->back()->with('error_message', 'Gagal menghapus');
        }

        return redirect()->back()->with('success_message', 'Berhasil menghapus');
    }

    public function laporanSaldo(Request $request)
    {
        $input = $request->all();
        $tglPemberangkatan = tglPemberangkatanList();
        $saldoRekening = $this->saldoRekening($request);
        
        return view('admin.master_data.rekening.laporansaldo', compact('tglPemberangkatan', 'input', 'saldoRekening'));
    }

    public function saldoRekening($request)
    {
        $newData = [];
        $totalUsd = 0;
        $totalIdr = 0;
        $data =  \App\Rekening::with(['pembayaran' => function($query) use ($request)   {
            $query->with(['peserta' => function($query) use ($request) {
                $query->with(['paket' => function($query) use ($request) {
                    
                }]);
            }]);
        }])->get(); 

         
        if(!empty($data)) {
            foreach ($data as $key => $value) {
            
            $newData[$key] = $value;
            $newData[$key]['jumlah'] = 0;

            if(!empty($value->pembayaran->toArray())) {
                foreach($value->pembayaran as $keyPembayaran => $valuePembayaran) {
                    
                   if($valuePembayaran->peserta->paket->tgl_berangkat == $request->input("tanggal")) {
                        $newData[$key]['jumlah'] += $valuePembayaran->jumlah;
                   }
                    
                }
             }

            if($value->mata_uang == "usd") {
                $totalUsd += $value->jumlah;
            } else {
                $totalIdr += $value->jumlah;
            }  
                
          }    
        }

        return ['data' => $newData, 'totalUsd' => $totalUsd, 'totalIdr' => $totalIdr];       
        
    }


}

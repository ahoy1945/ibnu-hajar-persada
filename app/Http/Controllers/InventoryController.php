<?php

namespace App\Http\Controllers;

use App\Http\Requests\InventoryRequest;
use App\InventoryHistory;
use App\Repositories\Inventory\InventoryRepository;
use App\Repositories\Peserta\PesertaRepository;
use Barryvdh\DomPDF\PDF;
use App\Setting;
use Illuminate\Http\Request;
use App\TanggalPemberangkatan as Date;
use Maatwebsite\Excel\Excel;


class InventoryController extends Controller
{

    /**
     * @var App\Repositories\Inventory\InventoryRepository
     */
    protected $inventory;

    /**
     * @var App\InventoryHistory
     */
    protected $inventoryHistory;
    
    /**
     * @var App\HargaPaket
     */
    protected $hargaPaket;

    /**
     * @var App\Setting
     */
    protected $settings;
    
    /**
     * @var Barryvdh\DomPDF\PDF
     */
    protected $pdf;
    
    /**
     * App\Repositories\Peserta\PesertaRepository
     */
    protected $peserta;


    protected $tanggal;

    protected $excel;

    protected $export;

    public function __construct(
        InventoryRepository $inventory,
        InventoryHistory $inventoryHistory,
        PesertaRepository $peserta,
        PDF $pdf,
        Setting $settings,
        Request $request,
        Excel $excel)
    {
        $this->inventory             = $inventory;
        $this->inventoryHistory      = $inventoryHistory;
        $this->peserta               = $peserta;
        $this->pdf                   = $pdf;
        $this->settings              = $settings;
        $this->tanggal               = $request->input('tanggal');  
        $this->export               = $request->input('excel');  
        $this->excel                 = $excel;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = $this->inventory->all();
        
        return view('admin.inventory.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.inventory.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InventoryRequest $request)
    {
        if(! $this->inventory->create($request->all())) {
            return redirect()->route('admin.inventory.index')->with('error_message', 'Gagal menyimpan barang');
        }

        return redirect()->route('admin.inventory.index')->with('success_messag', 'Berhasil menyimpan barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tanggal = $this->tanggal;
        $tanggalAwal = $tanggal."-01";
        $tanggalAkhir = $tanggal."-31";
        $namaBarang = $this->inventory->getItemBy('id', $id)->nama;

        $cekInventories = $this->inventoryHistory->where('inventory_id', $id)
        ->whereBetween('tanggal', [$tanggalAwal, $tanggalAkhir])->get()->toArray();

        if (!isset($this->tanggal) || empty($this->tanggal)) {
            $inventories = $this->inventoryHistory->where('inventory_id', $id)->orderBy('id', 'DESC')->paginate(10);
        } else {
            $inventories = [];
            if(!empty($cekInventories)) {
                $inventories = $this->inventoryHistory->whereBetween('tanggal', [$tanggalAwal, $tanggalAkhir])
                ->where('inventory_id', $id)->orderBy('id', 'DESC')->paginate(10);
            }
        }

        //if export excel
        if ($this->export == '1') {
            $this->exportHistory($inventories, $namaBarang);
        }

        return view('admin.inventory.show', compact('inventories', 'id', 'tanggal'));
    }

    public function exportHistory ($data, $namaBarang)
    {
        $this->excel->create("History Inventory $namaBarang", function($excel) use ($data, $namaBarang) {
          $excel->sheet('History Inventory', function($sheet) use ($data, $namaBarang) {
              $row = 2;
              $no = 1;

              $sheet->mergeCells("A1:F1");

              $sheet->row(1, [
                      "Data History $namaBarang"
                  ]);
              $sheet->setHeight(1, 30);

              $sheet->row($row, array(
                  'No',
                  'Tanggal',
                  'Masuk',
                  'Keluar',
                  'Sisa',
                  'Keterangan'
              ));

              foreach($data as $key => $value) {
                  $sheet->row(++$row, [
                          $no,
                          formatDate($value->tanggal),
                          $value->masuk,
                          $value->keluar,
                          $value->sisa,
                          $value->keterangan
                      ]);

                  $no++;
              }

               $sheet->setBorder("A1:F{$row}", 'thin');

               $sheet->cells("A1:F{$row}", function($cells) {
                      $cells->setAlignment('left');
                      $cells->setValignment('middle');
                  });


          });
      })->export('xlsx');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = \App\Inventory::find($id);
        
        return view('admin.inventory.edit', compact('barang'));     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipe_input = $request->input('tipe_input'); 
        if(isset($tipe_input)) {
            $input = $request->only('nama', 'ket');
            if(\App\Inventory::find($id)->update($input)) {
                return redirect()->route('admin.inventory.index')->with('success_message', 'Data berhasil diupdate');
            }

            return redirect()->route('admin.inventory.index')->with('error_message', 'Data gagal diupdate');

        }

        if($request->has('type')) {
           if( $this->inventory->updateStok($id, $request->all()) ) {
                return response(200);
           }
        }


    }

    public function updateNormal($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->inventory->delete($id)) {
            return redirect()->back()->with('error_message', 'Gagal menghapus barang');
        }

        return redirect()->back()->with('success_message' , 'Berhasil menghapus barang');
    }
    
  
    public function perlengkapanJamaah(Request $request)
    { 
        $tipe_paket     = $request->input("tipe_paket");
        $parameters     = $request->all();
        $tanggal        = $request->input("tanggal");
        $date = Date::all();

        $jamaah = [];       

        if($request->has('tanggal')) {
            $jamaah         = $this->peserta->getJamaah($request->all());
        }
        
        $inventories    = $this->inventory->getCollectionByType($tipe_paket);        
        $tglPemberangkatan = tglPemberangkatanList();
        $rangeYears = rangeYears();
        return view('admin.inventory.perlengkapan-jamaah', compact('jamaah', 'tipe_paket','tanggal', 'inventories', 'date', 'tglPemberangkatan', 'rangeYears', 'parameters'));
    }

    public function updatePerlengkapanJamaah($id, Request $request)
    {
       return $query = $this->inventory->updateStokPerlengkapanJamaah($request->all());
        if($query = $this->inventory->updateStokPerlengkapanJamaah($request->all())) {
           return response()->json($query, 200);
        }

        return response()->json($query, 500);
    }

    public function printPdf($id)
    {
       
        $jamaah = $this->peserta->getItemBy('id', $id); 

        $settings = $this->settings->all();

        $inventories = [];
        foreach($jamaah->inventories as $inventory) {
            array_push($inventories, $inventory->id);
        }
        $jamaah["inventories_id"] = $inventories;
        
        $inventories = $this->inventory->all(10, 'ASC');

        $pdf = $this->pdf->loadView('admin.inventory.print-perlengkapan', compact('jamaah', 'inventories', 'settings'));
        return $pdf->stream('invoice.pdf');
    }

    public function perlengkapanPdf(Request $request)
    {
        $tipe_paket     = $request->input('tipe_paket');
        $tanggal        = $request->input('tanggal');

        $jamaah = [];

        if($request->has('tanggal')) {
            $jamaah         = $this->peserta->getJamaah($request->all());
        }
 
        $inventories    = $this->inventory->getCollectionByType($tipe_paket);        

        $pdf = $this->pdf->setPaper('a4')->setOrientation('landscape')
                    ->loadView('admin.inventory.perlengkapan-jamaah-pdf', compact('jamaah', 'tipe_paket','tanggal', 'inventories'));
        return $pdf->stream('Perlengkapan.pdf');
    }

    public function printBlank(Request $request)
    {
        $tipe_paket     = $request->input('tipe_paket');
        $inventories    = $this->inventory->getCollectionByType($tipe_paket);
        
        $pdf = $this->pdf->setPaper('a4')
                          ->loadView('admin.inventory.print-perlengkapan-pdf-new', compact('tipe_paket', 'inventories'));
        return $pdf->stream();
    }

    public function kwitansiCreate(Request $request)
    {
        $inventories    = $this->inventory->getCollectionByType($request->input('tipe_paket'));

        return view('admin.inventory.kwitansi-create', compact('inventories'));
    }

    public function kwitansiPrint(Request $request)
    {
        $barang = $this->arraySortBarang($request->input('jumlah_barang'), $request->input('nama_barang'));
        $jamaah = $request->input('nama');
        $no_kwitansi = $request->input('no_kwitansi');
        $tanggal = date('d-m-Y');
        $pdf = $this->pdf->setPaper('a4')
                          ->loadView('admin.inventory.print-perlengkapan-pdf-new', compact('tipe_paket', 'inventories', 'tanggal', 'jamaah', 'barang', 'no_kwitansi'));
        return $pdf->stream();
    }

    private function arraySortBarang($jumlahBarang = [], $namaBarang = [])
    {
        $newData = [];

        $i = 0;
        foreach($jumlahBarang as $key1 => $value1) {
            foreach($namaBarang as $key2 => $value2) {
                if($key1 == $key2) {
                    $newData[$i]['nama_barang'] = $value2;
                    $newData[$i]['jumlah_barang'] = $value1;
                    $i++;
                }
            }
        }

        return $newData;
    }
}

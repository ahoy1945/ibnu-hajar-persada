<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\TanggalPemberangkatanRequest;
use App\Repositories\MasterData\TanggalPemberangkatan\TanggalPemberangkatanRepository;
use App\Http\Controllers\Controller;
use App\HargaPaket;
use JavaScript;

class TanggalPemberangkatanController extends Controller
{

  /**
   * @var App\HargaPaket
   */
  protected $hargaPaket;

  /**
   * @var App\Repositories\MasterData\TipeKeuangan
   */
    protected $tanggalPemberangkatan;

    public function __construct(TanggalPemberangkatanRepository $tanggalPemberangkatan, HargaPaket $hargaPaket)
    {
      $this->tanggalPemberangkatan = $tanggalPemberangkatan;
      $this->hargaPaket = $hargaPaket;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->tanggalPemberangkatan->all();
        //return $data;
        // echo "<pre>".print_r(compact('data','paket'),1)."</pre>";
        return view('admin.master_data.tanggal_pemberangkatan.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        JavaScript::put(['hargapaket' => $this->hargaPaket->all()]);
       
        return view('admin.master_data.tanggal_pemberangkatan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TanggalPemberangkatanRequest $request)
    {
        if(! $this->tanggalPemberangkatan->create($request->all())) {
            return redirect('/tanggal_pemberangkatan')->with('error_message', 'Gagal menyimpan tanggal pemberangkatan');
        }

        return redirect('/tanggal_pemberangkatan')->with('success_message', 'Berhasil menyimpan tanggal pemberangkatan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->tanggalPemberangkatan->edit($id);
        JavaScript::put(['hargapaket' => $this->hargaPaket->all()]);

        // echo "<pre>".print_r($data,1)."</pre>";
        return view('admin.master_data.tanggal_pemberangkatan.edit', compact('data', 'paket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TanggalPemberangkatanRequest $request, $id)
    {

      if( !$this->tanggalPemberangkatan->find($id)->update($request->except('_token'))) {
          return redirect('/tanggal_pemberangkatan')->with('error_message', 'Gagal update tanggal pemberangkatan');
      }

      return redirect('/tanggal_pemberangkatan')->with('success_message', 'Berhasil update tanggal pemberangkatan');
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->tanggalPemberangkatan->delete($id)) {
          return redirect()->back()->with('error_message', 'Gagal menghapus tanggal pemberangkatan');
        }

        return redirect()->back()->with('success_message', 'Berhasil menghapus tanggal pemberangkatan');
    }
}

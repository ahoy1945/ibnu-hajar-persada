<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TanggalPemberangkatan as Date;
use App\Repositories\DataJamaah\Manifest\ManifestRepository;
use Maatwebsite\Excel\Excel;

class ManifestController extends Controller
{


    protected $manifest;

    protected $excel;

    protected $request;

    public function __construct(ManifestRepository $manifest, Excel $excel, Request $request)
    {
      $this->manifest = $manifest;
      $this->excel = $excel;
      $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $manifest = $this->manifest->all($request);
      $query = $request->only('date', 'tipe');
      $date = Date::all();
  
      return view('admin.data_jamaah.manifest.index', compact('manifest', 'date', 'query'));
    }


    public function excel(Request $request)
    {
      $data = $this->manifest->all($request);
      $date = $this->request->input('date');
      $tipe = $this->request->input('tipe');

      $this->excel->create("Manifest {$date} {$tipe}", function($excel) use ($data, $date, $tipe) {
          $excel->sheet('Data Keuangan', function($sheet) use ($data, $date, $tipe) {
              $row = 2;
              $no = 1;

              $sheet->mergeCells("A1:Q1");

              $sheet->row(1, [
                      "Daftar Jamaah {$tipe} {$date} PT.IBNU HAJAR PERSADA"
                  ]);
              $sheet->setHeight(1, 30);

              $sheet->row($row, array(
                  'NO',
                  'NAME',
                  'PLACE OF BIRTH',
                  'DATE OF BIRTH',
                  'AGE',
                  'SEX',
                  'PASSPORT',
                  'DATE OF ISSUE',
                  'DATE OF EXP',
                  'ISS OFFICE',
                  'STATUS',
                  'DISTRICT',
                  'PROVINCE',
                  'POSTAL CODE',
                  'PLANE',
                  'HOTEL',
                  'NO PORSI'
              ));

              foreach($data as $key => $value) {
                  $hotel = ['ekonomis','bisnis','eksekutif'];
                  $bintang = array_search($value['paket_hotel'], $hotel) + 3;
                  $jenis_kelamin = ($value['jenis_kelamin'] == 'L') ? 'M' : 'F';

                  $sheet->row(++$row, [
                          $no,
                          $value['nama_peserta'],
                          $value['tempat_lahir'],
                          formatDate($value['tgl_lahir']),
                          age($value['tgl_lahir']),
                          $jenis_kelamin,
                          $value['no_passpor'],
                          formatDate($value['tgl_pembuatan']),
                          formatDate($value['tgl_expired']),
                          $value['tempat_pembuatan'],
                          $value['hubungan_mahram'],
                          $value['kabupaten'],
                          $value['provinsi'],
                          $value['kodepos'],
                          $value['tipe_pesawat'],
                          $bintang,
                          $value['no_porsi']
                      ]);

                  $no++;
              }

               $sheet->setBorder("A1:Q{$row}", 'thin');

               $sheet->cells("A1:Q{$row}", function($cells) {
                      $cells->setAlignment('left');
                      $cells->setValignment('middle');
                  });


          });
      })->export('xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->manifest->show($id);

        // return $data;
        return view('admin.data_jamaah.manifest.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      if(! $this->manifest->delete($id)) {
          return redirect()->back()->with('error_message', 'Gagal menghapus data');
      }

      return redirect()->back()->with('success_message', 'Berhasil menghapus data');
    }
}

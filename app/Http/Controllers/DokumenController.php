<?php

namespace App\Http\Controllers;

use App\RefDokumen;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\DokumenRequest;
use App\Http\Controllers\Controller;
use App\Repositories\Peserta\PesertaRepository;
use App\TanggalPemberangkatan;
use Maatwebsite\Excel\Excel;

class DokumenController extends Controller
{
    /**
     * @var Maatwebsite\Excel\Excel
     */
    protected $excel;

	/**
	 * @var App\RefDokumen
	 */
	protected $dokumen;

    /**
     * @var App\Repositories\Peserta\PesertaRepository
     */
    protected $peserta;
   
   	/**
   	 * Create a New instance
   	 * 
   	 * @param RefDokumen $dokumen [description]
   	 */
    public function __construct(RefDokumen $dokumen, PesertaRepository $peserta, Excel $excel)
    {
    	$this->dokumen = $dokumen;
        $this->peserta = $peserta;
        $this->excel = $excel;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$documents = $this->dokumen->orderBy('id', 'DESC')->paginate(10);

        return view('admin.master_data.dokumen.index', compact('documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master_data.dokumen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DokumenRequest $request)
    {
       	if(! $this->dokumen->create($request->all())) {
       		return redirect()->route('admin.dokumen.index')->with('error_message', 'Gagal menyimpan data');
       	}

       	return redirect()->route('admin.dokumen.index')->with('success_message', 'Berhasil menyimpan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$dokumen = $this->dokumen->find($id);

        return view('admin.master_data.dokumen.edit', compact('dokumen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DokumenRequest $request, $id)
    {
        if(! $this->dokumen->find($id)->update($request->except('_method'))) {
        	return redirect()->route('admin.dokumen.index')->with('error_message', 'Gagal mengudpate data');
        }

        return redirect()->route('admin.dokumen.index')->with('success_message', 'Berhasil mengudpate');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->dokumen->find($id)->delete()) {
        	return redirect()->back()->with('error_message', 'Gagal menghapus');
        }

        return redirect()->back()->with('success_message', 'Berhasil menghapus');
    }

    public function cekDokumenJamaah(Request $request) 
    {

       $jamaah      = $this->peserta->getCollectionForDokumen($request, 10);
       $documents   = $this->dokumen->where('tipe', $request->input('tipe'))->get();
       $tipe_paket  = $request->input('tipe');
       $tanggal     = $request->input('tanggal');
       $tglPemberangkatan = TanggalPemberangkatan::all();
      
       return view('admin.master_data.dokumen.dokumen-jamaah', compact('jamaah', 'documents', 'tipe_paket', 'tanggal', 'tglPemberangkatan'));
    }

    public function check(Request $request)
    {
        $dokumen_id = $request->input('dokumen_id');
        $jamaah = $this->peserta->find($request->input('id'));
         
        foreach($jamaah->documents as $document) {
            if($document->pivot->dokumen_id == $dokumen_id) {
                $jamaah->documents()->detach($dokumen_id);
                return;
            }
        }

        $jamaah->documents()->attach($dokumen_id);
    }

    public function exportExcel(Request $request)
    {
        $data        = $this->peserta->getCollectionForDokumenPrint($request);
        $documentsLists   = $this->dokumen->where('tipe', $request->input('tipe'))->lists('nama_dokumen')->toArray();
        $documents = $this->dokumen->where('tipe', $request->input('tipe'))->get()->toArray();
       
        $last = (count($data)-1);
        $header = [
             'Nama',
             'U',
             'Tgl Berangkat',
             'JK',
             'Alamat',
             'Dokumen'];

        $this->excel->create("Laporan Dokumen {$request->input('tipe')} Tanggal {$request->input('tanggal')}", function($excel) use ($data, $last, $request, $header, $documents, $documentsLists) {
            $excel->sheet('Data Dokumen', function($sheet) use ($data, $last, $request, $header, $documents, $documentsLists) {
                $letters = array_combine(range(1,26), range('a', 'z'));
                $row = 2;
                $no = 1;
                $dokumenMerge = strtoupper($letters[6]."2:".$letters[6+count($documentsLists)-1]."2");
                $judulMerge = strtoupper("A1:".$letters[6+count($documentsLists)-1]."1");
                $sheet->setWidth(array(
                    'A'     =>  20,
                    'B'     =>  5,
                    'C'     => 20,
                    'D'      => 5,
                    'E'      => 20
                ));


                $sheet->mergeCells($judulMerge);
                $sheet->mergeCells($dokumenMerge);
                $sheet->mergeCells("A2:A3");
                $sheet->mergeCells("B2:B3");
                $sheet->mergeCells("C2:C3");
                $sheet->mergeCells("D2:D3");
                $sheet->mergeCells("E2:E3");

                $sheet->row(1, [
                        "Laporan Dokumen {$request->input('tipe')} Tanggal {$request->input('tanggal')}"
                    ]);
                $sheet->setHeight(1, 30);

                $sheet->row($row, $header);
                $sheet->row(3, array_merge(['', '', '', '', '',], $documentsLists));

                $row = 3;
    
                foreach($data as $key => $value) {
                    $dataDokumen = [];
                    foreach($documents as $documentValue) {
                        if(in_array($documentValue['id'], $value['documents_group'])) {
                            array_push($dataDokumen, "v");
                        } else {
                             array_push($dataDokumen, " ");
                        }
                    }

                    $dataDatabase =  [  $value['nama_peserta'],
                                        age($value['tgl_lahir']),
                                        formatDate($value['tgl_berangkat']),
                                        $value['jenis_kelamin'],
                                        $value['alamat']['kabupaten']];
                    $mergeData = array_merge($dataDatabase, $dataDokumen);
                    
                    $sheet->row(++$row, $mergeData
                        );

                    $no++;
                } 

                 
                 $sheet->setBorder("A1:{$letters[6+count($documentsLists)-1]}{$row}", 'thin');

                 $sheet->cells("A1:{$letters[6+count($documentsLists)-1]}{$row}", function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('middle');
                    });


            });
        })->export('xlsx');   
    }
}

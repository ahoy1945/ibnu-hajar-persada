<?php

namespace App\Http\Controllers;

use App\HargaPaket;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\PembayaranRequest;
use App\RefTipeKeuangan;
use App\Rekening;
use App\Repositories\PesertaPaket\PesertaPaketRepository;
use App\Repositories\PesertaPembayaran\PesertaPembayaranRepository;
use App\Repositories\Peserta\PesertaRepository;
use App\Services\KursMandiri;
use App\Setting;
use Barryvdh\DomPDF\PDF;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use JavaScript;
use Jenssegers\Date\Date;
use Nasution\Terbilang;

class PembayaranController extends Controller
{
 
    /**
     * @var Illuminate\Auth\Guard
     */
    protected $auth;

    /**
     * @var Nasution\Terbilang
     */
    protected $terbilang;
   
    /**
     * @var App\Repositories\Peserta\PesertaRepository
     */
    protected $peserta;

    /**
     * @var App\HargaPaket
     */
    protected $hargaPaket;

    /**
     * @var JavaScript
     */
    protected $javascript;

    /**
     * @var App\RefTipeKeuangan
     */
    protected $refKeuangan;

    /**
     * @var App\Rekening
     */
    protected $rekening;

    /**
     *
     * @var Barryvdh\DomPDF\PDF;
     */
    protected $pdf;

    /**
     * @var App\Repositories\PesertaPembayaran\PesertaPembayaranRepository
     */
    protected $pesertaPembayaran;
   
    /**
     * @var Create a new instance
     */
    protected $pesetaPaket;

    /**
     * @var App\Settings;
     */
    protected $settings;

    /**
     * Create a new instance
     * 
     * @param PesertaRepository $peserta |
     */
    public function __construct(
        PesertaRepository $peserta,
        HargaPaket $hargaPaket,
        RefTipeKeuangan $refKeuangan,
        Rekening $rekening,
        PDF $pdf,
        PesertaPembayaranRepository $pesertaPembayaran,
        PesertaPaketRepository $pesertaPaket,
        Setting $settings,
        Terbilang $terbilang,
        Guard $auth)
    {
        $this->peserta = $peserta;
        $this->hargaPaket = $hargaPaket;
        $this->refKeuangan = $refKeuangan;
        $this->rekening = $rekening;
        $this->pdf = $pdf;
        $this->pesertaPembayaran = $pesertaPembayaran;
        $this->pesertaPaket = $pesertaPaket;
        $this->settings = $settings;
        $this->terbilang = $terbilang;
        $this->auth = $auth;
        Date::setLocale('id');
    }   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = ($request->input('page') ? $request->input('page') : 1 );
        $parameters = $request->all();
        $hargapaket = $this->hargaPaket->all();
        $tglPemberangkatan = tglPemberangkatanList();
        $rangeYears = rangeYears();

        JavaScript::put(['parameters' => $parameters]);
        if($request->has('tgl_berangkat') || $request->has('keyword') || $request->has('tipe') ) {  
            
            $peserta  = $this->pesertaPaket->search($request);
          
            return view('admin.pembayaran.search', compact('peserta', 'hargaPaket', 'parameters', 'page', 'tglPemberangkatan', 'rangeYears')); 
        } else {
            $peserta = $this->pesertaPembayaran->getAllPerTransaksi();
           // dd($peserta->toArray());

            return view('admin.pembayaran.index', compact('peserta', 'hargapaket', 'parameters', 'page', 'tglPemberangkatan', 'rangeYears'));
        }
        
       
           
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $peserta = $this->peserta->getItemBy('id',$id);
        $tipeKeuangan = $this->refKeuangan->where('tipe_keuangan', 'pemasukan')->get(); 
        $rekening = $this->rekening->all();
        
        return view('admin.pembayaran.create', compact('peserta', 'tipeKeuangan', 'rekening'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 

        $settings = $this->settings->all();
        //$this->pesertaPembayaran->create($request->all()); 
        $jamaah = $this->peserta->getItemBy('id', $request->input('peserta_id'));
               
            
        if($request->ajax()) {
            return;
        }   

        $terbilang =  $this->getCountPembayaran($jamaah); 
        $pdf = $this->pdf->loadView('admin.pembayaran.kwitansi', compact('settings', 'jamaah', 'terbilang'));

        return $pdf->stream();     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(! $pembayaran = $this->pesertaPembayaran->find($id)) {
            return redirect()->route('admin.pembayaran.index')->with('error_message', 'Pembayaran tidak ditemukan');
        }

        $rekening = $this->rekening->all();
        $tipeKeuangan = $this->refKeuangan->where('tipe_keuangan', 'Pemasukan')->get();

        return view('admin.pembayaran.edit', compact('pembayaran', 'rekening', 'tipeKeuangan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PembayaranRequest $request, $id)
    {
        $request = $request->all('_method', '_patch', 'jumlah');
        $request = array_add($request, 'jumlah', str_replace(",","",$request['jumlah']));
        if(! $pembayaran = $this->pesertaPembayaran->find($id)->update($request)) {
            return redirect()->route('admin.pembayaran.index')->with('error_message', 'Gagal mengupdate pembayaran');
        }

        return redirect()->route('admin.pembayaran.index')->with('success_message', 'Berhasil mengupdate pembayaran');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->pesertaPembayaran->find($id)->delete()) {
            return redirect()->back()->with('error_message', 'Gagal menghapus');
        }

        return redirect()->back()->with('success_message', 'Berhasil menghapus');
    }

    public function search(Request $request)
    {
        $peserta = $this->peserta->all();
        $hargapaket = $this->hargaPaket->all();
        
        return view('admin.pembayaran.index', compact('peserta', 'hargapaket'));
    }

    public function getCountPembayaran($data = [])
    {
        $jumlah = 0;

        foreach($data->pembayaran as $pembayaran) {
             $jumlah+= $pembayaran->jumlah;
        }

        return $this->terbilang->convert($jumlah);
    }

    /**
     * Convert USD to IDR (Mandiri Curs)
     * 
     * @param  $id 
     * @return string
     */
    public function konversi($id)
    {

        if(! $this->pesertaPembayaran->konversi($id)) {
          return redirect()->back()->with('success_message', 'Terjadi kesalahan'); 
        }
        return redirect()->back()->with('success_message', 'Terkonversi');

         
    }

    public function updateStatusPembayaran($id)
    {
        $jamaah = $this->peserta->find($id);
        $statusPembayaran = (int)!$jamaah->status_pembayaran;
      
        if(! $jamaah->update(['status_pembayaran' =>  $statusPembayaran])) {
            return redirect()->back()->with('error_message' , 'Terjadi kesalahan');
        }

        return redirect()->back()->with('success_message', 'Berhasil mengupdate status pembayaran');
    }

    public function createMulti()
    {
        $rekening = $this->rekening->all();
        JavaScript::put(['tipeKeuangan' => $this->refKeuangan->all()]);
        JavaScript::put(['rekening' => $rekening]);
            
        return view('admin.pembayaran.create-multi', compact('rekening'));
    }

    public function storeMulti(Request $request)
    { 
        $data = $this->sortArrayMulti($request->all());
        $penerima = $request->input('penerima');
        $isAgen = $this->auth->user()->is_agen;

        foreach($data as $dataJamaah) {
            try {
                $this->pesertaPembayaran->createMulti($dataJamaah);
            } catch (Exception $e) {
                return redirect()->back()->with("error_message", "Terjadi kesalahan mohon menginput ulang");
            }

         
        }  

        return redirect()->route('admin.pembayaran.index')->with('success_message', 'Pembayaran berhasil disimpan');
        
    }

    public function sortArrayMulti($inputData = [])
    {
        $data = [];
        $i = 0;
        $countPembayaran = $this->pesertaPembayaran->count()+1;
        $lastNoKwitansi = \App\PesertaPembayaran::orderBy('created_at', 'desc')->first();
       
        $lastNoKwitansi = (!empty($lastNoKwitansi) && isset($lastNoKwitansi) ? \App\PesertaPembayaran::orderBy('created_at', 'desc')->first()->no_kwitansi : 0 );
        
        for($i = 0; $i < count($inputData['peserta_id']); $i++) {
            $data[$i]['peserta_id'] = $inputData['peserta_id'][$i];
            $data[$i]['tipe_keuangan_id'] = $inputData['tipe_keuangan_id'][$i];
            $data[$i]['jumlah'] = str_replace(",", "", $inputData['jumlah'][$i]);
            $data[$i]['keterangan'] = $inputData['keterangan'][$i];
            $data[$i]['rekening_id'] = $inputData['rekening_id'];
            $data[$i]['tgl_bayar'] = $inputData['tgl_bayar'];
            $data[$i]['mata_uang'] = $inputData['mata_uang'];
            $data[$i]['no_kwitansi'] = str_pad($lastNoKwitansi + 1, 6, 0, STR_PAD_LEFT);
            $data[$i]['diskon'] = $inputData['diskon']; 
        }

        return $data;   
    }

    public function printKwitansi(Request $request)
    {

        switch ($request->input('tipe')) {
            case 'multi':
                $data = $this->kwitansiMulti($request->all());
                $pdf = $this->pdf->loadView('admin.pembayaran.kwitansi-multi', $data);
                 
                break;
            
            case 'single':

                $data = $this->kwitansiSingle($request->all());
                
                $pdf = $this->pdf->loadView('admin.pembayaran.kwitansi', $data);

                break;
        }

        return $pdf->stream();
        
    }

    private function kwitansiMulti($data)
    {
        $jamaah = $this->pesertaPembayaran->findByKwitansi($data['no_kwitansi']);
        $totalPembayaran = $jamaah->sum('jumlah');
        $terbilang = $this->terbilang->convert($totalPembayaran);
        $tglKwitansi = Date::parse($jamaah[0]->tgl_bayar)->format('d F Y');;
        $paymentMethod = $jamaah[0]->pembayaran;

        return ['settings' => $this->getSettings(), 'jamaah' => $jamaah, 'terbilang' => $terbilang, 'tglKwitansi' => $tglKwitansi, 'paymentMethod' => $paymentMethod ];
         
    }

    private function kwitansiSingle($data)
    {
        $jamaah = $this->peserta->getItemBy('id', $data['peserta_id']);

        $terbilang = $this->getCountPembayaran($jamaah); 
              
        return ['settings' => $this->getSettings(), 'jamaah' => $jamaah, 'terbilang' => $this->getCountPembayaran($jamaah)];
    }

    private function getSettings()
    {
        return $this->settings->all();
    }

}

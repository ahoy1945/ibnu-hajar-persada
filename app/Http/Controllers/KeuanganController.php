<?php

namespace App\Http\Controllers;

use DateTime;
use App\Http\Requests;
use Goutte\Client;
use Jenssegers\Date\Date;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use App\Http\Controllers\Controller;
use App\Http\Requests\KeuanganRequest;
use Illuminate\Contracts\Auth\Guard as Auth;
use App\Repositories\Keuangan\KeuanganRepository;
use App\RefTipeKeuangan;
use JavaScript;
use App\Rekening;

class KeuanganController extends Controller
{
    /**
     * @var [type]
     */
    protected $refKeuangan;

    /**
     * @var Maatwebsite\Excel\Excel
     */
    protected $excel;

    /**
     * @var App\Repositories\Keuangan\KeuanganRepository
     */
    protected $keuangan;

    /**
     * @var Goutte\Client
     */
    protected $http;

    /**
     * @var Illuminate\Http\Request
     */
    protected $request;

    /**
     * @var Illuminate\Contracts\Auth\Guard
     */
    protected $auth;


    /**
     * Create a new instance
     *
     * @param KeuanganRepository $keuangan
     */
    public function __construct(KeuanganRepository $keuangan,
        Excel $excel, 
        Request $request, 
        Auth $auth, 
        Client $http, 
        RefTipeKeuangan $refKeuangan)
    {
        $this->keuangan = $keuangan;
        $this->excel = $excel;
        $this->request = $request;
        $this->auth = $auth;
        $this->http = $http;
        $this->refKeuangan = $refKeuangan;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $query = $request->only('from', 'to', 'type', 'mata_uang');

        $keuangan = $this->keuangan->all($request->except('page', 'mata_uang'));
        // echo "<pre>".print_r($keuangan ,1)."</pre>";
        $jumlahBefore =  $this->getJumlahBefore($keuangan, $request->except('page', 'mata_uang'));

        return view('admin.keuangan.index', compact('keuangan', 'query','jumlahBefore'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $mata_uang = $this->request->input('mata_uang');
        $keuangan = $this->keuangan->getFirst('created_at', 'DESC');
        $rekening = Rekening::all()->toArray();
        JavaScript::put('tipe_keuangan', $this->refKeuangan->all());

        return view('admin.keuangan.create', compact('keuangan', 'mata_uang', 'rekening'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
    
        $input = $this->sortArrayRequest($request);
        
        
        $this->keuangan->create($input);
        $url = $request->all()[0]['mata_uang'];
        return Response()->json(['url' => '/keuangan?mata_uang='.$url]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $mata_uang = $this->request->input('mata_uang');

        $keuangan = $this->keuangan->item($id);

        return view('admin.keuangan.show', compact('keuangan', 'mata_uang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = $this->keuangan->item($id);
        $rekening = Rekening::all()->toArray();
        $mata_uang = $this->request->input('mata_uang');
        JavaScript::put('tipe_keuangan', $this->refKeuangan->all());
        JavaScript::put('keuangan', $data);
        return view('admin.keuangan.edit', compact('data', 'mata_uang', 'rekening'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(KeuanganRequest $request, $id)
    {
      
        $input = $request->except('amount', '_token', '_method', 'mata_uang');
        //$input = array_add($input, 'who_insert', $this->auth->user()->name);

        if($request->input('type') == 'debit') {
            $input = array_add($input, 'debit', str_replace(",", "", $request->input('amount')));
            $input = array_add($input, 'kredit', 0);

        } else  {
             $input = array_add($input, 'kredit', str_replace(",", "", $request->input('amount')));
             $input = array_add($input, 'debit', 0);
        }

        $this->keuangan->update($input, $id);

        return redirect()->route('admin.keuangan.index', ['mata_uang' => $request->input('mata_uang')])->with('success_message', 'Data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->keuangan->delete($id);

        return redirect()->route('admin.keuangan.index', ['mata_uang' => $this->request->input('mata_uang')])->with('success_message', 'Data berhasil dihapus');
    }

    public function search(Request $request)
    {
        $keyword = $request->except('page');

        $keuangan = $this->keuangan->search($request->input('keyword'));

        return view('admin.keuangan.search', compact('keuangan', 'keyword'));
    }

    public function exportExcel()
    {
        $data = $this->keuangan->export($this->request->all())->toArray();
        $last = (count($data)-1);

        $this->excel->create("Laporan Keuangan {$this->request->input('from')} {$this->request->input('to')}", function($excel) use ($data, $last) {
            $excel->sheet('Data Keuangan', function($sheet) use ($data, $last) {
                $row = 2;
                $no = 1;

                $sheet->mergeCells("A1:G1");

                $sheet->row(1, [
                        "Laporan Keuangan {$this->request->input('from')} {$this->request->input('to')}"
                    ]);
                $sheet->setHeight(1, 30);

                $sheet->row($row, array(
                    'No',
                    'Tanggal',
                    'Keterangan',
                    'Rekening',
                    'Debit',
                    'kredit',
                    'Saldo'
                ));

                foreach($data as $key => $value) {
                    $sum = 0;
                    for ($j=$last; $j >= $key ; $j--) {
                          $sum += ($data[$j]['debit'] - $data[$j]['kredit']);
                    }

                    $sheet->row(++$row, [
                            $no,
                            Date::parse($value['tanggal'])->format('d M Y'),
                            $value['keterangan'],
                            $value['no_rek']." an. ".$value['atas_nama'],
                            $value['debit'],
                            $value['kredit'],
                            $sum
                        ]);

                    $no++;
                }

                 $sheet->setBorder("A1:G{$row}", 'thin');

                 $sheet->cells("A1:G{$row}", function($cells) {
                        $cells->setAlignment('left');
                        $cells->setValignment('middle');
                    });


            });
        })->export('xlsx');
    }

    public function getJumlahBefore($data, $parameter)
    {
        if($parameter) {
            $dataBefore = \App\Keuangan::where(function($query) use ($parameter) {
                if($parameter['type'] != 'all' && !empty($parameter['type'])) {
                    $query->where('type', $parameter['type']);
                }

                if($parameter['from'] && $parameter['to']) {
                    $query->where('tanggal', '>=',  $parameter['from']." 00:00:00");
                    $query->where('tanggal', '<=', $parameter['to']." 23:59:59");
                }
            })
            ->take($data->perPage())
            ->skip($data->perPage() * $data->currentPage())
            ->orderBy('created_at', 'DESC')
            ->get();
        } else {

             if(!$data->hasMorePages()) return 0;

             $dataBefore = \App\Keuangan::take($data->total() - ($data->perPage() * $data->currentPage()))
                                          ->skip($data->perPage() * $data->currentPage())
                                          ->orderBy('created_at', 'DESC')->get();
        }



        $jumlahBefore = 0;

        foreach($dataBefore as $key => $value) {
            $jumlahBefore += $value->debit - $value->kredit;
        }

        return $jumlahBefore;
    }

    public function chart(Request $request)
    {
       // dd($request->all());
        $data = $this->keuangan->chart($request->input('from'), $request->input('to'));
        return view('admin.keuangan.chart', compact('data'));
    }

    public function kurs()
    {
        $crawler = $this->http->request('GET', 'http://www.bankmandiri.co.id/resource/kurs.asp?row=2');
        $crawler->filter('.tbl-view  tr')->siblings()->each(function($node, $i){
            if($node->attr('class') !== 'zebra') {
                if($node->filter('td')->eq(1)->text() === 'USD') {

                    $result = explode(",",$node->filter('td')->eq(4)->text())[0];
                    $result = str_replace(".","", $result);

                    echo $result;
                }
                    
            }
        });
    }

    public function sortArrayRequest($request)
    {
        $data = [];

        foreach($request->all() as $key => $value) {
            $data[$key] = $value;
            $data[$key]['amount'] = str_replace(",", "", $value['amount']);
            $data[$key]['user_id'] = $this->auth->user()->id;
            switch ($value['tipe']) {
                case 'Pemasukan':
                    $data[$key]['debit'] = str_replace(",", "", $value['amount']);
                    $data[$key]['type'] = 'debit';
                    break;
                
                case 'Pengeluaran':
                    $data[$key]['kredit'] = str_replace(",", "", $value['amount']); 
                    $data[$key]['type'] = 'kredit';
                    break;
            }
        }
        

        return $data;
    }

     

}

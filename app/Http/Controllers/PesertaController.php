<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PesertaRequest;
use App\Repositories\Peserta\PesertaRepository;
use App\HargaPaket;
use App\TanggalPemberangkatan;
use JavaScript;
use App\RefPesawat;
use App\RefDokumen;
use App\RefBus;
use App\Provinsi;
use App\Kecamatan;
use App\Kabupaten;
use App\Desa;
use Intervention\Image\ImageManager;

class PesertaController extends Controller
{
  /**
   * @var App\RefDokumen
   */
  protected $refDokumen;

  /**
   * App\RefBus
   * @var [type]
   */
  protected $bus;
  
  /**
   * @var App\RefPesawat
   */
  protected $pesawat;
  
  /**
   * @var Intervention\Image\ImageManager
   */
  protected $image;

  /**
   * @var App\TanggalPemberangkatan
   */
  protected $tanggalPemberangkatan;

  /**
   * @var App\Repositories\Peserta\PesertaRepository
   */
  protected $peserta;

  /**
   * @var App\HargaPaket
   */
  protected $hargaPaket;

  /**
   * @param PesertaRepository $peserta
   */
  public function __construct(PesertaRepository $peserta, 
    HargaPaket $hargaPaket, 
    TanggalPemberangkatan $tanggalPemberangkatan,
    ImageManager $image, 
    RefPesawat $pesawat, 
    RefBus $bus, 
    RefDokumen $refDokumen)
  {
    $this->peserta = $peserta;
    $this->hargaPaket = $hargaPaket;
    $this->tanggalPemberangkatan = $tanggalPemberangkatan;
    $this->image = $image;
    $this->pesawat = $pesawat;
    $this->bus = $bus;
    $this->refDokumen = $refDokumen;
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pendaftar(Request $request)
    {
      $page = ($request->input('page') ? $request->input('page') : 1 );

      $pendaftar = $this->peserta->all();
      
      $hargapaket = $this->hargaPaket->all();
      
      return view('admin.peserta.pendaftar', compact('pendaftar', 'hargapaket', 'page'));
    }

    public function pendaftarSearch(Request $request)
    {

      $pendaftar = $this->peserta->search($request->input('keyword'));
      $page = ($request->input('page') ? $request->input('page') : 1);
      $keyword = $request->input('keyword');
      $hargapaket = $this->hargaPaket->all();

      return view('admin.peserta.pendaftar', compact('pendaftar', 'keyword', 'hargapaket', 'page'));
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $dokumen =  $this->refDokumen->all();
      $pesawat = $this->pesawat->all();
      JavaScript::put(['tgl_berangkat' => pemberangkatan(), 'dokumen' => $this->refDokumen->all()]);

      return view('admin.peserta.create', compact('pesawat'));
    }


    public function store(PesertaRequest $request)
    { 
       
      $input = $request->except('_token', 'harga_paket_id', 'tgl_berangkat', 'photo');
      $splitPaketId = explode(',', $request->input('harga_paket_id'));
      
      if($request->hasFile('photo')) {
          $urlPhoto = $this->generatePhoto($request->file('photo')); 
          $input = array_add($input, 'photo', $urlPhoto);
      }
      

      $input = array_add($input, 'harga_paket_id', $splitPaketId[0]);
      $input = array_add($input, 'tgl_berangkat', $splitPaketId[1]);
    
      $store = $this->peserta->create($input);

      if($store) return redirect()->route('admin.daftar.index')->with('error_message', 'Gagal menyimpan');

      return redirect()->route('admin.daftar.index')->with('success_message', 'Berhasil menyimpan'); 
    }

    public function searchMahram(Request $request)
    {
      $data = $this->peserta->searchMahram($request->input('keyword'));
      
      return response()->json($data, 200);
    }

    public function destroy($id)
    {
      if(! $this->peserta->delete($id)) {
          return redirect()->back()->with('error_message', 'Gagal menghapus');
      }

      return redirect()->back()->with('success_message', 'Berhasil menghapus');
    }

    public function show($id, Request $request)
    {
      
      $peserta = $this->peserta->getItemBy('id', $id);
      $peserta->tgl_lahir = formatDate($peserta->tgl_lahir);
      $peserta->paket->tanggal_berangkatan = formatDate($peserta->paket->tgl_berangkatan);
      $peserta->identitas->tgl_pembuatan = formatDate($peserta->identitas->tgl_pembuatan);
      $peserta->identitas->tgl_expired = formatDate($peserta->identitas->tgl_expired);
      //return $peserta;

      if($request->ajax()) {
          return response()->json($peserta, 200);
      }
    }

    public function search(Request $request)
    {
      $data = $this->peserta->searchPeserta($request->input('keyword'));

      return response()->json($data, 200);
    }

    public function generatePhoto($photo)
    {
        $filename = date('YmdHis').'-'. $photo->getClientOriginalName();
        $path = public_path('attachs/').$filename;
        
        $this->image->make($photo->getRealPath())
              ->resize(116, 156)
              ->save($path);

        return "/attachs/".$filename;
        
    }

  public function edit($id)
  {
    $value = $this->peserta->getItemBy('id', $id);
    $pemberangkatan = $this->tanggalPemberangkatan->with('paket')->where('tanggal_pemberangkatan', '>=', date('Y-m-d'))->get();

    JavaScript::put([
        'jamaah' =>  $value,
        'tgl_berangkat' => pemberangkatan(),
        'dokumen' => $this->refDokumen->all(),
    ]);
    
    $pesawat = $this->pesawat->all();
    
    return view('admin.peserta.edit', compact('value', 'pesawat'));
  }

  public function update(Request $request, $id)
  {
    
     $input = $request->except('_token', '_method', 'harga_paket_id', 'tgl_berangkat', 'photo');
     $splitPaketId = explode(',', $request->input('harga_paket_id'));
      
      if($request->hasFile('photo')) {
          $urlPhoto = $this->generatePhoto($request->file('photo')); 
          $input = array_add($input, 'photo', $urlPhoto);
      }
      

      $input = array_add($input, 'harga_paket_id', $splitPaketId[0]);
      $input = array_add($input, 'tgl_berangkat', $splitPaketId[1]);
    
      $store = $this->peserta->update($input, $id);

      if(! $store) return redirect()->route('admin.daftar.index')->with('error_message', 'Gagal menyimpan');

      return redirect()->route('admin.daftar.index')->with('success_message', 'Berhasil menyimpan');
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tabungan;
use App\TabunganSaldo;

class TabunganController extends Controller
{
    /**
     * @var App\Tabungan
     */
    protected $tabungan;

    /**
     * @var App\TabunganSaldo
     */
    protected $tabunganSaldo;

    /**
     * Create a new instance
     * 
     * @param Tabungan $tabungan 
     */
    public function __construct(Tabungan $tabungan, TabunganSaldo $tabunganSaldo)
    {
        $this->tabungan = $tabungan;
        $this->tabunganSaldo = $tabunganSaldo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = ($request->input('page') ? $request->input('page') : 1);
        $tabunganSaldo = $this->tabunganSaldo->with('tabungan')->orderBy('id', 'DESC')->paginate(10);

        return view('admin.tabungan.index', compact('tabunganSaldo', 'page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tabungan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

       if(! $this->insert($request->all())) {
            return redirect()->route('admin.tabungan.index')->with('error_message', 'Terjadi kesalahan'); 
       }

        return redirect()->route('admin.tabungan.index')->with('success_message', 'Berhasil disimpan'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->tabunganSaldo->whereId($id)->with('tabungan')->first();

        return view('admin.tabungan.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(! $this->tabunganSaldo->find($id)->update($request->except('_token', '_method'))) {
            return redirect()->route('admin.tabungan.index')->with('error_message', 'Terjadi kesalahan');
        }

        return redirect()->route('admin.tabungan.index')->with('success_message', 'Berhasil mengupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! $this->tabunganSaldo->whereId($id)->delete()) {
            return redirect()->back()->with('error_message', 'Terjadi kesalahan');
        }

        return redirect()->back()->with('success_message', 'Berhasil dihapus');
    }

    public function search(Request $request)
    {
        if($request->ajax()) {
            $data = $this->tabungan->where('nama', 'LIKE', "%{$request->input('keyword')}%")->get();
            
            return response()->json($data);
        }
    }

    private function insert($data) 
    {
        $inserted = false;

        switch (!empty($data['tabungan_id'])) {
            case true:
                $tabungan = $this->tabungan->whereId($data['tabungan_id'])->first();
                $inserted = true;
                break;
            
            case false:
                 if($tabungan = $this->tabungan->create(['nama'=> $data['nama']])) $inserted = true;
                break;
        }
       
        if($tabungan->saldo()->create(['tipe' => $data['tipe'], 'jumlah' => $data['jumlah']])) $inserted = true;

        return $inserted;
    }

    public function listTabungan(Request $request)
    {
       $tabunganJumlah = $this->tabunganSaldo->listTabungan($request);
      
       $page = ($request->input('page') ? $request->input('page') : 1 );

       return view('admin.tabungan.list-jumlah', compact('tabunganJumlah', 'page'));
    }
}

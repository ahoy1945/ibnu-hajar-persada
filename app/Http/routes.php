<?php

// Authentication routes...
Route::get('auth/login', ['as' => 'auth.login.get', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login',['as' => 'auth.login.post', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth.login.logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::group(['middleware' => 'auth'], function() {

	Route::group(['prefix' => 'api'], function() {
		Route::get('desa/{id_kecamatan}', ['as' => 'api.desa', 'uses' => 'LokasiController@desa']);
		Route::get('kecamatan/{id_kabupaten}', ['as' => 'api.kecamatan', 'uses' => 'LokasiController@kecamatan']);
		Route::get('kabupaten/{id_provinsi}', ['as' => 'api.kabupaten', 'uses' => 'LokasiController@kabupaten']);
		Route::get('provinsi', ['as' => 'api.provinsi', 'uses' => 'LokasiController@provinsi']);
		Route::get('kurs', ['as' => 'api.kurs', 'uses' => 'KeuanganController@kurs']);
		Route::get('/paket', ['as' => 'api.paket', 'uses' => 'HargaPaketController@apiPaket']);
		Route::get('/search/peserta', ['as' => 'api.peserta.search', 'uses' => 'PesertaController@search']);
	});

	Route::group(['middleware' => 'role:agen,owner,admin,keuangan,manajer'], function() {
		Route::get('/peserta/create', ['as' => 'admin.daftar.create', 'uses' => 'PesertaController@create']);
		Route::get('/pendaftar', ['as' => 'admin.daftar.index', 'uses' => 'PesertaController@pendaftar']);
		Route::get('/pendaftar/search', ['as' => 'admin.daftar.search', 'uses' => 'PesertaController@pendaftarSearch']);
		Route::patch('/pendaftar/{id}', ['as' => 'admin.daftar.update', 'uses' => 'PesertaController@update']);
		Route::get('/pendaftar/{id}/edit', ['as' => 'admin.daftar.edit', 'uses' => 'PesertaController@edit']);
		Route::get('/peserta/searchmahram', ['as' => 'admin.peserta.searchmahram', 'uses' => 'PesertaController@searchMahram']);
		Route::post('/peserta', ['as' => 'admin.daftar.store', 'uses' => 'PesertaController@store']);
		Route::get('/peserta/{id}', ['as' => 'admin.peserta.show', 'uses' => 'PesertaController@show']);
		Route::delete('/peserta/{id}', ['as' => 'admin.peserta.destroy', 'uses' => 'PesertaController@destroy']);
	});
	
	Route::group(['middleware' => 'role:owner,admin,keuangan,manajer'], function() {
		Route::get('/peserta_online', ['as' => 'admin.daftar.online.index', 'uses' => 'PendaftaranOnlineController@index']);
		Route::get('/peserta_online/{id}/show', ['as' => 'admin.daftar.online.show', 'uses' => 'PendaftaranOnlineController@show']);
		Route::get('/pendaftaran_online/{id}/move', ['as' => 'admin.daftar.online.move', 'uses' => 'PendaftaranOnlineController@move']);
		Route::post('/pendaftaran_online/{id}/move_store', ['as' => 'admin.daftar.online.move_store', 'uses' => 'PendaftaranOnlineController@moveStore']);
		Route::delete('/pendaftaran_online/{id}', ['as' => 'admin.daftar.online.destroy', 'uses' => 'PendaftaranOnlineController@destroy']);
		Route::get('/pendaftaran/search', ['as' => 'admin.daftar.online.search', 'uses' => 'PendaftaranOnlineController@pendaftarSearch']);
	});

	Route::group(['middleware' => 'role:owner'], function() {
		Route::get('/dashboard', ['as' => 'admin.dashboard.index', 'uses' => 'DashboardController@index']);
		Route::get('/dashboard/export/pemasukan_perbulan', ['as' => 'admin.dashboard.pemasukan_perbulan', 'uses' => 'DashboardController@exportPemasukanPerbulan']);
		Route::get('/dashboard/export/pengeluaran_perbulan', ['as' => 'admin.dashboard.pengeluaran_perbulan', 'uses' => 'DashboardController@exportPengeluaranPerbulan']);
		Route::get('/dashboard/export/pemasukan_pertahun', ['as' => 'admin.dashboard.pemasukan_pertahun', 'uses' => 'DashboardController@exportPemasukanPertahun']);
		Route::get('/dashboard/export/pengeluaran_pertahun', ['as' => 'admin.dashboard.pengeluaran_pertahun', 'uses' => 'DashboardController@exportPengeluaranPertahun']);
		Route::get('/dashboard/export/jamaah_perbulan', ['as' => 'admin.dashboard.jamaah_perbulan', 'uses' => 'DashboardController@exportJamaahPerbulan']);
		Route::get('/dashboard/export/jamaah_pertahun', ['as' => 'admin.dashboard.jamaah_pertahun', 'uses' => 'DashboardController@exportJamaahPertahun']);
	});

	
	Route::group(['middleware' => 'role:admin,owner'], function() {
		Route::get('/users', ['as' => 'admin.users.index', 'uses' => 'UsersController@index']);
		Route::get('/users/create', ['as' => 'admin.users.create', 'uses' => 'UsersController@create']);
		Route::post('/users', ['as' => 'admin.users.store', 'uses' => 'UsersController@store']);
		Route::delete('/users/{id}', ['as' => 'admin.users.destroy', 'uses' => 'UsersController@destroy']);
		Route::patch('/users/{id}', ['as' => 'admin.users.update', 'uses' => 'UsersController@update']);
		Route::get('/users/edit/{id}', ['as' => 'admin.users.edit', 'uses' => 'UsersController@edit']);
		Route::get('/settings', ['as' => 'admin.settings.index', 'uses' => 'SettingsController@index']);
		Route::post('/settings', ['as' => 'admin.settings.store', 'uses' => 'SettingsController@store']);
	});

	Route::group(['middleware' => 'role:owner,admin,keuangan,perlengkapan'], function() {
		Route::get('/agen', ['as' => 'admin.agen.index', 'uses' => 'AgenController@index']);
		Route::get('/agen/create', ['as' => 'admin.agen.create', 'uses' => 'AgenController@create']);
		Route::post('/agen', ['as' => 'admin.agen.store', 'uses' => 'AgenController@store']);
		Route::get('/agen/{id}', ['as' => 'admin.agen.show', 'uses' => 'AgenController@show']);
		Route::delete('/agen/{id}', ['as' => 'admin.agen.destroy', 'uses' => 'AgenController@destroy']);
		Route::get('/agen/{id}/edit', ['as' => 'admin.agen.edit', 'uses' => 'AgenController@edit']);
		Route::patch('/agen/{id}', ['as' => 'admin.agen.update', 'uses' => 'AgenController@update']);
	});


	Route::group(['middleware' => 'role:owner,manajer,perlengkapan'], function() {
		Route::get('/inventory', ['as' => 'admin.inventory.index', 'uses' => 'InventoryController@index']);
		Route::get('inventory/perlengkapan-jamaah', ['as' => 'admin.inventory.perlengkapan-jamaah', 'uses' => 'InventoryController@perlengkapanJamaah']);
		Route::get('/inventory/print-blank', ['as' => 'admin.inventory.print-blank', 'uses' => 'InventoryController@printBlank']);
		Route::get('/inventory/create', ['as' => 'admin.inventory.create', 'uses' => 'InventoryController@create']);
		Route::get('/inventory/{id}', ['as' => 'admin.inventory.show', 'uses' => 'InventoryController@show']);
		Route::post('/inventory', ['as' => 'admin.inventory.store', 'uses' => 'InventoryController@store']);
		Route::delete('/inventory/{id}', ['as' => 'admin.inventory.destroy', 'uses' => 'InventoryController@destroy']);
		Route::get('/inventory/{id}/edit', ['as' => 'admin.inventory.edit', 'uses' => 'InventoryController@edit']);
		Route::get('/inventory/kwitansi/create', ['as' => 'admin.inventory.kwitansi.create', 'uses' => 'InventoryController@kwitansiCreate']);
		Route::post('/inventory/kwitansi/print', ['as' => 'admin.inventory.kwitansi.print', 'uses' => 'InventoryController@kwitansiPrint']);
		Route::patch('/inventory/{id}', ['as' => 'admin.inventory.update', 'uses' => 'InventoryController@update']);
		Route::patch('/inventory/perlengkapan-jamaah/{id}', ['as' => 'admin.inventory.perlengkapan-jamaah.update', 'uses' => 'InventoryController@updatePerlengkapanJamaah']);
		Route::post('/inventory/print/{id}', ['as' => 'admin.inventory.print', 'uses' => 'InventoryController@printPdf']);
		Route::post('/inventory/perlengkapan/pdf', ['as' => 'admin.inventory.perlengkapan.pdf', 'uses' => 'InventoryController@perlengkapanPdf']);
	});	

	//master data
	Route::group(['middleware' => 'role:owner,admin,manajer,keuangan'], function() {
		Route::get('/kuota', ['as' => 'admin.master_data.kuota.index', 'uses' => 'RefMaxKuotaController@index']);
		Route::get('/kuota/create', ['as' => 'admin.master_data.kuota.create', 'uses' => 'RefMaxKuotaController@create']);
		Route::get('/kuota/{id}/edit/', ['as' => 'admin.master_data.kuota.edit', 'uses' => 'RefMaxKuotaController@edit']);
		Route::post('/kuota/store', ['as' => 'admin.master_data.kuota.store', 'uses' => 'RefMaxKuotaController@store']);
		Route::delete('/kuota/{id}', ['as' => 'admin.master_data.kuota.delete', 'uses' => 'RefMaxKuotaController@destroy']);
		Route::patch('/kuota/{id}', ['as' => 'admin.master_data.kuota.update', 'uses' => 'RefMaxKuotaController@update']);

		// Route::get('/bus', ['as' => 'admin.master_data.bus.index', 'uses' => 'RefBusController@index']);
		// Route::get('/bus/create', ['as' => 'admin.master_data.bus.create', 'uses' => 'RefBusController@create']);
		// Route::get('/bus/{id}/edit/', ['as' => 'admin.master_data.bus.edit', 'uses' => 'RefBusController@edit']);
		// Route::post('/bus/store', ['as' => 'admin.master_data.bus.store', 'uses' => 'RefBusController@store']);
		// Route::delete('/bus/{id}', ['as' => 'admin.master_data.bus.delete', 'uses' => 'RefBusController@destroy']);
		// Route::patch('/bus/{id}', ['as' => 'admin.master_data.bus.update', 'uses' => 'RefBusController@update']);

		Route::get('/pesawat', ['as' => 'admin.master_data.pesawat.index', 'uses' => 'RefPesawatController@index']);
		Route::get('/pesawat/create', ['as' => 'admin.master_data.pesawat.create', 'uses' => 'RefPesawatController@create']);
		Route::get('/pesawat/{id}/edit/', ['as' => 'admin.master_data.pesawat.edit', 'uses' => 'RefPesawatController@edit']);
		Route::post('/pesawat/store', ['as' => 'admin.master_data.pesawat.store', 'uses' => 'RefPesawatController@store']);
		Route::delete('/pesawat/{id}', ['as' => 'admin.master_data.pesawat.delete', 'uses' => 'RefPesawatController@destroy']);
		Route::patch('/pesawat/{id}', ['as' => 'admin.master_data.pesawat.update', 'uses' => 'RefPesawatController@update']);
		
		// roomlist
		Route::get('/room_list', ['as' => 'admin.data_jamaah.room_list.index', 'uses' => 'RoomListController@index']);
		Route::post('/room_list/print', ['as' => 'admin.data_jamaah.room_list.print', 'uses' => 'RoomListController@printRoomPDF']);
		Route::get('/room_list/edit/{id}', ['as' => 'admin.data_jamaah.room_list.edit', 'uses' => 'RoomListController@edit']);
		Route::patch('/room_list/{id}', ['as' => 'admin.data_jamaah.room_list.update', 'uses' => 'RoomListController@update']);

		Route::get('/harga_paket', ['uses' => 'HargaPaketController@index']);
		Route::get('/harga_paket/create', ['uses' => 'HargaPaketController@create']);
		Route::post('/harga_paket', ['uses' => 'HargaPaketController@store']);
		Route::delete('/harga_paket/{id}', ['uses' => 'HargaPaketController@destroy']);
		Route::get('/harga_paket/{id}/edit', ['uses' => 'HargaPaketController@edit']);
		Route::post('/harga_paket/{id}', ['uses' => 'HargaPaketController@update']); //update

		Route::get('/tanggal_pemberangkatan', ['uses' => 'TanggalPemberangkatanController@index']);
		Route::get('/tanggal_pemberangkatan/create', ['uses' => 'TanggalPemberangkatanController@create']);
		Route::post('/tanggal_pemberangkatan', ['uses' => 'TanggalPemberangkatanController@store']);
		Route::delete('/tanggal_pemberangkatan/{id}', ['uses' => 'TanggalPemberangkatanController@destroy']);
		Route::get('/tanggal_pemberangkatan/{id}/edit', ['uses' => 'TanggalPemberangkatanController@edit']);
		Route::post('/tanggal_pemberangkatan/{id}', ['uses' => 'TanggalPemberangkatanController@update']); //update

		Route::get('/tipe_keuangan', ['uses' => 'RefTipeKeuanganController@index']);
		Route::get('/tipe_keuangan/create', ['uses' => 'RefTipeKeuanganController@create']);
		Route::post('/tipe_keuangan', ['uses' => 'RefTipeKeuanganController@store']);
		Route::delete('/tipe_keuangan/{id}', ['uses' => 'RefTipeKeuanganController@destroy']);
		Route::get('/tipe_keuangan/{id}/edit', ['uses' => 'RefTipeKeuanganController@edit']);
		Route::post('/tipe_keuangan/{id}', ['uses' => 'RefTipeKeuanganController@update']); //update

		Route::get('/rekening', ['as' => 'admin.rekening.index', 'uses' => 'RekeningController@index']);
		Route::post('/rekening', ['as' => 'admin.rekening.store', 'uses' => 'RekeningController@store']);
		Route::get('/rekening/create', ['as' => 'admin.rekening.create', 'uses' => 'RekeningController@create']);
		Route::delete('/rekening/{id}', ['as' => 'admin.rekening.destroy', 'uses' => 'RekeningController@destroy']);
		Route::get('/rekening/{id}/edit', ['as' => 'admin.rekening.edit', 'uses' => 'RekeningController@edit']);
		Route::patch('/rekening/{id}', ['as' => 'admin.rekening.update', 'uses' => 'RekeningController@update']);
	});

	Route::group(['middleware' => 'role:owner,admin,manajer'], function() {

		Route::get('/data-jamaah', ['as' => 'admin.data-jamaah.index', 'uses' => 'DataJamaahController@index']);
		Route::get('/data-jamaah/{id}', ['as' => 'admin.data-jamaah.show', 'uses' => 'DataJamaahController@show']);
		Route::patch('/data-jamaah/{id}', ['as' => 'admin.data-jamaah.update', 'uses' => 'DataJamaahController@update']);
		Route::get('/data-jamaah/{id}/edit', ['as' => 'admin.data-jamaah.edit', 'uses' => 'DataJamaahController@edit']);


		Route::get('/dokumen', ['as' => 'admin.dokumen.index', 'uses' => 'DokumenController@index']);
		Route::post('/dokumen', ['as' => 'admin.dokumen.store', 'uses' => 'DokumenController@store']);
		Route::get('/dokumen/create', ['as' => 'admin.dokumen.create' ,'uses' => 'DokumenController@create']);
		Route::patch('/dokumen/{id}', ['as' => 'admin.dokumen.update' ,'uses' => 'DokumenController@update']);
		Route::delete('/dokumen/{id}', ['as' => 'admin.dokumen.destroy', 'uses' => 'DokumenController@destroy']);
		Route::get('/dokumen/{id}/edit', ['as' => 'admin.dokumen.edit', 'uses' => 'DokumenController@edit']);
		Route::get('/dokumen/data-jamaah', ['as' => 'admin.dokumen.data-jamaah', 'uses' => 'DokumenController@cekDokumenJamaah']);
		Route::post('/dokumen/check', ['as' => 'admin.dokumen.check', 'uses' => 'DokumenController@check']);
		Route::get('/dokumen/excel', ['as' => 'admin.dokumen.excel', 'uses' => 'DokumenController@exportExcel']);

		Route::get('/manifest', ['as' => 'admin.data_jamaah.manifest.index', 'uses' => 'ManifestController@index']);
		Route::get('/manifest/search', ['as' => 'admin.data_jamaah.manifest.search', 'uses' => 'ManifestController@index']);
		Route::delete('/manifest/{id}', ['as' => 'admin.data_jamaah.manifest.destroy', 'uses' => 'ManifestController@destroy']);
		Route::get('/manifest/{id}/show', ['as' => 'admin.data_jamaah.manifest.show', 'uses' => 'ManifestController@show']);
		Route::post('/manifest/export/excel', ['as' =>  'admin.data_jamaah.manifest.export', 'uses' => 'ManifestController@excel']);
	
		Route::get('/print-idcard', ['as' => 'admin.print-idcard.create', 'uses' => 'IdCardController@create']);
		Route::get('/print-idcard/search', ['as' => 'admin.print-idcard.search', 'uses' => 'IdCardController@search']);
		Route::get('/print-idcard/print', ['as' => 'admin.print-idcard.print', 'uses' => 'IdCardController@cetak']);
	});

	Route::group(['middleware' => 'role:owner,keuangan'], function() {
		Route::get('/tabungan', ['as' => 'admin.tabungan.index', 'uses' => 'TabunganController@index']);
		Route::post('/tabungan', ['as' => 'admin.tabungan.store', 'uses' => 'TabunganController@store']);
		Route::get('/tabungan/create', ['as' => 'admin.tabungan.create', 'uses' => 'TabunganController@create']);
		Route::patch('/tabungan/{id}', ['as' => 'admin.tabungan.update', 'uses' => 'TabunganController@update']);
		Route::delete('/tabungan/{id}', ['as' => 'admin.tabungan.destroy', 'uses' => 'TabunganController@destroy']);
		Route::get('/tabungan/edit/{id}', ['as' => 'admin.tabungan.edit', 'uses' => 'TabunganController@edit']);
		Route::get('/tabungan/search', ['as' => 'admin.tabungan.search', 'uses' => 'TabunganController@search']);
		Route::get('/tabungan/list', ['as' => 'admin.tabungan.list', 'uses' => 'TabunganController@listTabungan']);
	});

	Route::group(['middleware' => 'role:owner,keuangan'], function() {
		Route::get('/pembayaran', ['as' => 'admin.pembayaran.index', 'uses' => 'PembayaranController@index']);
		Route::get('/search', ['as' => 'admin.pembayaran.search', 'uses' => 'PembayaranController@search']);
		Route::post('/pembayaran', ['as' => 'admin.pembayaran.store', 'uses' => 'PembayaranController@store']);
		Route::get('/pembayaran/create-multi', ['as' => 'admin.pembayaran.create-multi', 'uses' => 'PembayaranController@createMulti']);
		Route::post('/pembayaran/create-multi', ['as' => 'admin.pembayaran.create-multi-store', 'uses' => 'PembayaranController@storeMulti']);
		Route::get('/pembayaran/searchpeserta', ['as' => 'admin.pembayaran.searchPeserta']);
		Route::delete('/pembayaran/{id}', ['as' => 'admin.pembayaran.destroy', 'uses' => 'PembayaranController@destroy']);
		Route::get('/pembayaran/create/{id}', ['as' => 'admin.pembayaran.create', 'uses' => 'PembayaranController@create']);
		Route::post('/pembayaran/konversi/{id}', ['as' => 'admin.pembayaran.konversi', 'uses' => 'PembayaranController@konversi']);
		Route::put('/pembayaran/updatestatus/{id}', ['as' => 'admin.pembayaran.update-status-pembayaran' ,'uses' => 'PembayaranController@updateStatusPembayaran']);
		Route::post('/pembayaran/print', ['as' => 'admin.pembayaran.print', 'uses' => 'PembayaranController@printKwitansi']);
		Route::get('/pembayaran/{id}/edit', ['as' => 'admin.pembayaran.edit', 'uses' => 'PembayaranController@edit']);
		Route::patch('/pembayaran/{id}', ['as' => 'admin.pembayaran.update' ,'uses' => 'PembayaranController@update']);
		Route::get('/pembayaran/laporan', ['as' => 'admin.pembayaran.laporan', 'uses' => 'LaporanPembayaranController@index']);
		Route::get('/pembayaran/laporan-ummu-ahmad/print', ['as' => 'admin.pembayaran.laporan-ummu-ahmad-print', 'uses' => 'LaporanPembayaranController@printUmmuAhmad']);
	
		//laporan keuangan
		Route::get('/keuangan', ['as' => 'admin.keuangan.index', 'uses' => 'KeuanganController@index']);
		Route::get('/keuangan/show/{id}', ['as' => 'admin.keuangan.show', 'uses' => 'KeuanganController@show']);
		Route::get('/keuangan/search', ['as' => 'admin.keuangan.search', 'uses' => 'KeuanganController@search']);
		Route::get('/keuangan/create', ['as' => 'admin.keuangan.create', 'uses' => 'KeuanganController@create']);
		Route::post('/keuangan/store', ['as' => 'admin.keuangan.store', 'uses' => 'KeuanganController@store']);
		Route::get('/keuangan/edit/{id}', ['as' => 'admin.keuangan.edit', 'uses' => 'KeuanganController@edit']);
		Route::patch('/keuangan/{id}', ['as' => 'admin.keuangan.update', 'uses' => 'KeuanganController@update']);
		Route::delete('/keuangan/{id}', ['as' => 'admin.keuangan.destroy', 'uses' => 'KeuanganController@destroy']);
		Route::post('/keuangan/export/excel', ['as' => 'admin.keuangan.export.excel', 'uses' => 'KeuanganController@exportExcel']);

		Route::get('/rekening/laporan', ['as' => 'admin.rekening.laporan', 'uses' => 'RekeningController@laporanSaldo']);
	});

	//edit profile
	Route::get('/profile', ['as' => 'admin.profile.index', 'uses' => 'EditProfileController@index']);
	Route::patch('/profile/{id}', ['as' => 'admin.profile.update', 'uses' => 'EditProfileController@update']);

	//laporan perkuota
	Route::get('/laporan_perkuota', ['as' => 'admin.laporan_perkuota.index', 'uses' => 'LaporanPerkuotaController@index']);
});
	
	//pendaftaran Online
	Route::post('/pendaftaran_online/store', ['as' => 'admin.daftar.online.store', 'uses' => 'PendaftaranOnlineController@store']);
	Route::get('/pendaftaran_online', ['as' => 'admin.daftar.online.create',  'uses' => 'PendaftaranOnlineController@create']);
	

 

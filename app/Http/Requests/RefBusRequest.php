<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RefBusRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipe_paket' => 'required',
            'nama_bus' => 'required',
            'max_jamaah' => 'required|numeric'
        ];
    }
}

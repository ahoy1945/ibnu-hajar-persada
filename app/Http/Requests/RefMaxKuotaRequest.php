<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RefMaxKuotaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipe_paket' => 'required',
            'tgl_berangkat' => 'required',
            'max_kuota' => 'required|numeric'
        ];
    }
}

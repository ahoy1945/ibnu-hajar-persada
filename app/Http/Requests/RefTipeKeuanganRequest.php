<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RefTipeKeuanganRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
             'tipe_keuangan' => 'required',
             'nama_tipe' => 'required',
         ];
     }

     public function messages()
     {
       return [
         'tipe_keuangan.required' => 'Tipe keuangan tidak boleh kosong',
         'nama_tipe.required' => 'Nama tipe tidak boleh kosong',
       ];
     }
}

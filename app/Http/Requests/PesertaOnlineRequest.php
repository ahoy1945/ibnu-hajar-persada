<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use JavaScript;

class PesertaOnlineRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

     /**
     * Get the proper failed validation response for the request.
     *
     * @param  array  $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        if ($this->ajax() || $this->wantsJson()) {
            return new JsonResponse($errors, 422);
        }
        
      
        return $this->redirector->to($this->getRedirectUrl())
                                        ->withInput($this->except($this->dontFlash))
                                        ->withErrors($errors, $this->errorBag);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'nama_peserta' => 'required',
                    'tipe_paket' => 'required',
                    'harga_paket_id' => 'required',
                    'paket_hotel' => 'required',
                    'tipe_kamar' => 'required',
                    'tipe_pesawat' => 'required',
                    'pesawat_id' => 'required',

                ];
                break;
        case 'PATCH':
             return [
                    'nama_peserta' => 'required',
                    'kepala_keluarga' => 'required',
                    'tipe_paket' => 'required',
                    'harga_paket_id' => 'required',
                    'paket_hotel' => 'required',
                    'tipe_kamar' => 'required',
                    'tipe_pesawat' => 'required',
                    'pesawat_id' => 'required',
                    'hubungan_mahram' => 'required'
                ];
            break;
        }
        
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HargaPaketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipe_paket' => 'required',
            'nama_paket' => 'required',
            'harga_paket' => 'required|numeric'
        ];
    }

    public function messages()
    {
      return [
        'tipe_paket.required' => 'Tipe paket tidak boleh kosong',
        'nama_paket.required' => 'Nama paket tidak boleh kosong',
        'harga_paket.required' => 'Harga paket tidak boleh kosong',
        'harga_paket.numeric' => 'Harga paket harus berupa angka',
      ];
    }
}

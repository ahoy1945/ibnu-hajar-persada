<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PembayaranRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PATCH':
                return [
                    'tgl_bayar' => 'required|date',
                    'tipe_keuangan_id' => 'required',
                    'rekening_id' => 'required',
                    'mata_uang' => 'required',
                    'jumlah' => 'required|integer'
                ];
                break;
            
            
        }

    }
}

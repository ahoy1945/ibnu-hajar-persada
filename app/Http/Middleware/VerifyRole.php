<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class VerifyRole
{
    /**
     * @var  Illuminate\Contracts\Auth\Guard;
     */
    protected $auth;

    /**
     * Create a new instance
     * 
     * @param Guard $auth 
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $userRole = $this->auth->user()->roles->nama;
        if(! in_array($userRole, $roles)) {
            return redirect()->route($this->auth->user()->roles->default_route)
                             ->with('error_message', 'Maaf anda tidak bisa mengakses halaman tersebut');
        }

        return $next($request);
    }
}

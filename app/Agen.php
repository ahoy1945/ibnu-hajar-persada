<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agen extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agen';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['no_hp', 'alamat', 'desa', 'kecamatan', 'kabupaten' , 'provinsi', 'no_rek'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     * 
     * @var array
     */
    protected $dates = [];

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesertaInventory extends Model
{
    protected $table = ['peserta_inventory'];
    
    protected $fillable = ['peserta_id', 'inventory_id'];
    
    protected $guarded = [];
    
    protected $dates = [];
}

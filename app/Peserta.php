<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
 	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'peserta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_peserta',
        'nama_ayah',
        'tempat_lahir',
        'diskon',
        'tgl_lahir',
        'jenis_kelamin',
        'kewarganegaraan',
        'pendidikan',
        'pekerjaan',
        'pernah_haji_umroh',
        'golongan_darah',
        'no_telp',
        'no_hp',
        'ukuran_baju_kerudung',
        'tipe_paket',
        'who_insert',
        'who_insert',
        'kepala_keluarga',
        'hubungan_mahram',
        'peserta_id',
        'konversi',
        'status_pembayaran',
        'no_porsi',
        'attach_file',
        'photo'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     *
     * @var array
     */
    protected $dates = [];

    // public function getTglLahirAttribute($value)
    // {
    //     return date_diff(date_create($value), date_create('today'))->y;
    // }

    public function inventories()
    {
        return $this->belongsToMany('App\Inventory', 'peserta_inventory', 'peserta_id', 'inventory_id')->withTimestamps();
    }

    public function documents()
    {
        return $this->belongsToMany('App\RefDokumen', 'peserta_dokumen', 'peserta_id', 'dokumen_id');
    }

    //define relationship
    public function identitas()
    {
      return $this->hasOne('App\PesertaIdentitas');
    }

    public function alamat()
    {
      return $this->hasOne('App\PesertaAlamat');
    }

    public function paket()
    {
      return $this->hasOne('App\PesertaPaket', 'peserta_id', 'id');
    }

    public function mahram()
    {
      return $this->hasOne('App\Mahram');
    }

    public function peserta()
    {
      return $this->hasMany('App\Peserta', 'peserta_id', 'id');
    }

    public function pembayaran()
    {
      return $this->hasMany('App\PesertaPembayaran', 'peserta_id');
    }

    public function user()
    {
      return $this->belongsTo('App\User', 'who_insert');
    }



}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class PesertaPaket extends Model
{
    protected $table = 'peserta_paket';

    protected $fillable = [
      'peserta_id',
      'nama_paket',
      'tipe_kamar',
      'tgl_berangkat',
      'paket_hotel',
      'harga_paket_id',
      'bus',
      'tipe_pesawat',
      'pesawat_id'
    ];

    public function peserta()
    {
      return $this->belongsTo('App\Peserta', 'peserta_id', 'id');
    }

    public function harga()
    {
      return $this->belongsTo('App\HargaPaket', 'harga_paket_id');
    }

    public function bus()
    {
      return $this->belongsTo('App\RefBus', 'bus_id');
    }

    public function pesawat()
    {
      return $this->belongsTo('App\RefPesawat', 'pesawat_id');      
    }

    // public function getTglBerangkatAttribute($value)
    // {
    // 	Date::setLocale('id');
    //
    // 	return Date::parse($value)->format('d F Y');
    //
    // }

}

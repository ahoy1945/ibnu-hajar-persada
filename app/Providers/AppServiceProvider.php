<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
          $this->app->bind(
                'App\Repositories\User\UserRepository',
                'App\Repositories\User\EloquentUserRepository'
            );

          $this->app->bind(
                'App\Repositories\Role\RoleRepository',
                'App\Repositories\Role\EloquentRoleRepository'
            );

          $this->app->bind(
                'App\Repositories\Agen\AgenRepository',
                'App\Repositories\Agen\EloquentAgenRepository'
            );

          $this->app->bind(
                  'App\Repositories\Inventory\InventoryRepository',
                  'App\Repositories\Inventory\EloquentInventoryRepository'
              );
          $this->app->bind(
                  'App\Repositories\Peserta\PesertaRepository',
                  'App\Repositories\Peserta\EloquentPesertaRepository'
              );
          $this->app->bind(
                  'App\Repositories\PesertaOnline\PesertaOnlineRepository',
                  'App\Repositories\PesertaOnline\EloquentPesertaOnlineRepository'
              );

          $this->app->bind(
                'App\Repositories\MasterData\HargaPaket\HargaPaketRepository',
                'App\Repositories\MasterData\HargaPaket\EloquentHargaPaketRepository'
            );
          $this->app->bind(
                'App\Repositories\MasterData\TanggalPemberangkatan\TanggalPemberangkatanRepository',
                'App\Repositories\MasterData\TanggalPemberangkatan\EloquentTanggalPemberangkatanRepository'
            );
          $this->app->bind(
                'App\Repositories\MasterData\TipeKeuangan\TipeKeuanganRepository',
                'App\Repositories\MasterData\TipeKeuangan\EloquentTipeKeuanganRepository'
            );
          $this->app->bind(
             'App\Repositories\Keuangan\KeuanganRepository',
             'App\Repositories\Keuangan\EloquentKeuanganRepository'
         );
          $this->app->bind(
             'App\Repositories\PesertaPembayaran\PesertaPembayaranRepository',
             'App\Repositories\PesertaPembayaran\EloquentPesertaPembayaranRepository'
         );
          $this->app->bind(
             'App\Repositories\PesertaPaket\PesertaPaketRepository',
             'App\Repositories\PesertaPaket\EloquentPesertaPaketRepository'
           );
          $this->app->bind(
             'App\Repositories\DataJamaah\RoomList\RoomListRepository',
             'App\Repositories\DataJamaah\RoomList\EloquentRoomListRepository'
           );
          $this->app->bind(
             'App\Repositories\DataJamaah\Manifest\ManifestRepository',
             'App\Repositories\DataJamaah\Manifest\EloquentManifestRepository'
           );
          $this->app->bind(
             'App\Repositories\InventoryHistory\InventoryHistoryRepository',
             'App\Repositories\InventoryHistory\EloquentInventoryHistoryRepository'
           );

    }
}

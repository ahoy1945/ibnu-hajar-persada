<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ref_rekening';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['bank', 'atas_nama', 'no_rek', 'mata_uang'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     * 
     * @var array
     */
    protected $dates = [];

    public function pembayaran()
    {
        return $this->hasMany(\App\PesertaPembayaran::class, 'rekening_id');
    }
}

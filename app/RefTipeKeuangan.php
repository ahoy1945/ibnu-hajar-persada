<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefTipeKeuangan extends Model
{
    protected $table = 'ref_tipe_keuangan';

    protected $fillable = ['tipe_keuangan', 'nama_tipe'];


}

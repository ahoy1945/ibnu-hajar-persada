<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesertaOnline extends Model
{
  /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'peserta_online';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_peserta',
        'nama_ayah',
        'tempat_lahir',
        'diskon',
        'tgl_lahir',
        'jenis_kelamin',
        'kewarganegaraan',
        'pendidikan',
        'pekerjaan',
        'pernah_haji_umroh',
        'golongan_darah',
        'no_telp',
        'no_hp',
        'ukuran_baju_kerudung',
        'tipe_paket',
        'who_insert',
        'who_insert',
        'kepala_keluarga',
        'hubungan_mahram',
        'peserta_id',
        'konversi',
        'status_pembayaran',
        'no_porsi',
        'attach_file',
        'photo',
        
        'no_ktp',
        'no_passpor',
        'tempat_pembuatan',
        'tgl_pembuatan',
        'tgl_expired',

        'kodepos',
        'alamat',
        'desa',
        'kecamatan',
        'kabupaten',
        'provinsi',

        'pesawat_id',
        'harga_paket_id',
        'paket_hotel',
        'tipe_pesawat',
        'tipe_kamar',
        'tgl_berangkat',
        'dokumen_id'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     *
     * @var array
     */
    protected $dates = [];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Keuangan extends Model
{
    protected $table = 'keuangan';

    protected $fillable = ['tanggal', 'ref_tipe_keuangan_id', 'keterangan', 'debit', 'kredit', 'saldo', 'type', 'user_id','id_rekening'];

    protected $dates = ['created_at', 'updated_at', 'tanggal'];

    public function user()
    {
    	return $this->belongsTo(\App\User::class);
    }

    public function tipe()
    {
    	return $this->belongsTo('App\RefTipeKeuangan', 'ref_tipe_keuangan_id');
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inventory';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nama', 'jumlah', 'ket', 'tipe'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     * 
     * @var array
     */
    protected $dates = [];

    /**
     * Pembayaran belongsTo Peserta
     * 
     * @return Relationship
     */
    public function peserta()
    {
    	return $this->belongsTo(App\Peserta::class, 'peserta_id');
    }

    /**
     * Pembayaran belongsTo Rekening
     * 
     * @return Relationship
     */
    public function rekening()
    {
    	return $this->belongsTo(App\Rekening::class, 'rekening_id');
    }

}

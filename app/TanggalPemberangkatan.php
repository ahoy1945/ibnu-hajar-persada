<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TanggalPemberangkatan extends Model
{
    protected $table = 'ref_tanggal_pemberangkatan';

    protected $fillable = ['nama_pemberangkatan', 'tanggal_pemberangkatan', 'tipe', 'paket_id'];

    public function paket()
    {
    	return $this->belongsTo(\App\HargaPaket::class, 'paket_id');
    }
}

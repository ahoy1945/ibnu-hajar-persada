<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provinces';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Additional fields to treat as Carbon instance
     * 
     * @var array
     */
    protected $dates = [];

    public function kabupaten()
    {
        return $this->hasMany('App\Kabupaten', 'province_id');
    }
}

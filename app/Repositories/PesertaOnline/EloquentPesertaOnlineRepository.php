<?php

namespace App\Repositories\PesertaOnline;

use App\Desa;
use App\Inventory;
use App\Kabupaten;
use App\Kecamatan;
use App\PesertaOnline;
use App\PesertaAlamat;
use App\PesertaIdentitas;
use App\PesertaMahram;
use App\PesertaPaket;
use App\Provinsi;
use App\TanggalPemberangkatan as Date;

use Illuminate\Auth\Guard  as Auth;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Pagination\LengthAwarePaginator;
use InvalidArgumentException;

use App\Peserta;

class EloquentPesertaOnlineRepository implements PesertaOnlineRepository
{

	/**
	 * @var App\User
	 */
	protected $model;

	/**
	 * @var App\User
	 */
	protected $inventory;


	/**
	 * @var App\PesertaAlamat
	 */
	protected $pesertaAlamat;

	/**
	 * @var App\PesertaMahram
	 */
	protected $pesertaMahram;

	/**
	 * @var App\PesertaIdentitas
	 */
	protected $pesertaIdentitas;

	/**
	 * @var App\PesertaPaket
	 */
	protected $pesertaPaket;

	/**
	 * @var App\Desa
	 */
	protected $desa;

	/**
	 * @var App\Kecamatan
	 */
	protected $kecamatan;

	/**
	 * @var App\Kabupaten
	 */
	protected $kabupaten;

	/**
	 * @var App\Provinsi
	 */
	protected $provinsi;
	/**
	 * @var Illuminate\Auth\Guard
	 */
	protected $auth;

	protected $peserta;

	/**
	 * @var Illuminate\Filesystem\Filesystem
	 */
	protected $filesystem;

	/**
	 * Create a new instance
	 *
	 * @param Peserta          $model
	 * @param InventoryRepository $inventory
	 * @param Auth             $auth
	 * @param PesertaMahram    $pesertaMahram
	 * @param PesertaPaket     $pesertaPaket
	 * @param PesertaIdentitas $pesertaIdentitas
	 * @param PesertaAlamat    $pesertaAlamat
	 * @param Desa             $desa
	 * @param Kecamatan        $kecamatan
	 * @param Kabupaten        $kabupaten
	 * @param Provinsi         $provinsi
	 */
	public function __construct(
		PesertaOnline $model,
		Peserta $peserta,
		Auth $auth,
		PesertaMahram $pesertaMahram,
		PesertaPaket $pesertaPaket,
		PesertaIdentitas $pesertaIdentitas,
		PesertaAlamat $pesertaAlamat,
		Desa $desa,
		Kecamatan $kecamatan,
		Kabupaten $kabupaten,
		Provinsi $provinsi,
		Filesystem $filesystem)
	{
		$this->model 			= $model;
		$this->peserta 			= $peserta;
		$this->auth  			= $auth;
		$this->pesertaMahram 	= $pesertaMahram;
		$this->pesertaPaket 	= $pesertaPaket;
		$this->pesertaIdentitas = $pesertaIdentitas;
		$this->pesertaAlamat 	= $pesertaAlamat;
		$this->desa 			= $desa;
		$this->kecamatan 		= $kecamatan;
		$this->kabupaten 		= $kabupaten;
		$this->provinsi 		= $provinsi;
		$this->filesystem 		= $filesystem;	
	}

	public function all($perPage = 10, $orderBy = 'DESC', $columns = ['*'])
	{
		
		$query = $this->model->orderBy('id', $orderBy)->paginate($perPage, $columns);

		return $query;			
	}

	public function search($keyword = "", $perPage = 10, $orderBy = 'DESC', $columns = ['*'])
	{
		return $this->model
					->where('nama_peserta', 'LIKE', "%{$keyword}%")
					->orderBy('id', $orderBy)
					->paginate($perPage, $columns);
	}

	public function getItemBy($attribute, $value, $columns = ['*'])
	{ 
		$query = $this->model->where($attribute, '=', $value)->first($columns);

		return $query;
	}

	public function find($id)
	{
		return $this->model->find($id);
	}

	public function where($attr, $val)
	{
		return $this->model->where($attr, $val);
	}


	public function create($data = [])
	{

		try {

			if(isset($data['kepala_keluarga']))  
				unset($data['kepala_keluarga']); 

			if (isset($data['dokumen'])) {
				$data['dokumen_id'] = json_encode($data['dokumen']);	
			}
			
			$peserta = $this->model->create($data);
			if(isset($data['photo']) && !empty($data['photo'])) {
				$peserta = array_add($peserta, 'photo', $data['photo']);
			}


		} catch (Exception $e) {
			echo "Error, silahkan hubungi web master!";
		}


	}

	public function moveToPeserta(array $data)
	{
		try {
			
		
            
			$peserta = ($data['kepala_keluarga'] === "1" ? $this->peserta->create($this->dataMahram($data)) : $this->peserta->create($this->dataPeserta($data)) );
			if (isset($data['dokumen'])) {
				foreach($data['dokumen'] as $dokumen) {
					$peserta->documents()->attach($dokumen);
				}
			}
			 
			
			$this->pesertaAlamat->create([
				'peserta_id' => $peserta->id,
				'desa' => $data['desa'],
				'kecamatan' => ifIsset($data, 'kecamatan'),
				'kabupaten' => ifIsset($data, 'kabupaten'),
				'provinsi' => ifIsset($data, 'provinsi'),
				'kodepos' => ifIsset($data, 'kodepos'),
				'alamat' => ifIsset($data, 'alamat')]);

			$this->pesertaPaket->create([
				'peserta_id' => $peserta->id,
				'harga_paket_id' => $data['harga_paket_id'],
				'paket_hotel' => $data['paket_hotel'],
				'tipe_kamar' => $data['tipe_kamar'],
				'tgl_berangkat' => $data['tgl_berangkat'],
				'pesawat_id' => $data['pesawat_id'],
				'tipe_pesawat' => $data['tipe_pesawat'],
			]);

			$this->pesertaIdentitas->create([
				'peserta_id' => $peserta->id,
				'no_ktp' => ifIsset($data, 'ktp'),
				'no_passpor' => ifIsset($data, 'no_passpor'),
				'tempat_pembuatan' => ifIsset($data, 'tempat_pembuatan_passpor'),
				'tgl_pembuatan' => ifIsset($data, 'tgl_pembuatan_passpor'),
				'tgl_expired' =>  ifIsset($data, 'tgl_expired_passpor')
				]);


		} catch (Exception $e) {
			echo "Error hubungi web master";
		}
	}

	public function deleteOldData($id)
	{
		if(is_null($id) || empty($id))
		{
			throw new InvalidArgumentException('Invalid ID');
		}
		
		$jamaah =  $this->model->where('id', $id)->first();
		
		return $jamaah->delete();		
	}

	public function delete($id)
	{
		if(is_null($id) || empty($id))
		{
			throw new InvalidArgumentException('Invalid ID');
		}

		$jamaah =  $this->model->where('id', $id)->first();
		if($this->filesystem->exists(public_path().$jamaah->photo)) {
			 $this->filesystem->delete(public_path().$jamaah->photo);
		}

		return $jamaah->delete();
	}

	public function searchMahram($keyword)
	{
		return $this->model->where('peserta_id', NULl)
						   ->where('nama_peserta', 'LIKE', "%$keyword%")
						   ->get();
	}

	public function searchPeserta($keyword)
	{
		return $this->model->with(['user' => function($query) {
			$query->with('roles');
		}])->where('nama_peserta', 'LIKE', "%{$keyword}%")->get();
	}

	public function dataMahram($data)
	{
		$mahram = [
			'nama_peserta' => ifIsset($data, 'nama_peserta'),
			'tempat_lahir' => ifIsset($data, 'tempat_lahir'),
			'nama_ayah' => ifIsset($data, 'nama_ayah'),
			'no_telp' => ifIsset($data, 'no_telp'),
			'no_hp' => ifIsset($data, 'no_hp'),
			'pekerjaan' => ifIsset($data, 'pekerjaan'),
			'golongan_darah' => ifIsset($data, 'golongan_darah'),
			'tipe_paket' => ifIsset($data, 'tipe_paket'),
			'jenis_kelamin' => ifIsset($data, 'jk'),
			'kewarganegaraan' => ifIsset($data, 'kewarganegaraan'),
			'pernah_haji_umroh' => ifIsset($data, 'pernah_haji_umroh'),
			'ukuran_baju_kerudung' => ifIsset($data, 'ukuran_baju_kerudung'),
			'who_insert' => $this->auth->user()->id,
			'tgl_lahir' => ifIsset($data, 'tgl_lahir'),
			'hubungan_mahram' => ifIsset($data, 'hubungan_mahram'),
		];

			//image
			if(isset($data['photo']) && !empty($data['photo']) && empty($data['old_photo'])) {
				$mahram = array_add($mahram, 'photo', $data['photo']);
			} else {
				$mahram['photo'] = $data['old_photo'];
			}

		return $mahram;
	}

	public function dataPeserta($data)
	{

		$peserta =  [
			'nama_peserta' => ifIsset($data, 'nama_peserta'),
			'tempat_lahir' => ifIsset($data, 'tempat_lahir'),
			'nama_ayah' => ifIsset($data, 'nama_ayah'),
			'no_telp' => ifIsset($data, 'no_telp'),
			'no_hp' => ifIsset($data, 'no_hp'),
			'pekerjaan' => ifIsset($data, 'pekerjaan'),
			'golongan_darah' => ifIsset($data, 'golongan_darah'),
			'tipe_paket' => ifIsset($data, 'tipe_paket'),
			'jenis_kelamin' => ifIsset($data, 'jk'),
			'kewarganegaraan' => ifIsset($data, 'kewarganegaraan'),
			'pernah_haji_umroh' => ifIsset($data, 'pernah_haji_umroh'),
			'ukuran_baju_kerudung' => ifIsset($data, 'ukuran_baju_kerudung'),
			'who_insert' => $this->auth->user()->id,
			'tgl_lahir' => ifIsset($data, 'tgl_lahir'),
			'peserta_id' => ifIsset($data, 'mahram_id'),
			'hubungan_mahram' => ifIsset($data, 'hubungan_mahram'),
			
		];

			//image
			if(isset($data['photo']) && !empty($data['photo']) && empty($data['old_photo'])) {
				$peserta = array_add($peserta, 'photo', $data['photo']);
			} else {
				$peserta['photo'] = $data['old_photo'];
			}

		return $peserta;


	}

	public function update($data, $id, $attribute = 'id') 
	{

	}
}

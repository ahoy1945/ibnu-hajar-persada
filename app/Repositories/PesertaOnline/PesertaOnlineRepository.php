<?php 

namespace App\Repositories\PesertaOnline;

interface PesertaOnlineRepository
{
	public function all($perPage = 10, $columns = ['*']);
	public function create($data = []);
	public function delete($id);
	public function update($data = [], $id, $attribute = 'id');
}
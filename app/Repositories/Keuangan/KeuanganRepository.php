<?php 

namespace App\Repositories\Keuangan;

interface KeuanganRepository
{
	public function all($coloums = ['*']);
	public function item($id);
	public function create(array $data = []);
	public function update(array $data = [], $id, $attribute = 'id');
	public function delete($id);
	public function getFirst($coloums = 'created_at', $orderBy = 'DESC');
}
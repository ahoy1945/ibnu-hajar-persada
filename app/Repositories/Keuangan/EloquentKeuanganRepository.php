<?php

namespace App\Repositories\Keuangan;

use Datetime;
use \RuntimeException;
use App\Keuangan;
use App\KeuanganDollar;
use Illuminate\Http\Request;
use \InvalidArgumentException;
use App\Repositories\Inventory\InventoryRepository;
use App\Repositories\InventoryHistory\InventoryHistoryRepository;

class EloquentKeuanganRepository implements KeuanganRepository
{
	protected $model;

	protected $inventory;

	protected $inventoryHistory;

	protected $tableKeuangan;
	
	public function __construct(
			Keuangan $modelRp, 
			KeuanganDollar $modelDollar, 
			InventoryRepository $inventory, 
			InventoryHistoryRepository $inventoryHistory,
			Request $request)
	{
		$this->model = $modelRp;
		$this->tableKeuangan = 'keuangan';
		if ($request->input('mata_uang') == 'dollar' || @$request->all()[0]['mata_uang'] == 'dollar') {
			$this->model = $modelDollar;
			$this->tableKeuangan = 'keuangan_dollar';
		}
 		$this->perPage = getenv('PER_PAGE') ? getenv('PER_PAGE') : 5;
		$this->inventory = $inventory;
		$this->inventoryHistory = $inventoryHistory;
	}

	public function all($parameter = [],$perPage = 10, $coloums = ['*'], $orderBy = 'DESC')
	{

		if($parameter) {
			 
			return $this->model->join('ref_rekening', $this->tableKeuangan.'.id_rekening', '=', 'ref_rekening.id')
			->where(function($query) use ($parameter) {
				if($parameter['type'] != 'all' && !empty($parameter['type'])) {
					$query->where('type', $parameter['type']);
				}

				if($parameter['from'] && $parameter['to']) {
					$query->where('tanggal', '>=',  $parameter['from']." 00:00:00");
			        $query->where('tanggal', '<=', $parameter['to']." 23:59:59");
				}
			})->with('tipe')
			->select($this->tableKeuangan.'.*', 'ref_rekening.atas_nama', 'ref_rekening.no_rek')
			->orderBy($this->tableKeuangan.'.created_at', $orderBy)->paginate($perPage, $coloums);
		
		}

		return $this->model->join('ref_rekening', $this->tableKeuangan.'.id_rekening', '=', 'ref_rekening.id')
		->where($parameter)->select($this->tableKeuangan.'.*', 'ref_rekening.atas_nama', 'ref_rekening.no_rek')
		->orderBy($this->tableKeuangan.'.created_at', $orderBy)->paginate($perPage, $coloums);
	}

	public function item($id)
	{
		return $this->model->findOrFail($id);
	}

	public function getFirst($coloums = 'created_at', $orderBy = 'DESC')
	{
		return $this->model->orderBy($coloums, $orderBy)->first();
	}

	public function create(array $data = [])
	{
		 
		foreach($data as $insertData) {
			if(isset($insertData['nama_barang'])) {
				$this->inventory->create(['nama' => $insertData['nama_barang'], 'jumlah' => $insertData['jumlah_barang'], 'tipe' => 'lain-lain']);	
			} 	 
			$this->model->create($insertData);
		}	

	}

	public function update(array $data = [], $id = null, $attribute='id')
	{
       
		if(empty($id) || is_null($id)) 
		{
			throw new InvalidArgumentException("Invalid ID");
		}

		$model = $this->model->find($id);

		if(empty($model) || is_null($model))
		{
			throw new InvalidArgumentException("Item not found");
			
		}

		try {
	
			$update = $model->where($attribute, '=', $id)->update($data);
	
		} catch (Exception $e) {
	
			throw new RuntimeException($e->getMessage(), 1);			
	
		}

		return $update;
	}

	public function delete($id)
	{
		return $this->model->find($id)->delete();
	}

	public function search($keyword = '', $perPage = 5, $orderBy = 'DESC', $columns = ['*'])
	{
		return $this->model->where('keterangan', 'LIKE', "%$keyword%")->orderBy('created_at', $orderBy)->paginate($perPage, $columns);
	}

	public function export($parameter = [], $orderBy = 'DESC', $columns = ['*'])
	{
		$from =  $parameter['from'];
		$to = $parameter['to'];	
		$type = $parameter['type'];
 
		return $this->model->join('ref_rekening', $this->tableKeuangan.'.id_rekening', '=', 'ref_rekening.id')
		->where(function($query) use ($parameter) {
			if($parameter['type'] != 'all' && !empty($parameter['type'])) {
				$query->where('type', $parameter['type']);
			}

			if($parameter['from'] && $parameter['to']) {
				$query->where('tanggal', '>=',  $parameter['from']." 00:00:00");
		        $query->where('tanggal', '<=', $parameter['to']." 23:59:59");
			}
		})->with('tipe')
		->select($this->tableKeuangan.'.*', 'ref_rekening.atas_nama', 'ref_rekening.no_rek')
		->orderBy($this->tableKeuangan.'.created_at', $orderBy)->get($columns);
	}

	public function chart($from, $to)
	{  
		 $newData = [];
		 $realData = [
		 	'categories' => [],
		 	'data' => [
		 		'kredit' => [],
		 		'debit' => []
		 	]
		 ];

		 $data = $this->model->where(function($query) use ($from, $to) {
			 $monthNow =  date("Y-m");
			 $monthPrev = date("Y-m", strtotime("-3 months"));
		 
			 if($from && $to) {
			 	//dd($from."-01 00:00:00",$to."-31 23:59:59");
			 	$query->where('tanggal', '>=', $from."-01");
			 	$query->where('tanggal', '<=', $to."-31");
			 } else {
			 	$query->where('tanggal', '>=', $monthPrev."-01");
			 	$query->where('tanggal', '<=', $monthNow."-31");
			 }
		})->get();

		 

		 $i = 0;
		 foreach($data as $key => $value) {
			$dateFromdatabase = $value->tanggal->format('Y-m');

			if(!$newData || !array_key_exists($dateFromdatabase, $newData)) {
				$newData[$dateFromdatabase]['kredit'] = (int) $value->kredit;
				$newData[$dateFromdatabase]['debit'] = (int) $value->debit;
			} else {
				foreach($newData as $keyLabel => $valueLabel) {
					if($keyLabel == $dateFromdatabase) {
						$newData[$dateFromdatabase]['kredit'] += (int) $value->kredit;
						$newData[$dateFromdatabase]['debit'] += (int) $value->debit;
						break;
					}
				}
				
			}

			$i++;
		 }

		 //return $newData;

		 foreach($newData as $keyNewdata => $valueNewData) {
		 	array_push($realData['categories'], $keyNewdata);
		 	array_push($realData['data']['kredit'], $valueNewData['kredit']);
		 	array_push($realData['data']['debit'], $valueNewData['debit']);
		 } 

		 return $realData;
		 	

	}

	public function statistic()
	{
		$awal = date('Y-m')."-01";
		$akhir = date('Y-m')."-31";
        
		$debit = $this->model->where('tanggal','>=', $awal)->where('tanggal', '<=', $akhir )->sum('debit');
		$kredit = $this->model->where('tanggal', '>=', $awal)->where('tanggal', '<=', $akhir )->sum('kredit');
		$saldo = $debit - $kredit;

		return [
			'debit' => $debit,
			'kredit' => $kredit,
			'saldo' => $saldo
		]; 

	}

	public function totalPemasukanPerbulan()
	{
		// $first  = strtotime('first day this month');
		$first = strtotime('first day of January '.date('Y'));
        $months = [];
        for ($i = 0; $i < 12; $i++) {
          $months[date('Y-m', strtotime("$i month", $first))] = 0;
          //array_push($months, date('Y-m', strtotime("-$i month", $first)));
        }

        foreach($this->model->where('debit', '>', 0)->get() as $key => $value) {
         	
        	$tglBerangkat = substr($value->tanggal, 0,7);
        	if(array_key_exists($tglBerangkat, $months)) {
        		$months[$tglBerangkat]+= $value->debit;
        	}
        } 
        return $months;
	}

	public function totalPemasukanPerTahun()
	{
		$years = [];

		for ($i=0; $i <=5 ; $i++) { 
			$years[date("Y",strtotime("{$i} year"))] =  0;
		}

		 foreach($this->model->where('debit', '>' ,0)->get() as $key => $value) {
        	$tglBerangkat = substr($value->created_at, 0,4);

        	if(array_key_exists($tglBerangkat, $years)) {
        		$years[$tglBerangkat]+= $value->debit;
        	}
        }

   
      	return $years;
	}

	public function totalPengeluaranPerbulan()
	{
		// $first  = strtotime('first day this month');
		$first = strtotime('first day of January '.date('Y'));
        $months = [];

        for ($i = 0; $i < 12; $i++) {
          $months[date('Y-m', strtotime("$i month", $first))] = 0;
          //array_push($months, date('Y-m', strtotime("-$i month", $first)));
        }
 
        foreach($this->model->where('kredit', '>', 0)->get() as $key => $value) {
         
        	$tglBerangkat = substr($value->tanggal, 0,7);
        	if(array_key_exists($tglBerangkat, $months)) {
        		$months[$tglBerangkat]+= $value->kredit;
        	}
        } 
       
        return $months;
	}	

	public function totalPengeluaranPerTahun()
	{
		$years = [];

		for ($i=0; $i <=5 ; $i++) { 
			$years[date("Y",strtotime("{$i} year"))] =  0;
		}

		 foreach($this->model->where('kredit', '>' ,0)->get() as $key => $value) {
        	$tglBerangkat = substr($value->created_at, 0,4);

        	if(array_key_exists($tglBerangkat, $years)) {
        		$years[$tglBerangkat]+= $value->kredit;
        	}
        }

   
      	return $years;
	}

}
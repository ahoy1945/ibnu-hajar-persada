<?php 

namespace App\Repositories\PesertaPaket;

interface PesertaPaketRepository
{
	public function all($perPage = 10, $columns = ['*']);
}
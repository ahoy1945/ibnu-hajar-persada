<?php 

namespace App\Repositories\PesertaPaket;

use App\Peserta;
use App\PesertaPaket;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class EloquentPesertaPaketRepository implements PesertaPaketRepository
{
	/**
	 * App\PesertaPaket
	 */
	protected $model;

	/**
	 * @var App\Peserta;
	 */
	protected $peserta;

	protected $tipePaket = ['haji', 'umroh'];

	/**
	 * create a new instance
	 */
	public function __construct(PesertaPaket $model, Peserta $peserta)
	{
		$this->model = $model;
		$this->peserta = $peserta;
	}

	public function all($where = [], $perPage = 10, $columns = ['*'])
	{
		 
	}

	public function search($request, $perPage = 20, $columns = ['*'])
	{		

		    $query = $this->peserta->join('peserta_alamat', 'peserta.id', '=', 'peserta_alamat.peserta_id')
								   ->join('peserta_identitas', 'peserta.id', '=', 'peserta_identitas.peserta_id')
								   ->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')	 						   
								  
								   ->join('peserta_pembayaran', function($join) {
								   		$join->on('peserta.id', '=', 'peserta_pembayaran.peserta_id');
								   })
								   ->with('user')	
								   ->select('peserta.*', 'peserta_identitas.*', 'peserta_paket.*', 'peserta_pembayaran.*', 'peserta_pembayaran.id as pembayaran_id');	
			if($request->has('tipe') && !empty($request->input('tipe')) && in_array($request->input('tipe'), $this->tipePaket)) {
			  
			   $query = $query->where('peserta.tipe_paket', $request->input('tipe'));
			}					   
								   
			if($request->has('tgl_berangkat') && !empty($request->input('tgl_berangkat'))) {
				if($request->input('tipe') == "haji") {
					$query  = $query->whereBetween('peserta_paket.tgl_berangkat',[$request->input('tgl_berangkat')."-01-01", $request->input('tgl_berangkat')."-12-31"]);
				} else {
					$query  = $query->where('peserta_paket.tgl_berangkat', $request->input('tgl_berangkat'));				
				}

			}								   
			if($request->has('keyword') && !empty($request->input('keyword'))) {
				$query = $query->where('nama_peserta', 'LIKE', "%{$request->input('keyword')}%");
			}
			return $query->paginate($perPage, $columns);			 
	 
						     

		// $dataJamaah = [];
		// $i = 0;				    
		// foreach($queryJamaah as $jamaah) {
		// 	$dataJamaah[] =  $jamaah->peserta;
		// 	$i++;
		// }	
	 	 
		// return new LengthAwarePaginator($dataJamaah, count($dataJamaah), $perPage, $page, [
		// 		'path' => $request->url(),
  //               'query' => $request->query()
		// 	]); 				    
	}
}

<?php

  namespace App\Repositories\DataJamaah\RoomList;

  use App\Peserta;
  use App\PesertaPaket as Paket;
  use App\TanggalPemberangkatan as Date;
  use InvalidArgumentException;

  class EloquentRoomListRepository implements RoomListRepository
  {
      /**
      * @var App\Peserta
      * model
      */
      protected $model;

      protected $paket;

      /**
      * Create new instance use DI
      * @param peserta model
      * assigment type room
      */
      public function __construct(Peserta $model, Paket $paket)
      {
        $this->model = $model;
        $this->paket = $paket;
      }
      
      /**
       * for get all jamaah and setting by family and room
       * @param  array from request
       * @param  integer $perPage 
       * @param  array   $columns [*]
       * @return array
       */
      public function all ($request, $perPage = 10, $columns = ['*'])
      {
     
        $dateDb = Date::all()->toArray();
        $tipe = (!isset($request['tipe']) || empty($request['tipe'])) ? 'Umroh' : $request['tipe'];
        $date = (!isset($request['date']) || empty($request['date'])) ? $dateDb[0]['tanggal_pemberangkatan'] : $request['date'];

        $query = $this->model->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
                              ->with(['peserta' => function($query) {
                                $query->with('paket');
                              }])
                              ->select('peserta.*',
                                'peserta_paket.harga_paket_id',
                                'peserta_paket.paket_hotel', 
                                'peserta_paket.tipe_kamar', 
                                'peserta_paket.bus', 
                                'peserta_paket.tgl_berangkat')
                                ->where('peserta.peserta_id','=', NULL)
                                ->where('peserta_paket.tgl_berangkat', $date)
                                ->where('peserta.tipe_paket', $tipe)
                              ->get()->toArray();


        // echo "<pre>".print_r($query ,1)."</pre>";
        // die();
        $doubleMan = [];
        $doubleWoman = [];
        $doubleFamily = [];
        
        $tripleMan = [];
        $tripleWoman = [];
        $tripleFamily = [];


        $quadMan = [];
        $quadWoman = [];
        $quadFamily = [];

        $quintMan = [];
        $quintWoman = [];
        $quintFamily = [];

        foreach ($query as $room) {
            if ($room['peserta']) { //check have mahram or not
                  $double = array_filter($room['peserta'], 'doubleFamily');
                  $triple = array_filter($room['peserta'], 'tripleFamily');
                  $quad = array_filter($room['peserta'], 'quadFamily');
                  $quint = array_filter($room['peserta'], 'quintFamily');

               

                  $countDouble = count($double);
                  $countTriple = count($triple);
                  $countQuad = count($quad);
                  $countQuint = count($quint);

                  $arrayDoubleFamily = array_map(function($var) { 
                    unset($var['paket']['id']);
                    $var['type'] = 'double_family'; //push type
                    return $var;
                  }, $double);

                  $arrayTripleFamily = array_map(function($var) { 
                     unset($var['paket']['id']);
                    $var['type'] = 'triple_family'; //push type
                    return $var;
                  }, $triple);

                  $arrayQuadFamily = array_map(function($var) { 
                    unset($var['paket']['id']);
                    $var['type'] = 'quad_family'; //push type
                    return $var;
                  }, $quad);                  

                  $arrayQuintFamily = array_map(function($var) { 
                    unset($var['paket']['id']);
                    $var['type'] = 'quint_family'; //push type
                    return $var;
                  }, $quint);


                  // echo "<pre>double family".$room['nama_peserta'].print_r($arrayDoubleFamily ,1)."</pre>";
                  // echo "<pre>triple family".$room['nama_peserta'].print_r($arrayTripleFamily ,1)."</pre>";
                  // echo "<pre>quad family".$room['nama_peserta'].print_r($arrayQuadFamily ,1)."</pre>";
                  // die();

                  //DOUBLE
                  if ($room['tipe_kamar'] == 'double') { 
                    if (!empty($arrayDoubleFamily)) {
                      $room['type'] = 'double_family';
                      unset($room['peserta']);
                      $doubleFamily[] = $room; //push mahram to doubleFamily
                      
                      $i = 0;
                      foreach ($arrayDoubleFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']);
                        
                        if ($countDouble % 2 == 1) {
                          // echo "<pre>".print_r($val ,1)."</pre>";
                          $doubleFamily[] =  $val;
                        } else {
                          $lastKey = count($double) - 1;
                          if ($lastKey == $i) {
                            if ($val['jenis_kelamin'] == 'P') {
                              $val['type'] = 'double_woman';
                              $doubleWoman[] = $val;
                            } else {
                              $val['type'] = 'double_man';
                              $doubleMan[] =  $val;
                            }
                          } else {
                            $doubleFamily[] =  $val;   
                          }
                        }
                        $i+=1;
                      }
                    } else {
                      if ($room['jenis_kelamin'] == 'P' ) {
                        $room['type'] = 'double_woman';
                        $doubleWoman[] =  $room;
                      } else {
                        $room['type'] = 'double_man';
                        $doubleMan[] =  $room;
                      }
                    }

                    if (!empty($arrayTripleFamily)) {
                      $i = 0;
                      $lastKey = count($countTriple) - 1;
                      foreach ($arrayTripleFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 
                        if($countTriple % 3 == 0) {
                          $tripleFamily[] = $val;
                        } else if ($countTriple > 3) {
                          if ($countTriple % 3 == 1) {
                            if ($lastKey == $i) {
                              if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'triple_woman';
                                $tripleWoman[] = $val;
                              } else {
                                $val['type'] = 'triple_man';
                                $tripleMan[] = $val;
                              }
                            } else {
                               $tripleFamily[] =  $val;
                            }
                          } else {
                            if ($lastKey == $i || $lastKey - 1 == $i) {
                              if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'triple_woman';
                                $tripleWoman[] = $val;
                              } else {
                                $val['type'] = 'triple_man';
                                $tripleMan[] = $val;
                              }
                            } else {
                               $tripleFamily[] =  $val;
                            }
                          }
                        } else {
                          if ($val['jenis_kelamin'] == 'P') {
                            $val['type'] = 'triple_woman';
                            $tripleWoman[] = $val;
                          } else {
                            $val['type'] = 'triple_man';
                            $tripleMan[] = $val;
                          }
                        }
                        $i+=1;
                      }
                    } 

                    if (!empty($arrayQuadFamily)) {
                      $i = 0;
                      $lastKey = count($countQuad) - 1;
                      foreach ($arrayQuadFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 
                        if($countQuad % 4 == 0) {
                          $quadFamily[] = $val;
                        } else if ($countQuad > 4) {
                          if (($countQuad % 4) == 1) {
                              if ($lastKey == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          } else if (($countQuad % 4) == 2) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          } else if (($countQuad % 4) == 3) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i || ($lastKey - 2 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          }
                        } else {
                          if ($val['jenis_kelamin'] == 'P') {
                            $val['type'] = 'quad_woman';
                            $quadWoman[] = $val;
                          } else {
                            $val['type'] = 'quad_man';
                            $quadMan[] = $val;
                          }
                        }
                        $i+=1;
                      }
                    } 

                    if (!empty($arrayQuintFamily)) {
                      $i = 0;
                      $lastKey = count($countQuint) - 1;
                      foreach ($arrayQuintFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 
                        if($countQuint % 5 == 0) {
                          $quintFamily[] = $val;
                        } else if ($countQuint > 5) {
                          if (($countQuint % 5) == 1) {
                              if ($lastKey == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint % 5) == 2) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint % 5) == 3) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i || ($lastKey - 2 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint % 5) == 4) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i || ($lastKey - 2 ) == $i || ($lastKey - 3 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          }
                        } else {
                          if ($val['jenis_kelamin'] == 'P') {
                            $val['type'] = 'quint_woman';
                            $quintWoman[] = $val;
                          } else {
                            $val['type'] = 'quint_man';
                            $quintMan[] = $val;
                          }
                        }
                        $i+=1;
                      }
                    }
                  } //end if double

                  // TRIPLE
                  if ($room['tipe_kamar'] == 'triple') {
                    if (!empty($arrayTripleFamily)) {
                      $room['type'] = 'triple_family';
                      unset($room['peserta']);
                      if ($countTriple > 1)
                        $tripleFamily[] = $room;

                      $i = 0;
                      $mahram = true;
                      foreach ($arrayTripleFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']);

                        if (($countTriple + 1) % 3 == 0) {
                          $tripleFamily[] =  $val;
                        } else if ($countTriple < 2) {
                          if ($mahram) {
                            if ($room['jenis_kelamin'] == 'P' ) {
                                $room['type'] = 'triple_woman';
                                $tripleWoman[] =  $room;
                            } else {
                                $room['type'] = 'triple_man';
                                $tripleMan[] =  $room;
                            }
                            $mahram = false;
                          }

                            if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'triple_woman';
                                $tripleWoman[] = $val;
                            } else {
                                $val['type'] = 'triple_man';
                                $tripleMan[] = $val;
                            }
                        } else {
                          $lastKey = $countTriple - 1;
                          if (($countTriple + 1) % 3 == 1) {
                            if ($lastKey == $i) {
                              if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'triple_woman';
                                $tripleWoman[] = $val;
                              } else {
                                $val['type'] = 'triple_man';
                                $tripleMan[] = $val;
                              }
                            } else {
                               $tripleFamily[] =  $val;
                            }
                          } else {
                            if ($lastKey == $i || $lastKey - 1 == $i) {
                              if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'triple_woman';
                                $tripleWoman[] = $val;
                              } else {
                                $val['type'] = 'triple_man';
                                $tripleMan[] = $val;
                              }
                            } else {
                               $tripleFamily[] =  $val;
                            }
                          }
                        }
                        $i+=1;
                      }
                    } else {
                      if ($room['jenis_kelamin'] == 'P' ) {
                        $room['type'] = 'triple_woman';
                        $tripleWoman[] =  $room;
                      } else {
                        $room['type'] = 'triple_man';
                        $tripleMan[] =  $room;
                      }
                    }

                    if (!empty($arrayDoubleFamily)) {
                      $i = 0;
                      $lastKey = count($double) - 1;
                      foreach ($arrayDoubleFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']);
                        
                        if ($countDouble % 2 == 0) {
                          $doubleFamily[] =  $val;
                        } else {
                          if ($lastKey == $i) {
                            if ($val['jenis_kelamin'] == 'P') {
                              $val['type'] = 'double_woman';
                              $doubleWoman[] = $val;
                            } else {
                              $val['type'] = 'double_man';
                              $doubleMan[] =  $val;
                            }
                          } else {
                            $doubleFamily[] =  $val;   
                          }
                        }
                        $i+=1;
                      }
                    }

                    if (!empty($arrayQuadFamily)) {
                      $i = 0;
                      $lastKey = count($countQuad) - 1;
                      foreach ($arrayQuadFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 

                        if($countQuad % 4 == 0) {
                          $quadFamily[] = $val;
                        } else if ($countQuad > 4) {
                          if (($countQuad % 4) == 1) {
                              if ($lastKey == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          } if (($countQuad % 4) == 2) {
                              if (($lastKey == $i) || ($lastKey - 1) == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          } if (($countQuad % 4) == 2) {
                              if (($lastKey == $i) || ($lastKey - 1) == $i || ($lastKey - 2) == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          }
                        } else {
                          if ($val['jenis_kelamin'] == 'P') {
                            $val['type'] = 'quad_woman';
                            $quadWoman[] = $val;
                          } else {
                            $val['type'] = 'quad_man';
                            $quadMan[] = $val;
                          }
                        }
                        $i+=1;
                      }
                    }

                    if (!empty($arrayQuintFamily)) {
                      $i = 0;
                      $lastKey = count($countQuint) - 1;
                      foreach ($arrayQuintFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 
                        if($countQuint % 5 == 0) {
                          $quintFamily[] = $val;
                        } else if ($countQuint > 5) {
                          if (($countQuint % 5) == 1) {
                              if ($lastKey == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint % 5) == 2) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint % 5) == 3) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i || ($lastKey - 2 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint % 5) == 4) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i || ($lastKey - 2 ) == $i || ($lastKey - 3 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          }
                        } else {
                          if ($val['jenis_kelamin'] == 'P') {
                            $val['type'] = 'quint_woman';
                            $quintWoman[] = $val;
                          } else {
                            $val['type'] = 'quint_man';
                            $quintMan[] = $val;
                          }
                        }
                        $i+=1;
                      }
                    }

                  } //end if triple

                  //QUAD
                  if ($room['tipe_kamar'] == 'quad') {
                    if (!empty($arrayQuadFamily)) {
                      $room['type'] = 'quad_family';
                      unset($room['peserta']);
                      
                      if($countQuad > 2)
                        $quadFamily[] = $room; 


                      $i = 0;
                      $mahram = true;
                      foreach ($arrayQuadFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 

                        if(($countQuad + 1) % 4 == 0) {
                          $quadFamily[] = $val;
                        } else if ($countQuad < 3) {

                            if($mahram) {
                              if ($room['jenis_kelamin'] == 'P' ) {
                                  $room['type'] = 'quad_woman';
                                  $quadWoman[] =  $room;
                              } else {
                                  $room['type'] = 'quad_man';
                                  $quadMan[] =  $room;
                              }
                              $mahram = false;
                            }

                            if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'quad_woman';
                                $quadWoman[] = $val;
                            } else {
                                $val['type'] = 'quad_man';
                                $quadMan[] = $val;
                            }
                        } else {
                          $lastKey = $countQuad - 1;
                          if (($countQuad + 1) % 4 == 1) {
                            // echo "<pre>".print_r($val ,1)."</pre>";
                              if ($lastKey == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          } else if (($countQuad + 1) % 4 == 2) {
                              if ($lastKey == $i || ($lastKey - 1) == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          } else if (($countQuad + 1) % 4 == 3) {
                              if ($lastKey == $i || ($lastKey - 1) == $i || ($lastKey - 2) == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          }
                        }
                        $i+=1;
                      }
                    } else {
                      if ($room['jenis_kelamin'] == 'P') {
                        $room['type'] = 'quad_woman';
                        $quadWoman[] = $room;
                      } else {
                        $room['type'] = 'quad_man';
                        $quadMan[] = $room;
                      }
                    }
                  
                    if (!empty($arrayDoubleFamily)) {
                      $i = 0;
                      $lastKey = count($double) - 1;
                      foreach ($arrayDoubleFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']);
                        
                        if ($countDouble % 2 == 0) {
                          $doubleFamily[] =  $val;
                        } else {
                          if ($lastKey == $i) {
                            if ($val['jenis_kelamin'] == 'P') {
                              $val['type'] = 'double_woman';
                              $doubleWoman[] = $val;
                            } else {
                              $val['type'] = 'double_man';
                              $doubleMan[] =  $val;
                            }
                          } else {
                            $doubleFamily[] =  $val;   
                          }
                        }
                        $i+=1;
                      }
                    }

                    if (!empty($arrayTripleFamily)) {
                      $i = 0;
                      $lastKey = count($countTriple) - 1;

                      foreach ($arrayTripleFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 

                        if($countTriple % 3 == 0) {
                          $tripleFamily[] = $val;
                        } else if ($countTriple > 3) {
                          if ($countTriple % 3 == 1) {
                            if ($lastKey == $i) {
                              if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'triple_woman';
                                $tripleWoman[] = $val;
                              } else {
                                $val['type'] = 'triple_man';
                                $tripleMan[] = $val;
                              }
                            } else {
                               $tripleFamily[] =  $val;
                            }
                          } else {
                            if ($lastKey == $i || $lastKey - 1 == $i) {
                              if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'triple_woman';
                                $tripleWoman[] = $val;
                              } else {
                                $val['type'] = 'triple_man';
                                $tripleMan[] = $val;
                              }
                            } else {
                               $tripleFamily[] =  $val;
                            }
                          }
                        } else {
                          if ($val['jenis_kelamin'] == 'P') {
                            $val['type'] = 'triple_woman';
                            $tripleWoman[] = $val;
                          } else {
                            $val['type'] = 'triple_man';
                            $tripleMan[] = $val;
                          }
                        }
                        $i+=1;
                      }
                    } 

                    if (!empty($arrayQuintFamily)) {
                      $i = 0;
                      $lastKey = count($countQuint) - 1;
                      foreach ($arrayQuintFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 
                        if($countQuint % 5 == 0) {
                          $quintFamily[] = $val;
                        } else if ($countQuint > 5) {
                          if (($countQuint % 5) == 1) {
                              if ($lastKey == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint % 5) == 2) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint % 5) == 3) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i || ($lastKey - 2 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint % 5) == 4) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i || ($lastKey - 2 ) == $i || ($lastKey - 3 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          }
                        } else {
                          if ($val['jenis_kelamin'] == 'P') {
                            $val['type'] = 'quint_woman';
                            $quintWoman[] = $val;
                          } else {
                            $val['type'] = 'quint_man';
                            $quintMan[] = $val;
                          }
                        }
                        $i+=1;
                      }
                    }

                  }

                  //QUINT (lima orang)
                  if ($room['tipe_kamar'] == 'quint') {
                    if (!empty($arrayQuintFamily)) {
                      $room['type'] = 'quint_family';
                      unset($room['peserta']);
                      
                      if($countQuint > 3)
                        $quintFamily[] = $room; //push mahram to doubleFamily


                      $i = 0;
                      $mahram = true;
                      foreach ($arrayQuintFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 

                        if(($countQuint + 1) % 5 == 0) {
                          $quintFamily[] = $val;
                        } else if ($countQuint < 4) {
                          if ($mahram) {
                            if ($room['jenis_kelamin'] == 'P' ) {
                                $room['type'] = 'quint_woman';
                                $quintWoman[] =  $room;
                            } else {
                                $room['type'] = 'quint_man';
                                $quintMan[] =  $room;
                            }
                            $mahram = false;
                          }

                            if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'quint_woman';
                                $quintWoman[] = $val;
                            } else {
                                $val['type'] = 'quint_man';
                                $quintMan[] = $val;
                            }
                        } else {
                          $lastKey = $countQuint - 1;
                          if (($countQuint + 1) % 5 == 1) {
                            // echo "<pre>".print_r($val ,1)."</pre>";
                              if ($lastKey == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint + 1) % 5 == 2) {
                              if ($lastKey == $i || ($lastKey - 1) == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          } else if (($countQuint + 1) % 5 == 3) {
                              if ($lastKey == $i || ($lastKey - 1) == $i || ($lastKey - 2) == $i || ($lastKey - 3 ) == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quint_woman';
                                  $quintWoman[] = $val;
                                } else {
                                  $val['type'] = 'quint_man';
                                  $quintMan[] = $val;
                                }
                              } else {
                                 $quintFamily[] =  $val;
                              }
                          }
                        }
                        $i+=1;
                      }
                    } else {
                      if ($room['jenis_kelamin'] == 'P') {
                        $room['type'] = 'quint_woman';
                        $quintWoman[] = $room;
                      } else {
                        $room['type'] = 'quint_man';
                        $quintMan[] = $room;
                      }
                    }

                    if (!empty($arrayDoubleFamily)) {
                      $i = 0;
                      $lastKey = count($double) - 1;
                      foreach ($arrayDoubleFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']);
                        
                        if ($countDouble % 2 == 0) {
                          $doubleFamily[] =  $val;
                        } else {
                          if ($lastKey == $i) {
                            if ($val['jenis_kelamin'] == 'P') {
                              $val['type'] = 'double_woman';
                              $doubleWoman[] = $val;
                            } else {
                              $val['type'] = 'double_man';
                              $doubleMan[] =  $val;
                            }
                          } else {
                            $doubleFamily[] =  $val;   
                          }
                        }
                        $i+=1;
                      }
                    }

                    if (!empty($arrayTripleFamily)) {
                      $i = 0;
                      $lastKey = count($countTriple) - 1;

                      foreach ($arrayTripleFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 

                        if($countTriple % 3 == 0) {
                          $tripleFamily[] = $val;
                        } else if ($countTriple > 3) {
                          if ($countTriple % 3 == 1) {
                            if ($lastKey == $i) {
                              if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'triple_woman';
                                $tripleWoman[] = $val;
                              } else {
                                $val['type'] = 'triple_man';
                                $tripleMan[] = $val;
                              }
                            } else {
                               $tripleFamily[] =  $val;
                            }
                          } else {
                            if ($lastKey == $i || $lastKey - 1 == $i) {
                              if ($val['jenis_kelamin'] == 'P') {
                                $val['type'] = 'triple_woman';
                                $tripleWoman[] = $val;
                              } else {
                                $val['type'] = 'triple_man';
                                $tripleMan[] = $val;
                              }
                            } else {
                               $tripleFamily[] =  $val;
                            }
                          }
                        } else {
                          if ($val['jenis_kelamin'] == 'P') {
                            $val['type'] = 'triple_woman';
                            $tripleWoman[] = $val;
                          } else {
                            $val['type'] = 'triple_man';
                            $tripleMan[] = $val;
                          }
                        }
                        $i+=1;
                      }
                    }

                    if (!empty($arrayQuadFamily)) {
                      $i = 0;
                      $lastKey = count($countQuad) - 1;
                      foreach ($arrayQuadFamily as $key => $val) {
                        $val = array_merge($val, $val['paket']);
                        unset($val['paket']); 
                        if($countQuad % 4 == 0) {
                          $quadFamily[] = $val;
                        } else if ($countQuad > 4) {
                          if (($countQuad % 4) == 1) {
                              if ($lastKey == $i) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          } else if (($countQuad % 4) == 2) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          } else if (($countQuad % 4) == 3) {
                              if ($lastKey == $i || ($lastKey - 1 ) == $i || ($lastKey - 2 ) == $i ) {
                                if ($val['jenis_kelamin'] == 'P') {
                                  $val['type'] = 'quad_woman';
                                  $quadWoman[] = $val;
                                } else {
                                  $val['type'] = 'quad_man';
                                  $quadMan[] = $val;
                                }
                              } else {
                                 $quadFamily[] =  $val;
                              }
                          }
                        } else {
                          if ($val['jenis_kelamin'] == 'P') {
                            $val['type'] = 'quad_woman';
                            $quadWoman[] = $val;
                          } else {
                            $val['type'] = 'quad_man';
                            $quadMan[] = $val;
                          }
                        }
                        $i+=1;
                      }
                    } 
                    
                  }
                  

                  
            } else {

                  //single and not have mahram
                   if ($room['jenis_kelamin'] == 'P') {
                      if ($room['tipe_kamar'] == 'double') {
                        $room['type'] = 'double_woman';
                        $doubleWoman[] =  $room;
                      } else if ($room['tipe_kamar'] == 'triple') {
                        $room['type'] = 'triple_woman';
                        $tripleWoman[] =  $room;
                      } else if ($room['tipe_kamar'] == 'quad'){
                        $room['type'] = 'quad_woman';
                        $quadWoman[] =  $room;
                      } else {
                        $room['type'] = 'quint_woman';
                        $quintWoman[] =  $room;
                      }
                  } else {
                      if ($room['tipe_kamar'] == 'double') {
                        $room['type'] = 'double_man';
                        $doubleMan[] =  $room;
                      } else if ($room['tipe_kamar'] == 'triple') {
                        $room['type'] = 'triple_man';
                        $tripleMan[] =  $room;
                      } else if ($room['tipe_kamar'] == 'quad'){
                        $room['type'] = 'quad_man';
                        $quadMan[] =  $room;
                      } else {
                        $room['type'] = 'quint_man';
                        $quintMan[] =  $room;
                      }
                  }
            }
        }//end foreach
        // echo "<pre>doubleFamily".print_r($quint ,1)."</pre>";

        $collection = array_merge(
          $doubleMan,
          $doubleWoman,
          $doubleFamily,
          
          $tripleMan,
          $tripleWoman,
          $tripleFamily,
          
          $quadMan,
          $quadWoman,
          $quadFamily, 

          $quintMan,
          $quintWoman,
          $quintFamily
        );


        // echo "<pre>collection".print_r($doubleFamily ,1)."</pre>";
        // die();
        return $collection;
      }

      public function edit($id)
      {

        $query = $this->model->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
                              ->select('peserta.*',
                                'peserta_paket.harga_paket_id',
                                'peserta_paket.paket_hotel', 
                                'peserta_paket.tipe_kamar', 
                                'peserta_paket.bus', 
                                'peserta_paket.tgl_berangkat')
                                ->where('peserta.id','=', $id)
                              ->get()->toArray();
        return $query;
      }

      public function destroy($id)
      {
       
      }

      public function update($data, $id)
      {
        if(empty($id) || is_null($id)) {
          throw new InvalidArgumentException("Invalid ID");
        }

        $model = $this->paket->where('peserta_id','=',$id)->first();

        if(empty($model) || is_null($model))
        {
          throw new InvalidArgumentException("Item not found");

        }


        try {

        $update = $model->where('peserta_id','=',$id)->update($data);

        } catch (Exception $e) {

          throw new RuntimeException($e->getMessage(), 1);

        }

        return $update;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
    }



  }
?>
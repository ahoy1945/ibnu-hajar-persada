<?php
  namespace App\Repositories\DataJamaah\RoomList;

  interface RoomListRepository
  {
    public function all($request, $perPage = 10, $columns = ['*']);
    public function update($data,$id);
    public function edit($id);
    public function destroy($id);
  }

<?php

namespace App\Repositories\DataJamaah\Manifest;

use App\Peserta;
use InvalidArgumentException;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class EloquentManifestRepository implements ManifestRepository
{
  /**
  * @var App\Peserta
  */
  protected $model;


  public function __construct(Peserta $model)
  {
    $this->model = $model;
  }


  public function all ($request, $perPage = 10, $columns = ['*'])
  {
      $date = $request->input('date');
      $tipe = $request->input('tipe');

      

      $query = $this->model->join('peserta_alamat', 'peserta.id', '=', 'peserta_alamat.peserta_id')
  							 ->join('peserta_identitas', 'peserta.id', '=', 'peserta_identitas.peserta_id')
  							 ->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
                 ->with(['peserta' => function($query) {
                   $query->with('alamat')
                         ->with('identitas')
                         ->with('paket');
                 }])
                 ->select('peserta.*',
                          'peserta_alamat.desa',
                          'peserta_alamat.kecamatan',
                          'peserta_alamat.kabupaten',
                          'peserta_alamat.provinsi',
                          'peserta_alamat.kodepos',
                          
                          'peserta_identitas.no_ktp',
                          'peserta_identitas.no_passpor',
                          'peserta_identitas.tempat_pembuatan',
                          'peserta_identitas.tgl_pembuatan',
                          'peserta_identitas.tgl_expired',
                          
                          'peserta_paket.harga_paket_id',
                          'peserta_paket.paket_hotel',
                          'peserta_paket.tipe_kamar',
                          'peserta_paket.tipe_pesawat',
                          'peserta_paket.tgl_berangkat'
                          )
                 ->where('peserta.peserta_id','=', NULL)
                 ->where(function($query) use ($date, $tipe){
                   if (!empty($date)) {
                     $query->where('peserta_paket.tgl_berangkat', $date);
                   }
                   if (!empty($tipe)) {
                     $query->where('peserta.tipe_paket', $tipe);
                   }
                 })
                 ->get()->toArray();

       $single = [];
       $womanGroup = [];
       $family = [];

       if (!empty($query)) {
         foreach ($query as $key => $peserta) {

           $peserta['hubungan_mahram'] = nameStatus($peserta['hubungan_mahram']);

           if ($peserta['peserta']) { //check have family or not
             $mahram = true;
             foreach ($peserta['peserta'] as $key => $keluarga) {

                 $keluarga['hubungan_mahram'] = nameStatus($keluarga['hubungan_mahram']);
                 $age = age($keluarga['tgl_lahir']);
                 if ($keluarga['jenis_kelamin'] == 'P' && $age > 45) {


                    $keluarga['hubungan_mahram'] = 'Woman group';

                    $dataWoman = array_merge(
                    $keluarga,
                    $keluarga['alamat'],
                    $keluarga['identitas'],
                    $keluarga['paket']);

                    unset($dataWoman['identitas'],$dataWoman['paket']);
                    $womanGroup[] =  $dataWoman; //push wife to array woman group


                    // $peserta['hubungan_mahram'] = 'Father'; //change hub mahram husband to be single!
                    if (count($peserta['peserta']) == 1 && $keluarga['hubungan_mahram'] == 'Woman group') {
                      $peserta['hubungan_mahram'] = 'Single'; //change hub mahram husband to be single!
                      $single[] =  $peserta; //push Husband to array single
                      unset($peserta['peserta']);
                    }

                    unset($peserta['peserta'][$key]); //remove wife from family because she moved to woman group
                 } else {

                    if($mahram) { //just run 1 x
                     $removeFamily = $peserta;
                     unset($removeFamily['peserta']);
                     $family[] = $removeFamily;
                     $mahram = false;
                    }

                    $dataFamily = array_merge(
                    $keluarga,
                    $keluarga['alamat'],
                    $keluarga['identitas'],
                    $keluarga['paket']);
                    unset($dataFamily['identitas'],$dataFamily['paket']);
                    $family[] = $dataFamily;
                 }

             }
           } else {
             $peserta['hubungan_mahram'] = 'Single';
             $single[] =  $peserta;
           }
         }
        }

      $collection = array_merge($family,$womanGroup,$single);

      $page = $request->input('page');
      $paginate = arrayPagination($page, $collection, $perPage);

      //if not export excel, data will be show in pagination
      if( $request->input('excel') == null ) {
        $collection = new LengthAwarePaginator($paginate, count($collection), $perPage, $page,['path' => Paginator::resolveCurrentPath()]);
      }

      return $collection;

  }

  public function show($where = '',$orderBy = 'DESC')
  {
    return $this->model
                  ->with('identitas')
                  ->with('alamat')
                  ->with('paket')
                  ->where('id', $where)
                  ->get()->toArray();
  }

  public function delete($id)
  {
    if(is_null($id) || empty($id))
    {
      throw new InvalidArgumentException('Invalid ID');
    }

    return $this->model->where('id', $id)->first()->delete();
  }

  public function update($data = [], $id, $attribute = 'id')
  {
    if(empty($id) || is_null($id))
		{
			throw new InvalidArgumentException("Invalid ID");
		}

		$model = $this->model->find($id);

		if(empty($model) || is_null($model))
		{
			throw new InvalidArgumentException("Item not found");

		}

		try {

			$update = $model->where($attribute, '=', $id)->update($data);

		} catch (Exception $e) {

			throw new RuntimeException($e->getMessage(), 1);

		}

		return $update;
  }

}


?>

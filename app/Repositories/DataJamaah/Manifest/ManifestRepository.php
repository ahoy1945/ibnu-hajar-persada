<?php

namespace App\Repositories\DataJamaah\Manifest;

interface ManifestRepository
{
  public function all($request, $perPage = 10, $columns = ['*']);
  public function delete($id);
  public function update($data = [], $id, $attribute = 'id');
}

?>

<?php 

namespace App\Repositories\Agen;

use App\Agen;
use App\User;
use App\Desa;
use App\Kecamatan;
use App\Kabupaten;
use App\Provinsi;

class EloquentAgenRepository implements AgenRepository
{
	/**
	 * App\Role
	 */
	protected $model;

	/**
	 * App\Role
	 */
	protected $user;

	/**
	 * App\Desa
	 */
	protected $desa;

	/**
	 * App\Kecamatan
	 */
	protected $kecamatan;

	/**
	 * App\Kabupaten
	 */
	protected $kabupaten;

	/**
	 * App\Provinsi
	 */
	protected $provinsi;

	/**
	 * Create a new instance
	 * 
	 * @param Role $model 
	 * @param Agen $user 
	 */
	public function __construct(Agen $model, User $user, Desa $desa, Kecamatan $kecamatan, Kabupaten $kabupaten, Provinsi $provinsi)
	{
		$this->model = $model;
		$this->user = $user;
		$this->desa = $desa;
		$this->kecamatan = $kecamatan;
		$this->kabupaten = $kabupaten;
		$this->provinsi = $provinsi;
	}

	public function all($perPage = 10, $columns = ['*'])
	{
		return $this->user->with('roles')->with('agen')->where('is_agen', true)->paginate($perPage, $columns);
	}

	public function create($data = [])
	{
		$userTable = [
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => $data['password'],
			'role_id' => '6',
			'is_agen' => true
		];



		$agenTable = [
			'no_hp' => ifIsset($data, 'hp'),
			'alamat' => ifIsset($data, 'alamat'),
			'desa' => ifIsset($data, 'desa'),
			'kecamatan' => ifIsset($data, 'kecamatan'),
			'kabupaten' => ifIsset($data, 'kabupaten'),
			'provinsi' => ifIsset($data, 'provinsi'),
			'no_rek' => json_encode(['no_rek' => ifIsset($data, 'no_rek'), 'bank' => ifIsset($data, 'bank'), 'nama_rek' => ifIsset($data, 'nama_rek')])
		];

		 
	    return $this->user->create($userTable)->agen()->create($agenTable);
 


	}

	public function delete($id)
	{

	}

	public function update($data = [], $id, $attribute = 'id')
	{

	}

	public function getItemBy($attribute, $value, $columns = ['*'])
	{
		return $this->model->with('users')->where($attribute, $value)->first($columns);
	}
}
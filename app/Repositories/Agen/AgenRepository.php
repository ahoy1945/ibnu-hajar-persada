<?php 

namespace App\Repositories\Agen;

interface AgenRepository
{
	public function all($perPage = 10, $columns = ['*']);
	public function create($data = []);
	public function delete($id);
	public function update($data = [], $id, $attribute = 'id');
}
<?php

namespace App\Repositories\Inventory;

use App\Paket;
use App\Inventory;
use App\InventoryHistory;
use InvalidArgumentException;
use App\Peserta;
use Carbon\Carbon;


class EloquentInventoryRepository implements InventoryRepository
{
	/**
	 * @var App\Barang
	 */
	protected $model;

	/**
	 * @var App\Paket
	 */	
	protected $paket;

	/**
	 * @var App\Peserta
	 */	
	protected $peserta;

	/**
	 * @var App\InventoryHistory
	 */	
	protected $inventoryHistory;

	/**
	 * Create a new instance
	 * 
	 * @param User $model 
	 */
	public function __construct(Inventory $model, InventoryHistory $inventoryHistory, Peserta $peserta)
	{
		$this->model = $model;
		$this->inventoryHistory = $inventoryHistory;
		$this->peserta = $peserta;
	}

	public function all($perPage = 10, $orderBy = 'DESC', $columns = ['*'])
	{
		return $this->model->orderBy('id', $orderBy)->paginate($perPage, $columns);
	}

	public function getItemBy($attribute, $value, $columns = ['*'])
	{
		return $this->model->where($attribute, $value)->first($columns);
	}

	public function create($data = [])
	{
		return $this->model->create($data);
	}

	public function delete($id)
	{
		if(is_null($id) || empty($id)) 
		{
			throw new InvalidArgumentException('Invalid ID');
		}

		return $this->model->where('id', $id)->first()->delete();
	}

	public function update($data = [], $id, $attribute = 'id')
	{
		if(empty($id) || is_null($id)) 
		{
			throw new InvalidArgumentException("Invalid ID");
		}

		$model = $this->model->find($id);

		if(empty($model) || is_null($model))
		{
			throw new InvalidArgumentException("Item not found");
			
		}

		try {
	
			$update = $model->where($attribute, '=', $id)->update($data);
	
		} catch (Exception $e) {
	
			throw new RuntimeException($e->getMessage(), 1);			
	
		}

		return $update;
	}

	public function updateStokPerlengkapanJamaah($data)
	{	
		$idJamaah = $data['id'];
		$idInventory = $data['id_barang'];

		$jamaah = $this->peserta->find($idJamaah);
  
  		$attached = [];
		foreach($jamaah->inventories as $inventory) {
			array_push($attached, $inventory->id); 			
		}

		if(in_array($idInventory, $attached)) {
			$data = ['type' => 'increment', 'input' => 1, 'tanggal' => Carbon::now(), 'keterangan' => "Dikembalikan oleh jamaah {$jamaah->nama_peserta}"];

			$this->updateStok($idInventory, $data);
			$jamaah->inventories()->detach($idInventory);
			return;
		}
		$data = ['type' => 'decrement', 'input' => 1, 'tanggal' => Carbon::now(), 'keterangan' => "Diambil oleh jamaah {$jamaah->nama_peserta}"];

		$jamaah->inventories()->attach($idInventory);  
		$this->updateStok($idInventory, $data);
		return;
	}

	public function updateStok($id, $data, $attribute = 'id') {

		$type  = $data['type'];
		$input = $data['input'];

		if(empty($id) || is_null($id)) 
		{
			throw new InvalidArgumentException("Invalid ID");
		}

		$model = $this->model->find($id);

		 
		$stok = ($type === 'increment' ? $model->jumlah + $input : $model->jumlah - $input);
		 
		 

		if(empty($model) || is_null($model))
		{
			throw new InvalidArgumentException("Item not found");
			
		}

		try {
	
			$update = $model->where($attribute, '=', $id)->update(['jumlah' => $stok]);
			$this->createHistory($model,$data);

		} catch (Exception $e) {
	
			throw new RuntimeException($e->getMessage(), 1);			
	
		}

		return $update;
	}

	public function createHistory($model, $data)
	{
	 	$tipe = ($data['type'] === 'increment' ? ['masuk' => (int) $data['input']] : ['keluar' => (int) $data['input']]);
	 	$sisa = ($data['type'] === 'increment' ? $model->jumlah + $data['input'] : $model->jumlah - $data['input'] );
	 	$data = [
	 		'keterangan' => $data['keterangan'],
	 		'tanggal' => $data['tanggal'],
	 		'inventory_id' => $model->id,
	 		'sisa' => $sisa

	 	];
	 	
	 	$this->inventoryHistory->create(array_merge($data, $tipe)); 
	}

	
	public function getCollectionByType($type = 'umroh')
	{
		return $this->model->where('tipe', $type)->orWhere('tipe', 'umrah dan haji')->get();
	}
}

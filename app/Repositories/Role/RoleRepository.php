<?php 

namespace App\Repositories\Role;

interface RoleRepository
{
	public function all($columns = ['*']);
}
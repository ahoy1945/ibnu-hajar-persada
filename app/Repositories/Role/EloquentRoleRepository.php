<?php 

namespace App\Repositories\Role;

use App\Role;

class EloquentRoleRepository implements RoleRepository
{
	/**
	 * App\Role
	 */
	protected $model;

	/**
	 * Create a new instance
	 * 
	 * @param Role $model 
	 */
	public function __construct(Role $model)
	{
		$this->model = $model;
	}

	public function all($columns = ['*'])
	{
		return $this->model->all($columns);
	}
}
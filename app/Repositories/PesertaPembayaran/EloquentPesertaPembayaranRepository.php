<?php 

namespace App\Repositories\PesertaPembayaran;

use DB;
use App\Keuangan;
use App\Peserta;
use App\PesertaPembayaran;
use Illuminate\Auth\Guard;
use App\Services\KursMandiri;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\KeuanganDollar;

class EloquentPesertaPembayaranRepository implements PesertaPembayaranRepository
{
	/**
	 * @var App\KeuanganDolar
	 */
	protected $keuanganDollar;

	/**
	 * @var Illuminate\Auth\Guard
	 */
	protected $auth;

	/**
	 * @var App\Keuangan;
	 */
	protected $keuangan;

	/**
	 * @var App\Repositories\Peserta\PesertaRepository
	 */
	protected $peserta;

	/**
	 * @var App\PesertaPembayaran
	 */
	protected $model;
	
	/**
	 * Create a new instance
	 */
	public function __construct(PesertaPembayaran $model, Peserta $peserta, Keuangan $keuangan, Guard $auth, KeuanganDollar $keuanganDollar)
	{	
		$this->model = $model;
		$this->peserta = $peserta;
		$this->keuangan = $keuangan;
		$this->auth = $auth;
		$this->KeuanganDollar = $keuanganDollar;
	}

	public function all($perPage = 10, $columns = ['*'])
	{

	}

	public function create($data = [])
	{
		$data_pembayaran = [
			'pembayaran' => $data['pembayaran'],
			'mata_uang' => $data['mata_uang'],
			'tgl_bayar' => $data['tgl_bayar'],
			'jumlah' => $data['jumlah'] - $data['harga_perlengkapan'],
			'keterangan' => $data['keterangan'],
			'peserta_id' => $data['peserta_id'],
			'tipe_pembayaran' => strtolower($data['tipe_pembayaran'])
		];
		$data_pembayaran_perlengkapan = [
			'pembayaran' => $data['pembayaran'],
			'mata_uang' => $data['mata_uang'],
			'tgl_bayar' => $data['tgl_bayar'],
			'jumlah' => $data['harga_perlengkapan'],
			'keterangan' => $data['keterangan'],
			'peserta_id' => $data['peserta_id'],
			'tipe_pembayaran' => 'perlengkapan'
		];


		if($data['perlengkapan'] === '1') {
			//pembayaran biasa
			$this->model->create($data_pembayaran);
			//pembayaran kwitansi
			$this->model->create($data_pembayaran_perlengkapan);
			return;
		}

		$this->model->create($data_pembayaran);

		return $this->model->create($data);
	}

	public function createMulti($data)
	{
		 	
		$peserta = $this->peserta->find($data['peserta_id']);
		
 		$dataInsertToKeuangan = [
			'tanggal' => $data['tgl_bayar'],
			'debit' => $data['jumlah'],
			'user_id' => $this->auth->user()->id,
			'type' => 'debit',
			'id_rekening' => $data['rekening_id'],
			'ref_tipe_keuangan_id' => $data['tipe_keuangan_id'],
			'keterangan' => "Pembayaran {$peserta->paket->harga->nama_paket}({$peserta->paket->tgl_berangkat}) dari {$peserta->nama_peserta}"

		];

		switch ($data['mata_uang']) {
			case 'idr':
				$this->keuangan->create($dataInsertToKeuangan);
				break;
			
			case 'usd':
				$this->KeuanganDollar->create($dataInsertToKeuangan);
				break;
		}
	

		
		return $this->model->create($data);
	}

	public function konversi($id)
	{
		$pembayaran = $this->model->findOrFail($id);

		return $pembayaran->konversi()->create([
					'peserta_pembayaran_id' => $pembayaran->id,
					'tgl_konversi' => date('Y-m-d'),
					'kurs' => KursMandiri::get()
				]);


	}


	public function delete($id)
	{

	}

	public function update($data = [], $id, $attribute = 'id')
	{

	}

	public function find($id)
	{
		return $this->model->find($id);
	}

	public function getAllPerTransaksi($paginate = 20, $columns = ['*'], $orderBy = 'DESC')
	{
		return $this->model->with(['konversi' => function($query) {
			$query->get()->toArray();
		}])->with(['peserta' => function($query) {
			$query->with(['paket' => function($query) {
				$query->with(['harga' => function($query) {
					$query->with('hotel');
				}]);
			}])->with('user');
		}])->orderBy('id', $orderBy)->paginate($paginate, $columns);
	}

	public function count()
	{
		return $this->model->all()->count();
	}

	public function findByKwitansi($noKwitansi)
	{
		return $this->model->where('no_kwitansi', $noKwitansi)->with(['peserta' => function($query) {
			$query->with(['paket' => function($query) {
				$query->with('harga');
			}]);
		}])->with('tipe_keuangan')->get();
	}

	public function search($request, $paginate = 10, $columns = ['*'])
	{
		return $this->model->join('peserta', 'peserta_pembayaran.peserta_id', '=', 'peserta.id')
						   ->where('peserta.nama_peserta', 'LIKE', "%$request->input('nama_peserta')%")
						   ->get();	
	}

	public function laporanUmmuAhmad($request, $perPage = 20)
	{

		if(empty($request->input('tgl_berangkat'))) {
			
			return [];
		}

		$data = [];
	 
		$page = ($request->input('page') ? $request->input('page') : 1);
		$i = 0;	
 
		$hasKeyword = $request->has('keyword');
		$hasTgl = ($request->has('tgl_berangkat') && !empty($request->input('tgl_berangkat')) ? true : false);
		

		$queryLike = $this->peserta->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.id')->where('nama_peserta', 'LIKE', "%{$request->input('keyword')}%")->get()->toArray(); 
		$query = $this->peserta->take($perPage)->skip($perPage * ($page-1))->with(['pembayaran' => function($query) {
			$query->with('konversi');
		}])->with(['paket' => function($query) {
			$query->with('harga');
		}])
		->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.id')
		->select("peserta.*", "peserta_paket.id as peserta_paket_id", "peserta_paket.tgl_berangkat");
		if(!empty($request->input('keyword'))) {

			$query = $query->where('peserta.nama_peserta', 'LIKE', "%{$request->input('keyword')}%");
		}

		if($hasTgl) {
			$query = $query->where('peserta_paket.tgl_berangkat', $request->input('tgl_berangkat'));
		}
	
		$query = $query->get();
		
		//dd($query->toArray()); 

		foreach($query as $jamaah) {
			$data[$i] = $jamaah;
			$data[$i]['jumlah_bayar_rupiah'] = 0;
			$data[$i]['jumlah_bayar_dollar'] = 0;
			$data[$i]['belum_dikonversi'] = 0;
			$data[$i]['diskon'] = 0;
			 

			foreach($jamaah->pembayaran as $pembayaran) {
				if($pembayaran->mata_uang == "idr") {
					$data[$i]['jumlah_bayar_rupiah'] += $pembayaran->jumlah;
					 
				}
				
				if(empty($pembayaran->konversi->toArray()) && $pembayaran->mata_uang == "idr") {
					$data[$i]['belum_dikonversi'] +=1;
				} 

				if(! empty($pembayaran->konversi->toArray()) && $pembayaran->mata_uang == "idr") {
					 	foreach($pembayaran->konversi as $konversi) {
					 		$data[$i]['jumlah_bayar_dollar'] += $pembayaran->jumlah/$konversi->kurs;			
				 	}
				} else if($pembayaran->mata_uang == "usd") {
					 		$data[$i]['jumlah_bayar_dollar'] += $pembayaran->jumlah;
					 		$data[$i]['diskon'] += $pembayaran->diskon;			
				} 
			}
			$i++;
		}

        $newData = [];
		if(!empty($request->input('status')) && $request->has('status')) {

			foreach ($data as $newKeyData => $newValueData) {
				//sudah lunas
				if($request->input('status') == "lunas" && $newValueData->jumlah_bayar_dollar >= ($newValueData->paket->harga->harga_paket - $newValueData->diskon)) {
					$newData[$newKeyData] = $newValueData;
				//belum bayar alias nol
				} else if($request->input('status') == "belum" && $newValueData->jumlah_bayar_dollar == 0) {
					$newData[$newKeyData] = $newValueData;
				//sisa
				} else if($request->input('status') == "sisa" && $newValueData->jumlah_bayar_dollar <  ($newValueData->paket->harga->harga_paket - $newValueData->diskon)) {
					$newData[$newKeyData] = $newValueData;
				}
			}

			$dataCount = (! $hasKeyword ? count($this->peserta->all()) : count($queryLike) );
			return new LengthAwarePaginator($newData, $dataCount, $perPage, $page, ['path' => $request->url()]);	 
	 	    
		}
 
	 	$dataCount = (! $hasKeyword ? count($this->peserta->all()) : count($queryLike) );
	 	 
		return new LengthAwarePaginator($data, $dataCount, $perPage, $page, ['path' => $request->url()]);	 
	}

	public function laporanUmmuAhmadPrint($request)
	{
		 

		if(empty($request->input('tgl_berangkat'))) {
			
			return [];
		}

		$data = [];
	
		$i = 0;	
 
		$hasKeyword = $request->has('keyword');
		$hasTgl = ($request->has('tgl_berangkat') && !empty($request->input('tgl_berangkat')) ? true : false);
		

		$queryLike = $this->peserta->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.id')->where('nama_peserta', 'LIKE', "%{$request->input('keyword')}%")->get()->toArray(); 
		$query = $this->peserta->with(['pembayaran' => function($query) {
			$query->with('konversi');
		}])->with(['paket' => function($query) {
			$query->with('harga');
		}])
		->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.id')
		->select("peserta.*", "peserta_paket.id as peserta_paket_id", "peserta_paket.tgl_berangkat");
		if(!empty($request->input('keyword'))) {
			$query = $query->where('peserta.nama_peserta', 'LIKE', "%{$request->input('keyword')}%");
		}

		if(!empty($request->input('tipe_paket'))) {
			$query = $query->where('peserta.tipe_paket', $request->input('tipe_paket'));
		}

		if($hasTgl) {
			if($request->input('tipe_paket') == 'haji') {
				$query = $query->whereBetween('peserta_paket.tgl_berangkat', [$request->input('tgl_berangkat')."-01-01", $request->input('tgl_berangkat')."-12-31"]);

			} else {
				$query = $query->where('peserta_paket.tgl_berangkat', $request->input('tgl_berangkat'));
			}
		}
	
		$query = $query->get();
		
		//dd($query->toArray()); 

		foreach($query as $jamaah) {
			$data[$i] = $jamaah;
			$data[$i]['jumlah_bayar_rupiah'] = 0;
			$data[$i]['jumlah_bayar_dollar'] = 0;
			$data[$i]['belum_dikonversi'] = 0;
			$data[$i]['diskon'] = 0;
			 

			foreach($jamaah->pembayaran as $pembayaran) {
				if($pembayaran->mata_uang == "idr") {
					$data[$i]['jumlah_bayar_rupiah'] += $pembayaran->jumlah;
					 
				}
				
				if(empty($pembayaran->konversi->toArray()) && $pembayaran->mata_uang == "idr") {
					$data[$i]['belum_dikonversi'] +=1;
				} 

				if(! empty($pembayaran->konversi->toArray()) && $pembayaran->mata_uang == "idr") {
					 	foreach($pembayaran->konversi as $konversi) {
					 		$data[$i]['jumlah_bayar_dollar'] += $pembayaran->jumlah/$konversi->kurs;			
				 	}
				} else if($pembayaran->mata_uang == "usd") {
					 		$data[$i]['jumlah_bayar_dollar'] += $pembayaran->jumlah;
					 		$data[$i]['diskon'] += $pembayaran->diskon;			
				} 
			}
			$i++;
		}

        $newData = [];
		if(!empty($request->input('status')) && $request->has('status')) {

			foreach ($data as $newKeyData => $newValueData) {
				//sudah lunas
				if($request->input('status') == "lunas" && $newValueData->jumlah_bayar_dollar >= ($newValueData->paket->harga->harga_paket - $newValueData->diskon)) {
					$newData[$newKeyData] = $newValueData;
				//belum bayar alias nol
				} else if($request->input('status') == "belum" && $newValueData->jumlah_bayar_dollar == 0) {
					$newData[$newKeyData] = $newValueData;
				//sisa
				} else if($request->input('status') == "sisa" && $newValueData->jumlah_bayar_dollar <  ($newValueData->paket->harga->harga_paket - $newValueData->diskon)) {
					$newData[$newKeyData] = $newValueData;
				}
			}
			 
			return $newData; 
	 	    
		}

		return $data;
	}
}


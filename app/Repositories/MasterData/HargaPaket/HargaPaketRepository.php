<?php

namespace App\Repositories\MasterData\HargaPaket;

interface HargaPaketRepository
{

  // public function all($perPage = 10, $columns = ['*']);
  public function all();
  public function create($data = []);
  public function delete($id);
  public function edit($id);
  public function update($data = [], $id, $attribute = 'id');

}

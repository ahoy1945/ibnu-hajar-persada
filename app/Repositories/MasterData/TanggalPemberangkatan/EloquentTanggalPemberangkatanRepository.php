<?php

namespace App\Repositories\MasterData\TanggalPemberangkatan;

use App\TanggalPemberangkatan;
use InvalidArgumentException;

class EloquentTanggalPemberangkatanRepository implements TanggalPemberangkatanRepository
{
  /**
  *@var
  */
  protected $model;

  public function __construct(TanggalPemberangkatan $model)
  {
    $this->model = $model;
  }

  // public function all($perPage = 10, $orderBy = 'DESC', $columns = ['*'])
  public function all()
  {
    // return $this->model->paginate($perPage, $columns);
    return $this->model->with('paket')->get();
  }

  public function create($data = [])
  {
    return $this->model->create($data);
  }

  public function delete($id)
	{
		if(is_null($id) || empty($id))
		{
			throw new InvalidArgumentException('Invalid ID');
		}

		return $this->model->where('id', $id)->first()->delete();
	}

  public function edit($id)
  {
    return $this->model->where('id', $id)->with('paket')->get();
  }

  public function find($id)
  {
    return $this->model->findOrFail($id);
  } 

  public function update($data = [], $id, $attribute = 'id')
	{
		if(empty($id) || is_null($id))
		{
			throw new InvalidArgumentException("Invalid ID");
      $update = false;
		}

		$model = $this->model->find($id);

		if(empty($model) || is_null($model))
		{
			throw new InvalidArgumentException("Item not found");
      $update = false;
		}

		try {

			$update = $model->where($attribute, '=', $id)->update($data);

		} catch (Exception $e) {

			throw new RuntimeException($e->getMessage(), 1);
      $update = false;
		}

		return $update;
	}


}


?>

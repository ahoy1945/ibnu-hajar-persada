<?php

namespace App\Repositories\Peserta;

use App\Desa;
use App\Inventory;
use App\Kabupaten;
use App\Kecamatan;
use App\Peserta;
use App\PesertaAlamat;
use App\PesertaIdentitas;
use App\PesertaMahram;
use App\PesertaPaket;
use App\Provinsi;
use App\TanggalPemberangkatan as Date;
use Illuminate\Auth\Guard  as Auth;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Pagination\LengthAwarePaginator;
use InvalidArgumentException;

class EloquentPesertaRepository implements PesertaRepository
{

	/**
	 * @var App\User
	 */
	protected $model;

	/**
	 * @var App\User
	 */
	protected $inventory;


	/**
	 * @var App\PesertaAlamat
	 */
	protected $pesertaAlamat;

	/**
	 * @var App\PesertaMahram
	 */
	protected $pesertaMahram;

	/**
	 * @var App\PesertaIdentitas
	 */
	protected $pesertaIdentitas;

	/**
	 * @var App\PesertaPaket
	 */
	protected $pesertaPaket;

	/**
	 * @var App\Desa
	 */
	protected $desa;

	/**
	 * @var App\Kecamatan
	 */
	protected $kecamatan;

	/**
	 * @var App\Kabupaten
	 */
	protected $kabupaten;

	/**
	 * @var App\Provinsi
	 */
	protected $provinsi;
	/**
	 * @var Illuminate\Auth\Guard
	 */
	protected $auth;


	/**
	 * @var Illuminate\Filesystem\Filesystem
	 */
	protected $filesystem;

	/**
	 * Create a new instance
	 *
	 * @param Peserta          $model
	 * @param InventoryRepository $inventory
	 * @param Auth             $auth
	 * @param PesertaMahram    $pesertaMahram
	 * @param PesertaPaket     $pesertaPaket
	 * @param PesertaIdentitas $pesertaIdentitas
	 * @param PesertaAlamat    $pesertaAlamat
	 * @param Desa             $desa
	 * @param Kecamatan        $kecamatan
	 * @param Kabupaten        $kabupaten
	 * @param Provinsi         $provinsi
	 */
	public function __construct(
		Peserta $model,
		Inventory $inventory,
		Auth $auth,
		PesertaMahram $pesertaMahram,
		PesertaPaket $pesertaPaket,
		PesertaIdentitas $pesertaIdentitas,
		PesertaAlamat $pesertaAlamat,
		Desa $desa,
		Kecamatan $kecamatan,
		Kabupaten $kabupaten,
		Provinsi $provinsi,
		Filesystem $filesystem)
	{
		$this->model 			= $model;
		$this->inventory		= $inventory;
		$this->auth  			= $auth;
		$this->pesertaMahram 	= $pesertaMahram;
		$this->pesertaPaket 	= $pesertaPaket;
		$this->pesertaIdentitas = $pesertaIdentitas;
		$this->pesertaAlamat 	= $pesertaAlamat;
		$this->desa 			= $desa;
		$this->kecamatan 		= $kecamatan;
		$this->kabupaten 		= $kabupaten;
		$this->provinsi 		= $provinsi;
		$this->filesystem 		= $filesystem;	
	}

	public function all($perPage = 10, $orderBy = 'DESC', $columns = ['*'])
	{
		

		$query =  $this->model
					->with('identitas')
					->with('alamat')
					->with('paket')
					->with('peserta')
					->with('user')
					->with('pembayaran');
		if($this->auth->user()->roles->nama === "agen") {
		$query = $query->where("who_insert", $this->auth->user()->id);
		}
		$query = $query->orderBy('id', $orderBy)->paginate($perPage, $columns);

		return $query;			
	}

	public function search($keyword = "", $perPage = 10, $orderBy = 'DESC', $columns = ['*'])
	{
		return $this->model
					->with('identitas')
					->with('alamat')
					->with('paket')
					->with('peserta')
					->where('nama_peserta', 'LIKE', "%{$keyword}%")
					->orderBy('id', $orderBy)
					->paginate($perPage, $columns);
	}

	public function getItemBy($attribute, $value, $columns = ['*'])
	{ 
		return $this->model->where('id', $value)
						   ->with('identitas')
						   ->with('alamat')
						   ->with('documents')
						   ->with(['paket' => function($query) {
						   		$query->with('pesawat')
						   			  ->with('harga');

						   }])
						   ->with(['pembayaran' => function($query) {
						   		$query->with('konversi')
						   			  ->with('rekening');
						   }])
						   ->first($columns);
	}

	public function find($id)
	{
		return $this->model->find($id);
	}

	public function getCollectionForDokumenPrint($request, $columns = ['*'])
	{
		$query = $this->model
							 ->where('tipe_paket', $request->input('tipe'))
							 ->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
							 ->with('identitas')
							 ->with('alamat')
							 ->with('pembayaran')
							 ->with('documents')
							 ->select(
							 	      'peserta.*',
							 	      'peserta_paket.id as peserta_paket_id',
							 	      'peserta_paket.tgl_berangkat');

		if($request->has('tanggal') && $request->input('tanggal')) {
		   $query->where('peserta_paket.tgl_berangkat', $request->input("tanggal"));
		}					 
							 
		$query = $query->get($columns);					 		   
         
		$newData = [];
		$i = 0;
		foreach($query->toArray()  as  $jamaah) {
			    $newData[$i] = $jamaah;

				$dataDocuments = [];
			 		foreach($jamaah['documents'] as $document) {
			 				array_push($dataDocuments,$document['pivot']['dokumen_id']);
			 		}
			 	$newData[$i]['documents_group'] = $dataDocuments;
			 
				$i++;
				 

		}

		return $newData;
	}

	public function getCollectionForDokumen($request, $perPage = 10, $columns = ['*'], $isPaginate = true)
	{
		if(empty($request->input('tanggal'))) {
			return [];
		}
		$currentPage = (int) ($request->input('page') ? $request->input('page') : 1 );	
		$offset = ($perPage * $currentPage) - $perPage;

		$query = $this->model
							 ->where('tipe_paket', $request->input('tipe'))
							 ->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
							 ->with('identitas')
							 ->with('alamat')
							 ->with('pembayaran')
							 ->with('documents')
							 ->select(
							 	      'peserta.*',
							 	      'peserta_paket.id as peserta_paket_id',
							 	      'peserta_paket.tgl_berangkat');

		if($request->has('tanggal') && $request->input('tanggal')) {
		   $query->where('peserta_paket.tgl_berangkat', $request->input("tanggal"));
		}					 
							 
		$query = $query->get($columns);					 		   
         
		$newData = [];
		$i = 0;
		foreach($query  as  $jamaah) {
			    $newData[$i] = $jamaah;

				$dataDocuments = [];
			 		foreach($jamaah->documents as $document) {
			 				array_push($dataDocuments,$document->pivot->dokumen_id);
			 		}
			 	$newData[$i]['documents_group'] = $dataDocuments;
			 
				$i++;
				 

		}
		$itemsForCurrentPage = array_slice($newData, $offset, $perPage, false);
		return new LengthAwarePaginator($itemsForCurrentPage, count($newData), $perPage, $currentPage,[
				'path' => $request->url()
			]);						   
	}

	public function getJamaah($where = [], $perPage = 10, $orderBy = 'DESC', $columns = ['*'])
	{
		$query = $this->model
							 ->with('inventories')
							 ->with(["peserta" => function($query) {
							 	$query->with("inventories");
							 }])
							->join('peserta_alamat', 'peserta_alamat.peserta_id', '=', 'peserta.id')
							->join('peserta_identitas', 'peserta_identitas.peserta_id', '=', 'peserta.id')
							->join('peserta_paket', 'peserta_paket.peserta_id', '=', 'peserta.id')
							->select('peserta.*',
									 'peserta_paket.id as peserta_paket_id',
									 'peserta_paket.tgl_berangkat',
									 'peserta_alamat.id as peserta_alamat_id',
									 'peserta_identitas.id as peserta_identitas_id')
							->where('peserta.tipe_paket', '=', $where['tipe_paket'])
							->where('peserta.peserta_id', NULL)
							->has('pembayaran');
	  
		if(!empty($where['tanggal'])) {
			if($where['tipe_paket'] == 'umroh') {
				$query = $query->where('peserta_paket.tgl_berangkat', '=', $where['tanggal']);
			} else {
				$query = $query->whereBetween('peserta_paket.tgl_berangkat', [$where['tanggal']."-01-01", $where['tanggal']."-01-01"]);
			}
		}
		$finalQuery = $query->orderBy('peserta.id', $orderBy)->get()->toArray();

	    $newData = [];
	   
		$i = 0;
		foreach($finalQuery as  $jamaah) {
			    $newData[$i] = $jamaah;

				$dataInventory = [];
			 		foreach($jamaah['inventories'] as $inventory) {
			 				array_push($dataInventory,$inventory['id']);
			 		}
			 	$newData[$i]['td_rowspan'] = count($jamaah['peserta']) + 1;

			 	$newData[$i]['inventories'] = $dataInventory;
				$i++;

				foreach($jamaah['peserta'] as $peserta) {
					$dataInventoryPeserta = [];
			    	$newData[$i] = $peserta;
			    	foreach($peserta['inventories'] as $pesertaInventory) {
			    		array_push($dataInventoryPeserta, $pesertaInventory['id']);
			    	}
			    	$newData[$i]['td_rowspan'] = 0;
			    	$newData[$i]['inventories'] = $dataInventoryPeserta;
			    	$i++;
			    }

		}
        //dd($newData);
		return $newData;


	}

	public function update($data = [], $id = null, $attribute = 'id')
	{
		
		if(empty($id) || is_null($id)) 
		{
			throw new InvalidArgumentException("Invalid ID");
		}

		$model = $this->model->find($id);

		if(empty($model) || is_null($model))
		{
			throw new InvalidArgumentException("Item not found");
			
		}

		try {
	 	
			$dataToInsert = ($data['kepala_keluarga'] === "1" ? $this->dataMahram($data) : $this->dataPeserta($data));
	 	 	 
			$peserta = $model->where($attribute, '=', $id)->first();
			unset($dataToInsert['who_insert']);
			
			$peserta->update($dataToInsert);
			if(isset($data['dokumen']) && !empty($data['dokumen'])) {
				$peserta->documents()->detach();	
				foreach($data['dokumen'] as $valueDokumen) {
					$peserta->documents()->attach($valueDokumen);
				}	

			}
				

		 	$this->pesertaAlamat->where('peserta_id', $id)->update([
				'desa' => ifIsset($data, 'desa'),
				'kecamatan' => ifIsset($data, 'kecamatan'),
				'kabupaten' => ifIsset($data, 'kabupaten'),
				'provinsi' => ifIsset($data, 'provinsi'),
				'kodepos' => ifIsset($data, 'kodepos'),
				'alamat' => ifIsset($data, 'alamat')]
				);
		 	$this->pesertaPaket->where('peserta_id', $id)->update([
				'harga_paket_id' => $data['harga_paket_id'],
				'paket_hotel' => $data['paket_hotel'],
				'tipe_kamar' => $data['tipe_kamar'],
				'tgl_berangkat' => $data['tgl_berangkat'],
				'pesawat_id' => $data['pesawat_id'],
				'tipe_pesawat' => $data['tipe_pesawat']
			]);

			$this->pesertaIdentitas->where('peserta_id', $id)->update([
				'no_ktp' => $data['ktp'],
				'no_passpor' => $data['no_passpor'],
				'tempat_pembuatan' => $data['tempat_pembuatan_passpor'],
				'tgl_pembuatan' => $data['tgl_pembuatan_passpor'],
				'tgl_expired' =>  $data['tgl_expired_passpor']
				]);
			
	
		} catch (Exception $e) {
	
			throw new RuntimeException($e->getMessage(), 1);			
	
		}

		return $peserta;
	}


	public function create($data = [])
	{


		try {
            
			$peserta = ($data['kepala_keluarga'] === "1" ? $this->model->create($this->dataMahram($data)) : $this->model->create($this->dataPeserta($data)) );
			
			if(isset($data['dokumen'])) {
				foreach($data['dokumen'] as $dokumen) {
					$peserta->documents()->attach($dokumen);
				}
			}
			
			$this->pesertaAlamat->create([
				'peserta_id' => $peserta->id,
				'desa' => $data['desa'],
				'kecamatan' => ifIsset($data, 'kecamatan'),
				'kabupaten' => ifIsset($data, 'kabupaten'),
				'provinsi' => ifIsset($data, 'provinsi'),
				'kodepos' => ifIsset($data, 'kodepos'),
				'alamat' => ifIsset($data, 'alamat')]);

			$this->pesertaPaket->create([
				'peserta_id' => $peserta->id,
				'harga_paket_id' => $data['harga_paket_id'],
				'paket_hotel' => $data['paket_hotel'],
				'tipe_kamar' => $data['tipe_kamar'],
				'tgl_berangkat' => $data['tgl_berangkat'],
				'pesawat_id' => $data['pesawat_id'],
				'tipe_pesawat' => $data['tipe_pesawat'],
			]);

			$this->pesertaIdentitas->create([
				'peserta_id' => $peserta->id,
				'no_ktp' => ifIsset($data, 'ktp'),
				'no_passpor' => ifIsset($data, 'no_passpor'),
				'tempat_pembuatan' => ifIsset($data, 'tempat_pembuatan_passpor'),
				'tgl_pembuatan' => ifIsset($data, 'tgl_pembuatan_passpor'),
				'tgl_expired' =>  ifIsset($data, 'tgl_expired_passpor')
				]);

	
		} catch (Exception $e) {

		}


	}

	public function delete($id)
	{
		if(is_null($id) || empty($id))
		{
			throw new InvalidArgumentException('Invalid ID');
		}


		$jamaah =  $this->model->where('id', $id)->first();
		if($this->filesystem->exists(public_path().$jamaah->photo)) {
			 $this->filesystem->delete(public_path().$jamaah->photo);
		}

		return $jamaah->delete();
	}

 


	public function searchMahram($keyword)
	{
		return $this->model->where('peserta_id', NULl)
						   ->where('nama_peserta', 'LIKE', "%$keyword%")
						   ->with('alamat')
						   ->get();
	}

	public function dataPeserta($data)
	{

		$peserta =  [
			'nama_peserta' => ifIsset($data, 'nama_peserta'),
			'tempat_lahir' => ifIsset($data, 'tempat_lahir'),
			'nama_ayah' => ifIsset($data, 'nama_ayah'),
			'no_telp' => ifIsset($data, 'no_telp'),
			'no_hp' => ifIsset($data, 'no_hp'),
			'pekerjaan' => ifIsset($data, 'pekerjaan'),
			'golongan_darah' => ifIsset($data, 'golongan_darah'),
			'tipe_paket' => ifIsset($data, 'tipe_paket'),
			'jenis_kelamin' => ifIsset($data, 'jk'),
			'kewarganegaraan' => ifIsset($data, 'kewarganegaraan'),
			'pernah_haji_umroh' => ifIsset($data, 'pernah_haji_umroh'),
			'ukuran_baju_kerudung' => ifIsset($data, 'ukuran_baju_kerudung'),
			'who_insert' => $this->auth->user()->id,
			'tgl_lahir' => ifIsset($data, 'tgl_lahir'),
			'peserta_id' => ifIsset($data, 'mahram_id'),
			'hubungan_mahram' => ifIsset($data, 'hubungan_mahram'),
			
		];

		if(isset($data['photo']) && !empty($data['photo'])) {
			$peserta = array_add($peserta, 'photo', $data['photo']);
		}

		return $peserta;


	}

	public function dataMahram($data)
	{
		$mahram = [
			'nama_peserta' => ifIsset($data, 'nama_peserta'),
			'tempat_lahir' => ifIsset($data, 'tempat_lahir'),
			'nama_ayah' => ifIsset($data, 'nama_ayah'),
			'no_telp' => ifIsset($data, 'no_telp'),
			'no_hp' => ifIsset($data, 'no_hp'),
			'pekerjaan' => ifIsset($data, 'pekerjaan'),
			'golongan_darah' => ifIsset($data, 'golongan_darah'),
			'tipe_paket' => ifIsset($data, 'tipe_paket'),
			'jenis_kelamin' => ifIsset($data, 'jk'),
			'kewarganegaraan' => ifIsset($data, 'kewarganegaraan'),
			'pernah_haji_umroh' => ifIsset($data, 'pernah_haji_umroh'),
			'ukuran_baju_kerudung' => ifIsset($data, 'ukuran_baju_kerudung'),
			'who_insert' => $this->auth->user()->id,
			'tgl_lahir' => ifIsset($data, 'tgl_lahir'),
			'hubungan_mahram' => ifIsset($data, 'hubungan_mahram'),
		];

		if(isset($data['photo']) && !empty($data['photo'])) {
			$mahram = array_add($mahram, 'photo', $data['photo']);
		}

		return $mahram;
	}

	public function searchPeserta($keyword)
	{
		return $this->model->with(['user' => function($query) {
			$query->with('roles');
		}])->where('nama_peserta', 'LIKE', "%{$keyword}%")->get();
	}

	public function totalJamaahPerbulan()
	{
		$tanggalBerangkat = Date::all()->toArray();
		$jamaah = [];

		foreach ($tanggalBerangkat as $key => $val) {
			$tgl = $val['tanggal_pemberangkatan'];
			$jamaah[$tgl]['agen'] = $this->model->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
										->join('users', 'peserta.who_insert', '=' ,'users.id')
										->where('peserta.tipe_paket', '=', 'umroh')
										->where('peserta_paket.tgl_berangkat', '=',  $tgl)
										->where('users.role_id', '=', '6')->count();

			$jamaah[$tgl]['langsung'] = $this->model->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
										->join('users', 'peserta.who_insert', '=' ,'users.id')
										->where('peserta.tipe_paket', '=', 'umroh')
										->where('peserta_paket.tgl_berangkat', '=',  $tgl)
										->whereNotIn('users.role_id', [6])->count();
		}
		return $jamaah;
	}

	public function totalJamaahPertahun()
	{
		$jamaah = [];
		$tahun = 2016;
		$range = 10;
		for ($i=1; $i < $range; $i++) {
			$blnAwal = $tahun."-01-01"; 
			$blnAkhir = $tahun."-12-31"; 
			
			$jamaah[$tahun]['agen'] = $this->model->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
							->join('users', 'peserta.who_insert', '=' ,'users.id')
							->where('peserta.tipe_paket', '=', 'umroh')
							->whereBetween('peserta_paket.tgl_berangkat', [$blnAwal, $blnAkhir])
							->where('users.role_id', '=', '6')->count();			

			$jamaah[$tahun]['langsung'] = $this->model->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
							->join('users', 'peserta.who_insert', '=' ,'users.id')
							->where('peserta.tipe_paket', '=', 'umroh')
							->whereBetween('peserta_paket.tgl_berangkat', [$blnAwal, $blnAkhir])
							->whereNotIn('users.role_id', [6])->count();
			$tahun++;
		}

		return $jamaah;

	}	

	public function totalJamaahHajiPertahun()
	{
		$jamaah = [];
		$tahun = 2016;
		$range = 10;
		for ($i=1; $i < $range; $i++) {
			$blnAwal = $tahun."-01-01"; 
			$blnAkhir = $tahun."-12-31"; 
			
			$jamaah[$tahun]['agen'] = $this->model->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
							->join('users', 'peserta.who_insert', '=' ,'users.id')
							->where('peserta.tipe_paket', '=', 'haji')
							->whereBetween('peserta_paket.tgl_berangkat', [$blnAwal, $blnAkhir])
							->where('users.role_id', '=', '6')->count();			

			$jamaah[$tahun]['langsung'] = $this->model->join('peserta_paket', 'peserta.id', '=', 'peserta_paket.peserta_id')
							->join('users', 'peserta.who_insert', '=' ,'users.id')
							->where('peserta.tipe_paket', '=', 'haji')
							->whereBetween('peserta_paket.tgl_berangkat', [$blnAwal, $blnAkhir])
							->whereNotIn('users.role_id', [6])->count();
			$tahun++;
		}

		return $jamaah;

	}

	public function getCollectionForDataJamaah($request, $perPage = 20, $columns = ['*'])
	{
		$dataJamaah = [];

		$query =  $this->model//->with('identitas')
					          ->with('alamat')
					          ->with(['peserta' => function($query) {
					          	 $query->with('alamat');
					          }])

					          ->where('tipe_paket', $request['tipe'])
					          ->where('peserta_id', null);
					           
		if(isset($request['tgl_berangkat']) && !empty($request['tgl_berangkat'])) {
			$tanggal = trim($request['tgl_berangkat']);
			$tipe = $request['tipe'];

			//untuk haji
			$awal = $tanggal."-01-01";
		   	$akhir = $tanggal."-12-31"; 
			
		   
			$query = $query->whereHas('paket', function($q) use ($awal, $akhir, $tipe, $tanggal) {
				if($tipe === 'haji') {
					$q->whereBetween('tgl_berangkat', [$awal,$akhir])
					  ->with('paket');
				} else {
					$q->where('tgl_berangkat', $tanggal)
					  ->with('paket');
				}
				
			});
		} else {
			$query = $query->whereHas('paket', function($q){
				$q->with('paket');
		    });
		}

		if(isset($request['keyword']) && !empty($request['keyword'])) {
			$query = $query->where('nama_peserta', 'LIKE', "%{$request['keyword']}%");
		}	

		$dataQuery = $query = $query->get();
	
		$i = 0;
		foreach($dataQuery as $dataKeyLoopQuery => $dataValueLoopQuery) {
			$dataJamaah[$i]['id'] = $dataValueLoopQuery->id;
			$dataJamaah[$i]['nama_peserta'] = $dataValueLoopQuery->nama_peserta;
			$dataJamaah[$i]['jenis_kelamin'] = $dataValueLoopQuery->jenis_kelamin;
			$dataJamaah[$i]['photo'] = $dataValueLoopQuery->photo;
			$dataJamaah[$i]['tipe_paket'] = $dataValueLoopQuery->tipe_paket;
			$dataJamaah[$i]['mahram'] = true;
			$dataJamaah[$i]['no_porsi'] = $dataValueLoopQuery->no_porsi;
			$dataJamaah[$i]['tgl_lahir'] = $dataValueLoopQuery->tgl_lahir;
			$dataJamaah[$i]['hubungan_mahram'] = $dataValueLoopQuery->hubungan_mahram; 
			$dataJamaah[$i]['nama_paket'] = $dataValueLoopQuery->paket->harga->nama_paket;
			$dataJamaah[$i]['tgl_berangkat'] = $dataValueLoopQuery->paket->tgl_berangkat;
			$dataJamaah[$i]['tgl_daftar'] = $dataValueLoopQuery->created_at;
			$dataJamaah[$i]['alamat'] = $dataValueLoopQuery->kabupaten;
			$dataJamaah[$i]['td'] = count($dataValueLoopQuery->peserta) + 1;
			$i++;

			foreach($dataValueLoopQuery->peserta as $pesertaKeyLoopQuery => $pesertaValueLoopQuery) {
				$dataJamaah[$i]['id'] = $pesertaValueLoopQuery->id;
				$dataJamaah[$i]['nama_peserta'] = $pesertaValueLoopQuery->nama_peserta;
				$dataJamaah[$i]['photo'] = $pesertaValueLoopQuery->photo;
				$dataJamaah[$i]['tipe_paket'] = $pesertaValueLoopQuery->tipe_paket;
				$dataJamaah[$i]['mahram'] = false;
				$dataJamaah[$i]['no_porsi'] = $pesertaValueLoopQuery->no_porsi;
				$dataJamaah[$i]['tgl_lahir'] = $pesertaValueLoopQuery->tgl_lahir;
				$dataJamaah[$i]['hubungan_mahram'] = $pesertaValueLoopQuery->hubungan_mahram;
				$dataJamaah[$i]['nama_paket'] = $pesertaValueLoopQuery->paket->harga->nama_paket;
				$dataJamaah[$i]['tgl_daftar'] = $dataValueLoopQuery->tgl_daftar;		
				$dataJamaah[$i]['tgl_berangkat'] = $pesertaValueLoopQuery->paket->tgl_berangkat;
				$dataJamaah[$i]['alamat'] = $pesertaValueLoopQuery->kabupaten;
				$dataJamaah[$i]['td'] = 0;
				$i++;
			}
			
		}	

		return $dataJamaah;			      
	}

	public function searchIdCard($request, $tipe)
	{
		$query = $this->model
					  ->join('peserta_identitas', 'peserta_identitas.peserta_id', '=', 'peserta.id')
					  ->join('peserta_alamat', 'peserta_alamat.peserta_id', '=', 'peserta.id')
					  ->join('peserta_paket', 'peserta_paket.peserta_id', '=', 'peserta.id')	
		 		      ->has('pembayaran')
		 		      ->select('peserta.nama_peserta', 'peserta.tipe_paket', 'peserta.photo', 'peserta.id as peserta_id_peserta', 'peserta_identitas.*', 'peserta_alamat.*', 'peserta_paket.*');
	 				  	
		if(isset($request['id_peserta']) && !empty($request['id_peserta'])) {
		  $query = $query->where('peserta.id', $request['id_peserta']);	
		}		 
								   					   
		if(isset($request['keyword']) && !empty($request['keyword'])) {
			$query = $query->where('nama_peserta', 'LIKE', "%{$request->input('keyword')}%");	
		}

		if(isset($request['tipe_paket']) && !empty($request['tipe_paket'])) {
			$query = $query->where('tipe_paket', $request['tipe_paket']);	
		}

		if(isset($request['tgl_berangkat']) && !empty($request['tgl_berangkat'])) {
			 
			if($request['tipe_paket'] == "haji") {
				$awal = $request['tgl_berangkat']."-01-01";
				$akhir = $request['tgl_berangkat']."-12-31";

				$query = $query->whereBetween('peserta_paket.tgl_berangkat', [$awal, $akhir]);
			} else {

				$query = $query->where('peserta_paket.tgl_berangkat',$request->input('tgl_berangkat'));	
			}
		}

        if($tipe == 'print') {
        	return $query->get();
        }

		return $query->paginate(40);
	}


}

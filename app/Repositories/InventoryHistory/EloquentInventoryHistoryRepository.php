<?php

namespace App\Repositories\InventoryHistory;

use App\InventoryHistory;
use InvalidArgumentException;

class EloquentInventoryHistoryRepository implements InventoryHistoryRepository
{
	/**
	 * @var App\Barang
	 */
	protected $model;


	/**
	 * Create a new instance
	 * 
	 * @param User $model 
	 */
	public function __construct(InventoryHistory $model)
	{
		$this->model = $model;
		 
	}

	public function all($perPage = 10, $orderBy = 'DESC', $columns = ['*'])
	{
		return $this->model->paginate($perPage, $columns);
	}

	public function getItemBy($attribute, $value, $columns = ['*'])
	{
		return $this->model->where($attribute, $value)->first($columns);
	}

	public function create($data = [])
	{
		return $this->model->create($data);
	}

	public function delete($id)
	{
		if(is_null($id) || empty($id)) 
		{
			throw new InvalidArgumentException('Invalid ID');
		}

		return $this->model->where('id', $id)->first()->delete();
	}

	public function update($data = [], $id, $attribute = 'id')
	{
		if(empty($id) || is_null($id)) 
		{
			throw new InvalidArgumentException("Invalid ID");
		}

		$model = $this->model->find($id);

		if(empty($model) || is_null($model))
		{
			throw new InvalidArgumentException("Item not found");
			
		}

		try {
	
			$update = $model->where($attribute, '=', $id)->update($data);
	
		} catch (Exception $e) {
	
			throw new RuntimeException($e->getMessage(), 1);			
	
		}

		return $update;
	}

	 
}

<?php 

namespace App\Repositories\InventoryHistory;

interface InventoryHistoryRepository
{
	public function all($perPage = 10, $columns = ['*']);
	public function create($data = []);
	public function delete($id);
	public function update($data = [], $id, $attribute = 'id');
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesertaMahram extends Model
{
    protected $table = 'mahram';

    protected $fillable = [
      'peserta_id',
      'nama_mahram',
      'hubungan_mahram'
    ];
}

<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
 
        	[
        		'nama' => 'owner',
        		'default_route' => 'admin.dashboard.index'
        	],
        	[
        		'nama' => 'admin',
        		'default_route' => 'admin.pendaftaran.index' 
        	],
        	[
        		'nama' => 'manajer',
        		'default_route' => 'admin.pendaftaran.index'
        	],
        	[
        		'nama' => 'keuangan',
        		'default_route' => 'admin.keuangan.index'
        	],
        	[
        		'nama' => 'perlengkapan',
        		'default_route' => 'admin.inventory.index'
        	],	
        	[
        		'nama' => 'agen',
        		'default_route' => 'admin.daftar.index'
        	],
      ];

        DB::table('roles')->insert($data);


    }
}
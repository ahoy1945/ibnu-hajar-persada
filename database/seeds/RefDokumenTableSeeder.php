<?php

use Illuminate\Database\Seeder;

class RefDokumenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	['nama_dokumen' => 'Passpor', 'tipe' => 'umroh'],
        	['nama_dokumen' =>  'Kartu Kuning', 'tipe' => 'umroh'],
        	['nama_dokumen' => '8 Pas Photo 4x6', 'tipe' => 'umroh'],
        	['nama_dokumen' =>  'Buku Nikah', 'tipe' => 'umroh'],
        	['nama_dokumen' =>  'Akte Kelahiran', 'tipe' => 'umroh'],
        	['nama_dokumen' =>  'KK', 'tipe' => 'umroh'],
        	['nama_dokumen' =>  'Passpor', 'tipe' => 'haji'],
        	['nama_dokumen' =>  'Buku Hijau', 'tipe' => 'haji'],
        	['nama_dokumen' =>  '17 Pas Photo 4x6', 'tipe' => 'haji'],
        	['nama_dokumen' =>  '27 Pas Photo 3x4', 'tipe' => 'haji'],
        	['nama_dokumen' =>  'Buku Nikah', 'tipe' => 'haji'],
        	['nama_dokumen' =>  'Akte Kelahiran', 'tipe' => 'haji'],
        	['nama_dokumen' =>  'KK', 'tipe' => 'haji'],
        	['nama_dokumen' =>  'KTP', 'tipe' => 'haji'],

        ];

        DB::table('ref_dokumen')->insert($data);
    }
}

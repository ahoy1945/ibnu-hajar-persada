<?php

use Illuminate\Database\Seeder;
 

class InventoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
                   ['nama' => 'Koper Haji', 'jumlah' => 10, 'tipe' => 'umroh', 'ket' => 'Lorem'],
                   ['nama' => 'Seragam', 'jumlah' => 10, 'tipe' => 'umroh', 'ket' => 'Lorem'],  
                   ['nama' => 'Tas Passpor', 'jumlah' => 10, 'tipe' => 'umroh', 'ket' => 'Lorem'],  
                   ['nama' => 'Tas Sandal', 'jumlah' => 10, 'tipe' => 'umroh', 'ket' => 'Lorem'],  
                   ['nama' => 'Koper Kabin', 'jumlah' => 10, 'tipe' => 'umroh', 'ket' => 'Lorem'],  
                   ['nama' => 'Kain Ihram', 'jumlah' => 10, 'tipe' => 'umroh', 'ket' => 'Lorem'],  
                   ['nama' => 'Tas Sandal', 'jumlah' => 10, 'tipe' => 'umroh', 'ket' => 'Lorem']  
                                            
            ];
            
        DB::table('inventory')->insert($data);    
    }
}

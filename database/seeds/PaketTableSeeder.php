<?php

use Illuminate\Database\Seeder;

class PaketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
             ['tipe_paket' => 'umroh', 'nama_paket' => 'Umroh Plus Mesir', 'harga_paket' => 2350],
             ['tipe_paket' => 'umroh', 'nama_paket' => 'Umroh Plus Dubai', 'harga_paket' => 3000],   
             ['tipe_paket' => 'umroh', 'nama_paket' => 'Umroh Eksekutif', 'harga_paket' => 2400],  
        ];
        
        DB::table('ref_harga_paket')->insert($data);
    }
}

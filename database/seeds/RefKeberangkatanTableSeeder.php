<?php

use Illuminate\Database\Seeder;

class RefKeberangkatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	['paket_id' => 1, 'tipe' => 'umroh', 'tanggal_pemberangkatan' => '2016-2-12'],
        	['paket_id' => 2, 'tipe' => 'umroh', 'tanggal_pemberangkatan' => '2016-3-31'],
        	['paket_id' => 3, 'tipe' => 'umroh', 'tanggal_pemberangkatan' => '2016-5-15']
        ];

        DB::table('ref_tanggal_pemberangkatan')->insert($data);
    }
}

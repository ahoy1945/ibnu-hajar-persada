<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    protected $tables = ['users', 'ref_tipe_keuangan', 'roles', 'inventory', 'ref_harga_paket', 'settings', 'ref_rekening', 'ref_dokumen', 'ref_tanggal_pemberangkatan', 'ref_pesawat'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->cleanDatabase();
        
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(InventoryTableSeeder::class);
        $this->call(PaketTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(RekeningTableSeeder::class);
        $this->call(RefDokumenTableSeeder::class);
        $this->call(RefKeberangkatanTableSeeder::class);
        $this->call(TipeKeuanganSeeder::class);
        $this->call(RefPesawatSeeder::class);

        Model::reguard();
    }

    private function cleanDatabase()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
            foreach($this->tables as $table) {
                DB::table($table)->truncate();
            }
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

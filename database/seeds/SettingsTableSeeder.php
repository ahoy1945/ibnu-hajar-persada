<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	['meta_name' => 'site_name', 'meta_value' => 'Ibnu Hajar Persada', 'label' => 'Nama Situs', 'type_input' => 'text'],
        	['meta_name' => 'alamat', 'meta_value' => "Villa Blok V2 No.18, Jl. Villa Cibubur, Jakarta Timur, DKI Jakarta, Daerah Khusus Ibukota Jakarta 13720
              ", 'label' => 'Alamat', 'type_input' => 'textarea'],
            ['meta_name' => 'telepon', 'meta_value' => '02187753476', 'label' => 'Telepon', 'type_input' => 'text'],
            ['meta_name' => 'harga_perlengkapan', 'meta_value' => 1200000, 'label' => 'Harga Perlengkapan', 'type_input' => 'text'],
            ['meta_name' => 'diskon_umroh', 'meta_value' => 50, 'label' => 'Diskon Umroh', 'type_input' => 'text'],
        ];

        DB::table('settings')->insert($data);
    }
}

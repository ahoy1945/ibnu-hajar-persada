<?php

use Illuminate\Database\Seeder;

class TipeKeuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		'tipe_keuangan' => 'Pemasukan',
        		'nama_tipe' => 'Umroh'
        	],
        	[
        		'tipe_keuangan' => 'Pemasukan',
        		'nama_tipe' => 'Haji'
        	]
        ];

        DB::table('ref_tipe_keuangan')->insert($data);
    }
}

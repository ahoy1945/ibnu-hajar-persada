<?php

use Illuminate\Database\Seeder;

class KeuanganTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
            [
                'tanggal' => date('Y-m-d H:i:s'),
                'keterangan' => 'biaya kos',
                'debit' => 700000,
                'kredit' => 2100000,
                'saldo' => 100000
            ],
            [
                'tanggal' => date('Y-m-d H:i:s'),
                'keterangan' => 'biaya listrik',
                'debit' => 700000,
                'kredit' => 2100000,
                'saldo' => 200000
            ],
            [
                'tanggal' => date('Y-m-d H:i:s'),
                'keterangan' => 'biaya pulsa',
                'debit' => 700000,
                'kredit' => 2100000,
                'saldo' => 300000
            ],
    	];

        DB::table('keuangan')->insert($data);
    }
}

<?php

use Illuminate\Database\Seeder;

class RefPesawatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	['kode_pesawat' => 'GA', 'nama_pesawat' => 'Garuda Indonesia'],
        	['kode_pesawat' => 'JT', 'nama_pesawat' => 'Lion Air'],
        	['kode_pesawat' => 'QZ', 'nama_pesawat' => 'Airasia'],
        	['kode_pesawat' => 'SJ', 'nama_pesawat' => 'Sriwijaya'],
        	['kode_pesawat' => 'QG', 'nama_pesawat' => 'Citilink'],
        	['kode_pesawat' => 'MZ', 'nama_pesawat' => 'Merpati Nusantara'],
        	['kode_pesawat' => 'MV', 'nama_pesawat' => 'Aviastar'],
        	['kode_pesawat' => 'ID', 'nama_pesawat' => 'Batik Air'],
        	['kode_pesawat' => 'TN', 'nama_pesawat' => 'Trigana'],
        	['kode_pesawat' => 'RI', 'nama_pesawat' => 'Mandala'],
        	['kode_pesawat' => 'KD', 'nama_pesawat' => 'Kalstar'],
        	['kode_pesawat' => 'FS', 'nama_pesawat' => 'Airfast'],
        	['kode_pesawat' => 'IW', 'nama_pesawat' => 'Wings Air'],
        	['kode_pesawat' => 'XN', 'nama_pesawat' => 'Express'],
        	['kode_pesawat' => 'sy', 'nama_pesawat' => 'Sky Aviation'],
        	['kode_pesawat' => 'SI', 'nama_pesawat' => 'Susi Air']
        ];

        DB::table('ref_pesawat')->insert($data);
    }
}

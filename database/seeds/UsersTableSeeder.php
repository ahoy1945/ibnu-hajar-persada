<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
    		['name' => 'Ummu Ahmad', 'role_id' => 1, 'email' => 'admin@gmail.com', 'password' => bcrypt('qweasd123')],
    		['name' => 'Basir', 'role_id' => 1, 'email' => 'basir@gmail.com', 'password' => bcrypt('qweasd123')],

    	];	
       DB::table('users')->insert($data);
    }
}

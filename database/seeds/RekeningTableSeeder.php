<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class RekeningTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $banks = ['BRI', 'BNI', 'BCA', 'Mandiri'];

        foreach(range(1, 10) as $index) {
        	DB::table('ref_rekening')->insert([
        		'bank' => $faker->randomElement($banks),
        		'atas_nama' => $faker->name,
        		'no_rek' => $faker->randomNumber(),
                'mata_uang' => $faker->randomElement(['idr', 'usd'])		
        	]);
        	
        }

    }
}

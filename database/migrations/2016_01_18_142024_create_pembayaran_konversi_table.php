<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranKonversiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_pembayaran_konversi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peserta_pembayaran_id')->unsigned();
            $table->date('tgl_konversi');
            $table->integer('kurs');
            $table->timestamps();
        });

        Schema::table('peserta_pembayaran_konversi', function(Blueprint $table) {
            $table->foreign('peserta_pembayaran_id')->references('id')->on('peserta_pembayaran')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peserta_pembayaran_konversi');
    }
}

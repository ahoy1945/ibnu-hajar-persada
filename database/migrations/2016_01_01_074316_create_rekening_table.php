<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekeningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_rekening', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank');
            $table->enum('mata_uang', ['idr', 'usd'])->default('idr');
            $table->string('atas_nama');
            $table->string('no_rek');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_rekening');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaOnlineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_online', function (Blueprint $table) {
            //general information
            $table->increments('id');
            $table->integer('peserta_id')->unsigned()->nullable();
            $table->integer('who_insert')->unsigned()->nullable();
            $table->string('nama_peserta');
            $table->string('nama_ayah');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->enum('jenis_kelamin', ['L', 'P']);
            $table->enum('kewarganegaraan', ['WNI', 'Asing']);
            $table->enum('pendidikan', ['SD', 'SLTA', 'SM/D1/D2/D3', 'S1', 'S2', 'S3', 'Lain-lain', '-']);
            $table->enum('pekerjaan', ['PNS', 'TNI/POLRI', 'Dagang', 'Tani/Nelayan', 'Swasta', 'Ibu Rumah Tangga', 'Pelajar/Mahasiswa', 'BUMN/BUMD', 'Lain-lain', '-']);
            $table->boolean('pernah_haji_umroh')->default(false);
            $table->string('hubungan_mahram');
            $table->enum('golongan_darah', ['A', 'B', 'AB', 'O', '-']);
            $table->string('no_telp');
            $table->string('no_hp');
            $table->enum('ukuran_baju_kerudung', ['M', 'L', 'XL', 'XXL', 'XXXL', '-']);
            $table->enum('tipe_paket', ['haji', 'umroh']);
            $table->boolean('status_pembayaran')->default(0);
            $table->integer('diskon');
            $table->string('no_porsi')->nullable();
            $table->string('attach_file')->nullable();
            $table->string('photo')->nullable();
            
            //identity data
            $table->string('no_ktp');
            $table->string('no_passpor');
            $table->string('tempat_pembuatan');
            $table->date('tgl_pembuatan');
            $table->date('tgl_expired');
            
            //address data
            $table->integer('kodepos');
            $table->string('alamat');
            $table->string('desa');
            $table->string('kecamatan');
            $table->string('kabupaten');
            $table->string('provinsi');

            //paket data
            $table->integer('pesawat_id')->unsigned();
            $table->integer('harga_paket_id')->unsigned();
            $table->string('paket_hotel');
            $table->string('tipe_pesawat');
            $table->string('tipe_kamar');
            $table->date('tgl_berangkat');

            //dokumen data
            $table->string('dokumen_id');

            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peserta_online');
    }
}

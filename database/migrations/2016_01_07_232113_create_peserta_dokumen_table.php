<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaDokumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_dokumen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peserta_id')->unsigned();
            $table->integer('dokumen_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('peserta_dokumen', function(Blueprint $table) {
            $table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('dokumen_id')->references('id')->on('ref_dokumen')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peserta_dokumen');
    }
}

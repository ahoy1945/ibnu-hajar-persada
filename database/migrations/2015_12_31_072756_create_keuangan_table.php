<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeuanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keuangan', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('peserta_id')->unsigned()->nullable();
            $table->integer('ref_tipe_keuangan_id')->unsigned();
            $table->integer('id_rekening')->unsigned();
            $table->date('tanggal');
            $table->text('keterangan');
            $table->integer('debit')->default(0);
            $table->integer('kredit')->default(0);
            $table->enum('type', ['kredit', 'debit']);
            $table->timestamps();

        });

        Schema::table('keuangan', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ref_tipe_keuangan_id')->references('id')->on('ref_tipe_keuangan')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('keuangan');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefDokumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_dokumen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_dokumen');
            $table->enum('tipe', ['umroh', 'haji']);
            $table->bigInteger('jumlah');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_dokumen');
    }
}

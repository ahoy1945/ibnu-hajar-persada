<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefHargaPaket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_harga_paket', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipe_paket',['haji', 'umroh']);
            $table->string('nama_paket');
            $table->integer('harga_paket')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('ref_harga_paket');
    }
}

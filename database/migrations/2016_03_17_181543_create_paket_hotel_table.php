<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaketHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_harga_paket_hotel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paket_id')->unsigned();
            $table->enum('kamar', ['double', 'triple', 'quad', 'quint']);
            $table->integer('harga');
            $table->timestamps();
        });

        Schema::table('ref_harga_paket_hotel', function(Blueprint $table) {
            $table->foreign('paket_id')->references('id')->on('ref_harga_paket')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paket_hotel');
    }
}

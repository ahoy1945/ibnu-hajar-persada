<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabunganSaldo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabungan_saldo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tabungan_id')->unsigned();
            $table->enum('tipe', ['debit', 'kredit']);
            $table->integer('jumlah');
            $table->timestamps();
        });

        Schema::table('tabungan_saldo', function(Blueprint $table) {
            $table->foreign('tabungan_id')->references('id')->on('tabungan')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tabungan_saldo');
    }
}

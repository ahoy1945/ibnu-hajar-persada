<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_id')->unsigned();
            $table->integer('masuk')->nullable();
            $table->integer('keluar')->nullable();
            $table->integer('sisa')->nullable();
            $table->date('tanggal');
            $table->string('keterangan');
            $table->timestamps();
        });

        Schema::table("inventory_history", function(Blueprint $table) {
            $table->foreign('inventory_id')->references('id')->on('inventory')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inventory_history');
    }
}

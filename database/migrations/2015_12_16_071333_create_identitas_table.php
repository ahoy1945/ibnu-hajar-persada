<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdentitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_identitas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peserta_id')->unsigned();
            $table->string('no_ktp');
            $table->string('no_passpor');
            $table->string('tempat_pembuatan');
            $table->date('tgl_pembuatan');
            $table->date('tgl_expired');
            $table->timestamps();
        });

        Schema::table('peserta_identitas', function(Blueprint $table) {
          $table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peserta_identitas');
    }
}

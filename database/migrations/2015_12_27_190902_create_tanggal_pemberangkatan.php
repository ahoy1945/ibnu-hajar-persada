<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTanggalPemberangkatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ref_tanggal_pemberangkatan', function (Blueprint $table) {
          $table->increments('id');
          $table->integer("paket_id")->unsigned();
          $table->enum('tipe', ['haji', 'umroh']);
          $table->date('tanggal_pemberangkatan');
          $table->timestamps();
      });

      Schema::table('ref_tanggal_pemberangkatan', function(Blueprint $table) {
        $table->foreign('paket_id')->references('id')->on('ref_harga_paket')->onDelete('cascade')->onUpdate('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_tanggal_pemberangkatan');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefTipeKeuangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('ref_tipe_keuangan', function (Blueprint $table) {
             $table->increments('id');
             $table->enum('tipe_keuangan',['Pemasukan', 'Pengeluaran']);
             $table->string('nama_tipe');
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
           Schema::drop('ref_tipe_keuangan');
     }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefMaxKuotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_max_kuota', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('tipe_paket',['haji', 'umroh']);
            $table->date('tgl_berangkat');
            $table->integer('max_kuota')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_max_kuota');
    }
}

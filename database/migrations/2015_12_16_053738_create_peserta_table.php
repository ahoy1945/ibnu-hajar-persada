<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peserta_id')->unsigned()->nullable();
            $table->integer('who_insert')->unsigned();
            $table->string('nama_peserta');
            $table->string('nama_ayah');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->enum('jenis_kelamin', ['L', 'P']);
            $table->enum('kewarganegaraan', ['WNI', 'Asing']);
            $table->enum('pendidikan', ['SD', 'SLTA', 'SM/D1/D2/D3', 'S1', 'S2', 'S3', 'Lain-lain', '-']);
            $table->enum('pekerjaan', ['PNS', 'TNI/POLRI', 'Dagang', 'Tani/Nelayan', 'Swasta', 'Ibu Rumah Tangga', 'Pelajar/Mahasiswa', 'BUMN/BUMD', 'Lain-lain', '-']);
            $table->boolean('pernah_haji_umroh')->default(false);
            $table->string('hubungan_mahram');
            $table->enum('golongan_darah', ['A', 'B', 'AB', 'O', '-']);
            $table->string('no_telp');
            $table->string('no_hp');
            $table->enum('ukuran_baju_kerudung', ['M', 'L', 'XL', 'XXL', 'XXXL', '-']);
            $table->enum('tipe_paket', ['haji', 'umroh']);
            $table->boolean('status_pembayaran')->default(0);
            $table->integer('diskon');
            $table->string('no_porsi')->nullable();
            $table->string('attach_file')->nullable();
            $table->string('photo')->nullable();
            $table->timestamps();
        });

        Schema::table('peserta', function(Blueprint $table) {
            $table->foreign('peserta_id')->references('id')->on('peserta')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('who_insert')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peserta');
    }
}

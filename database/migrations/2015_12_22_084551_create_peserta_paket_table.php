<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaPaketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_paket', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peserta_id')->unsigned();
            $table->integer('pesawat_id')->unsigned();
            $table->integer('harga_paket_id')->unsigned();
            $table->string('paket_hotel');
            $table->string('tipe_pesawat');
            $table->string('tipe_kamar');
            $table->string('bus');
            $table->date('tgl_berangkat');
            $table->timestamps();
        });

        Schema::table('peserta_paket', function(Blueprint $table) {
          $table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade')->onUpdate('cascade');
          $table->foreign('pesawat_id')->references('id')->on('ref_pesawat')->onDelete('cascade')->onUpdate('cascade');
        }); 

        Schema::table('peserta_paket', function(Blueprint $table) {
            $table->foreign('harga_paket_id')->references('id')->on('ref_harga_paket')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peserta_paket');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peserta_id')->unsigned();
            $table->integer('inventory_id')->unsigned();
            $table->timestamps();
        });
        
        Schema::table('peserta_inventory', function(Blueprint $table) {
            $table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('inventory_id')->references('id')->on('inventory')->onDelete('cascade')->onUpdate('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peserta_inventory');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peserta_id')->unsigned();
            $table->integer('tipe_keuangan_id')->unsigned();
            $table->integer('rekening_id')->unsigned();
            $table->string('no_kwitansi');
            $table->date('tgl_bayar');
            $table->integer('diskon');
//            $table->string('pembayaran');
            $table->enum('mata_uang', ['idr', 'usd']);
            $table->bigInteger('jumlah');
            $table->string('keterangan');
            $table->timestamps();
        });

        Schema::table('peserta_pembayaran', function(Blueprint $table) {
            $table->foreign('tipe_keuangan_id')->references('id')->on('ref_tipe_keuangan')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('peserta_id')->references('id')->on('peserta')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('rekening_id')->references('id')->on('ref_rekening')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peserta_pembayaran');
    }
}

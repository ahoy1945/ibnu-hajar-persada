<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRefPesawat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('ref_pesawat', function (Blueprint $table) {
             $table->increments('id');
             $table->string('kode_pesawat');
             $table->string('nama_pesawat');
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
           Schema::drop('ref_pesawat');
     }
}
